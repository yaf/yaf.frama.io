http://www.infoq.com/news/2011/10/Time_to_Stop_Estimating_Stories


Is it Time to Stop Estimating User Stories?

Posted by Todd Charron on Oct 01, 2011

Sections
    Process & Practices,
        Architecture & Design,
            Development
            Topics
                Team Collaboration , 
                    Process , 
                        Agile Techniques , 
                            Agile 
                            Tags
                                Scrum , 
                                    User Stories , 
                                        Kanban 

                                        Share Share |

                                        Most new Agile teams transition from hours based estimates to relative estimation using story points, but do we even need estimates at all?

                                        Michael Dubakov suggests the following reasons why you should stop estimating user stories:

                                                You don’t waste time on estimation
                                                        You shouldn’t explain to higher managers why it took soooo loooooooong
                                                                You don’t give promises that are hard to keep
                                                                        You don’t put additional pressure on development team
                                                                                You focus on really important things

                                                                                Stephan Schwab, worries about how estimation can result in silos and prevent team based solutions:

                                                                                    Imagine an organization where teams are expected to have a fully estimated backlog in order to determine the cost for the project. At first glance such an approach seems to be a good idea. The people who will do the work will provide the estimate and thus based on what they say the true cost of the work will be known.

                                                                                        But then is the true cost of the project really known? What about all the discoveries that will be made once a team of smart people starts solving a problem? It seems unlikely that a few analysts will be able to analyze a problem and create a good solution expressed in small story cards for a 6 months project within a few weeks. They may be able to create 500 story cards over a few weeks but I think that will be all based on early assumptions. If the problem can be solved in a few weeks, then there is no need to pay the whole team over 6 months.

                                                                                            So to me it seems more like the attempt of predict the future, create a plan and then manage to the plan.

                                                                                                The fact that a team is asked to provide a fully estimated backlog – and a detailed one! – creates the prescriptive behavior and discourages the technical team members from developing a solution using the input from analysts, user experience person and product owner. In the end it should be no surprise, if the quality of the resulting “solution” is lower than expected. The team has been prevented from doing a good job.

                                                                                                    Quality has been traded for false predictability and it is likely that this happened based on requests from the very same stakeholders who expect a high quality product.

                                                                                                        What is much more important than to “calculate” cost is to build something of value. Something that can be used and in the end makes the stakeholders to want come back to the same people for extensions or with new ideas.

                                                                                                        Mike Cottmeyer comes to the defence of estimates:

                                                                                                            Since we don’t like managers and we don’t like death marches, we conclude the creation of software is an unpredictable process, that estimates are bad, and we should never estimate at all. Is it possible that we don’t really have an estimating problem? Is it possible that we have a bad management problem?

                                                                                                                I don’t see any reason to stop estimating. In fact, unless your business model supports totally emergent outcomes, chances are you have some sort of goal, some sort of business outcome you are tracking toward that is tied to a hard, somewhat pre-defined deliverable. Chances are you’ve sold something to some customer and now you have to make good on that commitment.

                                                                                                                    We can debate this business model all day long, but if that’s your reality, you need estimates.

                                                                                                                        Estimates have to have ranges and probabilities. Assumptions have to be managed and risks have to be mitigated. We have to be able to measure how wrong our estimates are so that we can start to forecast that error forward.

                                                                                                                            We ignore what the data is telling us. We ignore the team’s actual performance against the estimate. There is so much competitive pressure to deliver, so much pressure to add those extra features by the end of next month so we can make the sale, so much pressure to deliver those features our sales team committed to win that big deal. So much pressure that we ignore reality.

                                                                                                                                The real problem is that, far too often, we oversell the organizations ability to deliver. The real problem lies in creating a ‘you have to deliver at all costs’ culture that doesn’t respect the teams established capacity to build working tested software.

                                                                                                                                    Bad estimates become a problem, bad management becomes a problem, in the face of unyielding pressure to deliver against unreasonable expectations and inflexible project schedules and commitments.

                                                                                                                                    Bob Marshall in the same article adds his comments:

                                                                                                                                        Further, I regularly feel compelled to ask “why estimate?”. Until folks grok the requirements for which estimation is (most often) an implicit solution, how can anyone say with any certainty that estimation has any value?

                                                                                                                                        Many believe Kanban has done away with estimates, Karl Scotland clarifies the situation:

                                                                                                                                            Kanban teams do not eschew estimation simply because it is hard. Some teams choose not to estimate because they can realise the same benefits that estimation gives with a lower cost. The old joke does still apply (“Doctor, Doctor it hurts when I do this”, “Don’t do that then”). If it hurts to estimate, then find other ways to encourage whole team interaction and collaboration, learn about past capability and forecast future capability. On the other hand, if estimation doesn’t hurt, or costs too much, then it may be the right thing to do in your context.

                                                                                                                                                When we are planning, we should decompose work for understanding, rather than sizing.

                                                                                                                                                Jeff Anderson gives another take on estimating in Kanban:

                                                                                                                                                    As teams progress, work accepted will typically become categorized into better understood work types that have less variability. i.e. team A might typically work on java enhancements, emergency defects, and canned reports.

                                                                                                                                                        The most important part of estimating is breaking up the work into small pieces, once you've done that, the only reason to care about differences in the small range is to provoke discussion.

                                                                                                                                                        While it seems the value of the output of estimation may be in doubt, it seems the real benefit of estimation comes from the whole team conversations that occur along the way. What have your experiences been?


