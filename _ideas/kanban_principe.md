
Kanban, basé sur un système à flux tiré, encourage également unengagement au plus tard, à la fois sur la priorisation du travail et lalivraison du travail en cours. Typiquement, les équipes vont convenird'une fréquence pour rencontrer les parties prenantes et décider sur quoitravailler dans la suite. Ces réunions peuvent être tenues régulièrementparce qu'elles sont généralement très courtes. On peut y aborder une question très simple, quelque chose comme : "Depuis notre dernièreréunion, deux créneaux de travail se sont libérés. Notre temps de cycleactuel jusqu'à la livraison est de 6 semaines. Quelles sont les 2 choses quevous aimeriez voir livrées dans 6 semaines ?". Cela a une doubleincidence. Poser une question simple se traduit généralement par uneréponse de bonne qualité obtenue rapidement et permettant de maintenirune réunion courte. La nature de cette question signifie que l'engagementde sur quoi travailler est retardé jusqu'au dernier instant. Cela améliorel'agilité en gérant les exigences, en raccourcissant les temps de cycle entrel'engagement et la livraison, et en éliminant le travail de remise à niveaupuisque le risque d'un changement dans les priorités sera minimisé.


A ETUDIER !





