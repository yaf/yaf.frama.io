# OpenBadge

Quelques notes à propos des openbadges. Peut-être que cela pourra servir à la reflexion de comment les utiliser, et en même temps à « justifier » la démarche, a défaut, la documenter.

- [Comment les badges numériques ont débarqué en normandie](https://usbeketrica.com/article/comment-les-badges-numeriques-ont-debarque-en-normandie)
- [Badgeons la normandie](https://badgeonslanormandie.fr/)
- [openrecognation.org](https://reconnaitre.openrecognition.org/)
- [IMS Global](https://www.imsglobal.org/article/ims-global-mozilla-foundation-and-lrng-announce-next-steps-accelerate-evolution-open-badges)
- [IBM Digital Badge](https://www-03.ibm.com/services/learning/ites.wss/zz-en?pageType=page&c=M425350C34234U21)
- [Etudes sur les Open Badges](https://support.mozilla.org/fr/kb/etudes-open-badges)
- [Open BadgePassport](https://openbadgepassport.com/app/page/view/2845)
- [Open Badges for Lifelong Learning](https://wiki.mozilla.org/images/5/59/OpenBadges-Working-Paper_012312.pdf)


Pour moi, ces badges correspondent aux brevts de Célestins Freinet (ou pas loin).

- [Évaluer avec les brevts de Célestin Freinet](http://www.svt-egalite.fr/index.php/outils/evaluer-avec-les-brevets-de-celestin-freinet)


Un point clefs des badges, c'est le stockage.

- [Mozilla Open Badges Backpack](https://github.com/mozilla/openbadges-backpack)
