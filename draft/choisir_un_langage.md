# Choisir un langage

C'est LA grande question des personnes qui démarre en programmation : « quel langage je dois choisir ? »

Et moi, je pense que ça dépends. Essayons de lister des critères. Je choisi le langage : 
- utilisé par la boite dont je rêve
- qu'un proche, relativement disponible, connait
- que je connais déjà un peu
- qui a une communauté sympa
- qui a une communauté active
- qui a une communauté prêt de chez moi
- qui est bien documenté
- qui n'est pas compilé
- qui est typé statiquement
- qui est typé dynamiquement
- ...

Au moment de ce choix, il y a souvent très peu de containte extérieure (ce qui est rarement le cas au moment de rejoindre un emploi où l'équipe aura déjà choisi un langage par exemple).

Comment constituer une grille de choix ? Un point intéressant serais, quoiqu'il arrive, de documenter cette grille de choix pour pouvoir revenir dessus si un critère change ou un nouveau apparait.
