# TODO

- construire un index par année basé sur une recherche d'entrée dans les histoires (basé sur une structure avec date/ancre)
- construire un index par mois basé sur une recherche d'entrée dans les histoires (basé sur une structure avec date/ancre)
- construire un index par jour basé sur une recherche d'entrée dans les histoires (basé sur une structure avec date/ancre)
- construire des outils de validations avant publication (html, css, flux, ...)
- ajouter le contenu (ou résumé) de l'article dans le flux
- avoir un script qui vérifie que les liens ne sont pas cassé (localement ? En prod ? les deux ?)
- déployer sur un autre serveur qu'un gitlab
- ne plus avoir une navigation par menu, mais au fil de la lecture (?)
- construire un sitemap.xml automatiquement
- correcteur orthographique ?


## Orientation

Construire des histoires plus longue, basé sur la consolidation des journaux quotidien. Ajouter chaque petit morceaux dans une grand page. Avoir des ancres daté qui permette de fabriquer des listes, des index, basé sur la date.

Ça apporterais deux niveaux de lecture :
- histoire thématique
- éléments temporelle

Construire les index daté à la main ? Un pour l'année, dans lequel on aurait la page des mois ? un pour le mois, dans lequel on aurait la liste des jours / billets. Chaque billet de journée reprendrais des pointeurs vers l'histoire ou les histoires lié à la journée (avec ancre pour le positionnement). Cette page pourrait aussi reprendre les trucs lu et partagé sur les réseeaux sociaux ?

En fait, je pourrais maintnir mon activité de journal quotidien comme aujourd'hui, mais consolider au fur et à mesure, des histoires par ailleur ? Deux vitesses d'écriture ? La quotidienne serais une sorte brouillon ?

Et le flux RSS dans ce cas ?

