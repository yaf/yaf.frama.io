
Il serait intéressant que chaque personne qui pense pouvoir prendre un badge, puisse copier coller le bout de code en question dans le tableau de badge, que le lien existe quelque part

## Boucles


## Controle de flux de programmation

- avoir écrit (et compris) un type de structure conditionnée dans un langage de programmation
- avoir écrit (et compris) un type de structure conditionnée dans 3 langages de programmation différent
- avoir écrit (et compris) 2 types de structure conditionnée dans un langage de programmation
- avoir écrit (et compris) 2 types de structure conditionnée dans 3 langages de programmation différent (if/else, switch/case)
- avoir écrit (et compris) une condition simple (une vérification d'égalité ou de supériorité) dans un langage de programmation
- avoir écrit (et compris) une condition simple (une vérification d'égalité ou de supériorité) dans 3 langages de programmation
- avoir écrit (et compris) une condition combinée (une vérification d'égalité ou de supériorité combiné avec une autre via un ET ou un OU) dans un langage de programmation
- avoir écrit (et compris) une condition combinée (une vérification d'égalité ou de supériorité combiné avec une autre via un ET ou un OU) dans 3 langages de programmation
- avoir écrit (et compris) une condition basé sur le type dans un langage de programmation
- avoir écrit (et compris) une condition basé sur le type dans 3 langages de programmation différent
- avoir écrit (et compris) un type de boucle dans un langage de programmation
- avoir écrit (et compris) un type de boucle dans 3 langages de programmation différent
- avoir écrit (et compris) 3 types de boucle dans un langage de programmation (for, while, récursion)
- avoir écrit (et compris) 3 types de boucle dans 3 langages de programmation différent (for, while récursion)
- avoir écrit (et compris) un type de boucle avec des conditions de fonctionnement inhabituel (ne pas démarrer à 0, faire un pas de plus de 1, avancer en ordre décroissant ...) dans un langage de programmation
- avoir écrit (et compris) 3 types de boucle avec des conditions de fonctionnement inhabituel (ne pas démarrer à 0, faire un pas de plus de 1, avancer en ordre décroissant ...) dans 3 langages de programmation différent



## Analyse et lecture de code

## Complexité d'un logiciel

- avoir écrit (et compris) un programme qui tiens dans un fichier dans un langage
- avoir écrit (et compris) un programme qui tiens dans un fichier dans 3 langages différent
- avoir écrit (et compris) un programme qui utilise plusieurs fichiers dans un langage
- avoir écrit (et compris) un programme qui utilise plusieurs fichiers dans 3 langages différent
- avoir écrit (et compris) un programme qui utilise au moins une librairie interne (core lib ou standard lib) dans un langage
- avoir écrit (et compris) un programme qui utilise au moins une librairie interne (core lib ou standard lib) dans 3 langages différent
- avoir écrit (et compris) un programme qui utilise au moins une librairie extérieur dans un langage
- avoir écrit (et compris) un programme qui utilise au moins une librairie extérieur dans 3 langages différent

## Refactoring

- renome un champ (https://www.refactoring.com/catalog/renameField.html)
- supprimer du code mort (https://www.refactoring.com/catalog/removeDeadCode.html)
- supprime les arguments d'état (flag) (https://www.refactoring.com/catalog/removeFlagArgument.html)
- retourne la valeur modifié (plutôt que de modifier la valeur dans la fonction) (https://www.refactoring.com/catalog/returnModifiedValue.html)
- remplace une valeur par une référence (https://www.refactoring.com/catalog/changeValueToReference.html)
- remplace une référence par une valeur (https://www.refactoring.com/catalog/changeReferenceToValue.html)
- découpe une boucle (https://www.refactoring.com/catalog/splitLoop.html)
- remplace une boucle par un enchainement (map filter) (https://www.refactoring.com/catalog/replaceLoopWithPipeline.html)
- remplace un type par des sous-classe (https://www.refactoring.com/catalog/replaceTypeCodeWithSubclasses.html) (inverse de suppression des sous-classe)
- supprime une sous-classe (https://www.refactoring.com/catalog/removeSubclass.html) inverse de remplace un type par une sous-classe
- remplace une classe parente (super classe) par une délégation (https://www.refactoring.com/catalog/replaceSuperclassWithDelegate.html)
- déplace une méthode dans une classe fille (https://www.refactoring.com/catalog/pushDownMethod.html)
- déplace une méthode dans une classe parente (https://www.refactoring.com/catalog/pullUpMethod.html)
- factorise les éléments de construction dans le constructeur parent (https://www.refactoring.com/catalog/pullUpConstructorBody.html)
- extrait une classe parent (https://www.refactoring.com/catalog/extractSuperclass.html)
- remplace une fonction par une méthode de classe (https://www.refactoring.com/catalog/replaceFunctionWithCommand.html) inverse de remplace une méthode de classe par une fonction
- remplace une méthode de classe par une fonction (https://www.refactoring.com/catalog/replaceCommandWithFunction.html) inverse de remplace une fonction par une méthode de classe (ou commande)
- remplace un parametre par une query (https://www.refactoring.com/catalog/replaceParameterWithQuery.html) inverse de remplace une query par un parametre
- remplace un code d'erreur par une exception (https://www.refactoring.com/catalog/replaceErrorCodeWithException.html)
- supprime les methodes de changement de valeur d'attributs (https://www.refactoring.com/catalog/removeSettingMethod.html)
- paramètrise une fonction (https://www.refactoring.com/catalog/parameterizeFunction.html)
- remplace une condition par du polymorphisme (https://www.refactoring.com/catalog/replaceConditionalWithPolymorphism.html)
- remplace un flag de controle par un break (https://www.refactoring.com/catalog/replaceControlFlagWithBreak.html)
- remplace des conditions imbriquées par des gardes (https://www.refactoring.com/catalog/replaceNestedConditionalWithGuardClauses.html)
- créer un cas spécial (aussi appelé l'objet nulle) (Introduce Special Case)
- introduit une assertion (https://www.refactoring.com/catalog/introduceAssertion.html)
- consolide une expression de condition (https://www.refactoring.com/catalog/consolidateConditionalExpression.html)
- décompose une expression de condition (https://www.refactoring.com/catalog/decomposeConditional.html)
- découpe une variable (https://www.refactoring.com/catalog/splitVariable.html)
- remplace un literal magique (https://www.refactoring.com/catalog/replaceMagicLiteral.html)
- remplace du code en une ligne par un appel de fonction (https://www.refactoring.com/catalog/replaceInlineCodeWithFunctionCall.html)
- réorganise les instructions (https://www.refactoring.com/catalog/slideStatements.html)
- découpe les phases (https://www.refactoring.com/catalog/splitPhase.html)
- déplace des instructions en dehors d'une function (https://www.refactoring.com/catalog/moveStatementsToCallers.html) inverse de déplace des instruction dans une fonction
- déplace des instructions dans une function (https://www.refactoring.com/catalog/moveStatementsIntoFunction.html) inverse de déplace des instruction en dehors d'une fonction
- renome une function pour clarifier l'usage (https://refactoring.com/catalog/changeFunctionDeclaration.html)
- supprime la hierarchir entre deux classes (https://refactoring.com/catalog/collapseHierarchy.html)
- combine des fonctions dans une classe (https://refactoring.com/catalog/combineFunctionsIntoClass.html)
- combine des fonctions dans une autre fonction (https://refactoring.com/catalog/combineFunctionsIntoTransform.html)
- encapsule collection (https://refactoring.com/catalog/encapsulateCollection.html) 
- encapsule un enregistrement de donnée (https://refactoring.com/catalog/encapsulateRecord.html)
- encapsule une variable (https://refactoring.com/catalog/encapsulateVariable.html)
- extrait une classe (https://refactoring.com/catalog/extractClass.html)
- extrait une fonction (https://refactoring.com/catalog/extractFunction.html)
- extrait une variable https://refactoring.com/catalog/extractVariable.html
- masque une delegation https://refactoring.com/catalog/hideDelegate.html
- internalise une classe https://refactoring.com/catalog/inlineClass.html
- internalise une fonction https://refactoring.com/catalog/inlineFunction.html
- internalise une variable https://refactoring.com/catalog/inlineVariable.html
- introduit un paramètre objet https://refactoring.com/catalog/introduceParameterObject.html
- introduit un objet special (nulle) https://refactoring.com/catalog/introduceSpecialCase.html
- déplacé un attribut https://refactoring.com/catalog/moveField.html
- déplacé une fonction https://refactoring.com/catalog/moveFunction.html
- utilisé un objet entier plutôt que ces attributs en paramètre https://refactoring.com/catalog/preserveWholeObject.html
- remonté un champ à la classe parent https://refactoring.com/catalog/pullUpField.html
- descendu une méthode dans une classe fille https://refactoring.com/catalog/pushDownMethod.html
- supprime une délégation masquée https://refactoring.com/catalog/removeMiddleMan.html
- renome une variable https://refactoring.com/catalog/renameVariable.html
- construit à partir d'une fonction de fabrication https://refactoring.com/catalog/replaceConstructorWithFactoryFunction.html
- remplace une variable par l'execution d'un calcul  https://refactoring.com/catalog/replaceDerivedVariableWithQuery.html
- remplace une exception par un test de condition https://refactoring.com/catalog/replaceExceptionWithPrecheck.html
- remplace une primitive par un objet https://refactoring.com/catalog/replacePrimitiveWithObject.html
- remplace une execution de requête par un paramètre https://refactoring.com/catalog/replaceQueryWithParameter.html
- remplace une sous classe par une delegation https://refactoring.com/catalog/replaceSubclassWithDelegate.html
- remplace une variable temporaire par un appel https://refactoring.com/catalog/replaceTempWithQuery.html
- sépare les requêtes des modificaitons https://refactoring.com/catalog/separateQueryFromModifier.html
- remplace un algrorithme https://refactoring.com/catalog/substituteAlgorithm.html
