[
  {
    auteur: ["Joanna MACY", "Molly YOUNG BROWN "],
    titre: "Ecopsychologie pratique et rituels pour la Terre"
  },
  {
    auteur: ["Gaston Pineau", "Dominique Bachelart", "Dominique Cottereau", "Anne Moneyron"],
    titre: "Habiter la Terre : Ecoformation terrestre pour une conscience planétaire"
  },
  {
    auteur: ["Christine Delphy"],
    titre: "L'ennemi principal"
  },
  {
    auteur: ["Grégoire Chamayou"],
    titre: "La société ingouvernable"
  },
  {
    auteur: ["Isabelle Collet"],
    titre: "Les oubliées du numérique"
  },
  {
    auteur: ["Matteo Papantuono", "Claudette Portelli", "Padraic Gibson"],
    titre: "Vaincre sans combattre"
  },
  {
    titre: "Apprendre à transgresser",
    url: "https://m-editeur.info/apprendre-a-transgresser-leducation-comme-pratique-de-la-liberte/",
    auteur: ["bell hooks"]
  },
  {
    titre: " Qu’est-ce que l’écologie sociale ?",
    url: "http://www.atelierdecreationlibertaire.com/Qu-est-ce-que-l-ecologie-sociale,756.html",
    auteur: ["Murray Bookchin"]

  },
  {
    titre: "La Vie secrète des arbres",
    auteur: ["Peter Wohlleben"]
  },
  {
    titre: "A contre-courant : entretiens",
    auteur: ["Jacques Ellul"],
  },
  {
    titre: "Tiers-lieux, fablab, hackerspaces. Et plus si affinités",
    auteur: ["Antoine Burret"]
  },
  {
    titre: "Qu'est-ce que l'approche narrative ?: Une brève introduction pour tous",
    auteur: ["Alice Morgan"]
  },
  {
    titre: "Je sais pourquoi chante l'oiseau en cage",
    auteur: ["Maya Angelou"]
    deja_lu: "?",
  },
  {
    titre: "Sous les branches de l'udala",
    auteur: ["Chinelo OKPARANTA"]
  },
  {
    titre: "Le deuxième sexe, tome 1",
    auteur: ["Simone de Beauvoir"]
  },
  {
    titre: "Le deuxième sexe, tome 2",
    auteur: ["Simone de Beauvoir"]
  },
]



La Ligne de Couleur de W.E.B. du Bois - Representer l Amerique Noire au Tournant du Xxe Siecle Broché – 22 novembre 2019
de Collectif/Morris (Auteur) 

https://www.editionsladecouverte.fr/catalogue/index-Les_Noirs_de_Philadelphie-9782348043482.html


Murray Bookchin
Changer sa vie sans changer le monde
L’anarchisme contemporain entre émancipation individuelle et révolution sociale



Joëlle Zask, Introduction à John Dewey
Lionel Francou


Sarah Vanuxem, La propriété de la terre, Wildproject, coll. « Le monde qui vient », Marseille, 2018, 144 p


https://www.parislibrairies.fr/livre/9782841389216-la-foret-jardin-martin-crawford/

https://www.foretpriveefrancaise.com/publications/voir/477


Être Radical, Saul Alinsky, 1971


https://radioparleur.net/2019/03/11/clowns-autonomes-feministes-qui-sont-les-nouveaux-anarchistes/

Osons l'école ! Plaidoyer pour une école audacieuse Auteurs : Adèle Monti et Hervé Bernard https://eud.u-bourgogne.fr/sciences-sociales/665-osons-l-ecole--9782364413283.html


https://fr.wikipedia.org/wiki/Gilbert_Simondon https://fr.wikipedia.org/wiki/Du_mode_d%27existence_des_objets_techniques

http://www.cnt-f.org/nautreecole/?Francisco-Ferrer-une-education

pédagogie solidaire http://www.sipayat.com/pedagogie-solidaire.html

https://www.tela-botanica.org/2019/10/les-gouts-et-les-couleurs-du-monde/ LIVRE / ACTES SUD publie le nouvel ouvrage de Marc-André Selosse sur les tannins végétaux : l'histoire naturelle des tanins, de l'écologie à la santé !

Réinventer l'association Jean-Louis Laville



Introduction de bell hooks à son livre : apprendre à transgresser

https://agone.org/contrefeux/changersaviesanschangerlemonde/

Face aux arbres https://www.amazon.fr/Face-aux-Arbres-Apprendre-comprendre/dp/2379220298?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&tag=duckduckgo-ffab-fr-21&linkCode=xm2&camp=2025&creative=165953&creativeASIN=2379220298

Dictionnaire visuel de botanique https://livre.fnac.com/a7231997/Maurice-Reille-Dictionnaire-visuel-de-botanique


http://www.horsdatteinte.org/produit/organisons-nous/

Organisons-nous ! Manuel critique

http://www.seuil.com/ouvrage/des-hommes-justes-ivan-jablonka/9782021401561 Ivan Jablonka Des hommes justes

Pour travailler à l’âge du numérique, défendons la coopérative ! https://jean-jaures.org/nos-productions/pour-travailler-a-l-age-du-numerique-defendons-la-cooperative Jérôme Giusti, Thomas Thévenoud 
