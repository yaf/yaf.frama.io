#!/bin/sh

# Ligne complète pour pandoc:
# pandoc --template=lib/template.html -c lib/pandoc.css -H lib/head.html -B lib/header.html -A lib/footer.html "$mdfile" -o "$htmlfile"

MARKDOWN_FILES=$(find content -name \*.md)
FEED_FILE=content/feed.xml

echo "## Generate HTML files for each markdown files"

for mdfile in $MARKDOWN_FILES; do
  htmlfile="${mdfile%%.*}.html"
  if [ ! -f "$htmlfile" ] || [ "$htmlfile" -ot "$mdfile" ]; then
    echo "--> generate with pandoc '$htmlfile' from '$mdfile'"
    pandoc --template=lib/template.html -c lib/pandoc.css "$mdfile" -o "$htmlfile"
  fi
done

echo "## END"

exit 0
