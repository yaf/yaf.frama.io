---
layout: post
title: URL Parsing with Ruby
---

Encore une fois [Ruby](http://ruby-lang.org)  me confirme que c’est un langage que j’adore.

Pour divers raison, il arrive parfois que l’on veuille manipuler une URL. Fourni sous forme complête, c’esst pratique, ça peut servir. Mais il arrive que l’on ai besoin de *parser* cette URL pour en extraire le domaine, sont extension... J’ai voulu me lancer dans une [expression régulière \[ou Expression Rationnelles Wikipedia\]](http://fr.wikipedia.org/wiki/Expressions_rationnelles) pour analyser une URL...

Quelle idiotie ! Il faut toujours se renseigner avant, parcourir l’API. Et justement, j’ai fouillé un peu. Et voilà, dans la librairie du “noyau” de ruby, on trouve un Objet fait pour: [URI](http://www.ruby-doc.org/core/classes/URI.html#M004633).

Du coup, au lieu de faire une expression régulière, on applique simplement quelque méthode de l’objet URI.

<pre>
  {% highlight ruby %}
    mon_url = "http://www.typouype.org/xml/rss20/feed.xml/"
    mon_uri = URI.parse(mon_url)
    p mon_uri.host
    # => "www.typouype.org"
  {% endhighlight %}
</pre>

Bon ça c’est gentil, mais j’en veux plus moi ! Qu’a cela ne tienne, une méthode fourni tout ce qui manquerais:

<pre>
  {% highlight ruby %}
    p URI.split("http://www.ruby-lang.org/")
    # => ["http", nil, "www.ruby-lang.org",
    #                       nil, nil, "/", nil, nil, nil]
  {% endhighlight %}
</pre>


Et bien voilà :-). Pour info voici les éléments renvoyé par le tableau:

* Scheme
* Userinfo
* Host
* Port
* Registry
* Path
* Opaque
* Query
* Fragment


C’est presque dommage que le tableau ne ssoit pas un Hash avec cet nom d’élément en guise de clé, avec les Clé défini comme attribut de class... (Quoi ? faut bien chipoter un peu non :-) )

*Désolé pour le titre en anglais, mais souvent quand je parle de “code” je préfère utiliser l’anglais qui est, je trouve, plus approprié.
*

