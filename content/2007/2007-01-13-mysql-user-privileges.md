---
layout: post
title: Mysql user privileges
---





*Désolé pour l’apparition du précédent billet, c’étais une erreur de manipulation :-p*




Petite note pour l’ajout d’utilisateur avec Mysql.





Se connecter en root à Mysql

<pre>
mysql -u root -p
</pre>




Le -p c’est pour signaler qu’on vas saisir le mot de passe. J’espère que vous en avez mis un bon, root est un utilisateur ayant tout les droits sur toute vos bases Mysql...





Plusieur possibilités ensuite. La gestion des accès et des utilisateurs (ainsi que bien d’autres chose) sont faite par le biais d’une base *système* dans Mysql (dans oracle on a un petit peu ça aussi, je me demande si tout les moteur de base de donnée ne le font pas...). Donc soit on fait de la requête <span class="caps">SQL</span> basique sur les tables qui vont bien (dans notre cas mysql.user oÃ¹ mysql est le nom de la base accéssible par root uniquement et user la table de gestion des utilisateur mysql). Soit on utilise quelque raccourci (on vas faire les deux :p).





Pour ajouter un utilisateur “toto” pouvant se connecter à partir de la machine hebergant la base:

<pre>
USE mysql;
CREATE USER toto@localhost;
</pre>




N’oublié pas le <b>;</b> pour finir vos commande :). <b><span class="caps">USE</span></b> permet de spécifier sur quelle base on souhaite travailler. Le localhost précise que cet utilisateur ne pourra se connecté qu’à partir de “localhost”.

Voilà un petit contrôle maintenant:

<pre>
SELECT * FROM user;
</pre>




Doit nous retourner la liste des utilisateurs, “toto” doit apparaitre avec toute les valeurs par défaut qui vont bien, mais sans mot de passe... La boulette ! Corrigeons ça:

<pre>
UPDATE user SET password=PASSWORD("motdepasse")
WHERE user="toto";
</pre>




La fonction <b><span class="caps">PASSWORD</span></b> permet de crypter le mot de passe dans la base de donnée.





Un autre petit <b>select</b> devrais nous confirmer que le mot de passe à bien été ajouté (bon normalement il est illisible :p).




Les accès maintenant. Ben oui, notre utilisateur toto n’accède à aucune base par défaut. Ajoutons lui l’accès (toujours avec l’utilisateur root):

<pre>
GRANT ALL ON test.* TO 'toto'@'localhost';
</pre>




Voilà, c’est basique, pas forcement très fin, mais ça donne un accès complet à l’utilisateur toto sur la base “test”.





Il faut ensuite valider toute ces modifications (surtout les privileges, la création d’utilisateur n’en à pas bessoin.

<pre>
FLUSH PRIVILEGES;
</pre>




Et voilà, on peut sortir et se connecté avec toto :).

Il existe beaucoup d’option pour réglé les droits d’accès très finement. Si vous souhaité justement mettre en place une meilleur gestion d’utilisateur, je vous envoie vers la doc officielle qui est très bien faites : [http://dev.mysql.com/doc/](http://dev.mysql.com/doc/)


*Depuis longtemps je souhaite utiliser [PostgreSQL](http://www.postgresql.org/) en lieu et place de Mysql, mais voilà, le manque de temps tout ça... Bon okay, j’ai aucune excuse, promis je le ferais dès que j’ai le temps. En attendant, l’avantage affiché clairement de mysql c’est la simplicité, et il faut avouer que ça marche bien.*




**edit:**

Petite ajout de dernière minutes. On peut (et je dirais même que ça semble mieux) ajouter les privilèges comme ceci:

<pre>
grant all privileges on database_name.* to toto@host identified by "monpasswordenclair";
</pre>


