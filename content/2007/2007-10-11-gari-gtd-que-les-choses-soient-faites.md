---
layout: post
title: GARI, GTD, que les choses soient faites
---


Ca fait un moment que l’on entend parler du concept <acronym title="Getting Thinks Done"><span class="caps">GTD</span></acronym> (plus d’info sur [wikipedia:Getting Things Done](http://fr.wikipedia.org/wiki/Getting_Things_Done)). C’est une méthode qui doit permettre de **faire** les choses, avec gestion des prioritées tous ça...

Bon, j’avoue que ça semble très interessant, surtout quand on cherche à arreter de s’éparpiller sur divers projets, divers bricolage à faire pour la maison, les 3 tonnes de courrier non ouverts, etc... Mais voilà, appliquer une méthode demande la *rigueur* de l’_application_ de cette méthode (c’est con dit comme ça hein ?).

Et voilà qu’il y a quelque jour, [chez Ploum](http://ploum.frimouvy.org/), je tombe sur un billet traitant du sujet de [l’organisation tout en glandouillant:](http://ploum.frimouvy.org/?169-comment-s-organiser-sans-jamais-ranger-avec-la-methode-glande-appliquee-rangement-interdit)  <acronym title="Glande Appliquée &amp; Rangement Interdit"><span class="caps">GARI</span></acronym> qui est une sorte d’adaptation *Ploumesque* de <span class="caps">GTD</span>.

Comme Ploum le rappel, toute méthode est interessante, mais bien souvent nécessite des adaptations en fonction du contexte, et des gens qui l’applique. Ploum à donc très bien décrit ça vision du <span class="caps">GTD</span>. Et j’avoue que ça version du <span class="caps">GTD</span> me plait bien. J’ai donc décidé de l’appliquer, du moins en partie, pour voir.

J’ai donc enlver les règles de tri de mes messages, tout remis dans la **inbox** principal, supprimer les dossier de chaque *mailling-list* à laquelle je suis abonné, puis créé un dossier **deskbox**. Pour trier un peu dans la **deskbox** j’ai ensuite créé des sous dossiers par projet courant (bon pour le moment c’est ma boite perso, donc ça ne concerne que mes projet perso, pour le coté pro on verra plus tard).

Ensuite viens le temps du grand menage de **inbox**. Comme le dit Ploum, on stock tros de mail, et c’est vrai, qui fouille dans ces mails à la recherche d’une info sur comment on configure PF sur OpenBSD 4.2 ? On vas tous sur notre moteur de recherche préféré, voir sur plusieurs pour bien faire, et c’est tout... Les mails, ça ne sert pas à grand chose de tous les stocker, même en archives.

Attention me faite pas dire ce que je n’ai pas dit ! Certain sont à garder. J’ai donc aussi ajouté une boite **archive**.

Après suppression et tri des messages, j’ai une **inbox** vide (pour le moment) et il faut avouer que ce n’est pas désagréable. Les dossiers par projets ne sont pas trop plein et ne contiennent que des choses en rapport avec mes projets en cours. Et ma **deskbox** contient 2 mails sur lesquel il faut que je travail...

A suivre donc, on verra si ça me rend plsu efficace. Peut-être une tentative de **inbox** physique pourrais suivre...


