---
layout: post
title: Et pourquoi pas OpenSolaris
---


C’est la reflexion que je me suis faite il y a une semaine. [OpenSolaris](http://www.opensolaris.org/) est le projet d’ouverture du code du système d’exploitationSolaris de [Sun Microsystem](http://fr.wikipedia.org/wiki/Sun_Microsystems) ([voir la définition wikipedia](http://fr.wikipedia.org/wiki/OpenSolaris)). Ce projet à été lancé en 2005 (si je ne me trompe pas). C’est une bonne chose.

Beaucoup ce demandais pourquoi Sun (créateur de java, mais surtout fournisseur de machine et processeur Sparc) n’avais pas rempalcé son système unix propriétaire par un linux retravaillé (comme <span class="caps">IBM</span> et d’autres ont pu le faire). J’avoue que je suis content de la voie prise par Sun. Ce qui me plait dans le libre, ou même en général, c’est d’avoir le choix. Le choix c’est une forme de liberté. Ouvrir le code de leur système permet de le faire connaitre et évoluer plus facilement, plus rapidement.

Un autre point à joué dans mon choix, mon envie de tester OpenSolaris. [Ian Murdock](http://ianmurdock.com/) le papa de [Debian](http://www.debian.org) a rejoint Sun pour travailler sur OpenSolaris ! C’est une bonne nouvelle. Cela montre aussi que Sun ne fait pas cette ouverture par effet de mode, mais croit dans le mode de distribution libre (Java aussi est en train de devenir libre ;-) ).

Basé sur Gnome, le bureau et les application sont assez complète. Même si en regardant sous le capot il y a pas mal de chose très différentes, cela reste un Unix, et on ne s’y perd pas trop trop.

*Bon j’ai quand même pas mal de chose à apprendre sur ce système. Je vous en ferais part au fur et à mesure. Si jamais vous voulez l’essayer ça pourrais servir. Moi ça me fera un pense-bête ;-)*


