---
layout: post
title: OpenBSD 4.2 Expat missing
---


Les utilisateurs d’"OpenBSD":http://www.openbsd.org le savent déjà surement, le *set* `xbase` est nécessaire, même pour une installation serveur (enfin presque).

Effectivement, lors du passage à la version 4.2, le *set* graphique est passé d’un xorg à **xenocara** un serveur X basé sur xorg 7.2, revu et corrigé à la sauce Open. Du cout, la lib expat utilisé est celle de xenocara. Manque de bol (ou pas) expat est utilisé par beaucoup de paquet (comme vim par exemple) et du coup, pour une simple installation serveur il faut installer xbase (qui contient la lib expat).

C’est bien la première fois que je suis déçu par OpenBSD... Quel dommage d’avoir raté ça. Mais comme il est dit sur @misc (la mailling list principal d’OpenBSD) *plus que 5 mois a attendre pour la version 4.3*.

Effectivement, dans cette version, la lib expat est rappatrié dans le set *base* :-)

Alors, petit accro, mais rapidement corrigé.


