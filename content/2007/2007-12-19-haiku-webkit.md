---
layout: post
title: "Haiku: WebKit"
---

[Haiku](http://haiku-os.org/) fait parti de ces système d’exploitation que je surveil du coin de l’oeil.

L’équipe de développement a décidé d’implémenté un navigateur basé sur un portage de webkit pour Haiku :) Firefox version BeOS (feu BeOS devrais-je dire), système d’exploitation dont le *design* est repris par Haiku (mais sous licence libre cette fois) fonctionne également, mais rien de tel (dans un OS comme Haiku) qu’un navigateur *natif* je pense.

Bref, une petite copie d’écran du [premier rendu webkit dans haiku](http://haiku-os.org/blog/leavengood/2007-12-18/our_first_decent_webkit_rendering) confirme vraiment mon envie de suivre ce projet :D Miam !

!(http://zone.typouype.org/bebits-render2.png)

