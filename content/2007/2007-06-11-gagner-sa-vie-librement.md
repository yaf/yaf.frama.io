---
layout: post
title: Gagner sa vie librement
---


*ou les reflexions d’un trentenaire qui cherche une suite à ça carrière (attention poste un peu long, tout comme ma reflexion)*

Pfff. J’avais commencé un grand billet sur mes reflexion dans le domaine, mais je me suis fait rattrapé par l’"AFUL":http://www.aful.org qui a fait un très bon papier sur [les modèles économique du libre](http://www.aful.org/professionnels/modeles-economiques-ll).  Un papier bien plus complêt que mes propres reflexions.

*Disons que ça ne me permet toujours pas d’y voir plus clair. Il faut avouer que la province offre quand même moins de choix que Paris. Alors trouver un emploi lié aux logiciels libre en province... dur dur, mais je reste à l’écoute ;-)*

*edit: Correction orthographique dans le titre. Merci Terckan, je suis un boulet de l’orthographe, il faut que je me soigne*


