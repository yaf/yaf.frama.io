---
layout: post
title: Tout sur le Sumo
---

  
Non je ne deviens pas Chiraquien, et non plus un expert en Sumo.

Mais le frère d’un ami de longue date, franco-japonais, est parti au Japon pour parfaire son Japonais, et voir un peu de quoi il retourne là bas (rentrera, rentrera pas ?).

Il a décidé de tenir un blog pour garder le contact avec ces amis Francophone, et comme cela ce fait beaucoup, pour faire partager ses expériences là bas.

Il viens de signer un très bon billet, agrémenté de quelques photos, sur les combats Sumo.

Le blog de [Tarto au Japon – Sumo](http://www.tartoaujapon.com/fr/blog-post-100-sumo.html)

  