---
layout: post
title: Veni Vidi Vici
---


[La célèbre citation de Jules César](http://fr.wikipedia.org/wiki/Veni_vidi_vici)...

Elle s’appliquerais pas mal a l’expérience que nous avions entrepris au début de l’année 2007 (voir le billet [Tout ce bouscule](http://www.typouype.org/articles/2006/12/06/tout-ce-bouscule)). Mais voilà, on était parti de notre banlieue parisienne natale pour voir ce qu’est la vie en province, et plus particulièrement Montpellier.

Il faut avouer que c’est très agréable, mais niveau emploi ici, faut pas trop en demander, ou avoir un coup de bol, dans le cas d’un couple, deux coup de bol, ça comment à faire beaucoup.

Du coup, nous remontons, c’est décidé. Et ça va aller vite, plus vite que pour descendre ! Nous serons donc de retour sur Paris et sa région au tout début du mois de novembre.

**Et dans ce cadre, j’ai remis mon [CV](http://zone.typouype.org/cv_yannick_francois_20071016.pdf) à jour, je l’ai mis en ligne car je recherche du boulot sur Paris (et sa région).**

*J’en parle ici, on ne sait jamais...*


