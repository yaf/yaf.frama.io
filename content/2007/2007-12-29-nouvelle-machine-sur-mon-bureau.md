---
layout: post
title: Nouvelle machine sur mon bureau
---


Et voilà, j’ai un portable maintenat... Sous OpenBSD bien sur.

![Bureau appart de marly](/files/bureau_appart_marly.jpg)

J’avoue que l’écran en 1480×1050 m’a posé quelque soucis de configuration, mais avec le bon modeline on arrive à tous (enfin dans mon cas ça marche :p).

J’ai rendu mon [xorg.conf](http://zone.typouype.org/xorg.conf) disponible dans ma zone. Il va sûrement évoluer un peu, mais j’essayerais de le mettre à jour.

Bon maintenant faut que je vois pour l’ACPI et passé sur un noyau .MP.

*to be continued...*

*ps: le windows à coté c’est pas à moi. Disons que j’ai fait trop de lobbying auprès de mon amie, du coup c’est mort pour l’instant, je la ferais pas passer sous <span class="caps">GNU</span>/Linux et encore moi sous OpenBSD... Pour le moment j’espère :)*


