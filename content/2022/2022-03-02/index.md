---
title: Mercredi 2 mars 2022
---

Pour mettre en œuvre une authentification à deux facteurs sur mattermost, par défaut, ça me parle d'un QRCode et de Google Authentification.

Et j'entends à nouveau parlé de TOTP...

Et on me parle de Yubikey
https://fr.wikipedia.org/wiki/YubiKey

c'est même possible sous openbsd https://man.openbsd.org/login_yubikey.8

---

> Pour Isabelle Collet, ce cas d'école de réthorique sexiste au sein d'un secteur qui se dit progressiste, s'explique par la diffusion d'un discours particulier : " On imagine qu'il n'y a plus de problèmes d'inégalités parce qu'on est tous beaucoup trop intelligents, trop instruits pour être sexistes, racistes ou homophobes. Et donc les différences que l'on voit encore, puisque ça ne peut pas être nous qui sommes inégaux, c'est forcément la biologie. Donc en fait le raisonnement il est complètement à rebours."

> Science informatique hérite des mathématiques
c'est une erreur justement.

à la minute 25, on parle (enfin ?) du fait que c'est encore plus compliqué pour les femmes non blanche

https://www.franceculture.fr/emissions/le-meilleur-des-mondes/femmes-dans-la-tech-a-quand-la-revolution

---

“Code is like humour. When you have to explain it, it’s bad.”

-Cory House

---

> A minimalist stylesheet for HTML elements
>
> No class names, no frameworks, just semantic HTML and you're done.

https://andybrewer.github.io/mvp/
