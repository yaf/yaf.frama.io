---
title: Juillet 2022
---

[![Idée d'eau infusée](eau-infusee.jpg){width=600}](eau-infusee.jpg)

_Des recettes d'eau infusée_

- Citron vert et fraises
- Citron vert, menthe et gingembre
- Ananas et framboise
- Pomme verte, frases et menthe
- Concombre, fraises et orange
- Pomme, canelle et menthe
- Orange, framboise et myrtilles
- Kiwi, fraises et mangue
- Carotte et menther
- Orange et citron
- Fraises et mûres
- Fraises et fruit du dragon
- Citron vert, myrtilles et menthe
- Ananas et menthe
- Fraises, menthe et mangue
- Concombre et menthe

## Dimanche 31

[No Soap, comment se laver sans savon](https://kaizen-magazine.com/article/no-soap-comment-se-laver-sans-savon/)

> le Dr Yael Adler, dermatologue allemande, l’affirme, non sans humour (3) : «
> A trop se savonner, on finit par puer ! » Le savon modifie le pH de la peau,
> le rendant trop alcalin. Il détruit également son film hydrolipidique.
> Résultat ? « D’un seul coup, des germes que nous n’avons jamais invités se
> multiplient, et la modification du pH fait qu’il ne peuvent plus être tenus
> en respect. Ces germes modifient notre odeur corporelle, a priori agréable,
> et nous voilà partis du côté de chez beurk ! » Pour redonner l’équilibre à
> notre peau, elle conseille de revenir à « l’âge de pierre, le stade désirable
> par excellence, celui vers lequel il faut tendre pour réussir sa vie de peau
> ». Exit les douches quotidiennes, un savonnage par semaine suffit. Ou alors,
> écrit-elle, « Faisons un compromis : d’accord pour une douche quotidienne à
> condition de n’utiliser que de l’eau, ou presque. L’eau, qui a un pH neutre,
> dessèche moins la peau que le savon. » Et lave tout aussi bien, puisque la
> sueur, la poussière et les cellules mortes sont hydrosolubles.

> L’odeur dégagée par la peau est le reflet d’un équilibre global : odeur
> naturelle du corps, santé, alimentation. 

J'aime bien la démarche qui consiste à questionner pourquoi nous utilisons
quelque chose. Souvent je me demande comment fabriquer moi même, ou simplement,
comment c'est fabriquer. Mais la meilleur option est celle de s'en
passer :)

    1. Renoncer
    2. Réduire
    3. Réparer
    4. Réutiliser
    5. Recycler

## Vendredi 29

_Je viens de faire mon actualisation Pôle Emploi, je ne suis donc resté au
chômage officiellement que 2 mois. Merci [Chrysalide](https://www.cae29.coop/chrysalide.html), et [RDV Solidarités](https://beta.gouv.fr/startups/lapins.html)_

### RDV-Solidarités

Discussion avec Nesserine et Nathalie autour de la modification qui vise à
afficher le contexte sans possibilité de navigation coté usager dans le cadre
d'une invitation à prendre RDV.

Nous avons aussi profité de l'occasion pour intégrer Nesserine à l'équipe sur
Github, et fait en sorte qu'elle puisse « pousser en prod » \o/

Reprise de la PR sur la sélection de service pour mieux l'intégrer à la logique
actuelle.

TODO : impression de pouvoir revenir en arrière alors que non dans le cas d'un
seul service proposant des motifs ouverts au public.

[MobProg](https://mobprogramming.com/) avec Nathalie pour nettoyer et finaliser certaines PR.

### Notes

- À lire ? [La bosse des maths n'existe pas de Clémence Perronnet](https://www.autrement.com/la-bosse-des-maths-nexiste-pas/9782746755734)
- Une bonne liste de ressource en français à propos de la programmation : [Gunter Mueller / Free programming books [fr]](https://github.com/GunterMueller/free-programming-books/blob/master/free-programming-books-fr.md)


## Jeudi 28

### RDV-Solidarités

Discussion Nesserine à propos du questionnaire

Réunion tech

- migration sur OVH, on se met au clair avec Mohamed
- InsertionConnect ça serait cool de le mettre en place à la rentrée

    - Sécu ?
    - Multicompte (multi email) ? À 
    - Compatible avec AgentConnect
    - Gestion d'identité vérouillé ?

Discussiona avec Victor et Thomas à propos de Aide Jeunes et des perspective de
RDV-Solidarités.

Point avec Anne-Sophie. Elle va commencer le travail sur le statut de
participation  doucement en août et finira probablement début septembre.  Pour
qu'elle avance, il lui faut un environnement avec des choses dessus, en
utilisant son adresse beta ; lui envoyer les documents et autres notes à propos
de ce besoin.

Travail sur PR de notification en cas de changement d'adresse avec Nathalie.
Nous avons découvert un bug ancien : nous créons un nouveau lieu ponctuel à
chaque modification d'un RDV sur un lieu ponctuel.  Nous faisons une autre PR
vite fait pour corriger ce problème. Un peu trop vite fait. Nous avons oublié
de faire un test. Une fois réalisé, il se pose une autre question pour la PR
sur la notification : comment déceler qu'il y a eu un changement dans le lieu
ponctuel ?

Nous avons des cas de figure supplémentaire à mettre en œuvre.

## Mercredi 27

### RDV-Solidarités 

PR sur la possibilité de supprimer un RDV. Même si des `receipts` sont présents,
nous allons autorisé la suppression si le RDV est annulé. Cela devrait signifié
que l'usager à été notifié de l'annulation.

[ticket 2543](https://github.com/betagouv/rdv-solidarites.fr/issues/2543)

Ça tourne autour de la dénormalisation du RDV. Je repense encore à faire une
isolation du RDV, et même de travailler les données d'un RDV comme de l'event
sourcing. C'est à dire stocker les changements d'état et non l'état final.
Prendre en considération l'historique plutôt que l'état. La période de
modification est assez restreinte, il n'y aurait donc pas une trop grosse pile
d'évènement je pense. Au plus petit il y aurai « création » puis « changement de
statut ». Le plus long pourrait être un RDV qui est déplacé, annulé, remis en
place, déplacer, modifier, ... mais à un moment donnée, ce RDV «termine» malgré
tout... 

Et Victor me demande de documenter un peu plus la décision. Il a raison ! Je
continue à me demande si les tickets sont un endroit pertinent pour ce type de
documentation... Mais peut-être qu'il faut un peu des deux. J'aimerais lister
des cas d'utilisations dans la doc (notion à ce jour)...

Discussion avec Myriam pour enquêter sur « pourquoi supprimer des RDV ».
J'aimerais parler avec Victor sur le design et l'architecture cible, même si
nous n'y allons pas tout de suite.

Échange avec Gauthier. Un candidat que nous n'avons pas pris finalement, qui a
travaillé chez Doctolib et qui c'est lancé dans une PR pour le ticket 2650 à
propos de la restriction d'accès des agents à des RDV (voir sa [PR
2680](https://github.com/betagouv/rdv-solidarites.fr/pull/2680). C'était une
chouette rencontre.

Réponse à la Drôme à propos des nouveaux développements qui vont être amorcé
par Beezim à propos de l'interconnexion.

Début de travaux sur la PR d'ouverture des RDV Collectifs.
Patch sur la navigation : dans le cadre d'une invitation, on supprime cette navigation.

PR où on va garder visible le motif et le lieu (enfin, les éléments de
contexte) même s'il ne sont pas navigable histoire de respecter certains
critères (?) du [RGAA](https://www.numerique.gouv.fr/publications/rgaa-accessibilite-numerique/)

> - Critère 10.3. Dans chaque page web, l’information reste-t-elle compréhensible lorsque les feuilles de styles sont désactivées ? 
> - Critère 9.1. Dans chaque page web, l’information est-elle structurée par l’utilisation appropriée de titres ? 

## Mardi 26

### RDV-Solidarités

Travaux sur la navigation avec choix d'un service avant l'affichage des motifs lorsqu'il y a plusieurs services.

Préparation de la réunion référentes du 2 août 2022 avec Myriam.

Analyse des RDV avec statut en salle d'attente.

```sql
select count(*) from versions where item_type = 'Rdv' and object_changes ilike '%waiting%';
 count 
-------
 42029
```
- Seine-et-Marne
- Somme
- Calvados
- Pas-de-Calais
- Pyrénées-Atlantique
- Hauts-de-Seine
- Aveyron
- Drôme
- Meuse
- Ardennes
- Var
- Conseiller numérique
- Côtes d'Armor
- Haute-Savoie
- Manche

Mon approche un peu brute force pour explorer ça

```sql
select territories.id, territories.name, rdvs.id 
from versions
inner join rdvs on rdvs.id = versions.item_id
inner join organisations on organisations.id = rdvs.organisation_id 
inner join territories on territories.id = organisations.territory_id 
where item_type = 'Rdv' 
and object_changes ilike '%waiting%' 
and territories.id not in (1, 2, 5, 7, 9, 12, 11, 4, 6, 13, 25, 31, 3, 41, 19) 
limit 10;
```

Où j'ai exclu les id de territoire au fur et à mesure que je les voyais apparaître...

J'envisage de malgré tout poser la question, sans partager cette information,
aux référentes qui seront présentes mardi prochain. Mais là, ça dépasse un peu
le cadre du médico-social. Il y a les conseillers numériques (est-ce que ce
sont des tests ?) et du data.insertion également...

Ce qu'il me manque c'est une idée du volume dans cette requête / analyse...

Il faudrait d'ailleurs faire un peu plus qu'un `count`, parce que le statut
apparaît parfois 2 fois : un changement pour passer en salle d'attente, et un
changement pour en sortir... Il faudrait faire un unique sur le numéro de RDV,
et le faire par département...

Standup hebdomadaire. C'est plutôt chouette de faire un tour de ce qui est
prévu. En se projetant dans l'avenir proche, nous sommes moins dans un «
rapport d'activité » comme on peut le voir parfois. Comme dit Myriam, ça permet
de pouvoir s'inviter sur une réunion ou une rencontre :)

Reprise des test de la PR sur la sélection d'un service avant l'affichage des
motifs.

Contacter les UX/Desgin transverse de l'ANCT ?


### Terre de liens

Conseil d'administration Terre de liens.

Où nous parlons de faire des vidéo. Il y a une [chaine youtube de Terre de liens](https://www.youtube.com/channel/UCFDSg22G3RXJ4th9imzaG1Q). Ça serait tellement bien d'avoir une chaine peertube. 

Je crois que Terre de liens pourrais proposer une sorte de chatons, pour la fondation, la foncière, la fédération, les AT, et pourquoi pas, pour les fermes TDL et les GFA/SCI !

## Lundi 25

### Terre de liens

Formation Wordpress chez [Defis association](https://www.defis.info/).  Utilisation du thème
[Divi](https://www.elegantthemes.com/gallery/divi/).

Outil pour réduire la taille des images et le poids.

Discussion chatons : Viviana souhaite organiser une rencontre des chatons
bretons. C'est une chouette idée. Je lui parle du forum ouvert.

Discussion autour de la réparation de l'ordi d'Ydrisse. Il est bien cassé :-/
Autour de 100 € de réparation. Selon si on trouve une dalle d'occasion ou pas.

---

> "Les obligations sont attachées au foncier, pas aux personnes. Elles sont
> transmises de propriétaire en propriétaire. C'est vraiment l'idée de
> transmission d'un héritage vert", explique Vanessa Kurukgy, juriste chargée
> des ORE et de la stratégie foncière à la Fédération des Conservatoires
> d'Espace Naturels.

[Voici comment protéger la biodiversité de votre terrain pendant 99 ans (même
s'il change de
propriétaire)](https://www.18h39.fr/articles/voici-comment-proteger-la-biodiversite-de-votre-terrain-pendant-99-ans-meme-sil-change-de-proprietaire.html)


### RDV-Solidarités

À propos des recrutements ; Dante pourrait intégrer rdv-solidarités pour
apporter une dimension produit (qui manque ?) dans le médico-social, à la place
de François qui irait faire de l'insertion. On va rencontrer Dante.

Finalisation de la revue des retours des référentes. Éternelle question de la
priorisation et du mode de communication avec les référentes.

Rapide session avec Nathalie pour regarde pourquoi il y a des notifications à
chaque fois que le RDV change (y compris sur le changement de contexte) ?  Il y
a un pépin sur le lieu. Il doit y avoir une astuce sur la mise à jour pour un
lieu ponctuel qui déclenche un « changement » même si la valeur ne change pas.
Changement sur les valeurs à tester pour le changement de lieu...

Vis-à-vis des retours à propos du parcours utilisateurs, est-ce qu'il ne
faudrait pas ajouter une page `_service_selection` lors qu'il y a plus de 1
service possible ? (en attendant d'avoir une personne en design qui travaille
sur ce parcours) ?  Les retours de Christelle laisse entendre que c'était mieux
de passer par le choix du service. Je ne suis pas forcement pour revenir à ce
qu'il y avait avant, mais il y a peut-être quelque chose à prévoir au début
pour dire : Je cherche un RDV pour la PMI, la MDPH ou le social, ou autre...

Exploration de l'ajout d'un filtre sur les services...  J'ai ajouter une étape
sur laquelle il n'est pas simple de revenir. Il faudrait maintenir un statut
permettant de savoir s'il y a plusieurs services ou pas...

Faire en sorte que le retour arrière sur le motif arrive sur la liste des
motifs du service concerné.  Nous pouvons utiliser le `service_id` comme un
indice, dans la page des motifs, qu'il existe d'autres services... Mais ce
n'est pas super satisfaisant. **Comment maintenir l'information qu'il existe
plusieurs services ?**

### Notes

> Les marcheurs ne sont pas pressés. Ils cheminent à quatre ou cinq
> kilomètres-heure, n’hésitent pas à faire la sieste ou à lanterner quand, en
> avion, on traverse l’Atlantique en une dizaine d’heures. Une journée de
> marche revient à quinze-vingt minutes de voiture. Les marcheurs prennent leur
> temps et refusent que leur temps les prenne. Les heures sont à eux, non aux
> impératifs sociaux.

[Éloge de la marche dans un monde qui va trop
vite](https://reporterre.net/Eloge-de-la-marche-dans-un-monde-qui-va-trop-vite)

### Tech

L'autre jour, François m'a parlé de son ordi : un
[Framework](https://frame.work/fr/fr/). Intéressant concept. Le framework est à
l'ordinateur ce que je Fairephone est au mobile.

---

Les CUMA, j'y vois deux axes de travaille :
- participer à enrichir l'offre numérique des Cuma (il semble qu'il y ait déjà
  quelques logiciel de compta et autres)
- utiliser le principe pour proposer une mutualisation du matériel, des
  serveurs et du logiciel informatique...

### À lire ?

- À lire ? Marcher la vie — Un art tranquille du bonheur (2020) par David Le
Breton
- À lire ? Marcher — Éloge des chemins et de la lenteur (2012) par David
Le Breton
- À lire ? En roue libre — Une anthropologie sentimentale du vélo (aux
éditions Terre urbaine, 2020) par David Le Breton
- À lire ? [« Les Orages libertaires » de Max Leroy](http://atelierdecreationlibertaire.com/Les-Orages-libertaires.html)

## Samedi 23

Où j’apprends en discutant avec Gwendal que le Roi Morvan communauté est une
communauté de commune super pauvre, et pourtant réservoir d'eau pour une grosse
partie du sud Bretagne...  C'est un des sujets qui amène les grosse communautés
de commune environnante à vouloir découper le Roi Morvan Communauté en petit
pour pour ce les partager...

### Notes

Petite balade à Langonnet [Circuit Saint Maur Minez Levenez](https://www.langonnet.bzh/wp-content/uploads/sites/29/2018/06/CircuitRando_StMaurMinezLevenez.pdf)

---

> Use the most specific and expressive tool. The tool that best fits your use
> case is likely to be the fastest.
> 
> As a rough guide:
> 
> - searching for lines matching a substring or regexp? Use grep.
> - selecting certain columns from a simply-delimited file? Use cut.
> - performing pattern-based substitutions or ... other stuff sed can
>   reasonably do? Use sed.
> - need some combination of the above 3, or printf formatting, or general
>   purpose loops and branches? Use awk.

[Unix Stack Exchane - Using grep vs awk](https://unix.stackexchange.com/questions/88503/using-grep-vs-awk)

## Vendredi 22

### RDV-Solidarités

Binômage avec Nathalie.
- Nous avons repris le ticket sur la navigation que j'avais cassé (sur la vue
  de login) ;
- Finalisation du ticket sur les notifications en cas de changement de lieu

À faire pour plus tard : Refactoring des notifiers pour passer d'un paradigme
objet à un paradigme fonctionnel

## Jeudi 21

- Atelier autour des motifs, où l'on fini par reparler des droits d'accès
- réunion tech : on essaie de mettre en place nos client VPN...
- Rétrospective

Qui se sert du statut « en salles d'attentes » Qui utilise encore les Remarques
dans la fiche utilisateur

Suppression des territoires qui ne sont plus utilisé en prod : 13 en tout.
C'est justement en explorant un cas particulier où un usager à pris RDV en
Gironde que l'on se retrouve à faire le ménage.

## Mercredi 20

### RDV-Solidarités

Discussion avec Anne-Sophie

- audit RGAA
- recrutement d'une personne Design/ux
- aide sur le passage à un statut de participation

Point avec Data.insertion

- recrutement dev
- parcours usager unifié
- recrutement design/ux

Début d'analyse pour la [PR sur le fait de pouvoir supprimer un RDV une fois les notifications d'annulation bien parties](https://github.com/betagouv/rdv-solidarites.fr/issues/2543)

Les `event` possibles ?

- `new_creneau_available` : lié au fil d'attente
- `rdv_upcoming_reminder` : lié au rappel
- `rdv_created` : création
- `rdv_cancelled` : annulation
- `rdv_date_updated` : mise à jour Pourquoi il n'y a pas d'enum là dessus ?

Correspond au nom des 4 méthodes présentes dans `Users::RdvSms` ainsi que
`app/mailers/users/rdv_mailer.rb` et `app/mailers/users/file_attente_mailer.rb`
et `app/mailers/agents/rdv_mailer.rb` pour les agents.  C'est la structure en
métaprogrammation qui précise l'`event` Voir ligne 38 du fichier
`app/sms/application_sms.rb` 

```ruby
sms.receipt_params[:event] = symbol
```

Pour les mailers, c'est l'utilisation de [`action_name`](https://api.rubyonrails.org/classes/AbstractController/Base.html#method-i-action_name) qui rempli l'`event`

En prod 

```ruby
Receipt.select(:event).all.distinct
# => [#<Receipt:0x00007f3429df6998 id: nil, event: "new_creneau_available">,
# #<Receipt:0x00007f3429dc4d80 id: nil, event: "rdv_cancelled">,
# #<Receipt:0x00007f3429dc4c90 id: nil, event: "rdv_created">,
# #<Receipt:0x00007f3429dc4b78 id: nil, event: "rdv_date_updated">,
# #<Receipt:0x00007f3429dc4a88 id: nil, event: "rdv_upcoming_reminder">]
```

## Mardi 19

### RDV-Solidarités

Support utilisateurs, traitement de quelques tickets.

Mob avec Nathalie sur l'envoie de notification sur changement de lieu Mob avec
Nathalie et François pour le bug autour du manque d'usagers dans les webhook

Discussion avec Victor. Pas simple, mais au moins nous pouvons nous dire les
choses. J'ai apprécié son honnêteté. Il semble qu'il n'ai pas confiance en moi.
Ou plus confiance. C'est sans doute problématique pour la suite, il va falloir
reconstruire cette confiance. Ou bien souhaite-t-il que je me mette en retrait
? Et si je partait de RDV-S, c'est peut-être le bon moment...  Nous avons aussi
aborder des questions intéressantes autour des « tâches » un peu transverse à
l'équipe qui sont souvent saisie par moi. Victor à repris le recrutement parce
que je n'avançait pas dessus. Je n'avais plus envie de le faire. Je prends
encore pas mal d'autres choses qui mériterais d'être réparti sur l'équipe. Et
il y a sans doute des trucs que je ne vois pas, qui pose justement problème.

Un standup ? C'est quoi ? Pour quoi faire ? Faut-il faire le tour de toutes les
personnes pour qu'elles racontent ce qu'elles sont en train de faire ou bien
lister les « grandes tâches » transverse et feuille de route ?

Il y a un paquet de « dette technique » dans le code de RDV-Solidarités. Et si
je prenais l'invitation à ralentir comme une opportunité de nettoyer ?
Peut-être revoir l'usage abusif des `form_objects` ? Ou encore la trop grande
utilisation d'héritage (voir l'article wikipédia [composition over
inheritance](https://en.wikipedia.org/wiki/Composition_over_inheritance)). Nous
en parlons justement un peu avec Nathalie.

Participation à la fin de réunion avec le Lot-et-Garonne à propos des passes
numériques et particulièrement d'un système d'API pour la création de RDV.

PR pour regrouper les motifs présenté aux usagers par service. C'est un mail de
Sandra (80) qui nous a alerté là dessus. Deux autres tickets créés pour
proposer de faire de la recherche de motif ou du filtre (avec case à coché).
Cette dernière piste semble la plus intéressante.

Analyse autour du ticket sur les RDV collectif, pour mettre en place un statut
de participation.

## Lundi 18

### RDV-Solidarités

Grooming des tickets de RDV-Solidarités

MobProg avec Nathalie sur les PR en attente de revue, ou les retours...

:warning: statistique basé sur les RDV qui sont automatiquement supprimé au
bout de 2 ans !

Discussion nouvelle grille budgétaire pour la future convention avec Matis et
Victor


```bash
Top 3 slowest examples (3243.19 seconds, 44.0% of total time):
Agent#available_referents_for returns agent that not already referents array
without agents
  3208.39 seconds ./spec/models/agent_spec.rb:58
Agent can create a Rdv collectif default
  18.33 seconds
./spec/features/agents/rdvs_collectifs/agent_can_create_rdv_collectif_spec.rb:25
Agent can create a Rdv with wizard create a RDV with a single_use lieu is
expected to have text "Le rendez-vous a été créé."
  16.47 seconds
./spec/features/agents/agent_can_create_rdv_with_wizard_spec.rb:118

Finished in 122 minutes 50 seconds (files took 3.65 seconds to load) 1890
examples, 0 failures
```

Ça pique fort quand même nos tests pas cool.

Victor fait un commentaire sur une de mes PR, je ne suis pas confortable avec
le ton que je perçois...

Point avec Mohamed. Il avance bien sur la migration. J'aimerais que ce soit
plus soutenu en transverse. Son travail à de la valeur pour plein de monde je
pense, pour chaque service qui doit prendre soin de ses données. Je suis un peu
surpris par le tarif, mais en même temps, ça coûte d'avoir une à deux personnes
qui connaissent l'infrastructure mise en place. C'est donc logique, et il
serait complètement légitime de mutualiser tout ça.

Quelques retouches sur une PR, et la réalisation d'une PR de nettoyage pour
supprimer de vieilles vues.

## Vendredi 15

### RDV-Solidarités

Binomage avec Nathalie sur 

- PR sur l'ajout d'un filtre par motif
- PR sur la navigation sur le futur chemin de prise de rdv publique

Discussion avec Matis à propos du flow de prise de RDV pour le passe numérique
Discussion avec Victor à propos du recrutement

Grooming de tickets.

Reprise d'une petite PR pour faire avancer les droits d'accès. Ce n'est pas
dans la feuille de route, et pourtant je suis persuadé que c'est important.
Est-ce que ça signifie que nous oublions un truc dans la feuille de route ? Que
je me trompe ?  Il y a peut-être un jeu de dépendance, de la dette technique,
des trucs qui n'apparaissent pas et qui pourtant sont un frein pour avancer.

## Jeudi 14

_Dans le train de retour à la maison_

Ma box internet ne fonctionne pas bien dans le train. Pas de 4G. Est-ce lié à
la box ou bien à mon forfait Red-y-SFR qui ne fonctionne pas bien le long du
chemin de fer ?

Je bascule sur le wifi proposé dans le train, ça marche un peu.

Parce que je me demande toujours où acheter des livres en lignes... 

> librairiesindependantes.com est le premier moteur de recherche de livres en
> France. Il fédère 16 portails de libraires indépendants, nationaux, régionaux
> ou spécialisés, soit plus de 1200 librairies implantées sur l'ensemble du
> territoire. 

[Librairies Indépendantes](https://www.librairiesindependantes.com/)

> Leslibraires.fr a pour ambition de vous permettre de retrouver sur Internet
> vos librairies préférées, leur atmosphère particulière, la qualité de leur
> service et de leurs conseils, leur choix et leurs spécificités.

[Les librairies](https://www.leslibraires.fr/)

[Place des libraires](https://www.placedeslibraires.fr)

> La librairie en ligne qui défend les libraires indépendants
[La Librairie](https://www.lalibrairie.com/)

> Les Libraires ENSEMBLE, un réseau national de librairies multi-spécialistes
> indépendantes. Voilà un groupement qui porte vraiment bien son nom : être
> vraiment libraire et ne pas être isolé dans son magasin.

[Les libraires ensemble](https://www.libraires-ensemble.com)

> Depuis 1997, l’association Initiales rassemble des libraires indépendants
> partageant la même passion de leur métier. Par leurs actions communes, ils
> souhaitent réaffirmer le rôle essentiel du livre comme outil de connaissance,
> de réflexion et de liberté et œuvrer collectivement pour la diversité
> éditoriale et la liberté de toutes les lectures.

[Initiales](https://www.initiales.org)

> Livre Rare Book, depuis plus de 20 ans ! Le site des librairies spécialisées
> dans le livre d’occasion, ancien et moderne. 

[Livre rare book](https://www.livre-rare-book.com/)

> Premier vendeur français engagé de livres d’occasion en ligne, RecycLivre
> collecte, rachète et vend des livres d’occasion en Europe. Depuis sa création
> en 2008, RecycLivre a construit son ADN autour de 3 piliers pour lutter en
> faveur de la planète et de l’humain : l’environnement, le social et le
> sociétal. RecycLivre a pour mission de donner une seconde vie aux livres afin
> de réduire l’impact sur l’environnement et favoriser l’accès à la culture. De
> ce fait, ce qui ne peut être vendu est donné, ce qui ne peut être ni vendu ni
> donné est recyclé en pâte à papier en France. 

[Recyclivre](https://www.recyclivre.com/)

[Canal BD](https://www.canalbd.net/canal-bd)

[Original Comics](https://www.originalcomics.fr/)

[Librairies Sorcières](https://www.librairies-sorcieres.fr/)


## Mercredi 13

### Descodeuses

J'anime aujourd'hui une rétrospective pour la promo parisienne de
développeuses. C'est amusant de les voir en vraie. C'est quand même vachement
plus intéressant. J'apprécie les formations en présentiel...


### Ada tech school

Notes rapide, hétérogénéité, difficulté de recrutement, positionnement
encadrante, format, exercice, support.

Mauvaise compréhension des projets, peut-être faire plutôt des exerices ?


### RDV-Solidarités 

Première passe de travaux sur la convention avec Matis

Discussion avec Victor à propos des trucs sur les DPD et la publication des
données. Il y a de la documentation à rassembler pour éviter
ce genre de chose.

Nous avons aussi à nous accorder sur les « orientations » de solutions. Surtout
à propos de la personnalisation.

J'ajoute des éléments de documentation à propos du RGPD


## Mardi 12

_Dans les locaux de l'ANCT chez Pix, à Ourcq_

### RDV-Solidarités

Atelier avec Amélie, Myriam et Victor autour de la feuille de route, qui en
fait a permis de travailler sur les feuilles de route. Vraiment chouette.

Rétro qui a permis de présenter les travaux du matin.

Réunion avec Matis pour écouter les besoins d'une association
d'insertion/accompagnement des Côtes-d'Armor. À la fois intéressant et ça
questionne le fait de prendre du temps sur ces sujets.

Binômage avec Nathalie. C'est vraiment chouette de prendre le temps de bien
développer, et de prendre le temps de transmettre.

Debrief avec Victor. C'est chouette de pouvoir prendre ce temps en présentiel,
de prendre le temps de clarifier certaines choses, de revenir sur des échanges
qui auraient pu être mal compris.

## Lundi 11

_Finalement, je vais rester chez Hélène et Fred, prolonger le week-end ici. Je
ferais des allé-retour comme avant, en bon banlieusard mardi et mercredi. Puis
retour à la maison prévue jeudi._

### RDV-Solidarités

Analyse autour du [ticket sur la reinitialisation d'un RDV
annulé](https://github.com/betagouv/rdv-solidarites.fr/issues/2538). Après une
discussion avec Nathalie et Victor, nous optons pour l'option où l'on renvoie
une notification, et puis nous verrons bien. Il y a sans doute une analyse à
faire pour savoir pourquoi il y a des erreurs lors des changements de statut
des RDV. Une analyse UX et un peu de design peut-être ? À voir avec Myriam et
Nesserine.

Binomage avec Nathalie sur la PR, puis reprise des PR sur l'interface usager
pour simplifier la prise de RDV.

Reprise des éléments de budget pour le publié non pas sur le site, mais sur la
fiche beta. La PR initialisé a été supprimer.

Petit raté au moment de parler des tickets en attente. Nous avons inclu celui
de Victor. Nous avions évoqué le fait que si une PR n'a pas l'automerge,
l'auteur souhaite faire le merge et la présentation à Nesserine et Myriam
lui/elle même.

Ce petit raté m'a interrogé sur la « propriété collective du code ». Rien de
bien gênant.

Reprise de la PR sur l'envoie de notification au moment de revenir d'un statut
annulé.

## Samedi 9

[Who said classical music is boring?! Share this playlist with your friends and show them that classical music rocks!](https://www.youtube.com/watch?v=hLqe9v7GDfI)

Tracklist:

- Jenkins - Concerto Grosso for Strings "Palladio": I. Allegretto 0:02:33
Haydn - Die Worte des Erlösers am Kreuze, Hob. XX:1: IX. Il Terremoto 0:04:18
Litvinovsky - Suite for Strings "Le Grand Cahier": X. L'Incendie 0:06:38
Tchaikovsky - Swan Lake, Op. 20: Act III, No. 21 Spanish Dance Metamorphose
String Orchestra, Pavel Lyubomudrov

- Vivaldi - The Four Seasons, Concerto No. 2 in G minor, RV 315 "Summer":
III. Presto  0:11:30 Vivaldi - The Four Seasons, Concerto No. 4 in F minor, RV
297 "Winter": I. Allegro non molto 0:14:47 Vivaldi - The Four Seasons, Concerto
No. 4 in F minor, RV 297 "Winter": III. Allegro Metamorphose String Orchestra,
Pavel Lyubomudrov Violin: Yuliya Lebedenko

- Händel - Suite No. 11 in D Minor, HWV 437: III. Sarabande 0:20:41
Prokofiev - Suite No. 1 from Romeo and Juliet, Op. 64bis: No. 6, Death of
Tybalt 0:25:13 Mussorgsky - Night on Bald Mountain 0:36:29 Tchaikovsky - Swan
Lake, Op. 20: Scene by a Lake Metamorphose String Orchestra, Pavel Lyubomudrov

- Saint-Saëns - The Carnival of the Animals, R. 125: VII. Aquarium (Live)
Orchestra da Camera Fiorentina, Giuseppe Lanzetta

- Tchaikovsky - The Nutcracker Suite, Op. 71a: No. 2b, Dance of the Sugar
Plum Fairy Piano: Luke Faulkner

- Rossini - Il Barbiere di Siviglia: "Cavatina di Figaro" Mauro Bonfanti,
Filippo Neri

- Mozart - Piano Sonata No. 11 in A Major, K. 331: III. Alla turca Piano:
Luke Faulkner

- Tchaikovsky - The Seasons, Op. 37a: No. 2, February. The Carnival

- Chopin - Waltzes, Op. 64: No. 1 in D-Flat Major "Minute Waltz" Piano:
Vadim Chaimovich

- Chopin - Fantaisie impromptu in C-Sharp Minor, Op. 66 1:02:19 Liszt -
Grandes études de Paganini, S. 141: No. 3, Allegretto "La campanella" Piano:
Rogerio Tutti

- Prokofiev - Ten Pieces for Piano from the Ballet “Romeo and Juliet”:
No. 6, Montagues and Capulets (Dance of the Knights) Piano: Luke Faulkner

- Liszt - Mephisto Waltz No. 1, S. 514 Piano: Rogerio Tutti

- Orff/Killmayer - Carmina Burana: O Fortuna (Live) Orquesta Reino de
Aragón, Coro Amici Musicae, Igor Tantos

- Mozart - Requiem, K. 626: 1:26:50 Introitus. Requiem 1:32:14 Kyrie 1:34:55
Sequentia. Dies Irae Orchestra da Camera Fiorentina, Harmonia Cantata, Giuseppe
Lanzetta

- Verdi - Requiem: 2a. Dies Irae Orquesta Reino de Aragón, Jose Antonio
Sainz de Alfaro

- Beethoven - Symphony No. 3 in E-Flat Major, Op. 55 "Eroica": I. Allegro
con brio (Live) Metamorphose String Orchestra, Pavel Lyubomudrov

- Mendelssohn - Symphony No. 4 in A major, Op. 90 "Italian": IV.
Saltarello. Presto Orchestra da Camera Fiorentina, Giuseppe Lanzetta

- Schubert (arr. Mahler) - String Quartet No. 14 in D Minor, D. 810
"Death and the Maiden": IV. Presto - Prestissimo Metamorphose String Orchestra,
Pavel Lyubomudrov

- Dvořák - Symphony No. 9 in E Minor, Op. 95 "From the New World": IV.
Allegro con fuoco Orquesta Reino de Aragón, Ricardo Casero


### Notes

Reprise des journaux qui traine sur Elsif. Il y a un sacré boulot de nettoyage
et de publication.

### Tech

Je me demande si je vais pas retenter [NeoVim](https://neovim.io/)

### Rookie club

Rookie club, discussion avec Andy et Georges de
l'[AGECA](https://www.ageca.org/) (et petit fablab de paris).

Le rookie club, c'est 

- dojo de dev
- forum ouvert
- contexte d'apprentissage (spectacle, journal, pédago constructiviste)

Documenter le format du rookie club et les zones ouvertes (github, ... ?)

> Décomposer c'est comprendre
>
> -- Georges

Encore un déroulé de rookieclub les pieds dans le code.  Décomposition ;
Lecture de code (mots, bloc) Utilisation de LearnXinYMinutes et construction
graphique de mon schéma de pensée.


## Vendredi 8

### RDV-Solidarités

2 sessions de travail avec Nathalie sur

- pr avancer les insructions dans la prise de RDV usager
- ajout du filtre sur un motif dans la liste des RDV
- reinitialisation du formulaire de recherche de RDV
- jeter du cote d'exploration sur la résolution d'un pb UX : ne pas préremplir
  la liste des agents pour inciter à utiliser la recherche plutôt que la liste.

Mais pour des petites orga, c'est mieux la liste... Ce n'est pas un problème
grave, alors on le repousse à plus tard.

Point de synchro avec Mohamed. Il semble ok pour que l'on fasse un
rapprochement avec l'équipe ops transverse. Au moins pour qu'il ait des
collègues.  Nous avons envisager de basculer sur leur offre Scaleway, mais elle
n'est certifié que niveau 2, une régression vis-à-vis de notre situation
actuelle avec Scalingo.

Discussion avec Vincent. Ops transverse de l'ANCT, Chatons territoriaux et
quelques news de Scopyleft.  **Je dois lui envoyer ma clef SSH publique pour
accéder au serveur de collectivite.org** Je découvre des ouitls intéressant à
mettre en place chez un Chatons

- [Grist](https://github.com/gristlabs/grist-core/) un tableau / airtable en
  ligne
- [Cloudron](https://www.cloudron.io/) un peu à la Yunohost, pour faciliter
  l'installation d'application sur un serveur. Est-ce que c'est ce genre
d'outil de base que l'on peut utiliser pour lancer un chaton ?
- [PrettyNoemieCMS](https://framalibre.org/content/prettynoemiecms) un petit
  rappel, c'est l'outil qui était utilisé par Framapage pour faire de simple
site.


## Jeudi 7

### RDV-Solidarités

Échange avec Amélie par rapport à RDV-S, l'avenir, les orientation,
l’organisation de l'équipe, le budget. Très bien.

Forum ouvert BetaGouv à la DITP.

Discussion sur le Budget : commencé par publier le budget total, le budget par
bon de commande / année, ... Les incréments de valeurs seront détaillé dans une
page de la doc (?)

Ajoute d'une page budget pour RDV-S


Venir chercher les infos sur les emails bloqué pour faire un affichage dans
l'application Test en prod ? Et si on faisait en sorte que la config puisse
être copié en démo ?

## Mercredi 6

### RDV-Solidarités

Livraison en prod au petit matin. Tout va bien. Mais la stratégie de « toucher
» chaque usager et chaque agent n'était vraiment pas bonne à faire passer dans
une migration. Nous commençons a avoir trop de données pour jouer à ça.

Discussion avec Myriam à propos de la lettre d'information et des animations
que nous pourrions organiser pour les référentes.

Grooming PR sur la création de droits d'accès sur le système d'invitation
actuel, et correction sur le cible.

Binomage avec Victor sur la publication d'une url webcal pour faciliter
l'interconnexion agenda.

### Terre de liens

Accompagnement compta paysan ?  Agriculture paysanne 22 pour accompagner
l'humain dans les projets collectifs paysans

## Mardi 5

### RDV-Solidarités

Réunion référente ; Synchronisation avec Scopyleft ; Rétrospective ;
MobProgramming avec Nathalie ; Revue des tickets en attentes avec Nessrine,
Nathalie et Myriam ; Point avec Data.insertion.  Tout s’enchaîne très vite.
C'est plus calme quand je suis à la maison ? Pas sur. Je constate que je reste
à la boutique par contre. C'est plus simple que de me déplacer.

### Terre de Liens

CA TDL BZH

## Lundi 4

_La matinée dans le TGV pour Paris_

### RDV-Solidarités

Pas facile de faire des travaux d'adminitration ou de grooming avec une
connexion pas stable. J'aurais du m'organiser pour travailler sur une ou deux
grosses portion de code. Cela aurait été bien plus approprié.

Grooming ; Revue de PR ; Nettoyage des PR en attentent ; fusion et correction.

Session mobProg avec Nathalie. On reprend la PR sur l'union des parcours
usager. Ce que nous allons essayé de faire mieux

- changer de pilote plus souvent
- prendre des notes dans un journal (?)


## Dimanche 3

Je manque d'occupation ? Je suis à Six-fours, c'est chouette, mais que faire le
week-end ici ? Je pourrais faire le jardin de ma mère un peu... Ou des balades.
Mais l'été il fait trop chaud par ici. Je suis heureux d'être en Bretagne \o/

---

À lire ? Florent Bussy a notamment publié [William Morris ou la vie belle et
créatrice (Éditions Le passager clandestin,
2018)](http://www.journaldumauss.net/?William-Morris-penseur-et-artisan-de-l-autonomie-et-de-la-beaute)

J'ai déjà un livre de lui à propos de la main et la tête que j'ai trouvé
fabuleux.

> William Morris en est le principal inspirateur, parce qu’il s’est efforcé, à
> la fois, de poser les bases théoriques d’une rupture avec le processus
> industriel de production (et ses conséquences sur les travailleurs et les
> produits fabriqués), et de la réaliser pratiquement dans la création
> artisanale, l’entrepreneuriat et le militantisme politique.

> Durant ses études, il se tourne vers l’architecture et les arts, il est
> proche de peintres préraphaélites comme Burnes-Jones et Rossetti, et, très
> vite, décide, sous l’influence de John Ruskin qui reproche à l’époque moderne
> d’avoir transformé l’artisan en ouvrier, de créer, grâce à la fortune
> familiale, une entreprise pour produire des objets simples, beaux et solides,
> dans le domaine de l’ameublement, de la tapisserie et de la céramique.

> Morris adopte une démarche à la fois sociale et esthétique. S’inscrivant dans
> la sensibilité philanthropique naissante, dans son entreprise, les employés
> étaient mieux payés et travaillaient dans de meilleures conditions
> qu’ailleurs, les jeunes bénéficiaient d’un apprentissage, le travail des
> enfants était banni. Les objets produits devaient être beaux et durables, par
> opposition aux productions de piètre qualité de l’industrie en développement,
> et pouvoir toucher toutes les classes sociales, en étant concurrentiels,
> grâce à des prix abordables.

> C’est en tant que conférencier qu’il se fit connaître dans le monde ouvrier,
> il participa à plus de 500 meetings dans les vingt dernières années de sa
> vie. Il s’adressait aux travailleurs dans les usines afin de contribuer à la
> formation d’une conscience révolutionnaire et à rendre désirable une
> alternative au capitalisme en pleine expansion.

Je découvre à travers cet article la dimension social de Morris. Peut-être que
l'on en parlais dans l'autre livre, mais ça ne m'a pas marqué. À relire sans
doute.

> Le mouvement de résistance au développement du capitalisme industriel initié
> par William Morris est fondé sur plusieurs idées. 
> - Un producteur qui est autonome et qui maîtrise toutes les étapes de la
>   production est libre et heureux. 
> - Un artisan qui mobilise toutes ses facultés, parce qu’il n’est pas réduit à
>   l’état d’exécutant sans âme, produit des œuvres naturellement belles.
> - Il ne doit pas y avoir de séparation entre Beaux-Arts et artisanat, entre
>   arts nobles et arts décoratifs. 
> - L’art doit être présent dans tous les objets du quotidien, même les plus
>   banals.
> - Un objet ne doit pas être seulement utile, mais beau, la beauté ne se
>   rajoute pas à la fin de sa production, mais est intrinsèque à sa
>   fabrication, à sa forme, son usage.


> L’art est ce qui donne un sens humain au travail. Il est même, selon la
> version morrissienne du socialisme, « une nécessité de la vie humaine »,« la
> finalité de la vie », parce qu’il permet, seul, « l’emploi agréable de nos
> énergies ».

> L’authentique, ce qui a été créé avec plaisir par les mains du
> travailleur-créateur, est remplacé par des produits fabriqués par des corps
> esclaves de machine. 

> Dans notre société de consommation, le travailleur qui réalise souvent une
> tâche mécanique, sans initiative, devient un consommateur sollicité
> constamment par la publicité et gavé d’objets inutiles, standardisés et de
> piètre qualité.

### Plantes

Malgré la distance à mon jardin, j'ai enfin pris le temps de retrouver le nom
d'une plante que dont on m'a donné un pied : le Rau Ram, sorte de « coriande
Vietnamienne ».

> Noms latins : Polygonum odoratum, Persicaria odorata Noms communs : Rau ram,
> coriandre vietnamienne Famille : Polygonacées Type : Plante aromatique
> 
> Hauteur : 50 à 70 cm Distance de plantation : 50 cm Exposition : Soleil Sol :
> Riche, humide, mais bien drainé Rusticité : Non‑rustique

## Samedi 2

Ticket ou PR dans le tableau ?  Support utilisateur (pour le plaisir ?) PR sur
le tri des usagers et agents.  Est-ce une journée de travail ou pas ? Non.

### Terre de liens

Finalisation (enfin) de la migration du site pour Terre de lien Bretagne.
https://tdlbzh-wp.kaz.bzh/ Reste la redirection à effectuer et les contenus à
mettre en place.

Presque une année pour accompagner cette mise en ligne d'un wordpress.  Je suis

- heureux d'avoir rencontrer les personnes de l'association Defi ;
- heureux d'avoir rencontrer les personnes de l'association kaz.bzh.

J'ai par contre vraiment le sentiment qu'un site avec Wordpress fait parti du
passé. Quitte à devoir « former » des personnes à l'utilisation de wordpress,
pourquoi ne pas les former à Markdown ? Voir à l'utilisation de Github/Gitlab
de manière basique, pour de l'édition de contenu ?

Et pourquoi pas un wiki ?

Il y a des explorations que j'aimerais mener.

## Vendredi 1er

_Depuis Six-fours-les-plages_

### Tech

Dans le dépilage nocturne, j'essaie [kitty](https://sw.kovidgoyal.net/kitty/)

> The fast, feature-rich, GPU based terminal emulator

Je ne suis pas sur de comprendre ce que ça m'apporte.

> - Offloads rendering to the GPU for lower system load
> - Uses threaded rendering for absolutely minimal latency
> - Performance tradeoffs can be tuned

Je crois que ça réponds à ma question :)

### RDV-Solidarités

Binomage avec Nathalie pour 

- reprendre la PR sur la modif de RDV dans le cas de lieu ponctuel
- traiter les messages de zammad
- analyser le problème de prod de jeudi et ce matin
- voir la PR sur l'avancement de l'instruction lié au choix d'un motif qui nous
  amène à plutôt réaliser la PR suivante
- amorcer la PR sur la refonte de l'accueil usager.

Ajouter dans la PR un truc à propos de ne plus transférer l'adresse usager
saisi dans son adresse de fiche usager coté usager Lieux_path dans la
bifurcation invitation ou pas

Revue de PR de Victor.

