---
title: Mardi 22 mars 2022
---

> Le philosophe Byung-Chul Han l’explique très bien : le stade avancé du pouvoir libéral actuel, c’est de permettre aux gens de maximiser leur auto-aliénation.

>  On crée des systèmes qui échappent complètement à l’être humain, que personne n’arrive à maîtriser. C’est une dépossession. Une fois que ça a bien purulé et bien enflé, qu’on est noyé sous tout ça, intervient le mythe. Les mythes sont des symptômes.

> Le Comité invisible l’a très bien analysé : ce capitalisme n’a plus vocation et n’a plus envie de gérer la totalité de la société, il a abandonné cette idée. Il gère des poches de profit, et les autres se démerdent.

> C’est pareil dans l’éducation ou dans la santé : on va gérer celle des élites et des riches, et le reste, on s’en fout. 

> Aujourd’hui, on est en « démocrature » – c’est-à-dire avec des éléments de démocratie et des éléments de dictature, mais globalement plutôt en démocratie – et dans un système où tous les pouvoirs sont disséminés, dans un réseau, un maillage : on ne peut pas retourner ça comme une crêpe. Dans un régime aussi intégré, aussi cybernétique et aussi corrélé, il faut plutôt des îlots.

>  le capitalisme est comme de l’eau, qui s’insère dans tous les interstices. Dès qu’il y a une petite pente ou un défaut de gravité, cela vient tout remplir. Il y a un côté extrêmement liquide dans le fonctionnement du capitalisme qu’a bien montré Deleuze, ce côté déterritorialisé, dans lequel on patauge, dans lequel on se noie tous à moitié. Il faut plonger, fendre un peu la croûte terrestre pour faire remonter le magma, pour faire émerger un îlot. C’est un processus qui nécessite beaucoup d’efforts, et tout d’un coup tu fais sortir une énergie, une chaleur et ça remonte à la surface… Il y aura un îlot, puis un autre îlot, puis un archipel, et peut-être un pays. C’était un peu ça, la ZAD. Il y avait tellement peu de choses avant l’émergence de la ZAD que nous sommes tous allés là-bas et c’est devenu une plateforme.
>
> Il faut surtout garder cette idée de pluralité, parce qu’il n’y aura pas une révolution unique. On ne va pas réinventer une pensée unique qui va structurer le monde. Chaque fois qu’on a voulu le faire, cela a abouti à une catastrophe… L’homme est pluriel, il faut permettre à tous ces archipels d’exister. C’est la seule manière de retourner progressivement le capitalisme et d’amener autre chose. Et le capitalisme finira par s’effondrer, c’est un système extrêmement récent, très absurde même s’il a beaucoup de capacités de résilience.


https://basta.media/Alain-Damasio-Les-Furtifs-La-Volte-ultra-liberalisme-ZAD-pouvoir-alienation
