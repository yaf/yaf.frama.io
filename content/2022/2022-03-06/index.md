---
title: Dimanche 6 mars 2022
---

À lire ? [Décolonialité & Privilège Devenir complice](https://editionsdaronnes.fr/product/decolonialite-privilege/)

À lire ? [L’Empire qui ne veut pas mourir](https://www.contretemps.eu/histoire-francafrique-empire/)

À lire ? [Les nouveaux biens communs ? Réinventer l’Etat et la propriété au XXIe siècle, par Edouard Jourdain et Emmanuel Dupont](https://www.les-communs-dabord.org/parution-les-nouveaux-biens-communs-reinventer-letat-et-la-propriete-au-xxie-siecle-par-edouard-jourdain-et-emmanuel-dupont-janvier-2022/)
---

Un shell en lisp ? Original ! Peut-être un moyen de se mettre à Lisp ?

https://github.com/naver/lispe/wiki/7.-Comme-Shell

Est-ce qu'en faisant ça on se retrouve sur Emacs ensuite ?


---

J'avais fait une expérimentation assez intéressante avec DataTransition. Pour faire les specs de l'outils, nous écrivions une histoire.
En mettant en place un certain formalisme, j'arrivais à retrouver ce qu'il fallait faire pour avancer.

Et si j'essayais avec RDV-Solidarités ?

Le fait que j'utilise l'expression `À lire ?` pour référencer des livres dans mes journaux m'y fait pas mal penser...
