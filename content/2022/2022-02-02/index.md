---
title: Mercredi 2 février 2022
---

Yannick, [02/02/2022 20:39]
Peut-être entre le 8 et le 15... Je vais être en « congé » ; c'est la période de vacances scolaire pour la Bretagne, je vais aller déposer les enfants chez une belle sœur à Troyes, et plutôt que de rentrer en Bretagne et de retourner les chercher après, je vais laisser mon véhicule et allez sur Paris. Je vais donc travailler, mais dans un environnement qui ne sera pas celui habituel et sans enfants :)

Yannick, [02/02/2022 20:39]
J'ai appris aujourd'hui que je ne suis pas « retenu » dans la CAE qui m'intéressais dans le Finistère, sous pretexte que je suis dans le Morbihan.

Yannick, [02/02/2022 20:40]
Je suis un peu triste, déçu.  Ça a été ma première réaction.

Yannick, [02/02/2022 20:40]
Et puis je me dis que c'est l'occasion de réfléchir à pourquoi je voulais aller chez eux ? Pourquoi je suis triste ? Et si au contraire, c'était une opportunité de me poser la question et de faire un truc qui me branche vraiment ?

Yannick, [02/02/2022 20:41]
Et me viens à l'esprit 2 choses que j'aimerais : Soit monter un collectif local, avec une forte identité centre bretagne

Yannick, [02/02/2022 20:41]
(parce que bon, la CAE du Finistère est basé à Quimper, proche de la cote, et ils et elles m'ont invité à aller plutôt à la CAE de Lorient (qui a mauvaise réputation, c'est pour ça que j'ai pas envie d'y aller)... Alors que bon, je suis plutôt centre bretagne)

Yannick, [02/02/2022 20:43]
Il y a d'ailleurs une bataille (guerre ?) à venir (déjà en cours ?) sur l'identité du centre bretagne : est-ce que les bled comme le Faouët, Carhaix, Pontivy doivent être rattaché à une grosse ville de la cote (Lorient, Morlaix et Saint-Brieux respectivement sur les exemples prit) ? Ou bien est-ce que le centre bretagne peut se créer une identité propre et ne pas dépendre d'un rapprochement cotier ?

Yannick, [02/02/2022 20:43]
Créer une structure pourrait ajouter un petit caillou à cette édifice :)

Yannick, [02/02/2022 20:44]
L'autre option que j'envisagerais c'est de monter une structure d'insertion, potentiellement national, ayant pour vocation de permettre aux personnes en reconversion de construire les 3 années d'expérience qu'ils et elles ont souvent du mal à obtenir.

Yannick, [02/02/2022 20:44]
Ça serait un format ESN au niveau commercial

Yannick, [02/02/2022 20:45]
ou bien boite de recrutement peut-être.. Avec un modèle hybride. En prenant le schéma qui a déjà lieu, mais en le rendant clair, explicite : vous prenez une personne junior en mission, et si c'est ok, au bout de 2 ans ou 3, vous pouvez la recruter moyennant un peu d'argent pour nous (en plus de l'argent sur la mission).

Yannick, [02/02/2022 20:46]
J'imagine qu'il faudrait travailler deux axes : commercial pour trouver des missions, accompagnement pour faire en sorte que les recrues progresse, ne soit pas seules et en difficultés...

Yannick, [02/02/2022 20:46]
Les deux axes me branches bien.

Yannick, [02/02/2022 20:46]
(*structures)

Yannick, [02/02/2022 20:47]
Les deux options ne semblent pas compatible ensemble : pourquoi faire une structure d'insertion pour les personnes en reconversion dans le cœur Bretagne ? Il n'y a pas beaucoup de client ici ni de personnes en reconversion ...

Yannick, [02/02/2022 20:47]
Mais j'ai moyennement le courage de me lancer dans l'aventure.

Yannick, [02/02/2022 20:48]
Une option serait sans doute de rencontrer des personnes qui aurait envie de faire l'une ou l'autres avec moi, histoire de m'aider.

Yannick, [02/02/2022 20:48]
Sachant qu'au fond, moi j'ai envie de continuer à travailler sur RDV-Solidarités :D

Yannick, [02/02/2022 20:48]
alors une 3eme options est présente aussi : trouver un comptable et devenir un indépendant, vraiment seul sur l'aspect administratif... après tout, pourquoi pas.

Yannick, [02/02/2022 20:49]
(Et je me rends compte que notre échanges redevient une sorte de journal pour moi... Je vais copier coller ce que je viens de dire dans mon journal d'ailleurs)... Merci d'accueillir mes réflexions




