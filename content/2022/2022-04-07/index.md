---
title: Jeudi 7 avril 2022
---

> Voter, c'est abdiquer ; nommer un ou plusieurs maîtres pour une période courte ou longue, c'est renoncer à sa propre souveraineté.


> Vous nommez des hommes qui sont au-dessus des lois, puisqu'ils se chargent de les rédiger et que leur mission est de vous faire obéir. 


http://www.homme-moderne.org/textes/classics/ereclus/jgrave.html


---

https://mystery.knightlab.com/

> There's been a Murder in SQL City! The SQL Murder Mystery is designed to be both a self-directed lesson to learn SQL concepts and commands and a fun game for experienced SQL users to solve an intriguing crime. 

---

https://petitesruches.fr/spip.php?article20


