---
title: Lundi 4 avril 2022
---

UNE POTION ANCESTRALE DANS UN MONDE MODERNE : L’EAU D’ORTIE...
Se faire du bien actuellement, facilement et à tous les âges, en vous préparant une eau infusée d’ortie fraiche (Urtica dioica)...
Cette boisson est appréciée par sa simplicité, et en fonction des terroirs pour son subtil goût de banane.
C’est surtout un breuvage naturel,  souverain  pour accompagner ce monde d’aujourd’hui, en amenant une prise en charge « médicinale » dont on ne soupçonne pas l’ampleur...
Pour fabriquer l’eau d’orties, le printemps est un moment idéal pour la cueillir.
En plus de la puissance caractéristique des jeunes pousses, elle contient une forte concentration d’éléments actifs:
Des vitamines à foison, de la C (8 fois l’orange), A, E, K, B1, du zinc, de la silice bio assimilable, de la chlorophylle, du fer, du magnésium , du calcium, du phosphore, des antioxydants et j’en passe...!
C’est la potion idéale en cette époque, pour nettoyer en douceur son corps, renforcer son système immunitaire, alcaliniser son corps et retrouver de la souplesse dans les articulations...
LA PRÉPARATION :
Trouver des jeunes pousses d’orties piquantes dans un endroit préservé... car l’ortie absorbe les pollutions du sol.
Le bon sens vous guidera.
Cueillir les parties terminales( feuilles du haut) et en remplir un bocal.
Vous pourrez les rincer rapidement dans une eau citronnée ou vinaigrée.
Recouvrir d’eau froide et laisser reposer une nuit à température ambiante.
Au petit matin, c’est prêt!
On filtre et on boit...
Doucement d’abord car une vraie détoxication souterraine est en cours...1 à 2 verres par jour au début...et on écoute son corps...
On boit doucement, en conscience et sans exagération car les processus de détoxification vont se mettre gentiment en route...
Pour les plus «  encrassés », quelques effets secondaires bénins peuvent survenir :
maux de tête, diarrhée légère...😉
🎈Et n’oubliez pas sa forte proportion en fer... important de le savoir pour ceux qui en cherchent ou pour ceux qui le fuient...!
Tout l’organisme , en particulier les reins, la peau, le système osseux, les cheveux, les articulations, le sang vont profiter de ses vertus hautement reminéralisantes  et anti inflammatoires...
🎈Cette eau se garde 2 jours au frigo, pas plus!
Après, sa grande richesse en enzymes va la transformer inexorablement en purin...!
On la prépare donc chaque jour et les orties utilisées sont remises à la terre, pour le plus grand bien de cette dernière...🌳
Ah j’oubliais...la boisson est tonique...!
Elle n’est pas recommandée le soir, sauf si vous allez faire la fête...🍭

🎈les hypertendus devront consulter leur médecin avant d’en boire des quantités!
L’eau d’orties est une proposition facile à faire, adaptée à notre époque troublée  et indépendante de l’inflation! 😊
Si cette boisson vous convient, faites-en des cures régulières( il y a de l’ortie presque toute l’année)
Mère Nature vous prendra alors dans ses bras bienveillants et vous partagera ses secrets...✨🧙‍♀️🌎🍃


---

Retour avec un bureau debout. Je crois que j'ai un peu perdu l'habitude, et j'ai eu des petites douleurs en bas du dos. À voir si ça persiste. Mais globalement je suis super contenet d'être de retour comme ça.

Pour faire mieux encore, il me manque : 
- une base pour poser le portable
- une webcam externe (pour ne plus à avoir à ouvrir le portable)
- un bon micro externe
- des petites enceintes
- un tabouret haut pour me poser un peu parfois (surtout le soir quand je reviens sur l'ordi).

Peut-être une note intéressante, c'est qu'après une journée debout, je vais peut-être reutiliser mes soirées pour de la lecture de livre ? Ou je pourrais aussi me reinstallé un vieux portable sous openbsd pour me faire plaisir le soir devant le poêle ?

À suivre.

---

À propos de prix libre

> Mettre une boîte à l’entrée ou à la sortie, faire payer sa part par internet devant un écran et non un humain ça ne fonctionne pas / pas bien de mon expérience. Lors de ma première expérience d’étudiant ou je demandais une participation libre pour un service d’hébergement de site internet, par formulaire internet, il était très fréquent d’avoir des clients à 0,01€. Je suis persuadé que ces mêmes personnes, si elles n’avaient pas eu affaire à un clavier mais à une personne, ne se seraient pas permis de verser si peu pour un service/travail. Prendre 1 minute pour recevoir la contribution, en face à face et en main propre, ça fonctionne bien mieux. En effet, la personne est face à ses responsabilités / ses choix (sans pour autant avoir à se justifier).

https://david.mercereau.info/ma-pratique-du-prix-libre-et-conscient/

---

Note de lecture [La taille des arbres libres]() par Alain Pontoppidan

> « Qu'est-ce que la vie ? » demandait le chef Crowfoot, chef de la tribu des Blackfeet
> « C'est l'éclat d'une luciole dans la nuit. C'est le souffle d'un bison en hiver. C'est la petite ombre qui court dans l'herbe, et se perd au coucher du soleil. »
>
> Ce livre est un livre technique, dont le sujet est la taille des arbres. Il y sera expliqué en détail ce qu'il faut couper, comment, pourquoi. Mais c'est aussi un livre où il sera fait grand cas de la beauté. Tant de techniques de taille font l'impasse sur cette valeur essentielle.


> Colonisation
>
> L'arbre n'est pas unitaire, mais, selon l'expression de FRancis Hallé, un être coloniaire. C'est une colonie d'arbres empilés, emboîtés les uns sur les autres. Tout en étant reliées et coordonées, les individus de cette colonie sont relativement indépendants les uns par rapport aux autres.


> Leadership
>
> Le leader (bougeons leader), qui contrôle le bon développement de la pousse de l'année, n'a pas chez tous les arbres le même ascendant. Il est, chez certaines espèces, particulièrement dominant, tandis que chez d'atures, il est beaucoup plus libéral.

> Tailler sans déséquilibrer
>
> Les leaders jouent un rôle important dans le développemetn rythmique des ramifications. Il y en a à chacune des extrémités en croissance, et on peut regarder un grand arbre, avec ses multioples famifications, comme une structure régulée par une assemblée de bourgeons leader, gérant chacun leur portion de rameaux. Les leaders sont complémentaires de la dominances apicale : ils régulent la forme globale de l'arbre.
>
> La taille doit en tenir compte. « Le sécateur attire la sève », disent les arboriculteurs, signifiant par-là que la désorganisation du système de régulation des arbres provoque l'apparition de novuelles pousses. En particulier les tailles drastiques, qui se traduisent par l'émergence désordonnée d'une multitude de pousses nouvelles, très vigoureuses, et très abusivement qualifiées de « gourmands », qui sont supposés « voler » la sève au détriment des autres branches. Ils ne volent rien du tout, ils sont la meilleure réponse possible de l'arbre aux ravages qu'il vient de subir, le moyen de compenser le plus rapidement possible sa perte de surface foliaire. Ce sont de novuelles unités architecturales, des réitérations qui reconstruisent la couronne endommagée. Elles fabriquent leur propre sève élaborée, et ne prennent pas celles des voisines.
> **Une taille conforme à la physiologie de l'arbre, à son fonctionnement naturel, ne provoque pas l'apparition de réitérations traumatiques. Elle ne nuit pas à l'équilibre de la couronne, et conserve à l'arbre la forme naturelle qui lui est propre.**

> Les modalités de floraison sont très diverses. Elles peuvent toutefois être réparties en trois grandes catégories, d'après la position du bourgeon par rapport à la pousse.
> - les espèces qui fleurissent directemetn sur la pousse de l'année (vigne, kaki, mûrier, ...)
> - les espèces qui fleurissent à partir des bourgeoins situés sur la pousse de l'année précédente (prunier, pêcher, cerisier, abricotier, groseilliers et cassissiers)
> - les espèces comme le pommier ou le poirier dont les fleurs se forment à l'extré"mité des ramifications de deux ans.


