---
title: Lundi 28 mars 2022
---

[Avicenne](https://fr.wikipedia.org/wiki/Avicenne)

À lire ? Canon de la médecine. Al-Qânûn fi'l-Tibb (vers 1020) : pas de trad. fr. Trad. latine : Liber Canonis, Venise, 1515, 1520-1522, 1555. Traduction anglaise par Oskar Cameron Grüner : The Canon of Medicine of Avicenna (1930), livre I, AMS Press, New York, 1973, 612 p. Voir P. Mazliak, Avicenne et Averroès. Médecine et Biologie dans la civilisation de l'Islam, Vuibert/Adapt, 2004, 250 p. Qanûn (Avicenne) https://fr.wikipedia.org/wiki/Qan%C3%BBn_(Avicenne)
À lire ? Divisions des sciences intellectuelles, trad. G. C. Anawati, "Les divisions des sciences intellectuelles d'Avicenne", Mélanges de l'Institut dominicain d'études orientales, t. 13, Le Caire, 1977.
À lire ? Notes d'Avicenne sur la Théologie d'Aristote, Georges Vajda, Revue Thomiste, 51, 1951, p. 346-406.
À lire ? Poème de la médecine (Urguza fi't tibb), trad. Henri Jahier et al., Les Belles Lettres, 1956, 209 p. (condensé en 1326 vers du Canon de la médecine)


Ibn al-Baytar

https://fr.abcdef.wiki/wiki/Ibn_al-Baitar

https://www.persee.fr/doc/medi_0751-2708_1997_num_16_33_1392

À lire ? https://lire-demain.fr/produits/9782357590717-memoires-d-un-herboriste-andalou-ibn-baytar-savant-medecin-veterinaire-et-botaniste-en-orient-roman-historique/
i

https://www.lesclesdumoyenorient.com/D-Ibn-Baytar-a-Lucien-Leclerc-deux-honnetes-hommes-au-service-de-la.html

À lire ? Lucien Leclerc, Histoire de la médecine arabe, vol. 2, E. Leroux, 1876 (lire en ligne [archive]), p. 225-237
À lire ? Ibn al-Baytar (trad. Lucien Leclerc), Traité des simples, Institut du Monde Arabe, 1987 (1re éd. 1877, 1881, 1883) (ISBN 2906062030), 3 vol.(lire en ligne : vol. 1 [archive] – vol. 2 [archive] – vol. 3 [archive]).
À lire ? Max Meyerhof (it), « Esquisse d'histoire de la pharmacologie et de la botanique chez les musulmans d'Espagne », Al-Andalus, vol. 3,‎ 1935 (ISSN 0304-4335)


https://fr.wikipedia.org/wiki/Hildegarde_de_Bingen
À lire ? Les causes et les remèdes (Liber compositae medicinae. Causae et curae), traduction Pierre Monat, Jérôme Millon, 2005, 301 p.
