---
title: Vendredi 25 mars 2022
---

> Le développement de l’art et de la culture est profondément connecté à celui de l’éducation en général et de l’éducation populaire en particulier.
> -- Robin Renucci

http://www.educationparlart.com/2022/03/pour-l-education-populaire.html

---

> Céder/Consentir : « Céder n’est pas consentir » (NC Mathieu). Il n’y a pas de désir de soumission des opprimés, si les opprimés se soumettent c’est sous l’effet de violences (sociales, psychologiques ou physiques) qui ne sont pas toujours perceptibles. Le rôle d’une psychosociologie des opprimé-e-s est de dévoiler ces mécanismes de violences. [Voir également sur le consentement : Manon Garcia, La conversation des sexes]

> Intersectionnalité (K. Crenshaw) : L’intersectionnalité vient complexifier la psychologie de l’opprimé-e, car chacun peut être opprimé-e et en même temps privilégié socialement dans le cadre d’un autre rapport social de pouvoir.

https://lewebpedagogique.com/educationsdesoi/2022/01/24/lexique-de-psychologie-des-opprime-e-s/

---

À lire ? « Cartographie radicale : explorations » par Nepthys Zwer @nepthys et @reka de chez @visionscarto
À lire ? Le 21 novembre 2021 est paru aux Éditions de L’Éclat l’ouvrage « Le commun comme mode de production » signé par Alfonso Giuliani, Carlo Vercellone et Francesco Brancaccio.
