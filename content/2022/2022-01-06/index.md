---
title: Jeudi 6 janvier 2022
---

Comment retrouvé des références ? Il faudrait détecter que ce sont des références et les archiver comme telles ?

Je pense là à la notion de tunnel de verre que j'associe à Isabelle Collet, et que j'ai entendu dans un podcast... Peut-être dans celui-ci https://www.libreavous.org/73-les-femmes-et-l-informatique-l-installation-d-un-systeme-d-exploitation

J'ai également parlé d'articles à propos d'être dev à 30 ans, puis de l'autre article paru un peu après qui parlait d'être dev à 40 ans... Et je ne les retrouve pas :-/


---

Notes à propos du webinaire (étape 2) du Réseau Rural Breton à propos de la précarité en milieu rural.

Comment favoriser l'accès à une alimentation saine et plus durable ? Les épiceries sociales et solidaires sont une des réponses.  le jeudi 6 janvier 2022 de 10h à 12h - Webinaire


https://www.reseaurural.fr/centre-de-ressources/evenements/cycle-danimation-la-precarite-en-milieu-rural

https://andes-france.com/

Réseau Rural Breton, outil d'animation pour afficher une belle image du rural... Europe, France, Région.
Co-piloté par l'état et la région

valorisation de bonne pratique

https://fermesdavenir.org/

https://www.panierdelamer.fr/



---

Ticket pour faire apparaitre le lien d'invitation dans la page des agents

---

> On le sait, la propriété privée dans un système capitaliste et productiviste fait des ravages. Elle exacerbe la valeur marchande au détriment de la valeur d’usage. Elle permet l’accumulation sans limites écologiques ni égards de justice sociale.

> « Le premier qui, ayant enclos un terrain, s’avisa de dire : “Ceci est à moi”, et trouva des gens assez simples pour le croire, fut le vrai fondateur de la société civile, écrit Jean-Jacques Rousseau, dans le Discours sur l’origine et les fondements de l’inégalité parmi les hommes (1755). Que de crimes, que de guerres, de meurtres, que de misères et d’horreurs n’eût point épargnés au genre humain celui qui, arrachant les pieux ou comblant le fossé, eût crié à ses semblables : “Gardez-vous d’écouter cet imposteur ; vous êtes perdus, si vous oubliez que les fruits sont à tous, et que la terre n’est à personne”. »


> « Créer des lieux que personne ne possède et qui sont utiles à beaucoup »
>
> C’est la tâche à laquelle s’attelle depuis trois ans la foncière Antidote, en cherchant à neutraliser la partie la plus nocive de la propriété, l’abusus. Concrètement, si l’on prend l’exemple d’un arbre, l’usus donne le droit de dormir sous son feuillage, le fructus celui de manger ses fruits, et l’abusus le droit de le couper. Appliqué à un lieu collectif, neutraliser l’abusus revient donc à le sortir du marché afin qu’il ne puisse être vendu. Pour cela, la propriété en est confiée à un fonds de dotation, la foncière Antidote. Celle-ci, par le biais de baux emphytéotiques, va déléguer aux usagers tous les droits d’un propriétaire, sauf celui de vendre.

https://revoirleslucioles.org/habiter-sans-posseder-tel-est-lantidote/


---

https://www.foretsenvie.org/

---


