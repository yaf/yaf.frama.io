---
title: Vendredi 1er avril 2022
---

SEULS LES MEMBRES POLIS DIRONT MERCI POUR LA RECETTE 😊
Pain turc : le pain le plus délicieux et le plus facile que vous ferez jamais !
INGRÉDIENTS
500 g (1 livre) de farine;
150 ml (5 fl. oz) de lait chaud;
150 ml (5 onces liquides) d'eau tiède;
1 cuillère à café de sel;
2 cuillères à soupe de levure sèche;
3 cuillères à soupe d'huile d'olive;
persil haché, flocons de piment.
Préparation
Fouettez la farine avec le sel et la levure. Ajouter l'eau tiède et le lait et pétrir jusqu'à ce que les liquides soient absorbés.
Ajouter l'huile d'olive et pétrir jusqu'à l'obtention d'une pâte lisse (environ 10 minutes).
Façonner la pâte en boule, couvrir d'huile végétale et réserver dans un endroit chaud pendant 1 heure ou jusqu'à ce qu'elle ait doublé de volume.
Lorsque la pâte a fini de lever, coupez-la en 6 et roulez chaque morceau en boule.
Étalez chaque pain plat et placez-le sur une poêle préchauffée à feu moyen-vif. Cuire 5-6 minutes (2,5-3 minutes de chaque côté). Lorsque vous retournez chaque pain plat, il doit former une poche au milieu.
Badigeonner chaque pain plat chaud d'huile, puis garnir de persil et de flocons de piment.
