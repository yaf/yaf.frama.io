---
title: Dimanche 16 janvier 2022
---

Je ne garde pas assez de mou dans mon emploi du temps. C'est trop tendu et je n'arrive pas à glisser des choses, à faire face à des imprévus, et des options de dernières minutes.

Ça fait plaisir de revoir Max.

Nous parlons d'anachie, de logiciel libre, de communauté, d'intérêt général, de financement...


Pour s'assurer d'être égalitaire, la communauté où vie Max à un temps commun pour le ménage, et une réunion de synchro hebdomadaire pour tirer au sort qui fait la vaisselle. Je me demande comment ils et elles en sont arrivées là ? Le chemin a-t-il été long ? Quel cheminement pour arriver à ce qui me semble le plus simple à faire (et que se passe-t-il d'autres, il n'y a que ça ?)

Pourquoi chez /ut7 et chez Scopyleft, l'administratif échoue toujours dans un nombre de main réduite ? Chez /ut7, il y avait quelque chose d'assumé comme : celui ou celle que ça dérange le plus le fait (c'était peut-être celui ou celle pour qui ça importe le plus, mais ça revient un peu au même selon les sujets). C'est un peu violent vu d'ici maintenant, et on se retrouve avec toujours les mêmes personnes qui ont la même sensibilité qui font les mêmes tâches... Ce qui était intéressant c'est que c'était relativement bien documenté.

Chez Scopyleft il y avait un discours : Stéphane qui dit : moi ça me fait pas mal alors je peut le faire, et les autres dont moi qui disait : c'est relou alors si tu le fais c'est cool... Super violent aussi. Et aujourd'hui Stéphane dit : en fait c'est relou de le faire dans ces conditions. Je comprends ça. Là par contre, difficile de faire certaines choses plutôt mal documenté.

Avec le recul de cette discussion rapide de cette après midi (plus un bon moment dans la voiture à faire le trajet de retour) je perçoit les demandnes de faire ensemble autrement. J'en suis à me demande si ça ne vaudrait pas la peine de rester. Mais en fait non, la confiance que j'ai en Steph est cassée. Et puis il reste cette dimension qui ne semble pas aller vers moins d'administratif. Ou bien je me cache derrière ce truc pour partir, parce que j'ai envie de partir...

Le truc c'est sans doute de pouvoir prendre le temps de parler de tout ça. Or nous ne le prenons pas chez Scopyleft. Chez /ut7, il y a eu des échanges, et la place d'en parler. Je crois que nous aurions pu finir par trouver quelque chose...

Je me demande bien comment ça fonctionne chez Codeurs en Liberté. Et chez Multi ?

Passer par une CAE, ou le taf administratif est en grande partie déporté sur un certain nombre de personnes est un peu étrange. Est-ce que c'est un jeu de domination ? Un truc anti-auto-organisation ? Un truc anti-anarchiste ? Faut-il absoluement supprimer la spécialisation des tâches pour bien faire ?

Lister les trucs à faire pour que la boite tourne, en reparler souvent pour être sur de ne pas en oublier (rendre visible le travail). Ensuite il conviendrait de définir comment le répartir. J'aime bien ce qu'avait fait la CAE dans le nord, en valorisant ce travail. Mais ça fait peut-être un peu trop capitaliste ? Le répartir de manière équitable, ça peut fonctionner, mais ce n'est peut-être pas très cool pour les personnes qui à un moment donnée n'ont pas le temps, l'énergie ? Il faudrait pouvoir faire tourner les perm ?
