---
title: Samedi 22 janvier 2022
---

https://kpacite.initiative.place/

Je m'interroge...

> pour accompagner les habitant·es volontaires vers des activités marchandes reconnues.

> Au cœur du dispositif : l’éducation populaire à l’économie.

---

> The real-life impact of our technical decisions really hit home to me once again: my Mom had trouble volunteering and participating in her local community because somebody shipped the optional chaining operator in their production JavaScript.


https://blog.jim-nielsen.com/2022/a-web-for-all/

---

[Flowchart.fun](https://flowchart.fun/)

ou comment « écrire » un flowchart. Brillant.

https://github.com/tone-row/flowchart-fun

---

> Ce qui distingue cette machine d’une machine à laver à pédales par exemple, c’est la présence d’un filtre qui permet de réutiliser les 40 litres d’eau de la lessive pour faire la cuisine, ou irriguer un potager, par exemple ! 

https://escapethecity.life/lave-linge-low-tech-goodall-project

---

> Le petit monde de l’éducation au numérique est en train de s’élargir. Les enseignants, médiateurs, formateurs et acteurs sociaux existants, seront bientôt rejoints par les « Conseillers numériques ». Comme l’explique le site dédié du programme, « sur une durée de deux ans, l’État finance la formation et le déploiement de 4 000 conseillers numériques France Services ». Toujours selon le site, ces conseillers accompagneront les 13 millions de français qui « subissent » la transition [numérique].

https://resnumerica.org/eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/


---

Pour ne pas oublié https://helloada.fr/

> Le parcours ADA
>
> Le parcours Ada est un programme spécialement conçu pour accompagner les Femmes demandeuses d’emploi qui souhaitent se former dans le développement web, la data science, la cybersécurité ou la maintenance informatique. Le parcours Ada est un accompagnement gratuit, complet et personnalisé, de la recherche de formation, de stage jusqu’au recrutement en alternance ou poste junior en entreprise.

Aïssata Koité https://www.linkedin.com/in/aissata-koit%C3%A9-/

---

> Si la terre ne ment pas, l’idéologie du salaire-valeur, elle, nous embobine depuis un bail. Voilà la première idée que Friot nous administre. Le salaire n’est pas de l’ordre de la valeur. Il n’est pas un prix de marché, ni la contrepartie d’une contribution productive mesurable, encore moins les dividendes d’un capital humain. Le salaire ne ressortit pas aux mécanismes de l’économie, il est l’expression d’une distribution politique de la richesse. 


https://www.contretemps.eu/friot-recension-lordon/

---

https://man.sr.ht/~etalab/logiciels-libres/pourquoi-sourcehut.md

> Pour publier ses codes sources, chaque administration est libre de choisir entre plusieurs options, présentées en mai 2018 dans la politique de contribution de l'Etat aux logiciels libres, politique qui sera mise à jour en 2022.



Voici en détail les raisons qui ont guidé notre choix :

    SourceHut est un logiciel entièrement libre.

    Parmi les forges dont le code source est entièrement libre, SourceHut est la seule qui propose à la fois de l'intégration continue et des listes de discussion.

    Si vous voulez contribuer à un projet, vous n'avez pas besoin de créer de compte sur SourceHut : il suffit d'une adresse de courriel pour envoyer des correctifs et proposer des idées.

    SourceHut ne collecte aucune donnée de ses utilisateurs.


https://github.com/etalab/etalab/blob/main/logiciels-libres.md#les-logiciels-libres-utilis%C3%A9s-par-etalab
