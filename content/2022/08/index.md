---
title: août 2023
---

# Août 2023

## Mercredi 31

### Note de lecture

### Node de lecture : **« Pour une forêt primaire en Europe de L'ouest » de Francis Hallé.**

C'est le manifeste qui accompagne la création de l'association « [Francis Hallé pour la forêt primaire](https://www.foretprimaire-francishalle.org/) ».

[Francis Hallé](https://fr.wikipedia.org/wiki/Francis_Hall%C3%A9) est un des grands spécialistes des arbres.

> Qu'est-ce qu'une forêt ?
>
> (...)
> 
> Quelle que soit la région, tropicale ou tempérée, la forêt est la végétation la plus lourde, la plus complexe, la moins connue, la plus stable, la moins polluée et celle qui abrite, sur les sols, la plus riches, la plus haute diversité biologique animale et végétale.
>
> La forêt mérite une définition ambitieuses car c'est bien plus qu'une collection d'arbres vivant.

> Dans les régions à hivers froids de l'hémisphère nord, l'Europe par exemple, il faudra compter dix siècles à partir d'un sol nu. Pourquoi tant de temps ? Parce que les arbres de nos régions ne poussent que quatre mois par an, d'avril à juillet, quand les jours s'allongent, cessant donc toute croissance pendant huit mois de l'année : d'août à mars, même s'ils restent verts, ils ne grandissent plus.

> De tout cela (paragraphe précédent évoquant l'aspect agriculturelle de la gestion forestière moderne, avec engrain, coupe rase et intrant...) il semble qu'il faille garder l'idée forte et dérangeante que la diversité biologique et l'industrie du bois sont deux adversaires irréconciliables pour l'instant et que, dans leurs affrontements, la biodiversité est toujours perdante ; l'avenir dira s'il est possible d'imaginer un terrain d'entente, mais c'est un domaine où l'avenir est sombre.

> Je proteste ici contre la place trop réduite, voire presque nulle, accordée aux arbres et aux forêtes dans l'organigramme de notre gouvernement, et en particulier dans l'Éducation nationale et l'Enseignement supérieur.
> 
> La mise en place de monoculture et l'usager de méthodes agricoles pour les conduire, les gérer et les « récolter » sont des retombés directes sur le terain, de notre structure ministérielle.

> Pourquoi 70 000 hactares ? Parce que c'est, selon Gilbert Cochet, la surface nécessaire à la vie des grands animaux de la forêt primaire européenne : cerfs et daims, sangliers, lynx et chats sauvages, loups et ours, aurochs et bisons d'Europe. 70 000 hectares, c'est la surface de la partie polonaise de la forêt de Bialowieza, où tous ces animaux existent actuellement. La grande faune forestière nous impose donc la surface du projet.

> Planter des arbres n'est pas une solution parce qu'une « plantation d'arbres » est profondément différente d'une forêt. Cette dernière est un écosystème naturel dont la durée de vie indéfinie se compte en millénaires, tandis qu'une plantation d'arbres est une réalisation artificielle à finalité industrielle dont la durée de vie, déterminée par les contraintes commerciales, limitée à quelques dizaines d'années, se termine souvent apr une coupe à blanc.

> Voici quelques précisions concernant les forêts primaires :
> - les arbres atteignant les plus grandes dimensions en hauteur, en diamètre et en volume de bois pour chacune des espèces présentes, le stockage du carbone atmosphérique dans leur bois sera lui aussi maximal ;
> - les arbres morts se décomposant au sol entraîneront, là également, un stockage maximum du carbone dans l'humus et dans les horizons pédologiques supérieurs, la fertilité atteignant alors son niveau le plus élevé ;
> - le développement d'une faune abondante accomapgnant la décomposition des bois morts, avant et après la chute de l'arbre, les chaînes alimentaires se mettront en place, assuarnt le développement complet de la diversité animale, grande faune comprise ;
> - une haute diversité induisant une forte résilience, cela met la forêt primaire relativement à l'abri des dérèglements climatiques en cours ;
> - ajoutons des faits relativement inhabituels dans nos régions : l'absence de toute pollution, des sources pures et des paysages exempts de toute intervention humaine mais qui n'en seront pas moins d'une grande beauté ; **je suggère que la beauté prenne sa place dans l'écologie**, comme on le verra plus loin.

L'emphase est de moi.

> les dirigeants des pays tropicaux forestiers n'écoutent pas les écologistes européens qui viennent plaider en faveur des dernières forêts primaires tropicales, car ils savent qu'en Europe, nos forêts primaires ont été détruites et ils pensent que notre développement économique a été rendu posible par la destruiction de ces grandes forêts. Ce qu'ils nous disent est imparable : « Laissez-nous faire ici ce que vous avez fait chez vous. » Si l'Europe reconnaît qu'elle a fait une lourdre erreur écologique en rasant ses dernères forêts primaires et si elle travail maintenant à en faire renaître une, cela aura un impact philosophique et même politique qui pourrait être décisif.

> La science et lesthétique entretiennent des rapports parfois conflictuels ; à la Sorbonne en 1957, lorsque j'étais étudiant en biologie, nos professeurs nous mettaient en garde contre « l'admiration » ; c'était, nous disaient-ils, un sentiment subjectif qui risquait de fausser nos raisonnements : la beauté, selon eux, c'était bon poru les enfants, les amateurs, les artistes et les poètes, mais c'était « indigne des scientifiques ».

## Samedi 27

Quelques découvertes de la journée

- [Planches de dessins de plantes](https://plantes.bougeret.fr/). Superbe ! Inspirant !
- [Listes de fiches pédagogique sur la coopération dans une classe](https://www.icem34.fr/ressources/organiser-la-cooperation)

## Vendredi 26

### Code source

Un chouette site [pour apprendre Git](https://learngitbranching.js.org/?locale=fr_FR).

Pour trouver du code source super intéressant à lire [The Architecture of Open Source Applications](http://www.aosabook.org/en/index.html)



### Chemin de l'autonomie

[Le chemin de l’autonomie](https://reporterre.net/Le-chemin-de-l-autonomie)

> On ne sait plus jardiner, monter une charpente, coudre, tricoter, etc. On achète, on consomme, on fait faire. Mais bonne nouvelle : on peut y remédier !

1. [« Nulles en bricolage » ? Ces femmes qui dévissent les préjugés](https://reporterre.net/Nulles-en-bricolage-Ces-femmes-qui-devissent-les-prejuges)
2. [Je ne savais pas (du tout) bricoler : j’ai testé un chantier participatif](https://reporterre.net/Je-ne-savais-pas-du-tout-bricoler-j-ai-teste-un-chantier-participatif)
3. [Méconnues et comestibles : à la découverte des mauvaises herbes](https://reporterre.net/Meconnues-et-comestibles-a-la-decouverte-des-mauvaises-herbes)
4. [Pour être libres, soyons lents](https://reporterre.net/Pour-etre-libres-soyons-lents)

> En revanche, le botaniste (Christophe de Hody) en est convaincu, la cueillette « apporte une sensibilité à notre environnement ». « Le seul moyen d’avoir le désir de protéger quelque chose, c’est de l’aimer, ou au moins de le connaître ».

Tout à fait exact !

> Fait peu connu, c’est en discriminant la lenteur, justement, qu’ils y sont parvenus, raconte l’historien et sociologue Laurent Vidal dans Les Hommes lents, un ouvrage republié en poche fort à propos. Car en éclairant la condamnation pluriséculaire de la lenteur, il nous permet d’analyser nos résistances propres à son endroit, ce qui est aujourd’hui crucial pour l’écologie. Comment pourrions-nous en effet adopter des technologies moins polluantes, mais souvent plus lentes — marmite norvégienne ou vélo —, si nous restons attachés à l’idée que vitesse = puissance, temps, argent ?


### À lire 

À lire ? [« De la brièveté de la vie » essai de philosophie morale écrit par Sénèque vers 49 ap. J.-C.](https://fr.wikipedia.org/wiki/De_la_bri%C3%A8vet%C3%A9_de_la_vie)
À lire ? [Les Hommes lents — Résister à la modernité, XVe-XXe siècle, de Laurent Vidal](https://editions.flammarion.com/les-hommes-lents/9782080277428)

### Open / Closed principle

> « The open closed principle is overrated » 
>
> — @emilybache at #SoCraTes saying she would rather not design in order to avoid future changes but rely on making changes cheap

https://twitter.com/malk_zameth/status/1562741088989102087

Voir l'[article wikipedia sur le principe ouvert/fermé](https://fr.wikipedia.org/wiki/Principe_ouvert/ferm%C3%A9).

> En programmation orientée objet, le principe ouvert/fermé (open/closed principle) affirme qu'une classe doit être à la fois ouverte (à l'extension) et fermée (à la modification). Il correspond au « O » de l'acronyme SOLID. « Ouverte » signifie qu'elle a la capacité d'être étendue. « Fermée » signifie qu'elle ne peut être modifiée que par extension, sans modification de son code source.
>
> L'idée est qu'une fois qu'une classe a été approuvée via des revues de code, des tests unitaires et d'autres procédures de qualification, elle ne doit plus être modifiée mais seulement étendue.
>
> En pratique, le principe ouvert/fermé oblige à faire bon usage de l'abstraction et du polymorphisme. 

C'est effectivement pas forcement un bon principe il me semble. Ça pose la question du remaniement du code existant.


### Réseau rural Breton

Participer au [réseau rural breton local (pays COB)](https://www.reseaurural.fr/territoire-leader/la-carte-des-gal/gal-pays-du-centre-ouest-bretagne) ? Que font-ils là-bas ? Est-ce que ça pourrait être complémentaire à mon activité chez [Terre de liens Bretagne](https://terredeliens.bzh) ?

À lire l'article [Sobriété : « C’est dans les territoires ruraux que s’invente le monde de demain »](https://reporterre.net/Sobriete-c-est-dans-les-territoires-ruraux-que-s-invente-le-monde-de-demain), oui, c'est là dessus qu'il faut travailler. Et c'est bien parce que je suis convaincu de ça que nous sommes partis en Bretagne et que j'apprécie tant être à la campagne.


## Jeudi 25

> Based on data from 250K+ developers in our global community, developers code 52 minutes per day — about 4 hours and 21 minutes during a normal workweek from Monday to Friday.

Mais oui ! C'est tellement vrai !

> Coding tends to start later in the day and continue after traditional work hours. Just 10% of coding occurs between 9am and 11am, while 12% of coding happens after work hours between 5pm and 7pm.

Amusant, c'est au contraire la période où je code le moins, où je suis le moins efficace sur cette activité. J'apprécie beaucoup plus le matin ou la fin de journée. :shrug: Il faut bien se répartir dans les statistiques.

Ce que l'article ne précise pas c'est si l'on considère la lecture de code comme faisant parti du temps de programmation ?

[Code Time Report](https://www.software.com/reports/code-time-report)

## Mardi 23

### RDV-Solidarités

C'est reparti pour le module « InclusionConnect ». J'espère pouvoir finir dans la journée, avec Nathalie. Comme ça elle aura les infos et pourra faire des ajustements durant mon absence.

Nous finalisons l'utilisation d'Inclusionconnect en commençant par prendre en compte les cas d'erreur possible ; puis, en mettant un peu au propre l'utilisation de variables d'environnement. Le tout avec quelques tests assez basique, au niveau du contrôleur malgré tout. Pour finir, une petite extraction dans un service pour pouvoir découper cela proprement. Il resterais un peu de doc à fournir. Pour l'instant, c'est dans le pad.

Standup, c'était chouette de voir un peu tout le monde. J'ai évoqué l'utilisation du Nec pour faire un temps commun (genre loger ensemble dans une maison) ? Mais comme ne je suis pas sur de venir, je ne veux pas pousser. C'est dommage je crois.

J'ai l'impression de commencer à décrocher de RDV-Solidarités. Un peu de fatigue ou bien je me lasse ?

Travaux pour clarifier le compte rendu de l'atelier « RDV en binôme »

## Lundi 22

### RDV-Solidarités

Support.

Suppression d'un RDV, demandé par Clarisse (92). Faciliter cela en faisant en sorte qu'un RDV ne soit jamais vraiment complètement supprimer semble un sujet très important.

Analyse du problème de SMS dans les background jobs. C'est très louche. Il semblerait qu'une série de numéro en 07 pose problème à NetSize. Et je ne trouve pas d'info sur l'attribution de ces séries de numéro...

Réalisation d'un ticket à propos du problème d'emails bloqué suite à un changement apporté pour ajouter le lieu...

Traitement aussi de ticket à propos de problème pour le changement d'email d'agent. Et les alertes de chevauchement de RDV avec indisponibilité et plage d'ouvertures.

Réunion avec une personne de chez Numéricité à propos de la « potentielle fuite » de données. L'échange à permis de nous rassurer, il n'y a aucun problème. Nous allons quand même faire un petit travail pour cloisonner les données.

Discussion avec Antoine pour évoquer InclusionConnect et répondre à quelque questions que je me pose 
- url à utiliser
- comment revenir à RDV-Solidarités

Il y a des zones que je ne suis pas sur de comprendre dans InclusionConnect, je pense que j'ai mal compris certains mécanisme.

C'est beaucoup plus clair avec du code source, un schéma, et les bonnes informations pour pouvoir se connecter.
Reste à définir notre cas d'usage.


### Le sabotage

[Être stupide, où l’art du sabotage social selon les leçons de la CIA](https://www.hacking-social.com/2016/05/09/etre-stupide-ou-lart-du-sabotage-social-selon-les-lecons-de-la-cia/)

Un truc qui me dérange ici, c'est que l'on sabote le travail, la vie, d'autres personnes qui n'ont rien à voir avec le problème (les problèmes). C'est un peu trop « large » sans doute.


> Confrontant l’histoire des luttes passées à l’immense défi du réchauffement climatique, Andreas Malm interroge un précepte tenace du mouvement pour le climat : la non-violence et le respect de la propriété privée. Contre lui, il rappelle que les combats des suffragettes ou pour les droits civiques n’ont pas été gagnés sans perte ni fracas, et ravive une longue tradition de sabotage des infrastructures fossiles. La violence comporte des périls, mais le statu quo nous condamne. Nous devons apprendre à lutter dans un monde en feu.

À lire ? [« Comment saboter un pipeline »](https://lafabrique.fr/comment-saboter-un-pipeline/)
À lire ? [«  Hacker Protester - Le guide pratique des outils de lutte citoyenne ](https://hckr.fr/)

### Découverte

[La carte de la foudre](https://www.lightningmaps.org/#m=oss;t=3;s=0;o=0;b=;ts=0;y=45.9511;x=5.6689;z=5;d=2;dl=2;dc=0;)

### Phytoépuration

Pour démarrer, l'[article wikipédia sur la phytoépuration](https://fr.wikipedia.org/wiki/Phyto%C3%A9puration). Toujours une bonne base. Par contre, les références cités ont disparu pour la plupart :-/

[ Plantes filtrant l'eau pour le traitement en phytoépuration ](http://www.ecologs.org/vegetation/plantes-filtrant-l-eau-pour-le-traitement-en-phytoepuration-et-les-piscines-ecologiques.html)

À lire ? [L’auteur, André Paulus](http://www.le-filtre-plante.com/andre-paulus/)


## Dimanche 21

> "focus on delivery, not refactoring" = no cleaning means more time with our guests
> "technical debt" = we'll have to borrow ustensils and dishes to the neighbors before our guests arrive
> "let's rewrite the whole thing" = what we need in this house is a second kitchen
> #legacycode
>
> -- ToF https://twitter.com/ToF_/status/1561270834160963585

### À lire

À lire ? « Le seigle et la ronce : un artisanat huit fois millénaire » de Pierre Martel

## Vendredi 19

### RDV-Solidarités

Support.

Reprise du travail sur la PR d'ouverture public des RDV Collectifs. Je me confronte à des tests qui échouent sur le système de sectorisation. Ça fonctionne actuellement dans mon environnement de test. C'est donc que : c'est une configuration que je n'ai pas dans mon environnement qui est testé. Je pense à la sectorisation à l'agent.

Échanges avec Victor et François à propos de la PR sur l'ajout du nom de domaine. Nous avons surtout parlé architecture. Dans la PR, c'est le point qui me travail le plus.
Il y avait aussi un petit quelque chose autour de l'objet `Domain` qui est introduit ici dans le répertoire `models`, peut-être pas le bon endroit, et qui héride d'`OpenStruct`. Là aussi, je comprend la démarche, et je crois que je préfèrerais un vrai objet. À défaut, au moins utiliser `Struct` qui propose de meilleures performances (voir [When should I use Struct vs. OpenStruct? sur StackOverFlow](https://stackoverflow.com/questions/1177594/when-should-i-use-struct-vs-openstruct).

Un autre point de discussion tournais autour des noms. Puisqu'il y a deux domaines qui aujourd'hui représente d'une part le médico-social et de l'autre « l'inclusion numérique » au sens large, il est peut-être temps d'avoir un produit qui a son propre nom.

Les pages d'explications posent également quelques questions : est-ce qu'elles sont lus ? Par qui ? Il serait intéressant de regarder le volume de consultation et le temps que passe les personnes sur la page.

Nous avons également abordé rapidement les modifications potentiels à apporter à la politique de confidentialité et aux CGU.

Retour sur la PR d'ouverture public des RDV Collectif. Je me bas encore avec l'histoire du nombre d'allocation : là on passe de 2 200 à 15 086 allocations ! Je vais regarder.

StartupNotifications remonte un pépin de Soft Bounce sur les SMS.

Faire une campagne pour réduire l'utilisation des champs avec le numero de caf & co.

Discussion sur une potentielle fuite de donnée. https://pad.incubateur.net/h3EW57E2Sp2uJExIKwF6TA?edit Super intéressant autour de la fiche utilisateur.

### Entreprise Opale

[SquadRacer](https://squadracer.com/) est une [Entreprise Opale](https://fr.wikipedia.org/wiki/Entreprise_opale)... Comme le dis le podcast de Julien Marseille, c'est une façon de rehabilité les ESN ? https://open.spotify.com/show/1UWsdtnsHeSbm8wfjAWX1F

C'est pas si mal après tout. S'il peut y avoir un peu d'autogestion dans ces outils, ça permettra de réduire les personnes qui sont ici pour profiter du système.

Dans le premier épisode, découverte de [Edwyn Tech](https://www.edwyn.tech/).

Est-ce que [CodeWorks](https://www.codeworks.fr/) entre dans ce même mouvement ?


## Jeudi 18

### RDV-Solidarités

Point avec Gauthier à propos de sa PR sur la restriction des agents à participer a un RDV. Il faut que ces agents soient du même service que le motif du RDV. Nous avons prévu une évolution mais ça sera plus tard.

Un peu de support avec la création d'un petit ticket. Difficile de ne pas se jeter dessus : rapide et facile à faire...

Petit mail à Ethibox : l'application Zammad renvoie pas mal d'erreur 500 ces derniers jours :-/ Les limites des chatons ?

Vigie : Analyse des problèmes Sentry. Ajout de tickets :
- [ Ignorer les erreurs de connexions au webhook de la Drôme #2722 ](https://github.com/betagouv/rdv-solidarites.fr/issues/2722) ;
- [ Afficher les erreurs de mises à jour d'un RDV dans l'interface graphique #2723 ](https://github.com/betagouv/rdv-solidarites.fr/issues/2723)
- [ Mise à jour du SDK de Sentry #2724 ](https://github.com/betagouv/rdv-solidarites.fr/issues/2724)
- [ Maintenir l'onglet sélectionné après une recherche dans les plages d'ouvertures #2725 ](https://github.com/betagouv/rdv-solidarites.fr/issues/2725)

Travaux sur [ Pouvoir chercher des plages d'ouverture en tant qu'agent non admin #2701 ](https://github.com/betagouv/rdv-solidarites.fr/issues/2701) qui continue d'apparaître dans Sentry. C'est un bug bloquant pour les utilisateurs agents !

Une petite rétro light, comme c'est souvent le cas en période de vacances.

Un peu de support pour traiter une veille demande de Christelle (64) à propos d'un SMS que nous aurions envoyé. Aucune trace. C'est très surprenant.

Reprise de la PR de Gauthier [ RDV uniquement avec des agents du même service que le motif (reprise de la PR #2650) #2727 ](https://github.com/betagouv/rdv-solidarites.fr/pull/2727)

Pas de nouvelles à propos d'inclusionConnect.



---

Faire des exo Git ? Revoir peut-être comment fonctionne 

https://www.freecodecamp.org/news/learn-the-basics-of-git-in-under-10-minutes-da548267cc91/
https://learngitbranching.js.org/?locale=fr_FR

---

[PicoCMS](https://apps.nextcloud.com/apps/cms_pico), dans nextcloud, semble proposer quelque chose qui m'intéresse pour les sites des associations... Encore faut-il trouver comment l'installer :)

---

> Ma fille, j’ai grandi sans internet ni téléphone portable. Il n’y avait pas encore cette toile qui s’immisce partout : dans notre lit, sur la table du petit déjeuner, dans notre poche, dans notre main.

https://reporterre.net/Naguere-sans-portables-ni-drones-notre-jeunesse-insouciante

---

> L’agriculture industrielle est une grande contributrice à l’effet de serre. La première cause d’émission de gaz à effet de serre, à l’échelle mondiale, c’est la déforestation, notamment amazonienne, qu’impose la culture de soja nécessaire à l’alimentation de nos élevages industriels. Ces émissions sont certes lointaines, mais bien réelles.
>
> Il y a ensuite les émissions de méthane, produits par les rots des ruminants ; brebis, chèvres, vaches. Le méthane est 28 fois plus réchauffant que le CO2. C’est le second gaz en terme de contribution à l’effet de serre de l’agriculture mondiale, et française. Mais en France la principale source de production de gaz à effet de serre, c’est le protoxyde d’azote (N20). Libéré quand on épand des engrais azotés de synthèse (dont notre pays fait grand usage), il est presque 300 fois plus réchauffant que le CO2. C’est très grave, et totalement ignoré.
Sur le même sujet
> 
> Ces engrais sont en plus très coûteux en termes de consommation d’énergie fossile, puisqu’ils sont fabriqués à partir de gaz russe et norvégien. Avec l’actualité de la guerre en Ukraine, on voit à quel point cela pose des problèmes de dépendance. Les coûts de ces engrais vont grimper monstrueusement. Cela va mettre de nombreux agriculteurs en difficulté. On aurait pu anticiper cette dépendance il y a déjà une trentaine d’années, en tournant le dos à l’agriculture industrielle.
>
> -- Marc Dufumier

J'ai envie de le croire. Où sont les sources ?

> Notre agriculture industrielle est très destructrice : elle détruit la biodiversité mais aussi l’humus de nos sols. Elle consomme du carburant, elle use les tracteurs et elle est « suicidogène » pour nos agriculteurs. De plus, elle n’est pas du tout compétitive sur un plan monétaire. La filière des betteraves à sucre destinées à faire de l’éthanol ne tient que parce qu’elle est subventionnée. Idem pour les poulets bas de gamme nourris au soja brésilien et destinés à être exportés vers l’Arabie saoudite. Ce ne sont pas ces produits bas de gamme qui font nos excédents de balance commerciale.

> Si, depuis trente ans, on avait imposé des malus aux usagers d’engrais azotés de synthèse et accordé des subventions aux agriculteurs qui mettent des légumineuses, la transition agricole aurait déjà bien démarré et nous ne serions pas dans cette situation aujourd’hui.

https://basta.media/Agriculture-industrielle-dependance-au-gaz-pesticides-urgence-de-la-transition-agricole-climat-GIEC-agroecologie-Marc-Dufumier


## Mercredi 17 

### Terre de liens

Lister les outils utilisé à TDL BZH. Faire en sorte que la grille de renseignement soit utilisable par tout TDL.

Participer à un groupe de travail pour parler des valeurs de terre de lien par rapport au numérique.

Contacter Chloé CARON pour participer au Comité de Coordination Digital

> Réseaux et sites Web de tiers afin de nous permettre de faire de la publicité sur leurs plateformes ;

https://www.salesforce.com/fr/company/privacy/

https://cloud.inpact35.org/terredeliens/s/89g9Zek7A7fRwPr
https://www.cssfontstack.com/

### Le Grand Manger

Réunion le grand manger. L'idée est de reprendre les statuts de l'association pour en faire un outil pour les animations.

Certaines sont des locations par des structures morales qui paient une facture, d'autres sont des évènements d'association de fait ou juridique. 

L'association permettrait de distribuer des boisons alcoolisé (ou non) aux membres, sans avoir besoin d'une licence...

http://www.savoirscom1.info/2018/04/savoirscom1-devient-une-association-loi-1901/


## Mardi 16

_Je demande à la banque comment dépasser le plafond pour renvoyer le virement que Scopyleft m'a envoyé au lieu de l'envoyer à Chrysalide_

### RDV-Solidarités

Travaux sur l'ouverture de l'accès publique au RDV Collectif. Ça coince sur l'expression rationnel qui retrouve le motif collectif. Bizarre.

Fin de discussion avec Myriam sur les retours des entretiens référentes. Il y a de la matière... Comment extraire ça pour l'inclure dans la feuille de route ? J'ai parfois l'impression de ne pas savoir convaincre ou négocier les priorités avec l'équipe... Un sujet de rétrospective ? Je suis trop gentil ?

InclusionConnect pour les agents ? Et les prescripteur ? Considéré agent ou usager particulier ?

Faire un schéma d'architecture (DDD ?) et associer les limites d'aujourd'hui. Peut-être y associer les orientations à prendre pour demain.

Standup hebdo. C'est bizarre d'appeler ça comme ça, mais c'est sans doute plus adapté à notre échelle : organisation de la semaine.

Dans l'ouverture au public pour les RDV Collectif, je me retrouve confronté à la récupération de la liste des motifs. Il y a un truc un peu étrange pour lequel je n'arrive plus à retrouver de justification : l'utilisation du nom et du type de motif plutôt que l'ID ! Est-ce que vous avez une idée de pourquoi nous gardons ce principe ? Il me semble qu'une fois le motif choisi, nous pourrions transporter l'ID et aller plus vite sur les écrans suivant 

> Je me souviens pas d'avoir entendu d'infos là dessus :slightly_frowning_face:
> -- Victor Mours

J'ai l'impression que c'est lié au motif identique, multiple, car lié à chaque organisation. Dans le 64 il y a plus de 20 organisations, donc le même motif est dupliqué 20 fois, mais je dois apparaitre qu'une fois à l'usager… Et il faut qu'il puisse choisir plusieurs lieux (attaché, eux aussi, à plusieurs orgas)... 

Je vais mettre un petit commentaire dans ce sens dans le code. Je ne vois pas d'autre raison de le faire. Et tant que les motifs seront associés à une orga et non un territoire, il faudra sans doute passer par là.

En même temps, une fois le lieu et le motif choisi, nous ne devrions plus en avoir besoin. ⁣ Nous n'affichons pas des créneaux sur plusieurs orgas. Le nouveau calculateur de créneau prend un motif et plus un nom comme avant justement... C'était peut-être une forme de régression :cold_sweat: ? Est-ce qu'il y a prescription si la modification date de plus de 6 mois ?

---

> La forme du marché :
> - nous sommes payés à écrire du code, pas à lire du code
> - nous sommes considérés "junior" pendant à peu près un an, et "senior" à moins de trente ans (une autre manière de dire que notre art d'ingénieur·es est bouffé par le problème de l'agent intermédiaire)
> - c'est un savoir d'ingénieur·es que nous développons et non un savoir académique et nous le développons juste assez pour survivre dans un modèle économique qui professe le changement permanent, la destruction créatrice, et le potentiel de valorisation par un VC
> - en réalité on fabrique peut être les ponts et les édifices qui soutiennent cette ère du numérique, mais on le fait avec le court-termisme et le bagou des vendeur de remèdes
> -- Christophe Thibaut Slack des artisans du logiciel

## Lundi 15

Il y a une histoire de poule et d'œuf dans le fait que l'on dit : « il y a beaucoup de freelance en informatique ». Est-ce qu'il y a des freelances parce que les boites ne « recrutent » pas en interne ou parce que les personnes préfères vraiment ce statut ? Et si elles préfèrent ce statut, est-ce parce que les boites ne propose pas d'emploi « intéressant » avec de la liberté ? Ça serait intéressant d'explorer ces sujets :thinking:

## Dimanche 14

### Pandoc
[pblog.xyz - Pandoc static blog generator](https://pblog.xyz/). Je bricole (elsif.fr](https://elsif.fr/), mon site, avec [Pandoc](https://pandoc.org/) aussi. Intéressant. C'est vraiment un bel outil.


### Markdown

J'ai basculé, depuis plusieurs jours, sur [Ghostwriter](https://github.com/wereturtle/ghostwriter). [Apostrophe](https://opensourcemusings.com/writing-in-markdown-with-apostrophe) ne permet plus de retailler la fenêtre en largeur, or moi j'aime bien mettre ce type d'outil en demi-écran.

GhostWriter était pas si mal dans mes essais précédent. Et ça se confirme.

### CSS

[System.css](https://github.com/sakofchit/system.css)

> System.css is a CSS library for building interfaces that resemble Apple's System OS which ran from 1984-1991. Design-wise, not much really changed from System 1 to System 6; however this library is based on System 6 as it was the final monochrome version of MacOS.
>
> Fortunately, this library does not use any JavaScript and is compatible with any front-end framework of your choice. Most styles can also be overwritten to allow for deeper customization.


[CSS Bed - This is a collection of classless css themes to use as starting points in web development](https://www.cssbed.com/).

Ça me rappel un peu [CSS Zen Garden](http://www.csszengarden.com/).

L'approche sans l’utilisation de classe est cependant intéressante. Je suis souvent tiraillé entre l'utilisation des classes et leur non  utilisation.


### Bretagne

> C’est l’histoire d’un petit village breton qui résiste encore et toujours à l’artificialisation des sols. Trémargat, village pionnier dans l’écologie et la démocratie participative depuis plus de 50 ans, a un nouveau projet. La commune cherche des futurs habitants pour créer un éco-hameau sur une parcelle communale de 6000m2. Durée du bail : 99 ans.

[Le village breton de Trémargat lance un éco-hameau avec des habitats légers](https://lareleveetlapeste.fr/le-village-ecolo-de-tremargat-recherche-des-habitants-pour-creer-un-eco-hameau/)

### Accueil paysan

> Fondé en 1987, le [réseau Accueil paysan](https://www.accueil-paysan.com/fr/) propose une démarche similaire à celle du Wwoofing. « Depuis le départ, on est sur le concept de tourisme durable. Chez nous, l’idée de l’accueil à la ferme a été pensée pour soutenir économiquement une agriculture bio de montagne qui était alors à la peine », rappelle Pierre-Jean Bartheye, administrateur de ce réseau qui recense 900 fermes en France.

> [Cet été, et si on pratiquait « l’anti-tourisme » ?](https://vert.eco/articles/cet-ete-et-si-on-pratiquait-lanti-tourisme)

## Vendredi 12

C'est vraiment horrible de travailler sur Wordpress. 
- Trouver comment customiser la page blog
- imaginer la publication d'un site web à partir d'un répertoire du nextcloud.

## Jeudi 11

### RDV-Solidarités

Un peu de support 
- Un RDV qui n'a pas pu être envoyé : croisement entre la livraison qui modifie le job, et un job empilé avec d'autres données.
- Le message d'erreur que l'on reçoit de la Drôme : `Zimbra authentification failed`. J'ai envoyé un message à la DSI pour demander des informations.
- traitement du problème de fusion pour le 92. Il semble qu'un des « rapatriements » d'usager n'ai pas fonctionné. Je l'ai refait ce matin, et ça a fonctionné. **Nous devons abattre le mur entre les orgas**

Regroupement de quelques liens pour la mise en place d'InclusionConnect, et création du ticket associé.

Affichage des lieux avec prochaine disponibilité respective pour l'ouverture public des RDV Collectifs


## Mercredi 10

_Je vais travailler un peu pour RDV-Soliarités, mais considéré que c'est un complément des deux jours précédent où j'ai quitté un peu tôt dans l'après-midi._

### RDV-Solidarités

Reprise de la PR sur l'ouverture au publique des RDV-Collectifs.
Petite session de MobProgramming avec Nathalie

## Mardi 9

### RDV-Solidarités

MobProgramming sur la requête permettant d'afficher les motifs collectifs et individuel ouvert au publique (avec Nathalie, François et Victor).

Discussion avec Myriam
- support utilisateur, analyse de certains ticket support
- point sur les retour référentes, la présentation et les travaux à mener ensuite.    

Standup 
- Anne-Sophie nous fait une rapide présentation de ce qu'elle a commencé à faire à propos des statuts de participation
- Nous discutons feuille de route. Victor à participer à une réunion où il y a pas mal d'attente. Nous décidons d'avancer tous ensemble sur une même feuille de route.

## Lundi 8

### RDV-Solidarités

Discussion avec Nathalie pour déblocage sur la PR du contrôle sur la fusion usager.
Un peu de grooming avec elle pour nettoyer les tickets et prévoir les tickets suivant pour elle.

Point avec Anne-Sophie à propos du design sur les statuts de participation individuel.
Est-ce que l'option « détail usager » est toujours utilisée ?
Est-ce que la Somme, ou d'autres départements, font des ateliers collectif à plus de 20 personnes ?

https://discussion.conseiller-numerique.gouv.fr/cnum/pl/rosqxh4rkjn37bapiojfdpo5xw

---

Personalisation de l'utilisation mémoire de mon firefox, dans `about:config` puis http://kb.mozillazine.org/Browser.cache.memory.capacity
http://kb.mozillazine.org/Browser.cache.memory.capacity
http://kb.mozillazine.org/Browser.cache.memory.enable


## Dimanche 7

### Learn shell

[Game Shell](https://github.com/phyver/GameShell). Partagé sur les artisans du logiciel par Agathe, je viens aussi de le relayer sur le réseau Motiv'Her. Il faudrait peut-être que je l'expérimente. 
C'est une question intéressante à creuser : comment apprendre à utiliser un terminal ?

### Fenugrec

Beaucoup de fromage aux graines de [Fenugrec](https://fr.wikipedia.org/wiki/Fenugrec) en centre Bretagne j'ai l'impression. Pourquoi ? Est-ce que l'on pourrait faire pousser cette plante ici ?

## Jeudi 4

### RDV-Solidarités

Binomage avec Nathalie sur le ticket à propos de l'erreur lors de la fusion d'usager dont au moins un c'est connecté avec FranceConnect.
Il y a
- un controleur
- un `form_object`
- un service
- des modèles

Le pattern du `form_object` est relativement logique ici puisqu'il y a un formulaire qui mélange 2 usagers... Mais est-ce que ça ne fait pas trop ?
Est-ce que le form_object devrait contenir ce qu'il y a dans le service (et porter un autre nom du coup) ?

```SQL
User.select([:id, "count(organisations.territory_id)"]).joins(:organisations).group(:id).having("count(organisations.territory_id) > 1")
```
Analyse du nombre d'usager présent sur plusieurs territoire vis-a-vis du projet de fusion.

## Mercredi 3

[Polymathie](https://fr.wikipedia.org/wiki/Polymathie)
[Polymathie du Morbihan](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_polymathique_du_Morbihan)

### RDV-Solidarités

Discussion avec Myriam à propos d'un message utilisateur sur les notifications. Il manque des cas d'usages pour exprimer les mécanismes à l'œuvre à propos des notifications. **C'est un des points clefs de RDV-Solidarités, c'est un de ceux qui devraient être le mieux documenté**.

Compte rendu de la réunion référente du 2 août 2022. Je passe par Notion pour voir...

Déménagement de la doc vers le notion.

Discussion avec Anne-Sophie à propos de la fonctionnalité sur l'individualisation des statuts de participation.

Correction d'une issue Sentry à propos du calcul de jours fériés sur une période nulle (https://sentry.io/organizations/rdv-solidarites/issues/3276720900/?project=1811205&query=is%3Aunresolved+sdk.name%3Asentry.ruby.rails&sort=date&statsPeriod=14d).

Ajout de tickets suite à l'analyse de sentry.

Analyse et résolution du problème visible dans sentry et qui empile des erreurs dans les background-job. Dans les nouvelles organisations ajoutés dans la Drôme, il y avait des absences et des plages d'ouvertures cochées :-/

Nettoyage des job en erreurs dans les `Delayed_job`.


## Mardi 2

### RDV-Solidarités

Réunion référente

PR sur le retour d'un lien depuis la checklist vers la prise de RDV. Le souci est sur l'utilisation de `prendre_rdv` qui ne reste pas et devient `root_path` et donc termine chez les agents...

Est-ce que nous avons encore l'utilité du nextcloud ? Pas vraiment pour les documents, mais pour l'agenda partagé et pour les mots de passe. Est-ce qu'il y aurait un moyen d'avoir un autre agenda partagé ? En utilisant RDV-Solidarités ? Où mettrons nous les mots de passe ensuite ?

Point budget avec Fanny. Nous parlons de la mission courte UX/Design à venir et du ou des futures bon de commandes à préparer.

Point support avec Nathalie, Nesserine et Myriam.

Standup qui fini par une discussion sur la documentation.

Présentation de l'audit flash d’accessibilité par Gladys et Anne-Sophie. C'est chouette d'y participer toutes ensemble. Il y a du boulot, mais il y a aussi pas mal de bon truc.
Recruter une personne dev potentiellement avec peu d'expérience et surtout très orienté front pour prendre soin de l'interface ?
Le niveau simple A du RGAA sont sur les aspects bloquants. Reste à avoir l'expérience permettant de « savoir » si c'est un critère simple A du RGAA ou autre.
https://accessibilite.numerique.gouv.fr/

https://hellobokeh.notion.site/RDV-Solidarit-logu-agent-e9aa5705f0014cd5a301daead0dc2d46
https://hellobokeh.notion.site/La-hi-rarchie-des-titres-est-compl-te-et-coh-rente-d1abf22820b446bf984eedaa29e9eccf

Suppression de page inutilisé dans la doc. Migration de page de la doc vers le notion.

Déplacement de document vers le notion. Suppression d'autres pages. Faut-il vraiment tout garder ? Pas sur.

## Lundi 1er

### RDV-Solidarités

Support utilisateurs.
Debug de la fuite mémoire. Nous faisons l'exploration avec Nathalie. Nous avons finie par trouver que cela viens 	: la combinatoire au moment de filtrer sur les lieux (lorsqu'il y a un `lieu_id`.

Attention durant la fusion, on peut perdre les aspects franceconnect.

Grooming
