---
title: Mardi 25 janvier 2022
---

45 ans. Ce matin, tranquillement installé dans la cuisine avec un petit thé, je regardais les oiseaux dans le jardin, les arbres, les nuages... Et je me disais que j'avais sans doute fais plus de la moitié de ma vie. Je en suis pas sur d'y mettre une signification. J'ai regardé un peu derrière moi et je crois que je suis content de ce que j'ai fait jusqu'à présent.

Alors oui, j'aimerais en faire encore beaucoup. Et surtout, j'aimerais prendre le temps d'accompagner les enfants encore un peu plus... Au moins 15 ans de plus, ça serait chouette.

---

C'est peut-être dans lié à mon anniversaire ;
peut-être lié à Nicolas que j'ai vu ralenti par son IDE ;
peut-être lié à une discussion sur le slack de La Zone à propos des vieilles machine et de Vim pour développer dessus ;
Je suis de retour dans mon terminal avec Vim.

Je crois que j'apprécie VSCodium pour la facilité de navigation (type fuzy finder principalement), et j'imagine qu'il y a moyen de trouver quelque chose qui fonctionne bien dans Vim pour ça aussi.

Il y aurait aussi un truc que j'appréciais sur les IDE (que je n'avais pas dans VSCodium mais dans Eclipse, NetBeans et qui doit être dans RubyMine) c'est la navigation interne : aller à la déclaration d'une méthode, lister les appelant de cette méthode. Là aussi il doit y avoir moyen de faire quelque chose (avec les CTAGS ?).

L'avantage d'être avec Vim c'est d'être léger et d'être dans le terminal.

Peut-être que pour les fichiers Markdown, Apostrophe continue à être pas mal...

---

https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web

J'ai voulu partager ce lien en lieu et place d'un lien vers le W3Schools qui me semble moins bien. Et on m'a dit que MDN était un peu élitiste... :thikning: Je me demande ce que ça signifie. J'ai demandé des informations complémentaires.


