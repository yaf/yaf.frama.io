---
title: Lundi 7 février 2022
---

Je recherche encore des citations pour appuyer les idées pédagogiques... Alors en voilà quelques unes.


“when you don't create things, you become defined by your tastes rather than ability. your tastes only narrow & exclude people. so create.”
― Why The Lucky Stiff 


“Programs must be written for people to read, and only incidentally for machines to execute.”
― Harold Abelson, Structure and Interpretation of Computer Programs

“Any fool can write code that a computer can understand. Good programmers write code that humans can understand.”
― Martin Fowler

“I'm not a great programmer; I'm just a good programmer with great habits.”
― Kent Beck 


