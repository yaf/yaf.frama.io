# Juin 2022

Facturation RDV-Solidarités 11

## Mardi 28 (RDV-S)

Support utilisateur
Post-mortem sur l'incident du 9 juin
atelier sur le partage des rôles dans l'équipe

Reprise de l'atelier avec un temps autour des problématiques actuelles. J'ai beaucoup apprécier ce que Victor à évoqué sur les histoire de pouvoir.

Rapport de mission sécurité de Simon et perspective à venir. Nous devons trouver un moyen de mieux intégrer Mohamed. J'ai beaucoup apprécié l'approche de Simon. J'espère que nous aurons l'occasion de retravailler ensemble ici ou ailleurs !

PR sur le blocage de changement de statut depuis un statut de RDV annulé.

Par ailleurs, j'apprécie de plus en plus l'idée selon laquelle nous pourrions séparer l'application avec une partie « cycle de vie » d'un RDV et « les éléments qui amène à la création d'un RDV ». Ces derniers pouvant être spécifique au domaine du coup. Ça permettrais sans doute de réduire la configuration.
Reste un pépin il me semble : si l'application de cycle de vie du RDV est sur un autre serveur, il va falloir qu'il soit très performant pour répondre rapidement aux requêtes cherchant des listes de RDV pour un agent, pour un calcul de créneau & co. Mais rien d'insurmontable sans doute, d'autant que ça amènerais sans doute de gros gain sur d'autres points.
Je pense entre autre que nous pourrions envisager le RDV avec une structure d'évènement. Le cycle de vie est limité : création, quelques modification éventuellement, date dépassé, fin du RDV... La pile d'évènement n'est pas folle et peut facilement se manipuler, se recalculer...


## Lundi 27 (RDV-S)

Je me bagarre avec la connexion au serveur mattermost : besoin d'un jeton via mobile. J'ai pas de mobile intelligent !

Lettre d'info et test perf avec Myriam.
Grooming.

Atelier RDV en binôme. Beaucoup de chose apprissent.

Droits d'accès : proposer qu'un motif soit accessible sur tel ET tel service

PR pour ajouter le nom de l'organisation dans l'export CSV
Amorce de travail sur le blocage du changement de statut qui n'envoyais pas de notification (après une annulation).

## Dimanche 26

[Repository Structure and Python](https://kennethreitz.org/essays/2013/01/27/repository-structure-and-python)
À lire ? [The Hitchhickers guide to Python Pratcies development](https://docs.python-guide.org/writing/structure/)


## Vendredi 24 (RDV-S)

Écriture de test pour vérifier le chargement de l'historique (reproduire le bug https://github.com/betagouv/rdv-solidarites.fr/issues/2600)

Ça faisait longtemps que je n'avais pas pris le temps de résoudre un problème comme celui-ci (qui fini par être une simple mise à jour de Gem :)

Atelier RDV binôme avec les référentes
Point RGPD avec Nesserine

ajouter des ancres dans les pages statiques.

Archive statistique ou pas ?

---

Discussion avec Guillaume (Zifro). Il me parle encore de [Pony](https://www.ponylang.io/discover/#what-is-pony). Amusant. J'aime aussi l'aspect brut de leur blog http://guillaume-et-laetitia.eu/ Ça me rappel le site d'OpenBSD et cette époque de la page web très brute :)

## Jeudi 23 (RDV-S)

Revue de PR avec test sur les performances. J'apprécie beaucoup le soin que met François à documenter ces PR.

Grooming du backlog. Je fais des regroupement, et reformulation. C'est intéressant. Le hic, c'est qu'il y a sans doute trop de milestone :tinking:. Tiens, nous avions dit avec Myriam que nous ferions ce travail ensemble, ça m'aiderais sans doute à le faire mieux.

RDV avec Amélie Naquet (ANCT-SoNum) sur l'avenir et les enjeux de RDV-Solidarités.

Atelier synchronisation de calendrier via une url iCalendar.
On fait une premier ticket mvp...

Travail sur le support. Documenter où nous en sommes sur le RGPD.

Retour sur l'entretien avec Christelle. Nous avons moins de réactivité aujourd'hui. L'organisation de Data.insertion est peut-être pas mal.

**Mettre en place un journal de décision pour l'équipe ?**

Là nous évoquons le fait d'ajouter une étiquette avec le numéro du département sur tout les tickets qui proviennent d'un département, et une étiquette CNFS pour les tickets qui parlent des CNFS

Point tech autour de la migration vers OVH, avec les problématiques HDS, RGPD, et RGS.

Définir un plan d'action avec Simon.

Discussion avec l'équipe à propos des choix sur l'interaction avec les référentes.

Perte de l'historique des RDV, des motifs et des fiches usagers, que ce passe-t-il ? Il semble que ce soit lié à la mise à jour de Ruby. L'analyse YAML des textes semble bloquer sur les format date.

## Mercredi 22

Découverte via Claire du Grand Manger, de l'association [l'éclate](https://www.leclate.fr/) qui fait de la facilitation graphique.

Nous avons parlé et commencé à évoquer des prises de décisions au Grand Manger, mais nous n'avons toujours pas de journal de décision. J'ai bien envie de mettre ça en place. Je voulais un truc low-tech, à la main, mais je crois que nous allons devoir passer par un côté numérique puis impression... 

---

Documenter

> Une interprétation de la documentation en qualité d’objet à concevoir pourrait postuler d’inclure et de structurer celle-ci autour de quatre fonctions différentes : tutoriels, guides pratiques, explications et références techniques.
> - La forme tutorielle, qui est axée sur l’apprentissage, permet au nouvel arrivant de commencer, telle une leçon. Elle est similaire à l’acte dapprendre à planter des légumes ou d’apprendre à faire la cuisine à un individu.
> - La forme de guide pratique, qui est axée sur les buts, montre comment résoudre un problème spécifique, tout comme une série d’étapes. Elle est semblable à l’acte de cultiver des légumes ou à une recette dans un livre de cuisine.
> - La forme d’explication, qui est axée sur la compréhension, explique, fournit des renseignements généraux et le contexte. Elle est comparable à un article sur l’histoire sociale de la tomate ou l’histoire sociale culinaire.
> - La forme d’un guide de référence, qui est axé sur l’information, décrit la conception réalisée. Elle vise l’exactitude, donc la vérification et l’ajout de sources, et recherche à être complète. Elle est semblable à un article d’encyclopédie de référence.

https://sens-public.org/articles/1375/



## Mardi 21 (RDV-S)

Réunion référente.

Droits d'accès tout pété dans l'invitation. Faire en sorte de ne pas perdre les accès aux autres territoires auxquelles j'accède déjà, ajouter ce mécanisme dans l'ancien système d'invitation.

## Lundi 20 (RDV-S)

Accueil Simon

gommer les départements -> Ticket sur la recherche de créneau plus large qu'un territoire

Grooming backlog avec François.
Regrouper les tickers dans les milestones par thématique, puis prioriser les thématiques pour rendre visible l'agenda de réalisation. Associé au fait de faire en sorte que l'équipe travail sur la même thématique, ensemble (faciliter le mobprogramming pourquoi pas, et à défaut, le travail sur la même zone de code pour mieux l'enrichir).
À voir pour les petits tickets qui traines, sans milestone. Est-ce le travail de la vigie de manger ces petits tickets ?

Faire du grooming backlog AVEC Myriam ! (nettoyage de backlog).
Nettoyage de ticket avec Myriam.

Point avec François sur la semaine passé et les perfs de calcul de créneau.

Support utilisateur.
---

Depuis le slack des artisans

> ToF
> user story = pattern de traduction dans lequel des acteurs métiers et des acteurs techs élaborent ensemble une traduction exécutable par l'ordinateur dans laquelle le sens métier se réalise.
> ce qui n'a pas exactement le même sens que :
user-story-dans-jira : artefact constitué par la trace minimale résiduelle d'une session de traduction.
> pour prendre une métaphore :
> user story = une marche de 4 heures entre amis dans les cévennes
> user-story-dans-jira = le tracé en rouge sur carte IGN 1/25000

## Dimanche 19

Lit enfants à donner
- 70x160
- 70x140

> J’aurai donc tendance à privilégier le champ unique avec le format. Il faut sans doute le compléter avec un contrôle et une correction automatique des erreurs. Un affichage de la date en toutes lettres à côté du champ du type «  mardi 27 mai 2014 » peut permettre à l’utilisateur de se corriger, notamment pour les dates dans le futur proche, en corrélant jour de la semaine et jour dans le mois. On peut même aller assez loin dans l’aide à la saisie sur ce type de champ, afin de permettre à l’utilisateur de saisir aussi bien « 5514 », « 05052014 », « 5 5 14 » ou « 05/05/14 » pour obtenir dans tous les cas la date du lundi 5 mai 2014.

https://blocnotes.iergo.fr/concevoir/le-choix-de-la-date/

Réalisation de schema pour clarifier (j'espère ?) ma visio vis-à-vis de RDV-Solidarités.

Une question m'occupe l'esprit ces derniers jours : changer de mission ? Changer de facturation ? Est-ce que vendre mon temps de cerveau disponible a des gros projets (ou à beta.gouv) c'est le seul moyen de maintenir mon niveau de rémunération actuel ?

Note pour mémoire, ça fait 6 ans que je suis à 4 000 € net, depuis mon entrée chez /ut7 (1er décembre 2015) où tout le monde était à ce salaire là. Ça m'a fait une belle hausse de salaire à l'époque, j'étais sur 3 000 € net. En arrivant chez Scopyleft (7 janvier 2018) j'avais le choix, la politique interne de l'époque était le salaire au besoin. Je n'ai pas voulu réfléchir à mes besoins, j'avais l'impression qu'ils étaient bien satisfait comme ça, donc je suis resté à 4 000 € net.
Maintenant que je (re)bascule dans une CAE ; que je souhaite travailler plus pour le territoire où je vis (le Faouêt, Roi Morvan Communauté, le Centre Ouest Bretagne, la Bretagne, ...), la question de cette rémunération se pose.

Je pourrais faire du conseil et de la formation localement, mais personne ici pourrait me payer 700 € HT par jour.

Alors peut-être faut-il que je combine la diminution de mon niveau de facturation avec une augmentation de mon temps de travail. Après tout, si je fais un travail qui a plus de sens, qui me plait plus, est-ce qu'y passer plus de temps serait un fardeau ? Sans doute pas.

Dans les options que j'envisage localement:
- formation pro (à la programmation principalement, mais aussi en module court sur l'agilité, le développement piloté par les tests, la gestion produit, ...)
- atelier grand public (initiation à la programmation, compréhension du numérique, internet, ...)
- réparation et installation d'ordinateur (logiciel libre & co)

Dans l'esprit, je crois que j'aimerais monter un [chatons](https://www.chatons.org/).
Une autre option serai de monter une structure « façon CAE », peut-être en association dans un premier temps, puis en CAE pourquoi pas.

---

Du ménage dans mon ordi. Plus beaucoup de place. Steam prends toutes la place. Est-ce que j'ai besoin d'un nouveau disque dur ? Ou peut-être de fair en sorte que Steam soit uniquement sur l'ordi du salon, l'ordi pour jouer.

C'est bizarre aussi de devoir « installer » les jeux sur chaque compte. Est-ce qu'il n'y a pas moyen de dire à Steam d'installer les jeux sur uen clef USB ?

---

Remis en place la redirection [www.terredeliens.bzh](https://www.terredeliens.bzh) vers les pages gitlab [terre-de-liens-bretagne.gitlab.io](https://terre-de-liens-bretagne.gitlab.io)

J'ai aussi fait la procédure de migration pour le site de TDL BZH.

C'est sympa un peu d'admin de temps en temps... Et si j'en faisait un peu plus ? Devenir un chatons, j'en parlais déjà chez Scopyleft. Vincent me parlait d'un chatons pour l'ANCT aussi ... Il faut que je lui reparle.

## Vendredi 10 (RDV-S)

Identité agent + compte demo pour Thibaut MonSuiviSocial

Discussion avec Thibaut de MonSuiviSocial

Nahtalie
https://miro.com/app/board/uXjVO4FChDg=/
Rdv avec Raphaël

PR pour ajouter du contexte dans l'affichage des organisations dans la page d'invitation

Préparation de l'affichage des invitations en absence

Analyse du pépin qui fait que lorsque l'on invite un agent sur une orga d'un territoire, on supprime ces droits sur les autres territoires...

Rédaction des décisions de la réunion tech du jeudi 8

## Jeudi 9 (RDV-S)


Support utilisateur (vigie)
Débug suite à la livraison de la fonctionnalité sur les droits d'accès. Il y a quelques pépins sur la démo où la migration n'est pas bien passé.

Entretien avec Simon (Plup) pour parler SecOps. Nous avons conclu qu'une première intervention pour faire un état des lieux nous permettrais d'avoir une meilleure vision de nos besoins. Trop chouette !

Suite à la modification sur les droits d'accès et surtout ici sur le fait de tout rapatrier dans le module de configuration, PR en urgence pour faire un filtre des agents pour restreindre sur les agents de mes orgas, de mes services.

Point avec l'ANCT, la SoNum pour rencontrer Margot Aptel et Amélie Naquet (entre autre)

Point RDV-S/Data.insertion

Point tech

Binomage avec Nathalie

## Mardi 7 (RDV-S)

Réunion référentes.
Synchronisation avec Matis
Point avec Mohammed sur des problèmes de déploiement en production
Point budget avec Élie et Fanny
Rétrospective
Point avec Nathalie
Point avec Myriam (support utilisateur et lettre d'information)
Échange avec Thomas sur la suite et les recrutements
Support Zammad avec François.

Il n'y a que Victor et Mohamed avec qui je n'ai pas fait de tête à tête aujourd'hui :)


## Vendredi 3 (RDV-S)

Recrutement Ops et dev.

Une petite PR à propos d'information sur la suppression des PO dans l'interface graphique.

Grooming backlog

Pour libérer les absences (faire en sorte qu'une absence soit visible sur toutes les organisations), il faut libérer les webhooks. Je me lance dons dans ce mouvement.

    # ajoute la ref vers le territoire
    # ajoute la jointure vers plusieurs organisation (une orga peut avoir plusieurs webhook, un webhook peut avoir plusieurs orga)
    #   Attention, elle ne dois pas avoir le même nom que le lien existant vers les organsation

    # parcour les webhook existant
    #   récupérer le territoire de l'organisation du webhook
    #   chercher un webhook associé à ce territoire
    #     si on en trouve un, 
    #       alors on ajoute l'organisation au webhook trouvé
    #     sinon
    #       alors 
    #         - on ajoute l'organisation au webhook courant
    #         - on met le territoire sur le webhook courant
    #   

    # supprimer le lien belongs_to vers une organisation
    # supprimer les webhooks qui n'ont pas de territoire

Finalement, c'est peut-être plus intéressant d'explorer une modification d'interface graphique. Puis, si c'est concluant, de faire en sorte que la modélisation de la base de données suive.


## Jeudi 2 (RDV-S)

Suite (et fin ?) de la PR sur les droits d'accès.

Questionnement sur l'usage de Framaliste et des moyens de contact des référentes.

Faire réponse à Thomas, prendre contact avec Simon pour évoqué plutôt dans 6 mois.

Point avec Myriam à propos de la FAQ, du support et de la réunion référente. Nous avons également parlé des premiers retour qu'elle a des entretiens utilisateurs qu'elle met en place. C'est très intéressant, comme prévu ! Et je crois qu'elle aime bien faire ça et que ça lui va bien.

Pour le poste de CTO/PO de la généralisation de RDV, je crois que ce qui m'inquiète c'est que l'on recrute quelqu'un qui se place en posture de chef, d'autorité, et ça sa pourrait me gonfler très fort. Je suis partant pour rester sur le médico-social. Il faut malgré tout que j'écrive mes idées / envies.

Et si en fait ce n'était pas une bonne idée de généraliser ? Et si, comme le disait Victor, il fallait envisager que la parti amont d'un RDV soit la partie spécifique, propre à chaque usager, et que le cycle de vie du RDV soit la partie commune ? C'est un axe intéressant à suivre je crois.

Point tech. C'était intéressant de prendre le temps de parler de la performance pour le calcul de créneau. C'est chouette de découvrir une piste plus simple à mettre en œuvre, mais je crains que ce ne soit que repousser le gros travail à faire...

J'ai passé un certain temps à transférer les mails reçu sur contact@ vers support@.... J'ai encore du mal à trouver ça plus simple d'utiliser Zammad... 

Support Utilisateurs

## Mercredi 1er

Rencontre pour la première fois avec un membre de l'URSCOP. Une personne bien sympathique. C'était intéressant de découvrir un peu la proposition de l'URSCOP, les modes d'organisation et de fonctionnement.
Nous allons sans doute adhérer pour le Grand Manger.

https://labo.societenumerique.gouv.fr/2022/05/19/dossier-quelles-competences-numeriques-pour-les-salaries/
La société numérique, voilà un belle endroit pour travailler. J'aime bien ce qu'il s'y passe, vu de l'extérieur en tout cas. 
Je suis heureux dans l'éco-système de l'ANCT !

Thomas évoque le montage d'une équipe CTO/PM pour la généralisation de RDV-Solidarités. A chaque fois ça me renvoie à « je ne suis pas la bonne personne »... Je crois que je crains que les personnes recruté me prenne de haut et ne tienne pas compte de tout ce que j'ai déjà pensé, envisagé...
Après un long message à Raphaël qui fait un peu l'effet d'un canard en plastique (RDV pris malgré tout, ça m'aidera), j'ai l'impression que je dois écrire plus pour partager ce que je connais, ce que je pense. Pour le faire valoir et éviter de me retrouver dans une posture, plus tard, de dire : je vous l'avez bien dit (alors qu'en fait je n'aurais rien dit ou écrit)...
