---
title: Vendredi 25 février 2022
---

L'épaule, ça fait mal !

---

https://github.com/alextselegidis/easyappointments
https://easyappointments.org/docs.html#1.4.2/rest-api.md
https://developers.easyappointments.org/api/



https://www.hl7.org/fhir/modules.html

https://www.etesync.com/user-guide/evolution/

---

https://mastodon.social/@fribbledom/107844832008869309


> muesli @fribbledom@mastodon.social
> 
> Working as a developer doesn't mean you're smashing keys on your keyboard for 8 hours straight.
> 
> It's thinking, learning, experimenting, communicating, reading, trying, exploring, scribbling, and probably a dozen other things. Then, for just a couple minutes each day, you actually type some code.

---

> L’expérimentation du marché de lavoir n’est pas née de nulle part. Elle s’appuie en réalité sur un modèle bien réfléchi, celui de la « Sécurité sociale de l’alimentation » (SSA). À l’échelle nationale, un collectif informel regroupant onze associations [1] le porte dans le débat public depuis plus de deux ans. Leur objectif est « d’intégrer l’alimentation dans le régime général de la Sécurité sociale », tel qu’il a été mis en place en octobre 1945 par le ministre communiste Ambroise Croizat. Concrètement, une « carte vitale de l’alimentation » donnerait accès à un certain nombre de « produits conventionnés ». Le prix des aliments de base (pains, fruits, légumes) comme celui des produits transformés sera payé par la solidarité nationale.


> Ainsi, ces « caisses », indépendantes des pouvoirs publics et où seront représentés paysans et consommateurs, auront pour mission de décider des conditions de production des produits conventionnés comme l’origine bio ou locale. Mais aussi de fixer les contrats entre les différents maillons du système alimentaire (production, transformation, distribution), assurant ainsi aux agriculteurs leur fameux « prix de revient ».

> Enfin, pour les paysans aussi, ce modèle serait synonyme d’un mieux vivre. « Les terres des paysans participant au conventionnement ne feront plus partie de leur capital individuel. Elles appartiendront à la collectivité et ils n’en seront que les tenanciers. Socialiser l’outil de travail permettra de dégager le paysan d’une partie du capital à rembourser et donc de son endettement. La terre deviendra un moyen de production mis en commun, comme l’hôpital dans le service public de santé », dit cet éleveur breton à la retraite.

https://reporterre.net/Dans-la-Drome-on-experimente-la-Secu-de-l-alimentation


https://securite-sociale-alimentation.org/la-ssa/historique
