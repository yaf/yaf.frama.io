---
title: mercredi 20 avril 2022
---

> Dans le fonctionnement et l’organisation actuels des établissements scolaires, l’Autorité est représentée par les chef·fes d’établissement et par les inspecteurs·rices, relais privilégiés des mots d’ordre et des méthodes répressives du ministère : culpabilisation des personnels en difficulté ; posture démagogique ou autoritariste vis-à-vis des familles, selon leur profil ; chantages et menaces sur les personnels précaires ; ostracisation, voire sanction contre celles et ceux qui portent une autre voix ; sélection de favori·es dans l’équipe pour être les relais de la direction ; opacité dans les prises de décision ; économies de moyens, encore et toujours.

> Pourtant, face à ce fonctionnement qui veut que chacun·e se taise et obéisse, l’autogestion des établissements apparaît comme un horizon à rechercher, un avenir à construire pour que chacun·e puisse s’emparer des questions éducatives et en faire un bien commun qui ne soit plus aux seules mains des politiques.

> C’est en effet un fonctionnement autogestionnaire, démocratique, qui permettra aux travailleurs et aux travailleuses de l’éducation de reprendre la main sur leur métier et d’œuvrer en fonction de finalités éthiques, directement reliées au terrain, et non en fonction de lignes économiques, comme le font les dirigeant·es actuel·les, souvent bien éloigné·es de nos réalités, si ce n’est à l’extrême opposé. C’est bien là que se construira l’école de demain, collectivement, et non dans un programme donné à l’avance, sans réelle consultation du plus grand nombre.

> Parler de ses pratiques, se soutenir dans les apprentissages, choisir librement ses sujets de recherche et les présenter aux camarades, débattre et décider des projets de la classe sans que personne, pas même l’adulte, n’ait le monopole des savoirs et des décisions ; mettre en place des habitudes de compagnonnage entre les élèves et entre les adultes, afin de prendre du recul et nourrir les réflexions. Tout ceci permet de reconstruire du collectif et de la solidarité, de sortir de l’individualisme dans lequel nous sommes de plus en plus enfermé·es et de nous émanciper des rôles asséchants dans lesquels l’institution nous enferme, parfois du fait de notre classe sociale, de notre niveau culturel, de notre handicap voire de notre identité sexuelle ou de notre origine supposées-assignées.

> L’éducation pour la transformation sociale
>
> Nous touchons là au cœur du problème, au cœur de l’école : loin d’être le sanctuaire hermétiquement clos auquel certain·es voudraient la cantonner, l’école est la caisse de résonance de toutes les problématiques qui parcourent nos sociétés.

Je vais finir par la monter cette école !

> Les affirmations péremptoires telles que Quand on veut on peut, ou L’école leur donne tout, il leur suffit de travailler ou encore de toute manière, ils n’ont plus le goût de l’effort, vieux paradigme de la méritocratie, ne servent qu’à culpabiliser les jeunes en difficulté en évitant de prendre en compte leurs réalités sociales, familiales ou cognitives, en évitant de rechercher les nœuds qui se sont formés au fil d’années d’échecs ou d’empêchements liées aux parcours de vie.


https://www.contretemps.eu/ecole-hier-demain/

À lire ?  Jacqueline Triguel milite à Sud éducation 78, Questions de classe(s) et Lettres vives. Elle est l’autrice d’Étincelles pédagogiques (Libertalia, 2021).
