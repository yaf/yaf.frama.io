---
title: Lundi 24 janvier 2022
---


[Core Team] Officier Azarov:drapeau_arc_en_ciel: Hier à 17 h 34
Salut ! Je suis un cours à la fac et je bute sur une question de shell assez basique.
Si quelqu'un·e peut m'éclairer, je suis preneuse !
L'énoncé :
Dupont a fait une erreur de frappe, il a saisie la commande  mv a b/* au lieu de mv b/* a.
Que se passe-t-il si b est un répertoire contenant 3 sous-répertoires et aucun autre fichier ? Expliquez pourquoi cela se produit.
Dans mon terminal :
J'ai un répertoire a et un répertoire b, qui a lui-même 3 sous-répertoires : c, d et e. Soit visuellement :
a
b
c
d
e
Quand je tape mv a b/*, a est déplacé dans un des sous-répertoires de b (le dernier, selon l'ordre alphabétique). Les deux autres sous-répertoires de b sont déplacés dans ce sous-répertoire également. Soit visuellement :
b
e
a
c
d
Est-ce qu'une bonne âme aurait une idée de pourquoi ça arrive ? (et aussi, déjà, avez-vous les mêmes résultats chez vous ?) (interrogations existentielles, bonsoir)


Anh-Vu  il y a 14 heures
hello, le comportement que tu observes est tout à fait normal et déterministe, heureusement ! dans ton shell b/* est remplacé par la liste des répertoires b/c b/d b/e avant d'appeler la commande mv . Aussi le dernier argument de mv est le répertoire de destination. Donc quand tu appelles
mv a b/*
le shell traduit en
mv a b/c b/d b/e
et mv comprend qu'il faut déplacer a b/c b/d dans b/e
Aussi le shell assure que le remplacement de b/* est dans l'ordre alphabétique des occurences trouvées (modifié)


En général, un bout de code est analysé à partir de la droite (c'est un raccourci que je prends). Par contre, c'est vrai qu'il faut savoir que b/* est une expression qui va être évalué... c'est pas forcement évidant :slightly_smiling_face: (et j'ajoute ceci parce que le shell est un langage de programmation comme un autre, à ceci prêt que l'on passe notre temps dans son interpréteur à écrire une instruction à la fois et rarement dans un fichier à écrire plusieurs ligne, contrairement aux autres langages :))
