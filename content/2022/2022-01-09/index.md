---
title: Dimanche 9 janvier 2021
---

_note: communauté de recherche philosophique (CRP)_

> L'esclavage n'est plus légal, mais existe toujours. Le racisme n'est plus acceptable, mais existe toujours. La misogynie est inadmissible, mais existe toujours. Comment se fait-il qu'il y ait tant d'injustices dans le monde? Voilà une question que je me suis posée, déjà, lorsque j'étais enfant. Ensuite, m'est venue à l'esprit cette autre question : pourquoi les actions humaines ne suivent-elles pas le rythme de nos idées? Vivre une vie authentique et bienveillante n'est pas une tâche humaine facile. 

> Plus précisément, parmi les nombreuses incompréhensions que j'ai face à notre monde, une en particulier m'apparaît comme une injustice incontestable : la moitié de l'humanité a été et est encore aujourd'hui oppressée parce que ces personnes sont des femmes.

> L'histoire de la philosophie occidentale marquée par des biais androcentriques laisse entrevoir une société pensée par et pour les hommes (Hamrouni et Lamoureux, 2018). Les origines de la philosophie occidentale dégradent et effacent les capacités intellectuelles des femmes et leurs expériences sociales, morales et politiques propres. Pour Aristote, la philosophie est une discipline de l'élite masculine (Politiques, 1260a41) et le sexe masculin est supérieur à celui féminin (Politiques, 1254b13-14). Les personnes n'appartenant pas à ce groupe « hommes » ne sont pas suffisamment rationnelles (femmes, enfants et esclaves). 

> Les philosophes occidentaux reconnus sont majoritairement des hommes, blancs, privilégiés économiquement et sur le plan de l'éducation.

> Les pédagogies féministes ont pour but d'enseigner à penser de manière critique et à résister aux oppressions (Anzaldua, 1987; hooks, 1994 et Pagé, Solar et Lampron, 2018).

> La CRP a la qualité d'encourager la pluralité et d'accepter la diversité. D'une part, par la multitude de voix pour contribuer à la richesse de la quête. 

> La CRP est essentiellement égalitaire. La position de la classe en cercle n'est pas anodine. Personne n'est caché derrière. Ils et elles se regardent et se font exister par cette considération égale.

> La CRP contre les effets négatifs de la culture oppressante qu'est la nôtre en développant la confiance en soi des enfants.

> Selon Sharp, il faut savoir reconnaître l'autre comme un cadeau dont il faudrait prendre soin (1994 : 26). Il est important de comprendre l'équilibre entre la confiance en soi et l'humilité. Par la pratique de la CRP, les enfants peuvent apprendre à reconnaître les moments où ils pourraient s'affirmer et partager leurs idées avec confiance et les distinguer des moments où un certain recul est préférable. Pour ce faire, il faut être attentif aux besoins de la communauté et à ses propres besoins. Il est aussi nécessaire de savoir reconnaître notre faillibilité (Yorshansky, 2009 : 44). 

https://diotime.lafabriquephilosophique.be/numeros/090/001/

--

À lire ? [« Le vasculum ou boîte d’herborisation. Marqueur emblématique du botaniste du XIXe siècle, objet désuet devenu vintage. » Par Régine Fabri](https://www.tela-botanica.org/2021/12/le-vasculum-ou-boite-dherborisation-marqueur-emblematique-du-botaniste-du-xixe-siecle-objet-desuet-devenu-vintage/)

---

> Les écrivains ont toujours adopté les ­technologies qui rendent leur tâche moins ardue.

> Des romanciers comme Richard Powers, Vikram Chandra et Marisha Pessl se servent de toutes sortes de logiciels, d’Excel aux programmes de gestion de projet, pour organiser leur univers imaginaire. Loin de représenter une menace, l’informatique est pour eux un outil essentiel.

---

![Prendre ses signes vitaux psychologiques](prendre-ses-signes-vitaux-psychologiques.jpg)

Prendre ses signes vitaux psychologiques

https://www.cisssca.com/clients/CISSSCA/CISSS/COVID-19/Prendre_soin/Barom%C3%A8tre_sant%C3%A9_psychologique__1_.pdf

via https://www.facebook.com/wiracocha.co/ Wiracocha // Animation Education Formation et Insertion


---

> Le « data capitalism » contemporain repose sur une infrastructure numérique produite par des projets de logiciels open source : la contrainte d’accumulation constante des entreprises doit donc prendre en compte le hasard introduit par ces communautés de volontaires autogérés.

https://cis.cnrs.fr/sem-cis-22-mathieu-oneil-et-laure-muselli/?fbclid=IwAR3svtd6XMVs64ITYeK-TataaQHXZKCrtxlat23isRMpbxthBqltjF57XWk

Une belle thèse dont le développement disponible sur le monde diplomatique pour les abonnées...

https://www.monde-diplomatique.fr/2022/01/MUSELLI_LAURE/64221

Et si je m'abonnais ? Ça fait déjà pas mal de temps que ça me travail, et Rémy m'avait recommandé de le faire, en me disant que ça pourrait m'intéresser...

---

Des communautés de partout...

Cette fois, c'est What the fabrik, sur laquelle il y a un groupe de canaux « partenaires » avec [Atypikall](https://www.atypikall.com/) dont j'ai déjà pas mal entendu parlé.


> UN RECRUTEMENT DIFFERENT
> Trouver un emploi.
> Chercher un collaborateur.
>
> Entreprises, recrutez des personnes pour leur réel potentiel et leur personnalité.
> Candidats, mettez en avant qui vous êtes.

---

> Regroupons-nous pour bâtir une société démocratique, écologiste, égalitaire, multiculturelle et féministe :
> Passons à l’Offensive !

> Pour faire disparaître les inégalités systémiques et restaurer une harmonie entre les êtres humains et leur environnement naturel, notre seul espoir consiste à bâtir une société nouvelle : une société intégralement démocratique. Mais tenez-le-vous pour dit : les riches et leurs États ne nous laisseront pas faire. Ça tombe bien, nous ne comptons pas leur demander la permission !

https://offensive.eco/

J'y vois quelque chose d'intéressant... Sauf sur l'aspect massification

> Ouverture et massification
>
> Engagement à favoriser la participation et l’adhésion la plus large possible au nouveau modèle de société, pragmatisme, refus des postures de « pureté idéologique », refus des chapelles doctrinaires, attachement à la création d’une unité d’action en dépit des différences des participant·e·s.

Je pense qu'il faut garder plein de petite structure, c'est sans doute le meilleur moyen de passer « sous le radar ». Devenir gros,c'est devenir un état. C'est pour ça que je me méfie des fédérations comme celle-ci d'ailleurs.

_Tiens, j'ai partagé ça sur linkedin et pour la première fois, je me pose la question de l'autocensure vis-à-vis de ma participation à beta.gouv_

---

Découverte de Muséodev

https://musodev.com/

> Nous oeuvrons pour l'autonomisation des femmes depuis 2018
> Bienvenue sur le site de l’association pour la promotion des Femmes pour les TIC « Musodev ».

Tiens...
> Quelque soit votre domaine de compétences, nous sommes convaincus que vous pourrez contribuer à l’atteinte des objectifs de MUSODEV. Rejoignez dès aujourd’hui  notre équipe en tant que coach bénévole

---

> Lorsque vous suivez tutoriels après tutoriels et que vous ne construisez jamais quelque chose par vous-même, vous êtes coincé dans l'enfer des tutoriels.

https://devscast.tech/posts/devenir-developpeur-eviter-enfer-des-tutoriels-22

