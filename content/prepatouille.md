---
title: Prépatouille, rencontres de personnes souhaitant apprendre à programmer
---

## 8 décembre 2015

Ce soir, après la formation, j'accueille pour la première fois un groupe de
débutant en programmation.

J'ai préparé un plan, basé sur les recommandations de Raphaël, confirmé par
Manu : groupe de praticiens. Après un rapide tour de table, celui qui est
partant expose un problème qu'il rencontre actuellement dans son apprentissage
de la programmation, nous faisons ensuite un tour de table pour poser des
questions permettant d'avoir des précisions sur son problème, il répond dans la
foulé, puis nous faisons un deuxième tour de recommandation, une petite
conclusion de l'intéressé, puis nous pouvons passer à une autre personne. A la
fin, un debrief bien sur.

Et bien je n'ai pas du tout pu déroulé le plan \o/

Une personne est arrivé à l'heure. Nous avons commencé à discuté, j'ai fait
attention à ne pas trop partir sur les sujets que je voulais abordé ensuite...
Mais au bout de 20 minutes, nous sommes parti sur ces problématiques... Une
autre personne arrive vers 19h30. Nous repartons en mode freestyle, une
troisième personne arrive vers 20h. Et pour finir, à 21h, heure de fin prévu,
une personne demande à ouvrir la grille... Nous partons, c'est trpo tard.

Ce qui est chouette : 

* Les profile des trois personnes. Deux sont en reconversion, des bon candidats
  pour l'alternance. L'autre est en poste, vois des bouts de sql sans trop
  comprendre et aimerais en savoir un peu plus, sans pour autant avoir un plan
  en tête. Pas de wannabe entrepreneurs.
* Les échanges ont été intéressant, avec des remarques de chacun envers les
  autres. Du coup tout le monde s'échangeait des conseils, sans forcement que ce
  soit selon un plan. Vraiment sympa
* Ils étaient tous les trois ok pour refaire ça en janvier, et on dit vouloir
  regarder les divers _trucs_ échangé durant la session d'ici là.

Globalement très satisfait de la soirée.

Ce qui pourrais être mieux : 

* Faire en sorte que tous comprenne qu'il y a un code pour rentrer.
* Remplir un wiki au fur et à mesure avec le vidéo proj (ou le faire après coup
  ?)
* Trouver un moyen de pouvoir échanger en ligne (un slack sur lequel les
  participants qui sont venu au moins une fois pourrait discuter ?) C'est
  peut-être plus intéressant qu'un wiki ?
* Trouver une astuce pour que les inscrits viennent. Idée : faire payer en ligne
  l'accès, et rembourser ceux qui viennent à l'heure. C'est un peu abusé... ne
  plus être sur meetup en est une autre ?


## 12 janvier 2016

Ce soir, Vanessa et Yohan arrivent en même temps. Étienne et Jonathan sont
encore en train de faire des trucs... ça fait bizarre, mais bon, c'est pas
grave. J'ai même trouvé _intéressant_ que Jonathan soit encore là pendant le
début de la rencontre. Je ne sais pas s'il écoutait ou pas par contre. 

Yohan était déjà venu : commercial, immobilier, pas envie de continuer. Envie de
faire de la datascience, au chomage et partant pour une reconversion. Très
demandeur. Apparement il a un contact dans une boite que Xebia est en train de
monter autour du Grosse Aquing : Akamis. Ils ne sont pas encore prêt à le
prendre en CDD. Je lui suggère de parler de l'alternance.

Vanessa viens pour la première fois. Autodidacte, elle est déjà en poste dans
une ex-filliale de Lagardère. Elle fait de l'intégration... Elle souhaite monter
de niveau en javascript. J'essaie de savoir si sa boite est prête à payer des
formations... Pas de réponse clair. Je crois que je vais lui faire un petit
email pour lui proposer le GymClub ... 

J'essaie de mettre en place une session type cercle de pratique, comme Manu et
Raphaël me l'avait conseillé. La dernière fois, avec les arrivées au compte
goutte, ça avait été un peu dans le désordre, mais là, je peux tenter.

Le truc c'est qu'à deux, il y a peu de questions/interaction. Ou alors je rempli
trop les blancs ? Je tente quand même de poser des questions autour du problème
qu'expose Vanessa.

Je me demande si tout les débutants ont le même genre de problèmes : 

- ne pas savoir par quel morceau comencer
- mise en pratique après avoir lu/fait les tutos.
- s'approprié les concepts

« Assembler les légos, et après ? ». Tout les deux expriment les même
difficultés. J'ai l'impression qu'ils attendent que je donne un conseil.
J'essaie de repousser le moment, mais c'est plus fort que moi. Je vais faire le
consultant...

Je leur suggère de (re)voir le protocole http. Première réaction : « on creuse
encore un sujet, comment savoir quand s'arreter ? ». Puis j'explique que l'idée
derrière la revue d'http c'est d'essayer de mieux comprendre comment les choses
s'articule sur le web, qui parle avec qui, quand, comment, avec quoi.

Ensuite, c'est un peu plat. Nous tournons un peu en rond. J'ai du mal, je
patauge. Du coup, je sort mon ordi et nous regardons un peu de code. J'écris un
peu de code... je crois qu'ils sont content.

Nous verrons s'ils y reviennent... 

_Je ne sais pas si le cercle de pratique est un bon format pour des débutants
qui justement n'ont pas encore de pratique ?_

L'idée de les faire parlé de leurs problèmes est intéressante, mais comment
intérargir ? Je parle de faire un slack pour qu'ils puissent s'entraider, mais
ça ne me satisfait pas trop.

Est-ce que nous pourrions définir un parcour pour chacun ? et se servir de cette
rencontre pour faire un point d'étape ? Cela pourrait permettre que chacun
montre du code. Est-ce qu'ils aimeraient cela ? Est-ce que ça ne serais pas une
trop forte contrainte ?

Yohan me demande de manière plus ou moins masqué si je suis ok pour faire du
mentorat avec lui... Je répond que non.

Il va valloir sortir de meetup aussi. Ou alors il faut que je mette une limite
plus haute ? Je vais voir avec le prochaine rendez vous. Si le nombre reste
inférieur à 5, je monterais la limite à 17 personnes pour voir.


Je suis un peu déçu par cette session. Heureusement que j'ai déjà planifié la
suivante, ça m'oblige à la faire :-)


## 28 janvier 2016


C'est bizarre de faire ça après un jeudi, mais au moins je suis dans un état
d'esprit assez serein on va dire.

Jonhatan est resté pour voir combien de personnes allaient venir...

Le soucis dans un truc comme ça, c'est de gérer les arrivées. Là encore, c'est
un peu au compte goutte. Un autre soucis, c'est que je parle de
__L'algostrophe__ sur meetup, du coup les gens cherchent ça => Il faudrait que
je fasse un truc marqué Algostrophe quelque part dans la vitrie :-)

Du coup je démarre avec beaucoup de répétition à chaque nouvelle arrivée, mais
au final nous seront 7. 

- Denis (le premnier arrivé). Un certain age, il a fait du SAS, un peu de SQL et
  du R. Apparement il n'a pas de mission depuis un moment. C'est peut-être pour
  ça qu'il veut apprendre Java (il précise, langage objet et eclipse). Il me
  semble un peu _spécial_
- Christophe. Un certain age encore. Un designer-ux-graphiste. Apparement très
  photoshop-illustrator. Mais il dit avoir fait un peu de html-css en disant que
  ce n'est pas vraiment du code. Il s'intéresse à Python parce qu'il veut faire
  du traitement de donnée, analyse sémantique et autre big-data
- Zied. Age moyen. En poste dans une SSII qui fait du SAP. Il s'occupe de faire
  du paramétrage si j'ai tout compris. Il à fait un peu de code à la fac. Avec
  une association il participe à la création d'un supermarché coopératif dans le
  20ieme. Dans ce contexte, ils installent Odoo (ex-OpenERP). C'est écrit en
  python. Du coup il se remet un peu à coder, et souhaite s'améliorer.
- Vanessa. Intégratice, déjà venue la dernière fois. Souhaite élargir ces
  compétences avec du JS voir un peu de php/ruby.
- Carole, age moyen. A fait du code à la fac, mais à du arrêter. Maintenant elle
  fait des trucs administratif dans une boite (celle de son frère ou un ami, je
  ne sais plus). Elle a voulu bricolé des trucs pour se facilité la vie, et y
  prend gout. Elle voudrait se replonger dedans. Peut-être Python (elle à fait
  aussi un peu de C à la fac, mais souvenir lointain).
- Mik. Déjà venu aussi la dernière fois. Age moyen. Reconversion, cherche à
  faire du JS et du Ruby. Utilise FreeCodeCamp.
- Yohan. avec Mik, ce sont les deux qui sont là depuis le début. Orienté Data
  analyse, il essaie d'apprendre pytthon pour une reconversion.

Après ce tour de présentation un peu bégaillant, je tente de parler du format
où une personne pourrait exposer un de ces soucis ou point de blocage puis nous
pourrions en parler (questions/réponses/suggestions). Je ne met pas trop de
formalisme pour le moment. 

Personne n'ose prendre la parole. Au bout de quelques temps Zied prend la parole
et commence à essayer d'exprimer quelque chose. C'est autour de la sécurité mais
c'est pas clair. Quelques questions (de moi et d'autres) plus tard, et voilà que
l'on ne comprend toujours pas beaucoup. Le sujet sécurité va être traité en
large. Il y a de bonne suggestion, mais je ne peux pas m'empécher d'ajouter une
couche. C'est idiot quand j'y repense, mais je crois qu'ils ont aimé... Je
n'aurais peut-être pas du, ou alors suggérer ?

Ensuite, je relance un tour... mais personnes n'ose. Yohan évoque un email qu'il
m'a envoyé 2 jours avant à propos des tests unitaires... C'est un peu triché,
mais au moins nous abordons le sujet. Quelques échanges partent, et finalement
je me retrouve à expliquer le TDD. C'est un peu chiant d'être en position de
sachant qui parle du coup je trouve. Il faut que je cherche un autre moyen
d'intervenir pour transmettre de l'info, ou de ne pas intervenir peut-être ?

Enfin, sur un troisième et dernier tour, Carole essaie de formuler une
difficulté qu'elle a : par où commencer ? « J'ai une idée d'application, mais je
ne sais pas trop par où commencer ». Ça démarre en essayant de savoir c'est quoi
sont app, en détail. Je recardre un peu car ça ressemble à une app que l'on fait
pour apprendre, Carole confirme que c'est pour apprendre, pas d'autre objectif.

Tout le monde évoque des choses.

J'ai voulu lancer Christophe qui ne participe pas beaucoup, et là boum, il
déroule un pitch sur son projet : faire une app pour faciliter l'analyse et la
présentatio de données.. Je laisse dérouler, mais en fin de discour, après
l'avoir remercié, je reviens à la question de Carole en apportant mes 2
centimes... C'est peut-être normal que je fasse ça en fait ?

Un petit tour de debrief (pas plus car je suis pressé de partir). J'avais une
appréhension, l'impression que tous voulais avoir une formation initiation code
ou séance de mentorat, mais en fait tous disent être content, sans même suggérer
quelque changement ... :-)

Ce qu'ils ont aprécié :
- avis d'expert
- date fixe pour se retrouver
- échange avec d'autres en situation similaire
- partager ces connaissances acquise depuis peu
- réviser
- entendre de nouvelles idées
- aspect « psychologique » d'expliquer un soucis/blocage devant d'autres
  personnes

Zied à dit avoir été surpris, il ne saviat pas trop à quoi s'attendre (est-ce
que nous allons coder ?), Vanessa à trouver cela « relaxant », Denis à exprimer
un doure : « est-ce une bonne idée de faire du dev ? ». C'est en référence à une
réflexion que j'ai lancé et qui à été plussoyé par d'autre : certains dev sont
traité comme la 5ieme roue du carrosse, on leur demande de pisser du code... Je
lui précise que ça peut aussi se passer autrement... Ce mec est bizarre quand
même.

Les soucis qu'ils semblent avoir en commun, les trucs qui reviennent : 
- effet plateaux important. On lit un tuto ou un court en ligne et après ?
- isolation, manque d'entreaide ou d'accompagnement
- difficulté pour trouver une manière d'aborder un logiciel / programme
  (l'aspect construction ?)

J'aimerais placé ces points de blocage dans notre matrice, mais là j'ai du mal.
Un exercice pour une fois prochaine.

Je n'avais pas mis de date pour des prochaines rencontres, j'avais un gros doute
sur l'intérêt (3, puis 2 personnes juste ? ....) Mais ce soir, avec 7 personnes
et des échanges toujours intéressant, j'ai très envie de continuer et
d'améliorer le format.

Je pense aussi à des compléments, soit sous forme d'outils/jeux en ligne pour
travailler certains aspect (méthodes ?, lecture ?), soit sous forme d'une autre
rencontre où nous pourrions nous donner un cours à lire (avec des resources en
ligne ou pas), et un exercice, et la fois suivante le corriger ensemble, puis
recommencer. Cela nécessite de l'investissement. J'imagine à chaud que cette
deuxième proposition pourrait être payante...

Mik à encore parlé de [FreeCodeCamp](http://www.freecodecamp.com/). J'avais déjà
repéré, revu quand il en avait parlé la première fois... J'ai vraiment envie
d'essayer un truc aussi de ce coté là, Ce qui m'emmerde un peu c'est que c'est
que Javascript apparement... C'est peut-être une forme de JSLDD pour débutant ?
Peut-être que j'ai envie de faire un truc similaire, mais pas avec que du JS ? À
méditer.

Quelle belle journée ! Mon seul regret, j'ai pas vu les enfants et Sitina ce
soir :-(


## 15 février 2016

2 personnes sont arrivé à l'heure : Christophe, déjà venu, et une nouvelle tête
: Jean-Charles...

J'expose un peu les activités d'ut7 en attendant de voir si d'autre arrive.
Zied (déjà venu) et Alexandre (un nouveau) arrivent. Cool. Je refais un topo
rapide à Alexandre.

__Je les vois regarder un peu partout, j'aimerais prendre du temps pour faire
des posters, un peu comme les posters de propagande sur la sécurité dans les
ateliers : «La fraiseuse n'est pas un comptoir, vous y perdriez un doigt», et
autre «port du casque obligatoire» :-)__

Un petit tour de présentation. Là je commence à avoir des profils amusants : 
- Alexandre Montoya (Colombien d'origine), une école de méca je sais plus quoi,
  une école de commerce, et là un master 1 en Direction des système d'info à la
  Grande Ecole de Management ... Et il essaie d'apprendre à coder, un peu de
  html css, mais il aimerait aller plus loin. Cool. Je suis content de ne pas
  avoir trollé sur cette formation :-)
- Christophe, toujours graphiste qui veux faire une app de
  dataviz-machinelearning-bigdata-trophype-de-la-mort-qui-tue. 
- Zied, toujours intégrateur chez SAP, mais qui travail en bénévole pour le
  supermarché coopératif : [La Louve](http://dons.cooplalouve.fr/)
- Jean-Charles, Ph.D., Biomechanics ... se dit que pour monter ça boite, il va
  apprendre le code. Hésite entre elixir (est-ce un bon choix ?) ou apprendre
  la théorie des langages... Je l'ai trouvé un peu hautain, mais c'est
  peut-être un de mes apriori de banlieusard qui n'a qu'un bts en poche. Il
  était souriant, participant, et pas trop vantard sur la scéance.

Ensuite j'ai voulu les faire participer un peu plus que d'habitude. Quand je
proposait que quelqu'un explique un blocage pour que nous en parlions tous
ensemble, c'était silence radio, et un peu difficile de leurs tirer les vers du
nez. Là, j'ai opté pour des post-it, maximum trois par zone, avec 3 zones : les
trucs bloquant, les questions, les trucs cool que j'ai découvert.

Ça n'a pas trop mal fonctionné. Il y a eu un peu de tout, même si la zone
question est un piège, trop vaste sujet.

J'ai beaucoup de progrès à faire. Je trouve que je parle trop. Est-ce le soucis
d'être l'organisateur ET l'expert ?

Ça me donne une idée, peut-être que je pourrais inviter un expert justement ?

Un peu moins de monde aujourd'hui, j'avais gardé une limite à 20 pourtant.
Est-ce que le lundi c'est moins cool que le jeudi ? A voir.

## 1er mars 2016

9 personnes sur 15 inscrits \o/ c'est chouette.

Zied, Jean-Charles et Vanessa (un peu en retard) sont de retour, c'est vraiment
sympa. Dans les nouvelles tête, il y a un des copains de Andy, Théodore Stanton
co-créateur de ce groupe meetup. C'est marrant de le voir là. Pas sur que ce
soit intéressant pour lui, mais qui sait ?

Dans les nouveaux il y a :

* Anne-Sophie. Discalculique (pas sur de l'orthographe), elle s'intéresse à la
  programmation parce qu'elle a un projet entrepreunarial. Elle s'interesse à
  python, elle a fait un week-end python.

* Julien. Growth Hacker qui ne le dit pas. mais il fait de l'analyse de
  traffic, regarde les logs. Il essaie d'apprendre le JavaScript. 

* Camille. «J'y connais rien». Elle travail avec un presta qui fait un site
  pour sa boite (un gros truc dont elle semble ne pas vouloir donnerle nom).

* Anas Oualim : https://www.linkedin.com/in/anas-oualim-85b6b054/en. Il fait du
  JS, et voudrais en faire plus, mieux comprendre les framework et autres

* Elie https://www.linkedin.com/in/elie-adjedj-23ab2375/en . Il est passé ce
  matin en cherchant la maison du Bitcoin. Il fait de la vidéo, mais aimerais
  apprendre Java (il a commencé) pour faire des app Android. Pas sur qu'il
  souhaite devenir un dev d'app Android où si c'est pour un projet
  entrepreneur.

L'utilisation des post-its est intéressante encore.

J'ai oublié d'aborder la question de «Quand on fini ce soir ?» et le «Quand on
se vois la prochaine fois ?». Je le fais un peu à l'arrache. Globalement, à
part le vendredi et le lundi, ils ne semble évoquer aucun frein particulier.

Questions sur les structures/architectures informatique. Par où commencer, quel
langage, css positionnement, quel framework, point vocabulaire, ...

Nous évoquons des resources comme : livre et mooc, et la liste des meetups qui permettent de coder.

Un point FreeCodeCamp reviens sur la table. Je me demande si au lieu de faire
évoluer le meetup en cours (qui pour le moment me plais bien comme ça), nous ne
pourrions pas imaginer héberger une instance de ce truc... J'ai envie de
creuser ça.

Peut-être que la prochaine fois, nous pourrions imaginer construire une
resource ensemble (un poste qui explique l'architecture ? une liste de site
pour apprendre à coder ?). Je ne sais pas si c'est bien une bonne idée.



## 24 mai 2016

### Avant

Reprise avec un gros mois d'absence (congé et annulation de dernière minute
pour un soucis familliale).

Le format disucssion le soir commence à me peser un peu, j'ai l'impression de
ne pas avancer, de répéter les même chose. J'aimerais voir ce que nous pouvons
faire de plus, ou de différent. Est-ce qu'ils serait prêt à payer un peu ?
Est-ce que ça pourrait être en journée ? Je pourrais peut-être organiser un
truc le samedi matin ? Vérifier avec Sitina, peut-être que pour que ça soit ok,
il faudrait que je vienne avec un enfant. Balqis ? Aydan ? Peut-être pas une
bonne idée.

### Pendant

Je vois passer Christophe, un qui est déjà venu. Je vais l'attraper. En fait il
se pointe sous le 14, c'est à dire l'escalier de l'immeuble. _note_ il faut
préciser boutique, ou là boutique d'/ut7, et l'affichage en plus gros sera
surement bienvenu aussi. Une asiatique (choinoise ?) rodait dans les parrages
depuis une bonne demi-heure, elle viens en fait au meetup avec un ami
(compagnon ?, Frêre, cousin, ami ?) asiatique aussi.

Arrive ensuite deus ou trois personnes, puis deux retardataire. C'est un peu
chiant pour l'aspect présentation.

Je rate un peu le tour de table, mais on le fait en cours de chemin, tant pis
(au moins tout le monde était là).

Il y avait donc:

- Christophe, graphiste de retour, fait du python pour voir et avec un projet
  dataviz en tête.
- Fahd. Orientation business, beaucoup de tchatch... affiche une grosse
  motivation.
- Sophie, curieuse. Apaprement Chef de projets Digitaux. A regarder code
  academy sans trop se forcer. Ne sait pas trop par où aller.
- Pierre, arrivé tard, j'ai pas tout compris à ce qu'il fait au quotidien.
  C'est une chouette rencontre. Il fait parti d'une association qui, entre
  autre, fait des ateliers de programmation en python pour les enfants
  (http://www.legaragenumerique.fr/ ?). Il est venu voir, se poser des
  questions sur comment les gens apprennent à programmer... onpako@gmail.com
- Ilan qui souhaite apprendre à programmer pour retrouver le fun qu'il avait à
  faire des programmes sur sa calculatrice. Des trucs artistiques... Il a tenté
  processing, mais ça ne l'a pas amusé apparament, du coup il embraille sur
  Python. Il voudrait lancer un meetup python pour les débutants en python
  comme lui, pour qu'ils codent ensemble... ilanbrun@gmail.com
- Tan (?) qui est en fait en école d'ingénieur mécanique aux arts et métiers,
  et qui se rends compte que la programmation pourrait l'aider dans certains de
  ces projets. Son français n'est pas encore très bon, j'ai peur d'avoir parlé
  vite. Je crois qu'elle est chinoise, mais aucune certitude.- Xie (?) C'est un
  ami (frère ? compagnon ?) de Tan. Lui sait programmer en Java et Javascript.
  Son français à l'air mieux, mais il n'a que très peu participé. Je crois
  qu'il accompagnais Tan pour l'aider à comprendre...

Ilan à parlé de «Nature of code», un livre qui semble intéressant. Je ferais
bien de regarder ce que c'est.


Beaucoup de python encore, mais c'est normal, le meetup que je squatte est un
meetup pour apprendre à programmer en python à la base :-)

## Après

Je me pose plusieurs questions pour la suite à donner à ce truc : 

- Qu'est-ce que j'apprend ? En fait, je tourne un peu en rond sur ce que
  j'apprends.
- Est-ce que ça à de la valeur pour les participants ? J'ai l'impression que
  ceux qui reviennent le font parce que c'est sympa, pas parce qu'ils
  apprennent un truc. A vérifier peut-être ?
- Quelle energie je suis prêt à mettre dedans ? Vu la situation actuelle pas
  beaucoup. Je ne suis pas sur de pouvoir continuer à prendre un soir tout les
  15 jours...

Ce que j'ai en tête-mais-cest-pas-cuit :

- Monter une association qui permettrais aux adhérents de se retrouver pour
  apprendre ensemble ?
- Monter un slack/mailinglist/... pour proposer des temps d'aide en ligne ?
  (pas pratique d'aider en ligne mais bon).
- Organiser des rencontres courtes, mais individuel pour aider certaines
  personnes (ça me permet d'éliminer ceux qui veulent monter une boite, et de
  faire des rendez-vous le midi, ou juste en fin de journée, sans finir trop
  tard).
- Lancer un podcast/mooc/cours en ligne pour les débutants en programmation ?
- Faire des posters didactique à propos des questions de bases qui reviennent
  toujours ?
- Faire un livre pour les débutants en programmation ?

En fait j'ai envie de faire plein de trucs, mais il faut aussi financer tout
cela :-) Voilà en tout cas de la matière pour réfléchir, et tester le jeudi.


## 12 avril 2016

Trois personnes ce soir, mais elles sont arrivé presque en même temps. 3 nouveaux en plus.

Le problème avec les nouveaux c'est que je refait mon speech à propos d'/ut7 et de ce que nous faisons... C'est une forme d'entrainement d'un autre coté.

Il y avait ce soit :

- Ibrahim. Un taxi (pas un uber). Il a fait quelques mois de la première année d'un BTS info (IRIS ?) il y a 10 ans. Il a du arrêté. Là, avec un d'autres taxi, ils sentent le besoin de faire une application pour faciliter une partie de la gestion de leur temps. Il parle de « la concurrence qui est plus moderne ». Il s'est renseigné, et faire faire lui couterais trop chère, alors il essaie d'apprendre (ou reapprendre) à coder. Il a fait du HTML, CSS, et là se lance dans Java (il souhaite faire une application android).

- Marielle. Semble un peu timide ou impressioné, elle a surtout très faim. Je lui trouve un bout de bain allemand a manger, ça lui conviens on dirait, cool. **Note pour plus tard : préciser dans le meetup qu'il n'y a pas à manger sur place...**. Elle a fait une formation Electronique, puis u DUT G2i (Génie Electronique il me semble ?), du coup elle a fait un peu de C et de programmation indus. Là, elle fait du contrôle qualité de document chez un sous-traitant d'airbus... Il y a un trou entre les deux il me semble, mais bon. Là, elle voudrais reprendre la programmation qui est pour elle un moyen de création formidable. J'aime bien cette fille, je me demande quel est son niveau. Selon, ça pourait être une candidate intéressante.

- Damien. Sortie du Génie Civil. A joué avec Autocad, et fait maintenant du Revit (?). S'interesse au code pour améliorer son quotidien (faire des petits outils et autres bricolage pour faciliter son travail habituel).

Dans les points abordé, et dans le désordre :

- Marielle et une des premières personnes à mettre un post-it qui dit "code pas propre" dans les points de blocage :-). Elle identifie du code pas propre quand elle reviens dedans et qu'elle ne comprend pas son propre code. Ou que ça lui donne l'impression de pas ressembler à ce qu'elle avait écrit à l'époque du lycée-IUT
- Marielle demande comment travailler la programmation dans les trajets de RER, sur une tablette. Je lui dit de lire du code.
- La question du volume de langage reviens
- On parle aussi de « parcour » d'apprentissage
- Comment apprendre vite, combien de temps ça prend
- Une bonne session de partage de lien (Marielle)
- Comment choisir l'axe de progression ?

Je me demande comment ces questions persistent avec toutes les ressources disponible en ligne ? Est-ce qu'il manquerais des choses ? Est-ce que nous devons écrire un truc à ce sujet ? Est-ce tout simplement qu'il y en a trop (soit des listes de truc pour apprendre la programmation, soit des ressources) ?

Est-ce que des posters que l'on affiche et vend prix coutant pourrais être intéressant ? C'est une piste que j'explorerais bien. Avec celle d'écrire un ebook/site, avec nos mots et nos points de vue.

Peut-être devrait-on utiliser un wiki pendant ce meetup ?

Ibrahim pose la question du suivi. Je crois qu'il voudrais un cours. Il suggère que certaines séance soit dirigé. Par exemple, Marielle a mis un post-it à propos des rudiments de l'informatique, Ibrahim suggère une session qui ne traiterais que de ça. L'idée est intéressante, mais tourne au cours. Faut-il faire payer ? Peut-être qu'une fois sur deux, le meetup pourrais effectivement parler d'un sujet seulement (c'est déjà vaste !).

A suivre. Là je n'ai pas d'energie pour organiser un autre meetup, mais je me rend compte que j'ai pris les devants et le suivant est déjà réservé ! C'est un moyen de m'influancer. Intéressant.

## 17 mars 2016


## Les présents

- Carole
- Toavina [new]
- Zied
- Vanessa
- Elie

C'est un groupe restreint, mais de revenant.  17 inscrits sur meetup.com.

## Les choses nouvelles

Carole est inscrite pour une [formation AFPA «Concepteur Développeur
Informatique](https://www.afpa.fr/formation?p_p_id=_101_INSTANCE_formation&_101_INSTANCE_formation_assetEntryUuid=39b2b0bb-d59b-490b-a6df-0016ae6f853c)
qui débute en avril. Je redécouvre qu'elle a suivi des cours d'informatique à
la fac, jusqu'à un soucis. Puis (je ne sais pas quand) elle à commencer cette
formation, jusqu'à un «soucis de santé». C'est peut-être une bonne candidate
pour le rookie-club. Il faudrait que je lui en parle. J'aimerais rester en
contact avec elle pour suivre sa progression.

Toavina reviens de Londres. Il était dans la finance. Il a du se faire de
l'argent, il vie dessus pour le moment (pas d'inscription Pole Emploi). C'est
le datascientist de la soirée. Y'a vraiment un pattern/effet de mode en ce
moment sur le sujet. Un truc à creuser ? Il a démarré Python dans un livre, en
essai un autre (oublié de demander les noms). Il suit aussi un mooc sur
[udacity](https://www.udacity.com/) et c'est inscrit pour un online workshop
[SpringBoard](https://www.springboard.com/workshops/data-science/). Je ne
connaissais pas. Apparement, c'est un programme avec des cours, des exercices
et un suivi mentor sur hangout toutes les semaines je crois. Il y a un projet à
rendre à la fin. C'est payant : 

> Learn at your own pace, just $499 / month

sachant que

> We estimate the total effort required of you at 100 hours. Most working
> professionals are able to complete in 3 months. You're welcome to choose your
> own pace. The more time you devote, the less the program will cost.

Je me dit que nous pourrions vraiment faire des trucs dans le genre. Mais
est-ce que ça nous ferais plaisir ? Pas sur.

## Les sujets abordé

Nous avons beaucoup parlé de pratique et de méthode, plus que de programmation
directement. C'était amusant de voir ça, et d'entendre Zied le dire en debrief.

Il y a eu une scéance life-hacking avec des histoires de comment gérer mes
priorités, comment organiser mon temps, comment choisir...

Une peu de code quand même, Elie est allé vendredi dernier au Dojo de
Développement. Christophe à fait un kata en haskell :-). Elie demande pourquoi
tant de développeur semble intéressé par des langages de programmation
«antique». Nous corrigerons ensuite pour parler de langages «éxotique».

Taovina demande «comment travailler sur de vrai exercice» J'en déduis que nous
parlons de la motivation à apprendre. Nous évoquons les projets persos, et je
parle du logiciel libre et des implications dans des associations ou
coopérative comme Zied par exemple. Et ça me rappel que j'aimerais bien
travailler sur ces sujets : faire du logiciel libre pour apprendre à
programmer.

Carole aborde les problématiques de structuration sur l'approche. Comment
«faire» un logiciel. Définir le produit et les features prioritaire, mais aussi
comment réaliser une feature. Elle sait le faire, d'une façon, mais imagine que
l'on peut apprendre comment le faire bien. C'est une bonne question. Comment on
apprend ça ? Je parle d'au moins prendre du recul régulièrement :
rétrospective, débrief, post-mortem. Histoire d'apprende de ce que l'on viens
de faire.

Ils évoquent [CodingHour](http://codinghour.bemyapp.com/) un truc de bemyapp.
Encore un machine de concour où on boit de la bière, et on fait un concour (4
challenges).

J'évoque mon envie de faire un truc en plus avec «eux». Ça semble beaucoup
intéresser Elie, et un peu les autres je crois. A suivre.


J'apprécie beaucoup avoir un groupe qui reviens.

Pour le moment j'ai plusieurs idée à explorer:

- Transformer la session en une scéance de correction des exo et on en donne de
  nouveau.
- Créer une session en journéé, payante sur un theme particulier (avec des
  morceaux de programmation dedans)
- Trouver un format dans lequel nous coderions un truc sur du logiciel libre
  (Framasoft ?)
- Proposer du mentoring (payant ?) en journée, une heure ou deux par semaine

Mais je crois que j'ai besoin de temps. Peut-être pour porter un peu plus loin
ces idées, ou en trouver d'autres.


## 7 juillet 2016


## 2 personnes

- Léo. Il viens de Troyes pour participer à des meetups autour de la
  programmation. Il suit le mooc OCR sur le développement d'application
  Android. Objectif pas très clair: cherche la motivation pour avancer ? Mais
  il a réussi à faire une app qui fonctionne.

- Khadija. Déjà croisé lors d'un CodeLab.rb. Puis nous avons discuté en ligne
  et je l'ai vu une fois pour un cafe. Depuis le CodeLab, elle a décidé de
  s'inscrire au Cnam pour suivre des cours d'informatique. A la base elle
  travail dans le cinéma. Avec les cours, elle n'a pas pu venir au soirée
  prépatouille, là, c'est fini alors la voilà.

Soirée foot : match de la France en 1/4 de finale de l'EURO, face à
l'Allemagne. Ça explique peut-être le manque de personnes présente. Les
vacances aussi sans doute.

## Ce que nous avons fait

Fatigué de faire juste un cercle de parole (ça se vois, le dernier meetup date
de plus de presque deux mois), je voulais faire autre chose, plus prêt du code.
Dans la journée Raphaël m'a suggéré effectivement de coder.

Du coup, Khadija étant la première arrivé, et sachant ce qu'elle faisait au
Cnam, je lui ait proposé de me montrer les projets de fin d'année qu'elle a
réalisé :
- Un jeu de la vie en processing (UV NFP135)
- Un shell/système de fichier en Java (UV NFP136)

Je suis épaté par ce que ces UV du cnam propose de faire, c'est très chouette.
Il y a peut-être des gens à rencontrer là bas ?

Elle se fait aidé d'un de ces voisins qui lui a fait une config VIM, et qui
l'aide un peu sur le Java et autre. Lui fait parti des petits débrouillard, une
association qui mène des actions de vulgarisations des sciences. Grosse
association national que j'avais croisé à Simplon... forcement. Le monde est
petit. Le voisin s'appel Alexandre Dervish (pas sur de l'orthographe).

Qaund Léo est arrivé, j'ai fait une présentation en vrac d'/ut7, et de moi, et
je lui ait demandé de nous dire ce qu'il fait ici. Du coup j'ai demandé la même
chose à Khadija aussi.

Ensuite nous avons continué à regardé le code, en parler, jusqu'au moment de
manipuler un peu. C'était chouette de faire du code ensemble.

Par contre ça pose le problème du niveau des participants, qui est moins
visible lors de discussion. Là , Léo était un peu largué parfois, et ne le
disais pas. **Comment conjuguer le fait de programmer ensemble, et le fait que
tout le monde n'est pas le même niveau ?**

Au moment du debrief, Léo évoque le coté sympa de venir avec son bout de code
et d'en parler ou de le faire avancer, mais c'est dur de suire parfois (ce soit
en tout cas). Pour Khadija, elle est en recherche de personnes avec qui
programmer car seul, elle a du mal à se motiver (?).

Il y avais un début de communauté d'apprenant dans cette histoire. Mais quand
Khadija reprendra les cours, elles ne pourra pas revenir. L'idée de batir une
communauté me plait bien mais il restera la question de comment accueillir des
nouveaux qui auront un niveau différent ? Est-ce que chacun amène ces exercices
où bien est-ce que je doit prévoir quelque chose ?

A ce stade, j'aimerais faire encore quelques tests : rester sur meetup.com et
expliquer pour les prochains meetup ce que nous ferrons : regarder du code que
vous avez produit pour vous aider à avancer, et permettre à ceux qui débutent de
participer à des premieres lignes de programmation...

L'intention est toujours porté sur les vrai débutants. J'aimerais permettre à
ceux qui viennent pour savoir ce que c'est la programmation de se sentir bien
avec nous. Et à ceux qui apprennent seul, de trouver un moment d'aide pour
avancer.

J'ai envie de dire, comme souvent, j'étais pas super motivé avant, mais c'est
toujours sympa de rencontrer et d'aider des débutants. Khadija a une façon de
parlé à mi-chemin entre l'ivrogne et la racaille, mais elle est pleine
d'energie pour apprendre à programmer. Pour quoi faire, par contre c'est pas
clair.


## 19 juillet 2016


Kadija, déjà venu, est encore là en avance. Elle a eu son exam au CNAM. Elle
est bien motivé du coup ! J'apprend ce soir qu'elle est/était étalonneuse au
cinéma...

Vanessa est de retour aussi. Nous nous étions croisé hier soir à Women On
Rails.

Il y a Jean-David, qui après un parcouru d'ingénieur en automatisme, des
voyages, une formation de masseur, un passage dans une agence de voyage, et
quelques voyages a son actif, a décidé de suivre une formation de développeur à
l'IFOCOP. Une formation étrange : 4 mois théorique à l'école et 4 mois en
entreprise. Le coup demandé à l'entreprise : 1500 € net / mois, que l'IFOCOP se
met dans la poche (JD est financé pole emploi)

Stéfan se joint aussi à nous. Il a fait du service client chez Autolib, et il
en a marre de se faire insulter, du coup il essaie d'apprendre tout seul. Aidé
d'un pote graphiste, il essaie de faire de l'intégration de truc que son pote
lui file.

Le groupe est sympa, mais j'improvise trop. J'ai envie de faire du code, mais
je ne sais pas quoi, comment, pourquoi ? Du coup je propose de mettre sur des
post-its des trucs qu'ils voudraient apprendre ou découvrir... des questions...
Au final, Vanessa pose un truc sur la motivation, JD sur la stimulation, nous
en parlons vite fait, mais comme je voudrais changer le format pour faire du
code, du coup j'écourte un peu. Kadija veux faire de l'algorithmie et voir
comment on package un programme (comment il vie en fait ?), Stefan propose
bootstrap, jQuery et Responsie Design, logique.

Du coup on regarde ce que fait Stéfan, et on parle, on parle... html, http, le
web, le css... C'est un peu de ma faute. Je rentre trop dans le détail. Quand
j'y repense, j'ai pas du tout fait ce que j'aurais aimé voir.

En debrief, JD évoque «trop de trucs différents», Kadija parle des papotage
(elle n'aime pas le tour de table). Nous en parlons un peu et une idée ressort
: proposer un theme à l'avance, ou en définir un au départ, et s'y tenir. Le
hic pour moi c'est que je ne vois pas trop c'est quoi les «themes» de
l'apprentissage de la programamtion. On suggère aussi de faire une grille lié
au sujet abordé pour que chacun place un post-it avec son nom sur la grille.
C'est en renplacement du tour de parole qui me sert à avoir une vague idée de
ce que chacun cherche ici.

Dans le train j'ai repensé à tout cela, et je me demande dans quel mesure il ne
serais pas intéressant de faire de la lecture de code sur de l'opensource. Je
ne me sent pas laisser définir un thème (quand je vois ce qu'il se passe sur
cette soirée, j'ai un peu peur du resultat et des frustration), il faut que je
le définisse du coup. Mais c'est quoi un thème ?

Quand je regarde la carte, je vois la matières (var, boucle, structure, ...),
les gestes, les concepts, les principes... Mais pas de thèmes.

J'ai envie d'explorer la lecture de code, prendre un logiciel libre issu de
chez Framasoft ou du sgmap par exemple, et hop. On décortique, je pose des
questions, je fais travailler tout le monde à repérer les variables, les
boucles, imaginer ce qu'il pourrais se passer si on execute ce bout de code.

Une des difficultés c'est le niveau de chaque participant. Entre ceux qui ont
une idée du truc, ceux qui débarque...

Je suis déçu par cette soirée, mais c'est l'occasion de rebondir (j'ai bien
fait d'en programmer une autre bientôt, sinon, j'aurais laissé trainer ! :-))


## 2 août 2016

## 9 particpants.

Pour limiter la parole de présentation, j'avais fait deux post-its : «Déjà fait de la programmation ?» et «Pourquoi venir à ce meetup ?». Pour expliquer le fonctionnement, j'ai posé mes deux post-its : «tout les jours ou presque», «Explorer des façon d'apprendre à programmer».

Voici pour les participants.

Des habitués presque :
- **Kadija**, cinéma, cnam, parle beaucoup, mais grosse motivation. S'intéresse aux bases, aux concepts avant de coder des applications. Elle traine au Dojo de développement maintenant. Je crois qu'elle est fan. Elle n'a mis qu'un post-it avec deux points : «Code en java», «apprend python».

- **Léo**, antillais qui viens de Troyes. Approche opposé de Kadija, mais il est revenu, et apparement avance. Est-ce que la dernière séance l'a motivé ? Il a mis «Notion de programmation orienté object» et «motivation collective».

- **Mik**. Il était venu il y a longtemps. Je lui avait proposé en même temps que Yohann de venir au rookie club. Mais il devait partir en Russie (une histoire de femme je crois). Apparement il va faire des allé-retour. C'est aussi un de ceux qui était très porté sur FreeCodeCamp. Il a mis «JS/PHP, noob: Symfony», et «Apprendre de nouveaux trucs».

- **Carole**. Semble toutes fragile : fine, la voix tremblante dès que tout le monde l'écoute. Elle a fait un peu de programmation à la fac, puis autres choses, puis un accident si j'ai bien suivi. Et là elle veux se remettre à la programmation. Elle suit une formation AFPA. Elle a marqué «J'essaie de programmer presque tout les jours en ce moment» (sont écriture aussi est tremblante) et, sur 2 post-it «Apprendre des autres pour améliorer mon apprentissage» et aussi «me sentir moins seule».

Des nouveaux :

- **Juan**. Arrivé en retard, un fort accent d'amérique du sud, pas Brésil. Il sait déjà codé un peu. Apparement il faut de l'admin sys. Il a marqué «Mise a niveau sur PHP, JavaScript et Python».

- **Gummy**. Arrivé dans les premiers, plaisantant (pour essayer de se détentre peut-être ?), il est le doyen de la soirée. Il a écrit «Déjà programmé», mais c'était il y a longtemps (commentaire de lui), et «Trouver un déclic pour se remettre à programmer en Lua, JavaScript et Python».

- **Vanessa**. Elle fait 42. Elle a fait la première année, là elle est en congé (c'est la période des pisicnes là bas), et elle y retourne pour la prochaine saison. Ce qu'elle a écrit «Oui quasiment tous les jours» et «Rencontrer des dev, et apprendre comment programmer».

- **Amanda**. Un peu intimidé je crois. Elle écrit : «Non jamais», et «Notions de bases, langages & base de données»

- **Élodie**. Arrivé un peu en retard, je me suis demandé si elle suivait où si elle était paumé. J'ai eu l'impression qu'elle était dans la lune... Elle a écrit «J'ai pris des cours au CNAM (de C) et j'ai tenté d'apprendre le Java toute seul». Elle fait un master 2 en math je-ne-sais-plus-quoi. Sur l'autre elle marque «Rencontrer d'autres personnes qui codent».

## Ce que nous avons fait.

Je voulais leur faire lire du code, mais c'était pas évidant. Beaucoup de questions, et du coup d'explication. Le format ne me conviens toujours pas.

Pendant le dérief de fin, la plupart on demandé à coder. J'ai expliqué ma crainte : passer derrière tout le monde un par un pour répondre aux questions individuelles. Du coup je leur propose d'essayer la prochaine fois de codr tous ensemble en faisant tourner le clavier. Nous verrons bien qui viens.

Je me demande si un format à la JSLDD ne serais pas intéressant. Avec une activité custom qui serais : pour les débutants, avec moi, un petit exercice de programmation. Après tout, certains viennent surtout pour voir d'autres dev, pas forcement pour faire leurs premier pas...

Soit un dojo de dev pour deb, soit un PLDD(Programmer les doigts dedans), voir, comme le nom actuel «Premier pas avec les mains».

J'ai passé une bonne soirée, j'ai appris plein de truc, et en même temps je suis frustré parce que j'ai :
- trop parlé
- l'impression de en pas avoir beaucoup aidé Amanda qui était la vrai débutante du groupe
- pas utilisé les outils/conseil piagetien ou autres parlant de ne pas transmettre le savoir, mais de permettre que chacun se le fabrique.

Ce dernier points me parait tellement difficile avec des débutants plein de questions !

En partant je leur propose de venir sur le slack du rookie-club, ils sont tous assez chaud pour le faire... On dirait qu'il y a une communauté de développeur qui vont trainer par ici... Un vivier pour faire du recrutement ?


## 9 août 2016


Ce soir, j'ai vu débarqué un gars phénoménal... Avec une canne, un certain age
(55+ ?) qui se faufile directement pour découvrir les choses écrites, ce qu'il
y a sur les étagères, «Alors c'est là que tu bosses ?»... Je suis un peu
déconcerté, surpris. Valérie arrive, c'est une des deux filles qui était dans
le groupe que je «mentorais» hier soir à la rencontre Women On Rails. Le
William fait des blagues louche, insite sur le fait qu'il se branle quand je
lui demande ce qu'il fait toute la journée, charmant :-/. Gummy reviens aussi.
Apparement il se connaisse tout les deux, ils se chambre. Etrange duo.

J'attend un peu, et puis nous démarrons. Une nouvelle arrive, s'installe, et
finalement Kadija arrive aussi. Le groupe ne changera pas pour la soirée.

Il y a donc 5 personnes (sur les 19 inscrites sur meetup) : Kadija, Gummy était
là la semaine dernière. Les nouveaux sont : 
- Wiliam: Il a juste mis «membre de + de 200 groupes meetup» dans expérience de
  programmation, et rien dans la colonne «pourquoi venir ?». Décidement, un mec
  bizarre.
- Valérie: sait programmé en C/C++, débutante en Rails, HTML, CSS, JavaScript
  et Java. Elle vient parce qu'elle souhaite découvrir de nouvelle manière de
  programmer. Je me disait bien aussi qu'hier elle me semblais quand même assez
  à l'aise avec les principes de base.
- Hélène : Notions de Java, SQL et HTML. Elle viens pour apprendre.

J'avais en tête des faire un sorte de randori sur un RomanToNumber. Mais je ne
trouve pas de clavier qui me conviens dans la boutique, je ne me vois pas trop
passer mon ordinateur directement.

En plus j'hésite, je n'ai pas pensé à cela avant : Est-ce que je fait un kata
(tdd) ou bien un bricolage dans test ? Dans le doute, fait un petit pas, et du
coup je me dis tdd. Après une rapide présentation des étapes
(red-green-refactor), je me lance. Jasmine, Javascript, histoire de pas avoir
de soucis d'installation. C'est ce que je leur explique.

Ça se passe plutôt bien. Je dois freiner William qui cherche toujours à faire
un truc du futur. Hélène participe plutôt bien, mais elle propose des
implémentation direct, sans les tests. On dirait qu'elle a dejà un bout d'algo
dans la tête... Valérie est assez silencieuse, mais bon. Kadija par contre,
pour une fois, n'ose pas parlé fort, elle me donne l'impression de s'excuser de
poser une question.

Fin de séance, tous le monde est content. Trouve que c'est bien de regarder
faire, de pouvoir discuter de ce qu'il se passe. Hélène à des clients, donc
elle ne peux pas forcement venir souvent, il faudrait que ce soit plus tard ou
le samedi. Ils prennent en photo le code. Il faut mettre sur le github je
dirais ?

Bien que je ne sois pas fan d'avoir le clavier, ça permet peut-être de se
concentrer sur le contenu plus que la forme (le clavier et l'éditeur) durant
les 2 petites heures que dure la rencontre.

Faut-il que cela tourne en dojo pour débutant ? Ça me ferais plaisir, mais
c'est quoi un dojo pour débutant, un truc de ceinture blanche ? Tiens, un
système d'évaluation ? Est-ce qu'il faut prévoir un soir pour les ceintures
blanches (vrai débutant), une pour les jaunes (ceux qui savent un peu coder),
une pour les bleus,... Ce soir, je crois que j'étais en présence de personnes
qui en fait sont soit en poste de développeur, soit savent développé mais ne le
font plus. A part Kadija, et peut-être Hélène (j'ai un doute pour elle).

Je vais engager des discussions avec Valérie pour mieux saisir ce qu'elle fait
et ce qu'elle veux. Lui parler de LoC et JSLDD.

Chouette soirée.


## 18 août 2016


## Participants

- Quentin [nouveau] aka Grimbsi. Un petit jeune qui fait la
  [webSchoolFactory](http://www.webschoolfactory.fr/). Il entre en 3ieme année
  du cursus en 5 ans. Mais apparement il a des lacune en programmation.
  Apparement lié à un problème personnel (maladie, famille ? pas clair).

- Gummy aka Gunavadh Lim. J'ai appris ce soir que c'est un ancien de chez
  Allianz. Son profile [twitter](https://twitter.com/gunavadhlim/) et le
  [linkedin](https://www.linkedin.com/in/gunavadh-lim-6534a55b). Apparement il
  est en mode : je code pour moi une application pour aider à boursicoter.

- Hélène aka Lay's. Découvert ce soir qu'elle est chez Ernest & Young en tant
  que MOA autour de solution SAP. Mais en fait elle préfèrerais faire de la
  programmation. Reconversion ! C'est pour ça surement qu'elle ne peux pas trop
  venir, ça dépend du client chez qui elle est...

- Bertram (doute sur l'orthographe) aka Mr Meetup. Indien il me semble, ayant
  fait un peu d'informatique de gestion, mais ça l'emmerdait. Il dit qu'en
  France tout le monde fait des specs, pas assez de tech. Du coup il
  s'intéresse aux objets connecté...

## Ce qu'il c'est passé

Nous avons fait un Kata Bowling en JavaScript. Les premiers arrivés (Hélène et
Quentin) voulant faire du JavaScript, et n'ayant pas le courage de me lancer
dans du node, nous avons fait cela avec Jasmine.

C'est moi qui tenais le clavier, déroulais le code. C'est pas très
satisfaisant. Je parle beaucoup, j'explique, je montre. Si j'ai bien compris je
ne suis pas dans le constructivisme... Il faut que je trouve ce qui conviens à
un rythme aussi court : 2 heures tout les quinze jours, ou au mieux, toutes les
semaines.

J'ai utilisé le repo du rookie club. Il devient de plus en plus clair pour moi
que ces deux expériences sont lié, sont même la même chose en fait. Avec une
sorte d'énergie, de difficulté différente. Un autre rythme... Je trouve
intéressant de partager le code du coup. Je me demande même si je vais pas
utiliser le wiki pour prendre des notes. A terme, je pourrais même arreter
Meetup, je m'en sent de plus en plus proche.


## 8 septembre 2016


## Participants

Les revenants:
- Gum: J'ai eu l'impression de redécouvrir qu'il viens pour re-trouver un
  déclic en programmation.
- Léo: Celui qui viens de Troyes et qui apprend Java/Andriod, il est arrivé ne
  retard.


## Pendant

Gum m'offre un pot de miel. C'est un apiculteur amateur, il participe à une
association où ils apprennent à faire le boulot.

Ensuite, on regarde du code Lua. Il se retrouve, un peu comme pour nous au
rookie club, à faire du Lua parce qu'il utilise un outil de data analyse :
[Torch](http://torch.ch/).

On lit du code Lua du coup... On se retrouver rapidement à parler de la fiche
langage Lua, je me retrouve à essayer d'expliquer mon point de vue à Léo, faire
une démonstration.

C'est bien de faire en petit groupe. J'ai très envie de scinder cette
expérience en deux:
- Un groupe de parole, toujouers organisé via Meetup pour avoir de la rotation,
  voir des nouvelles tête, et faire une «vitrine dans la vitrine» d'/ut7.
- Un autre groupe, petit, limité, avec les mêmes motivé qui revienne, organisé
  en dehors de meetup. Peut-être en révisant les objectifs de chacun, faire un
  support sur Slack, des exercices... Rika ?

A suivre.

## 22 septembre 2016


## Participants

Les nouveaux:

- Lyuba: Une nouvelle qui viens pour mieux comprendre et être capable de
  discuter avec des équipes de réalisation. Vraiment premier pas. Elle a fait
  un peu de VBA il y a longtemps.
- Larbi: C'est lui qui est passé jeudi dernier. Il fait un peu de C tout seul.
  En fait c'est pour bricoler sur du Arduino principalement. Mais tout seul,
  c'est un peu dur.
- Nadir: à suivi un mooc python, sais se débrouiller, mais il stagne. Il
  souhaite passer un cap. Apparement, c'est pour le plaisir.
- Nicolas: Un accent de je ne sais où (Est ?). Il a fait un week-end (payant:
  160 €) au [Coding Days](https://coding-days.com/). Maintenant il aimerait
  passer à la suite.
- Saïd: Déjà programmer. J'apprend plus tard qu'il est chercheur/prof et fait
  du python dans ce contexte. Il s'interresse à la robotique/domotique.  8
  personnes.

Les revenants:
- Kadija: Un peu fatigué. Elle a ramené une bouteille de vin pour remercier de
  ce qu'on fait.
- Gum: J'ai eu l'impression de redécouvrir qu'il viens pour re-trouver un
  déclic en programmation.
- Léo: Celui qui viens de Troyes et qui apprend Java/Andriod, il est arrivé ne
  retard.


## Pendant

Je tente un split : 3 groupes. J'ai envie d'avoir un truc façon JSLDD, mais je
suis un peu obligé de le forcer (contrairement au JSLDD). Je pose un exercice:
un convertisseur RomainEnEntier. Je fais des groupes basé sur ceux qui semble
le plus à l'aise : 

- Saïd
- Nadir
- Kadija

Une fois les goupes fait, je passe les voir pour leur dire d'ecrire un
programme rapidement, quitte à ce qu'il ne fasse que renvoyer 1 quand on lui
passe un I.

- Nadir se retrouve avec Gum et Lyuba dans la pièce du fond. Narid pilote
  l'exerice en Python, en avançant pas à pas.
- Saïd se retrouve avec Larbi puis Léo qui arrive après. Saïd est parti très
  vite sur l'exercice, il a fait les additions, mais le groupe bloque et
  réfléchi sur les soustractions.
- Kadija, avec Nicolas, essaie de faire du Java. Elle lui raconte les histoires
  de variable, de type, et tout ça. Mais au moment d'essayer de compilé, on se
  rend compte qu'il n'a que la JRE. Le temps d'essayer de la télécharger, de
  l'installer ... Raté. Le temps passe, et on découvre qu'il a ruby sur ça
  machine. Il reste 10 minutes avant la fin, je me lance dans l'écriture de ce
  programme en mode light, histoire qu'il ait un bout de quelque chose en
  partant ce soir. Mais en fait ça va trop vite pour lui je pense.


Ensuite, on fait démo. C'est plutôt sympa. Quelques remarques et questions
intéressantes. Le format est intéressant, mais il ne faut peut-être pas
l'utiliser tout le temps.

Prévoir plusieurs format pour cette soirée semble une piste intéressante. Par
contre c'est peut-être bien de l'annoncer à l'avance pour que chacun se
positione sur le sujet.

La différence de niveau est ok sur ce format (pour les participants de ce
soir), tous semble avoir trouvé son compte.

A suivre.



## 4 octobre 2016


## Participants

- Carole, qui fini bientôt sa formation «developpeur-concepteur web» à l'AFPA,
  et a trouvé son stage de 3 mois.
- Gum, pas sur de ce qu'il fait.

## Soirée

On regarde un corrigé d'exercice en java de Carole, c'est diagramme de séquence
et code imprimé papier (elle n'a pas son ordi). J'ai décortiqué la série
d'appel, et tenté une représentation sous une autre forme (mais toujours
papier). Nous avons parlé UML, et architecture web.

Ensuite, Gum nous montre ensuite un exemple d'uage de Cypher, un langage
associé à Neo4J. C'est amusant. J'ai beaucoup parlé des base de données, des
différents style qui existe.


## Suite ?

Frustration encore : 8 inscrits, 2 participants. Garder le meetup pour avoir
une vitrine et recruter, c'est pas mal. Mais j'ai très envie d'essayer de créer
un groupe d'apprenants (rookie ?) en mode distant/soir/week-end... J'imagine
plusieurs mode d'interaction qui pourrait fonctionner. 

Le seul truc que j'aimerais avoir avant de commencer, c'est une sorte de plan
de ce que je souhaite atteindre. Ou au moins, proposer un outil d'auto
évaluation, ou d'évaluation par les pair, pour que chacun puisse se dire : «ok
je sais faire ça, et c'est bien»

Peut-être que c'est juste une excuse pour ne pas commencer.

Cette idée me rappel une envie de «parainage» dont j'ai déjà parlé avec
Ludwine. Sans vraiment savoir ce que je voulais y faire. J'imagine que ça
pourrais être un peu ça ?


## 5 octobre 2017


## Participant·e·s

- Yannick
- Christophe
- Manu
- Estelle
- Jean
- Paul

Estelle viens par Andy et le petit fablab de Paris. Elle travail pour une
compagnie de danse en tant qu'administratif. Elle s'intéresse au mouvement
makers. Elle a fait un peu de Arduino et se dit que pour ça et parce que son
bébé apprendra surement à coder à l'école, il faut qu'elle apprenne.

Paul et son pote Jean vienne de Strasbourg. Jean va y retourner, mais Paul
s'installe à Paris. Ils ont fait la 3wAcademy de Strasbourg, Jean poursuis la
formation avec eux en s'attaquant à Symphony (la formation de base ne couvre
que HTML, CSS, JavaScript, PHP et MySQL). Ils sont en mode curieux,
observation. Ils cherchent à étoffer leur réseau peut-être, et à prendre des
idées. Un petit coté espion, comme je l'ai déjà vu et déjà fait via meetup.


## Avant la session

On a choisi un fichier du projet
[youtube-dl](https://rg3.github.io/youtube-dl/) avec Christophe. C'est le
fichier
[RtspFD.py](https://github.com/rg3/youtube-dl/blob/f5d8743e0a1fdcbfed2bea4fb87bf5aaf40c1dfa/youtube_dl/downloader/rtsp.py).

Un petit fichier avec une classe qui fait un héritage et une méthode. Après
coup, je me dit que j'ai choisi celle là parce que justement, nous n'avons
beaucoup d'élément `class` et `method` à expliquer, découvrir.

On discute un peu avec Christophe. Je me rends compte que je ne lui ait pas
expliquer ce qu'il allait se passer :-D. Il a malgré tout un peu suivi car il
s'inquiète du vocabulaire (c'est quoi les éléments selon lesquelles tu veux
découper le code ? le nom que tu donnes à ça, ...) Et c'est vrai que je ne suis
pas au point là dessus. Disons pas très stable pour le moment. Il faudrait que
j'en fasse plus souvent, et peut-être un poster apparaitrait.

On parle rapidement des divers formats que j'ai envie d'essayer et d'affiner,
mais je lui explique aussi que j'aimerais constituer une communauté et
continuer sans meetup :-) Que ma priorité pour ce soir, c'est de faire un
premier truc simple.

## La session

On commence par discuter, pour occuper les premières minutes.

Et on parle un peu trop. Peut-être trop de détail sur ce que je pense, et pas
assez sur ce que j'attends de la session.

On fini par y aller.

1er round : le code c'est du texte, composé d'une suite de phrases, qui sont
une suite de mots. On va rangé les mots dans des catégories.  Code modifiable
(facilement):
- Litterals (chaine de charactère, booléens et nombre) _en bleu_
- variable (sur une suggestion de christophe) _en rouge_
- classe, fonction/méthode _en rouge_

Code plus figé:
- Élements clefs du langage (mots réservé) _en noir_
- Opérateurs _en noir_

On découvre que certains fonctions/méthodes sont en fait issu de la corelib
et/ou la stdlib. On en parle. Mais le découpage (affiché après) sur le code
accessible, et celui moins accessible fini par dire ok.

Il y a quelque chose à améliorer dans l'ordre dans lequel se fait ce découpage.
On peut imaginer le faire chacun sur une feuille, avec comfrontation tous
ensemble après...  Manu parle aussi d'utiliser les écarts de niveau pour faire
un peu de cuisine : les personnes qui savent le moins essaient de marquer les
mots, les personnes qui savent un peu plus les aident en donnant le moins
d'indication possible (tu as oublié un truc, tu t'es trompé sur celui là, ...)

2eme round : le code est organisé en bloque, chaque bloc à une strucure et une
raison.
- La classe, la méthode (les grosses boites dans lesquelles ont range les
  groupes d'instructions)
- les assignations
- les executions
- les bloc de controlle

On peut décliner le contenu des blocs de controller, ce sont des contenants.

Pour marque le code, il faudrait essayer du fond blanc, et/ou projeter par
derrière. -- Manu


3eme round : ce truc fait quoi ?

On parle des surprise, on décortique le comportement... C'est un peu acceléré,
nous sommes à la fin de la session. Pas sur qu'Estelle ait suivie. Mais c'est
amusant.


## Après

Pour le format 2eme round, ça serais intéressant de faire un arbre pour
représenter les blocs conteneur d'instruction de code, et les feuilles.
Attention, certains bloc on des conditions d'entrées (est-ce que ça fini par
faire du blockly ?).


Sur le 3eme round, un graph d'execution (façon graphcet) serait assez
intéressant.


Dans cette orientation, la lecture de code utiliser le code pour en faire une
représentation visuelle. C'est plutôt intéressant. Le pendant serait de faire
la lecture à haute voix... peut-être la prochaine fois.

J'aime bien la suggestion de Manu : si ça continue, avec des devoirs à la
maisons et un suivi/aide à distance, et qu'on se retrouve une fois par mois, ce
qui serait super c'est de se montrer un bout de code qu'on trouve remarquable,
et qu'on en parle, et qu'on l'affiche :-)


Je suis un peu déçu d'avoir eu aussi peu de monde, mais content qu'Estelle soit
venue.

