---
title: Semaine 44
---

_Du dimanche 1er au samedi 7 novembre_

## Dimanche 1 novembre 2020

Notes de lecture du livre « Woman, les bretonnes » d'après le film « Woman »

> Rendez-vous compte qu'il m'a fallu une autorisation de mon mari, qui était médecin, pour une hospitalisation longue ! Je ne vous parle pas de la France du Moyen Âge mais bien de celle des années 1960

> Du jour au lendemain, je me suis retrouvée seule avec les quatre enfants ! Je n'étais rien, je n'avais rien. J'étais la pauvre plaquée. Mais paradoxalement, ce jour là, tout m'était rendu. Je n'étais plus la femme de quelqu'un, j'étais moi. C'était finalement, un formidable cadeau.

> Prends ta main gauche pour attraper ta main droite... N'attends pas qu'une autre main vienne t'aider.

-- Claude


> J'ai trouvé « on endroit ». Ce n'était pas évident car, bien souvent, nous, les femmes, avons besoin de nous sentir légitimes pour nous lancer. Cette attitude n'est pas dans nos gènes : c'est plutôt le résultat d'un formatage éducatif, culturel.

> Je ne suis pas SA femme, mais la femme avec laquelle il a choisi de partager sa vie, de cheminer. C'est là que les mots «compagne » et « compagnon » me semblent plus appropriés à l'idée que je me fais du couple. Je suis attentive au sens des mots, et dans la définition de « compagnon », il y a à la fois les notions de « partager » et d'« accompagner ». Qu'est-ce que l'amour si ce n'est accompagner l'autre, cheminer à ses côtés, l'aider à se relever lorsqu'il tombe... Tout partager, même ce qui est difficile.

-- Blandine

> J'ai envie que mon expérience serve à d'autres. Et que ça change. Qu'il y ait moins de marches à gravir pour s'en sortir. À commencer par une plus grande réactivité des policiers ou des gendarmes. Je ne comprends toujours pas pourquoi on ne veut pas prendre votre plainte tout de suite. On va sur la lune mais on n'est pas fichu de nous protéger ! Ce qu'il manque ce sont des mesures d'urgences.

-- Christèle

> À partir du moment où j'ai compris que j'étais à l'origine de ce que je vivais à l'intérieur, j'ai compris que je pouvais le transformer et j'ai pu ainsi faire la paix avec ce passé là. J'ai pris la pleine responsabilité de ce que je vivais, des émotions et des mémoires que je portais. Ce pas vers la responsabilité a été mon premier pas vers la liberté.

> Il s'agit de faire le tri entre ce qui m'appartient et ce qui appartient aux autres, c'est ce que j'appelle le pardon. Ainsi je ne suis pas responsable de ce qui s'est passé. Je ne suis absolument pas responsable de ce que ces hommes m'ont fait, mais je suis responsable de ce que moi j'en ai fait après, c'est-à-dire, les peurs, la colère, la rancœur, etc.

-- Framboise

> Après avoir photographié cette famille tzigane et m'être retrouvé, malgré moi, photographiée par eux, l'envie m'est venue de jouer avec mon image... Je souhaitais montrer qu'on a tous une part de l'autre en soi. Il est important de se fondre, de se mélanger à tous les milieux et de ne pas porter de jugement sur l'apparence et l'appartenance.

-- Isabelle

> J'ai vécu la violence toute ma vie. Éclatée tout le temps. J'ai vu trop de choses... Malgré tout, j'ai avancé. Et je n'ai jamais voulu rendre aux autres ce que j'avais vécu. La méchanceté ne fait de bien à personne.

-- Marie-Françoise

> Je suis devenue experte en pauvreté au sein d'un groupe de formation de travailleurs sociaux ! C'est hallucinant ce que produit notre société ! Sans reconnaissance sociale de ce que l'on est vraiment, peu à peu, on n'existe plus, peu à peu on devient fou.

-- Isabelle

J'ai été ému par certains de ces témoignages. Toutes ces vies incroyable, toutes ces douleurs lié à notre société, lié aux hommes, lié à une pression sociales, éducatives... Il faut que ça change, à commencer par moi :)


---

[My Python testing style guide](https://blog.thea.codes/my-python-testing-style-guide/)

---

Dans la grande famille des astéracées, [_Pilosella officinarum_](https://fr.wikipedia.org/wiki/Piloselle)

> Elle est commune sur les sols arides et pauvres. Elle contient des substances antibiotiques et diurétiques. 

---

[Région naturelle de France](https://fr.wikipedia.org/wiki/R%C3%A9gion_naturelle_de_France)

[Carte des pays traditionnels de la Bretagne](https://fr.wikipedia.org/wiki/Pays_Glazik#/media/Fichier:France_Pays_bretons_map.svg)

---

[10 sorcières et consœurs avec qui passer Halloween](https://1001heroines.fr/10-sorcieres-et-consoeurs-avec-qui-passer-halloween/)


## Lundi 2 novembre

[Docusaurus.io](https://docusaurus.io/) est l'outil utilisé par [Women on rails pour partager des ressources](https://women-on-rails.github.io/ressources/docs).

Je suis un peu triste que ça ne soit pas un peu plus statique. Ça me fait me poser des questions sur l'usage sur un vieux navigateur ? Comment tester ce genre de chose ? Comment avoir un vieux navigateur sous le coude ? [Lynx](http://lynx.browser.org/) est-il un outil intéressant pour tester le fonctionnement sur un « vieux » navigateur ?

En dehors de [browserStack](https://www.browserstack.com/guide/test-on-older-browser-versions) existe-t-il des solutions _locales_ (au sens, sur ma machine, pas sur la machine de quelqu'un d'autre) ?

---

Une journée avec le bureau debout. Ça sera mieux avec un écran plus grand. J'ai aussi besoin de faire un peu de yoga, et du gainage. Je le sent dans mon dos. Intéressant de voir qu'en restant assis, je ne sentais pas du tout ça.

Pour le journal, est-ce que c'est important de parler de mes projets et thématiques du moment ? Est-ce que ça m'aiderais à remplir des zone dans ce journal ?

---


## Mardi 3

> ⚠️✈️ 4 Rafales doivent effectuer des exercices d’appontage simulés sur la piste de Lann-Bihoué ce mardi 3 et mercredi 4 novembre, de jour 🌞 comme de nuit🌛.
> ➡️ Des nuisances sonores à prévoir, mais ces entraînements sont indispensables à l'entraînement des pilotes des flottilles amenées à embarquer sur le porte-avion Charles de Gaulle !

[Communiqué BAN Lann-Bihoué |](https://www.facebook.com/villehennebont/posts/1665312690335549)

_La vie dans le pays de Lorient_

---

[Optéos, la CAE qui tourne au budget contributif !](https://www.opteos.fr/actualites/opteos-la-cae-qui-tourne-au-budget-contributif/)

## Mercredi 4

Discussions intéressante sur le slack des artisans du logiciel à propos de ce que chacun et chacune ferait s'il ne faisait pas d'informatique... Les réponses sont intéressantes. Ça renforce mon questionnement sur le fait de changer de boulot, de me lancer à reprendre une activité de plantes médicinales ou d'arboriculture.

Sans doute que l'approche douce, à mi-temps est la plus sécurisante pour ma famille. Une orientation pourrait passer par un habitat partagé, puis la recherche de terrain/verger pour des abeilles et/ou des arbres et/ou des plantes aromatiques. Ça pose la question de la distillation. Faut il avoir une formation pour distiller des plantes ?

---

> Vous voulez distiller des huiles essentielles :
> C'est plus simple disais je, mais il vous faut quand même déclarer a priori votre activité et vos achats et déplacements d'alambics aux douanes. Un alambic qui distille des plantes aromatiques (PPAM) à l'exclusion de toute autre chose (c'est à dire de l'alcool) s'appelle parfois un "extracteur de plantes aromatiques" et est rarement soumis aux obligations de scellés (d'ailleurs les alambics à alcools ne sont pas toujours scellés : l'usage régional décide).
> Si vous êtes agriculteur (exploitant agricole ou même cotisant solidaire), si votre activité précise "production et transformation de PPAM (Plantes à Parfums Aromatiques et Médicinales)", vous pouvez demander à vos douaniers l'achat et l'utilisation d'un alambic sans crainte de voir votre jardin d'Eden se transformer en enfer administratif.

[Devenir distillateur… comment faire ? + le statut de "petit opérateur"](http://www.devenir-distillateur.com/blog/dura-lex/devenir-distillateur-comment-faire-le-statut-de-petit-operateur.html)

---

Encore du très bon Arnaud sur [le Domain Driven Design sour l'angle stratégique](https://www.lilobase.me/le-domain-driven-design-sous-langle-strategique-une-introduction/).

---

Tentative d'[installation d'un correcteur orthographique dans Vim](https://www.saintcarre.fr/saintcarre/2018/08/correcteur-orthographe-vim.html).

> - `z=` sur un mot souligné affiche une liste de corrections possibles
> - `zg` rajoute un mot dans le dictionnaire
> - `zug` pour annuler l’ajout au dictionnaire
> - `]s` pour aller au prochain mot mal orthographié
> - `[s` pour le précédent


En cherchant à ce sujet, j'ai également découvert un [vimpédia](https://vim.programmingpedia.net/) avec une partie en français [vim.programminpedia.net/fr/Home](https://vim.programmingpedia.net/fr/Home).

Ce projet semble aller plus loin [programmingpedia.net](https://programmingpedia.net/). Où est le code source de ces pages ?

---

> Un Jardin-Hortus ?
>
> Un Hortus (du latin = jardin) est un jardin pour la nature et les humains.
> Ce concept a été conçu par Markus Gastl (Allemagne).
>
> Il est structuré en trois zones :
>
> - la Zone-Tampon qui entoure le jardin et les protège de l'extérieur
> - la Zone-Hotspot maigre, une réserve de biodiversité
> - la Zone de Production qui concentre la culture de légumes et de fruits.
>
> Ces trois zones constituent un modèle qui aidera tout jardinier à améliorer son jardin.
>
> Aucun produit de l'agrochimie ne vient perturber l'équilibre qui s'installe.

[Hortus france](https://www.hortus-france.org/)

Orientation intéressante ! Et très complémentaire à d'autres.

---

Faire une extension pour détecter les RSS/Atom ? Il y avait ça avant, et beaucoup de site utilisant des plateformes CMS ou autre ont ces flux mais ne le savent pas et ne les mettent pas en avant. Dommage.

Ça pourrait aussi être un outil à brancher avec [rss-bridge](https://docs.rsshub.app/en/social-media.html et rss-bridge) ?

Ou bien [rss-hub](https://docs.rsshub.app/en/social-media.html)

Peut-être qu'il n'y a rien à fabriquer, mais uniquement faire quelques branchement. Via [clochix](https://mastodon.social/@clochix/104726717858806333).

---

Ces fameuses journées où tu passes ton temps à lire du code, essayer, fouiller, te lancer dans un refactoring de folie pour corriger un problème, et une fois que toute cette énergie dépensée, tu te retrouve à commiter une ligne de code qui rectifie ton soucis.

Commande du jour à utiliser sans modération : `git checkout .`

[le commit en question](https://github.com/betagouv/rdv-solidarites.fr/pull/945/commits/978a064ea83c165b93866c315e4cdf508369cd6f).

Je suis de moins en moins convaincu par la syntaxe Rspec. Je trouve ça difficilement lisible. C'est sans doute pourquoi je n'ai pas réussi à faire l'effort de trouver ce qui ne fonctionnait pas dans un premier temps et que j'ai préféré envisager de reprendre tout le fichier :D

Puis, après avoir tout trituré, il y a la petite voix intérieure qui te dit : « en fait c'est con, tu pourrais juste ajouter une ligne ici et puis voilà... » :D

---

> Histoire Noire est un jeu de 54 cartes qui vous invite à découvrir des personnalités noires trop peu connues du grand public car trop souvent mis au ban de l’Histoire officielle.

[Histoire noire - jeu de cartes](https://fr.ulule.com/histoirenoire/)

---

J'ai enfin pris le temps d'envoyer un message plus complet aux personnes des [Chemins de travers](https://www.cheminsdetraverse.bzh/). J'ai été honnête sur le fait que je ne reprendrais sans doute pas leur activité, mais que ça m'aide à réfléchir, à avancer. Je vais reproduire ici me message que je leur aient envoyé.

Je m'attends à ce qu'ils ne me répondent pas. Tant pis.

> Bonjour,
>
> Je comprends très bien. Je n'ai pas encore donné beaucoup de contexte.
>
> Sérieux, je crois que je le suis, mais par contre, je ne pense pas être à la hauteur pour reprendre votre activité ! Et pourtant ce que vous avez fait m'enchante. Aussi, j'aimerais découvrir ce qu'il me manque pour me sentir prêt à reprendre ce type d'activité, voir à me lancer pour construire la mienne.
>
> Je vais poser mes questions ici, puis je me lancerais dans mon histoire. Ça vous permettra, selon votre temps et votre envie, de ne répondre qu'à mes questions, ou de ne lire que mon histoire, voir de faire les deux
>
> Quoiqu'il arrive merci ! Votre annonce me fait avancer dans ma réflexion. Et bien que ça ne soit pas la fin de mon message, je vous souhaite déjà plein de bonne chose.
>
> ## Questions
>
> - Est-ce que vous travaillez tout les deux à temps plein sur l'aspect plantes aromatique/transformation/distillation ?
> - Quel revenue (individuel ou familiale) arrivez vous à tirer de cette activité ?
> - Comment avez vous trouvez ce terrain ?
> - Qui sont les voisins : élevage ? bio ? céréales ? maraîchage ? forêt ?
>
> - Je n'ai aucun document officiel qui dit que je sais faire pousser une plantes, que je sais faire une greffe, que je sais reconnaître quelques plantes. Faut-il que je me lance dans une formation officielle ?
> - Est-ce qu'il faut une autorisation particulière pour distiller des plantes ?
>
> Pardon pour toutes mes questions, pour certaines vraiment basique. Je comprendrais tout à fait que vous ne répondiez pas à toutes
>
> ## Mon Histoire
>
> Depuis plusieurs année, j'amorce une transition. J'ai voulu travailler avec un paysan prêt de chez moi, et finalement, c'est avec un apiculteur que j'ai passé tout mon temps libre pendant 2 ans. Ça m'a amené beaucoup de question sur les plantes.
>
> Moi qui croyais en savoir un peu plus que les autres parce que je distinguais un chêne d'un châtaignier... La bonne blague.
>
> J'ai commencé à prendre des cours en ligne (très bon mooc de tela botanica) et j'ai rejoins un jardin partagé pour mettre en œuvre, me frotter au terrain.
> C'est en faisant qu'on apprends le mieux. Le mooc et les cours théorique apporte le vocabulaire, le glossaire. Permette de découvrir qu'un concept existe. Et c'est tellement plus efficace de le manipuler sur le terrain !
>
> J'ai également été pris sous l'aile d'un papy-serial-greffeur. Il m'a appris la taille des fruitiers, et amorcé ma connaissance sur les greffes.
>
> Il y avait tant à apprendre, et j'avais envie de changer de boulot, mais je ne savais pas trop quoi faire... Alors j'ai exploré.
>
> J'ai pris 3 jours avec une « sorcière » à la montagne. J'ai découvert l'alambic et la distillation, les hydrolat et les huiles solaires. Magique.
>
> Un élément était toujours présent : en fait je ne connais pas les plantes...
>
> J'ai donc opté pour un cours de botanique, en me disant qu'il me fallait les bases. J'ai pas osé prendre un truc trop gros, trop sérieux. J'ai suivi un cours, qui nous amenais tout le temps sur le terrain, via une association de ma région de l'époque : Communeherbe.
>
> Puis nous avons enfin mis notre plan de déménagement pour plus de verdure à exécution. Cette été nous sommes venue nous installer à Hennebont.
>
> C'est tellement plus simple d'être sur place pour pouvoir prendre le temps de rencontrer les lieux, les personnes, les projets pour arriver à comprendre ce que nous voulons faire au fond de nous.
>
> Nous avons commencé à faire quelques rencontres: l'écohammeau de Brangoulo, l'association cueillir, le Labourioù, le collectif syklett...
>
> Et un soir, je me suis dit que peut-être il me faudrait rencontrer des personnes qui souhaite transmettre leurs activités, ou bien qui cherche une personne en plus pour le collectif. Je suis donc retourné sur TerreDeLiens (encore une fois :)) et j'ai parcouru les annonces.
>
> Autant je ne me sent pas de faire du maraîchage, aligner les poireaux, c'est pas ce qui me plaît le plus dans ces activités.
>
> Ce qui m'intéresse :
> - être dehors
> - prendre soin de la nature
>
> J'ai envisagé de m'occuper d'arbre, de faire l'apiculteur. Des activités que j'ai explorer. Sur les plantes aromatiques et médicinales, mon exploration à été plus courte sur l'aspect matériel de distillation, mais le sujet m'intéresse beaucoup.
>
> Et pourquoi je ne me lance pas ?
>
> Je suis un hyper privilégié de notre système capitaliste : je suis un homme blanc qui travaille dans l'informatique. Bon, je commence peut-être à rentrer dans la team vieux (43 ans), mais pour le moment, je ne le sent pas comme un désavantage.
>
> Et en plus, j'ai réussi à évoluer dans des univers de SCOP, ce qui me permet d'être aujourd'hui dans une position plutôt confortable, à être en possession de mes moyens de production, à décider pour moi même, et tout ça en ayant un salaire permettant de faire vivre ma famille de 6 personnes (3 adultes dont moi, et 3 enfants de - de 10 ans).
>
> Alors forcement, la transition n'est pas simple. Nous devons apprendre à renoncer à certaines choses, et apprécier les autres que nous gagnons en échanges.
>
> Après en avoir parlé avec plusieurs personnes, j'ai le sentiment que l'approche la plus idéal pour moi serait de démarrer tranquillement une activité à temps partiel. Et de basculer (ou pas ?) au fur et à mesure.
>
> - L'apiculteur avec qui j'ai travaillé avait la moitié de ses revenues qui provenaient de l'apiculture, et l'autre moitié d'un autre boulot à temps partiel;
> - Une amie en savoir est éleveuse de vaches à viande (activités hérité de son père), et développeuse à mi-temps;
>
> Alors je me doute bien que votre activité nécessite un temps plein, nécessite d'y aller à fonds. Et c'est, entre autre, pour ça que je ne pense pas être un candidat sérieux.
>
> Je vous sollicite surtout pour apprendre, découvrir, écouter votre parcours, comprendre votre posture, votre chemin. Votre histoire m'enrichira et m'aidera sans doute à faire la mienne.
>
> Merci énormément, et encore plus si vous m'avez lu jusqu'ici
>
> Je vous souhaite plein de bonheur.
> À bientôt peut-être.
> -- Yannick le-bavard

C'est brouillon, assez maladroit. Est-ce que j'ai besoin de tout étaler ? Est-ce que j'arriverais à raconter une histoire plus courte ? Avec des ramifications pour creuser ?

---


> The most minimalist creative coding environment is alive: 
>
>[https://tixy.land](https://tixy.land/)
>
> Control the size and color of a 16x16 dot matrix with a single JavaScript function. The input is limited to 32 characters – but no limits to your creativity!

[Martin Kleppe @aemkei](https://twitter.com/aemkei/status/1323399877611708416)

---


> Récemment j'ai fait un petit tour des tables des matières d'ouvrages classiques pour trouver des références sur le couplage dans le code. Voici ce que j'ai récolté, sans ordre de préférence.

> C’était chouette de faire ce petit tour de tables des matières dans ces ouvrages, maintenant il me faut du temps pour les lire ou relire, et mettre en pratique 😯 Bonne lecture, bonne pratique de vos apprentissages !

[Quelques références sur le couplage dans le code](https://dev.to/sroccaserra/quelques-references-sur-le-couplage-dans-le-code-9g5)

Activité vraiment intéressante, merci !

---

> La réussite de la surveillance et de la gestion des ressources végétales nécessite de favoriser l’implication de la société civile pour soutenir les gestionnaires des réserves naturelles. Parce qu’il est difficile d’identifier correctement et rapidement les plantes pour les non‐spécialistes, le développement de techniques basées sur l’identification visuelle automatique devrait faciliter et accroître l’engagement du public dans les initiatives scientifiques citoyennes. Le réseau Tela Botanica participe ainsi depuis de nombreuses années au développement de telles initiatives.

[Les sciences participatives à l’honneur dans un article de la Société écologique Britannique](https://www.tela-botanica.org/2020/10/les-sciences-participatives-a-lhonneur-dans-un-article-de-la-societe-ecologique-britannique/)

Oui, c'est génial.

Et je ne peux m'empêcher de penser à la différence entre apprendre à utiliser un programme et apprendre à programmer. Sans forcement devenir un botaniste, je pense, je crois (sans doute à cause de ma naïveté sur le sujet) qu'il est possible de s'éduquer à l'identification des plantes.

Comme pour l'informatique, il y a un vocabulaire qu'il faut simplifier, ou limiter pour le rendre accessible; ensuite il y a le tri visuel, attirer l'attention sur une partie plutôt qu'une autre... Et, petit à petit, en guidant les personnes, j'imagine que chacune pourrait apprendre un peu plus l'observation des plantes et de la nature.

C'est peut-être à envisager en complément ? Il faudra sans doute associé une photo de toute façon, qui permettrais de faire des vérifications.

J'imagine un outil de portrait robot aussi.

Un parallèle également : utiliser wordpress vs apprendre à publier avec un site statique sur github ou autre.

---

[Un site de téléchargement de livre sur les plantes](https://www.arbres-lozere.fr/index.html)

---

> Plantes pour sol acide : les fleurs
> 
> Sur des sols acides, vous pouvez planter des fleurs comme :
> 
> - les azalées,
> - les camélias,
> - les bruyères,
> - les digitales,
> - les fougères,
> - les myrtilles,
> - la prêle,
> - les rhododendrons.
>
> Plantes pour sol acide : les légumes
>
> Vous pourrez également planter des légumes comme :
>
> - les aubergines,
> - les artichauts,
> - la chicorée,
> - les épinards,
> - les cardons,
> - les choux,
> - les tomates,
> - les haricots verts,
> - les petits pois,
> - la menthe,
> - la ciboulette,
> - le pissenlit, etc.

[Plantes pour sol acide](https://www.jardiniers-professionnels.fr/plantes-pour-sol-acide/)

> Les Méconopsis, des pavots rares pour les sols acides
>
> Réussir les Pavots de l’Himalaya est le rêve de tout jardinier ! Cette plante quasi mythique est considérée comme difficile à réussir. Pourtant si vous lui donnez ce qui lui plait : ombre ou mi-ombre, une terre fraîche mais bien drainée et un sol acide, elle n’est pas plus compliquée qu’une autre vivace (Michael vous donne quelques détails dans cet article : « Comment réussir le pavot bleu de l’Himalaya »). Elle nous fait toutefois des petites frayeurs une fois sa floraison terminée car elle a la malencontreuse idée de mourir juste après (car c’est une plante monocarpique). Pas de panique cependant, les petites graines disséminées ça et là auront tôt fait de germer l’année suivante en un mini-champ de pavots bleus. On connaît bien le Pavot de l’Himalaya pour sa magnifique floraison bleue apparaissant entre mai et juillet  mais on connaît un peu moins Meconopsis cambrica, le pavot du Pays de Galles, qui lui fleurit jaune. Et qui est bien moins difficile pour le sol et l’exposition que son cousin à fleurs bleues.

[10 vivaces à cultiver en sol acide ](https://www.promessedefleurs.com/conseil-plantes-jardin/ficheconseil/10-vivaces-a-cultiver-en-sol-acide)

[43 plantes acidophiles qui pousseront bien sur un sol acide](https://www.forestplatform.fr/guide/43-plantes-acidophiles-qui-pousseront-bien-sur-un-sol-acide/)

---

À lire ? [50 activités à faire avec les végétaux](https://www.tela-botanica.org/2020/10/50-activites-a-faire-avec-les-vegetaux/)

---

> Le rapport du WWF a raison de souligner que l’« explosion de la consommation humaine », et non la croissance démographique, est la principale cause d’extinction massive. Et il se donne beaucoup de mal pour illustrer le lien entre les niveaux de consommation et la perte de biodiversité. Mais il ne va pas jusqu’à souligner que c’est le capitalisme qui oblige à une consommation aussi irresponsable.

> En occultant le capitalisme par un terme qui n’est qu’un de ses symptômes – la consommation – on risque également de rejeter de manière disproportionnée la responsabilité de la perte des espèces sur les choix de vie individuels, alors que les systèmes et les institutions plus vastes et plus puissants qui obligent les individus à consommer sont, de manière inquiétante, laissés pour compte.

> Le rapport du WWF choisit « l’humanité » comme unité d’analyse, et ce langage totalisant est repris avec empressement par la presse. 

> Le mot fourre-tout « humanité » recouvre toutes ces fissures, nous empêchant de voir la situation telle qu’elle est. 

[C’est le capitalisme qui tue la faune sauvage](http://www.contretemps.eu/capitalisme-ecologie-climat-faune-sauvage/)

---

> Beaucoup de personnes autour de toi t’aiment et te protègent mais il arrive que des gens ne respectent pas les enfants même s’ils ont l’air gentils. Il existe, par exemple, des pédophiles ou des gens un peu malades dans leur tête qui font des gestes sexuels à des enfants : toucher le sexe, demander des caresses, se montrer nu. Il faut qu’un enfant sache que ça peut arriver, même si c’est interdit, pour savoir comment se protéger, en parler à quelqu’un pour se faire aider et comprendre que ce n’est jamais de sa faute.
>
> Passe ton permis de prudence en répondant aux questions du test, pour voir si tu sais éviter les situations dangereuses, et découvre ton résultat à la fin.

[Passe ton permis de prudence](https://permisdeprudence-preview.vercel.app/brochure/enfants)

---

> Better spaces to gather around
>
> Spend time with your friends, coworkers, and communities like you would in real life

[Gather.town](https://gather.town/)

---

Un projet intéressant de « tier lieu » à Hennebont. Est-ce encore actif ?
[Départ imminent pour l'hôtel de la gare](https://lhoteldelagare56700.wordpress.com/)

---

Différence entre l'usage de `./bin/rspec` et `bundle exec rspec` ?

---

[Rails ActiveRecord before_type_cast](https://api.rubyonrails.org/classes/ActiveRecord/AttributeMethods/BeforeTypeCast.html)

---

> A Meaningful Life Depends on Doing Real Things

[Do the Real Thing](https://www.scotthyoung.com/blog/2020/05/04/do-the-real-thing/)

---

[RFC 8927: JSON Type Definition](https://www.bortzmeyer.org/8927.html)

JSON suit le chemin de XML ? JTD me semble être l'équivalent de DTD pour XML... Le début de la fin ?

---

Tellement de vocabulaire en botanique. 

[Glossaire botanique illustré #1 – #BotaChezMoi](https://www.tela-botanica.org/2020/11/glossaire-botanique-illustre-1-botachezmoi/)

Ça me fait penser que c'est un point d'apprentissage intéressant. Mais, il me semble que c'est intéressant que le vocabulaire soit tiré par les connaissances, et non poussé par les sachant.

1. identifié un élément ;
2. apprendre à le reconnaitre ;
3. le nommer.


---

[1001 courges, descriptions de variétés](https://www.1001courges.ch/varietes_description.htm)

---

J'essaie de faire une commande qui me permettrais de savoir si j'ai un journal ouvert quelque part. Je me retrouve à explorer des commandes comme `lsof`, `pidof`, et puis finalement à revenir sur `pd`.

`ps -e -o pid,cmd | grep elsif | grep vi | awk {'print $1'}`

Je ne suis toujours pas à l'aise avec `awk`.
J'apprécie toujours autant les `|` Unix.


[La philosophie Unix](https://fr.wikipedia.org/wiki/Philosophie_d%27Unix) est vraiment magnifique, et permet tellement de chose. Pas facile de l'amener dans le monde du logiciel plus haut niveau j'ai l'impression. Nous pourrions nous dire que les microservices en sont les représentants, mais je ne trouve pas que ça corresponde.

Une des difficultés vient de la communication entrée/sortie de ce genre d'application, justement trop haut niveau sans doute.

Pour revenir à mon soucis, le hic c'est que c'est la commande qui va ainsi être listé. Et si jamais j'ouvre un journal autrement que par la commande journal, je ne pourrait pas m'en sortir.

Pour mettre au point la commande, j'ai ouvert le fichier source du script, et un journal. J'arrive à retrouver la commande qui à ouvert le fichier, mais si je ferme le fichier de journal seulement, la commande elle est toujours existante (l'autre fichier n'est pas fermé). Et je lancerais une alerte comme quoi ce fichier est ouvert.

En même temps, en général, j'ouvre mon journal part le biais d'une [commande maison](https://github.com/yaf/dotfiles/blob/master/bashrc.d/command.bashrc#L20). Est-ce que je n'aurais pas moyen de retrouver une trace de cette commande ? Non, apparemment, j'ai la commande `vi` qui est listé.

Et finalement, si j'utilisais une technique bien courante : listé le fichier ouvert dans `/var/lock` ?

Exploration de l'usage de `flock`. Finalement, c'est une solution qui me semble ok pour ce que je voulais faire. À suivre.

[Ma commande de journal utilisant Flock](https://github.com/yaf/dotfiles/blob/840a9cdab7a96333114df61f6dccbd57f953c179/bashrc.d/journal.sh)


---

Fabuleux Marc-André Selosse encore une fois. [Conférence/formation sur les mycorhizes](https://www.youtube.com/watch?v=Sh7VYUCTjIQ). Une production [ver de terre prod](https://www.verdeterreprod.fr/). Super collectif encore !

Et une belle référence à [Kropotkine](https://fr.wikipedia.org/wiki/Pierre_Kropotkine) :)
J'y découvre la [mésoporosité](https://fr.wikipedia.org/wiki/Porosit%C3%A9)


