---
title: Mardi 1er décembre 2020
---


[Le néo-syndicat des travailleurs indépendants]( https://www.independants.co/).

Et si je me syndiquais enfin ! Depuis le temps que ça me gratte. Je continue à avoir une grosse préférence pour [Solidaires](https://solidaires.org/) par contre.

---

Entendu dans une réunion avec des représentants départementaux

> C'est un nœud gordien

Mais c'est quoi un nœud gordien ?

> L’expression nœud gordien désigne, métaphoriquement, un problème qui ne
> présente pas de solution apparente, finalement résolu par une action
> radicale. Par extension, la solution apportée à ce problème est radicale, ce
> qui a forgé l’expression « trancher le nœud gordien ». Cette expression
> trouve son origine dans la légende d’Alexandre le Grand.

[Nœud Gordien sur wikipedia](https://fr.wikipedia.org/wiki/N%C5%93ud_gordien)

Quand même les personnes qui bossent dans l'administration évoque ce genre de
problème interne, c'est qu'il y a une belle marge de progression :)

---

> L’actualité éditoriale rencontre aussi l’actualité politique puisque le
> reconfinement de l’automne 2020 offre une deuxième raison de se tourner vers
> les textes de Gorz. En effet, un débat public s’est ouvert sur la question
> des commerces et besoins essentiels. Or, Gorz soulevait déjà cette question
> dans les années 1960 : de quelle production sociale avons-nous besoin ?
> Comment ôter au capitalisme le pouvoir de détermination de la production,
> qu’il utilise pour maximiser ses profits plutôt que pour satisfaire les
> besoins sociaux ? Si toute production et consommation est toujours
> destruction de ressources finies, alors il faut choisir la production sociale
> à privilégier : comment déterminer une production sociale capable de
> satisfaire les besoins sociaux et compatible avec les exigences écologiques ?

> A chaque fois que le statut de Sujet ainsi défini est attribué à Dieu, à la
> Vie, à la Planète, à l’Ecosystème (…) autrement dit au-delà des êtres
> humains, nous avons affaire à un anti-humanisme qui constitue le terreau
> idéal sur lequel la dictature, l’oppression, le totalitarisme peuvent fleurir

> quand ils choisissent une technologie, la grande compagnie ou l’entrepreneur
> ne recherchent pas seulement l’efficacité technique mais aussi un monopole
> local et régional et un pouvoir de contrôle conféré par une technologie
> donnée

> C’est pourquoi il critique la « technocratie » qui utilise « la technologie
> pour renforcer et développer la centralisation du pouvoir »

> Le projet gorzien d’affranchissement du travail capitaliste et de la sphère
> marchande créerait alors un cercle vertueux : en désengageant le travail de
> la croissance productive incessante et de la rentabilité, on prend déjà la
> voie d’une politique écologique de production, et on laisse aux travailleurs
> le temps de participer au débat démocratique pour confirmer et accentuer
> cette transition.

> On entend ainsi parfois que la transition écologique impliquera de travailler
> plus pour sauver nos conditions de vie. Le projet décroissant de Gorz ne va
> pas en ce sens.

> C’est seulement en séparant les producteurs directs des moyens de production
> et du résultat de la production qu’il a été possible de leur faire produire
> des surplus dépassant leurs besoins et d’utiliser ces « surplus économiques »
> à la multiplication des moyens de production et à l’accroissement de leur
> puissance. A supposer, en effet, que les moyens de production industriels
> aient été développés originellement par les producteurs associés eux-mêmes,
> les entreprises seraient restées maîtrisables par eux, ils n’auraient cessé
> d’autolimiter et leurs besoins et la nature et l’intensité de leur travail.

_Et vive les Scop et autres coopératives et collectifs de travail_

> D’autre part, le capitalisme a détruit la norme du suffisant dans le ressenti
> des besoins et partant, dans la consommation, en la stimulant
> artificiellement, que ce soit par une dégradation volontaire de la qualité
> des produits ou par des techniques de marketing visant à stimuler la demande.
> Ainsi le capitalisme a étendu sa domination à la force de travail et à sa
> reproduction, réduisant l’individu social à un « travailleur-consommateur (…)
> client du capital en tant qu’il dépend(ait) à la fois du salaire perçu et des
> marchandises achetées »

> Nous serons au moins aussi bien habillés, chaussés, logés, transportés,
> lavés, etc., qu’aujourd’hui, tout en vivant mieux mais autrement. Car, avant
> tout, nous travaillerons beaucoup moins puisque les produits, beaucoup plus
> durables, seront fabriqués en quantités plus petites. La course au rendement,
> le travail abrutissant et affolant, les ʺcadences infernalesʺ n’auront plus
> de raison d’être

> L’utopie ne consiste pas, aujourd’hui, à préconiser le bien-être par la
> décroissance et la subversion de l’actuel mode de vie ; l’utopie consiste à
> croire que la croissance de la production sociale peut encore apporter le
> mieux-être, et qu’elle est matériellement possible

[De quelle écologie avons-nous besoin ? À propos d’André Gorz](http://www.contretemps.eu/ecologie-gorz-anticapitalisme-democratie-decroissance-revenu-universel/)

Chaque extrait, article, échange à propos de Gorz me donne envie de le lire...
À suivre ?

