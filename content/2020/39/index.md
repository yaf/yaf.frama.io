---
title: Semaine 39
---

_Du 27 septembre 2020 au 3 octobre 2020_

Le retour sur un journal public. C'est parti. Je vais adopter le format
semaine. Au moins pour la publication. Ça me laissera la liberté d'avoir des
jours où je n'écris rien et surtout, de prendre le temps de faire un peu de
ménage.

Je vais continuer à écrire tous les jours (ou presque) dedans, mais je vais
affiner toute la semaine ces notes.

Je pourrais supprimer les paragraphes que j'écris pour faire sortir une émotion
de ma tête et l'observer tranquillement. Une fois fait, c'est souvent superflu
de la partager.

## Merci Edgar

Cette semaine, nous avons fait du support Merci Edgar. Deux personnes utilisant
le site nous ont alerté un matin que rien ne répondait. Effectivement, le nom
de domaine avait expiré hier. Christophe m'a donné accès à la gestion sur
Gandi.  J'ai fait le renouvellement. Ça fera parti d'une note de frais. Un nom
de domaine, ce n'est pas grand chose.

Je suis content d'avoir pu aider les personnes utilisant l'application.

Prochaine étape, changer l'hébergement ? Mettre en place une mailling list pour
faciliter la discussion (et peut-être sortir de Facebook ?).

---

Discussion Merci Edgar assez intéressante. Ça fait du bien d'avancer sur le
sujet. Reste à passer à l'acte sur le code.

Je me demande comment migrer d'une techno à l'autre sans couper le service et
en essayer de le faire petit à petit, sans utiliser une reécriture complète.

J'ai souvent vu et travailler avec des éditeurs ou service interne qui mettait
en « pause » toutes les corrections et évolutions d'un logiciel pour le refaire
« iso-fonctionnel » dans une autre techno. Je trouve ça très triste vis-à-vis
des utilisateurs.

Merci Edgar est en Ruby On Rails. J'aimerais l'amener vers du Python, ou bien
autre chose d'ailleur, pourquoi pas.

Petit à petit.

- Attaque par couche. L'accès aux données pourrait être refait et proposer des
  services http qui serait appelé par le code ruby. Puis petit à petit,
  remonter.
- Par tranche fonctionnelle. La feature de calendrier bascule dans une nouvelle
  techno, puis c'est au tour de la gestion utilisateurs, ...

En dehors de ces deux axes, je ne suis pas sur. Il y aurait peut-être le fait
de passer sur un front le plus autonome possible, appelant des services.
Utiliser HTTP comme une couche d'abstraction. Ça nécessiterais de mettre en
place ce qu'il faut pour gérer le soucis d'accès à deux serveurs distinct
depuis une même page.

À voir. J'ai à me renseigner sur le sujet.

## Jardin

Comment œuvrer pour un « monde meilleur » ?

Une partie de moi se dit qu'il faut changer d'activité professionnel pour être
plus proche de la terre, ou en tout cas, d'activité qui ne nuise pas à la
planête (ou au moins, le moins possible pour le moment).

Une autre partie se dit que prendre le temps pour les personnes autour de nous
est le plus important, tant pis si le boulot n'est pas top pour le moment...

Dans l'avenir, nous aurons peut-être plus besoin d'être ensemble que de savoir
faire pousser des carottes. Parce que nous pourrons apprendre à faire pousser
des carottes plus rapidement que d'apprendre à être ensemble...

Ça me fait pencher la balance dans la direction du « reste en Scop, à faire du
code le plus correctement possible, et utilise ces privilèges pour prendre soin
de la famille, aider dans les asso locale, faire un petit jardin, un autre
partagé, monter un rucher associatif, faire des formations, .... »

D'autant que mon boulot est plutôt agréable malgré tout.

---

Je découvre la [Charte de l'agriculture
paysanne](http://www.agriculturepaysanne.org/la-charte-de-l-agriculture-paysanne).
Très intéressant.

Et le [programme d'accomapgnement
Adear](https://www.jeminstallepaysan.org/accompagnement-adear) à explorer si
finalement l'envie de changement d'activité me reprend.

---

[Comment stocker les graines et faire des associations
?](https://www.facebook.com/groups/grainesdetroc/permalink/3345772058821787/).
Voici un échange sur à ce sujet.

J'ai découvert et rejoins ce groupe de troc de graines sur facebook.

Un travail que j'ai commencé est utilisé beaucoup : classer les graines par
date de semis ! Ça pose un problème pour les graines qui se sème sur une large
période par contre... Ça me rassure malgré tout.

Il y a les personnes qui garde par ordre alphabétique (l'histoire ne précise
pas si c'est le nom latin ou bien vernaculaire)...

Et pour les associations, il y a les tableaux, excel ou pas, les petites bases
de données (Wahou !) avec Acces voir encore mieux, LibreOffice Base \o/ ! et
puis les personnes qui note dans un carnet leurs expériences pour s'y référer
d'une année sur l'autre.

Bernard notais tout dans un carnet. Il faisait des dessins de ses
installations, les dates de semis, les dates de récoltes, les quantités, les
variétés...

J'ai l'impression que c'est ce que je pourrais faire. Là, je tente des choses,
mais je ne note rien. Et le noter ici, ne me semble pas pratique.  Alors que
noter dans un carnet pourrait ce faire pendant ce petit temps de repos une fois
le travail terminé ! Reste à avoir un carnet et un stylo / crayon dans mon
matériel de jardin.

---

> C'est paradoxal, surtout lorsque l'on sait que l'Afrique possède les plus
> importants gisements géologiques de phosphore, souligne Christine Alewell.
> Mais le phosphore, qui en est extrait, est exporté à un coût bien supérieur à
> celui que la plupart des fermiers africains peuvent payer, en comparaison des
> fermiers européens.

> Le phosphore ne se présente pas sous forme libre dans les sols : on le trouve
> fixé à des minéraux sous la forme de phosphates. À cause de l'érosion, ces
> minéraux sont délogés des sols et finissent dans les étendues d'eaux et les
> zones humides où l'accumulation de phosphore et d'azote provoque une
> croissance excessive des plantes et des algues (on parle d'eutrophisation).
> Cette prolifération cause l'appauvrissement, puis l'asphyxie du système
> aquatique qui est atteint.


[Les plantes affamées par le déclin du phosphate dans les
sols](https://www.futura-sciences.com/planete/actualites/developpement-durable-plantes-affamees-declin-phosphate-sols-34070/)

## Société

Mon premier voyage dans une grande surface de bordure de ville, en vélo. Je
sais que le sac à dos c'est pas top, et en attendant mieux, je fais avec.

Plusieurs apprentissage :
- un vélo sans vitesse, c'est sympa, mais je vais avoir besoin de me remettre
  en forme pour faire des trajet sans trop suer et tousser ! J'ai eu des
  sensations assez proche de ce que je vivait ado, après les vacances, quand on
  reprenais les entrainements. Le bon décrassage de rentrée :) Je devrais
  peut-être me prévoir un peu de route le long du Blavet, sans montée, histoire
  d'y aller doucement.

- Le sac à dos, c'est vraiment pas top. Sacoche ? Chariot ? Sans doute à
  combiner avec la réfléxion : comment transporter Malaïka ? Maintenant que
  Sitina a aussi un vélo, nous allons pouvoir nous déplacer à toute la famille
  ! J'ai l'impression que le chariot me conviendra mieux pour transporter la
  petite. Ça me permettra aussi de pouvoir transporter quelques courses plus
  facilement :)

- Le volume et le rythme de ravitaillement doit changer ! Nous nous procurons
  la plupart de ce que l'on mange au marché depuis un moment. Chez les
  producteurs locaux. Depuis que nous sommes à Hennebont, il y a un rythme un
  peu différent qui nécessite un peu d'adaptation. Ceci dit, nous n'achetons
  quasiment plus rien en grande surface. C'est tant mieux. Reste un caddie une
  fois par mois voir tout les deux mois, un gros caddie pour le lait, les
  couches, les huiles, les conserves. Là, en vélo, ça va plus passer :D C'est
  sans doute le moment de pousser un peu plus loin les courses dans des petites
  épiceries localo-bio-zero-dechet-plus-sympa. Quitte à y aller une fois par
  semaine, plusieurs fois par semaine même...

Beaucoup de chose à apprendre, défaire et reconstruire :D

Les personnes à qui nous avons acheté le vélo de Sitina dimanche matin nous ont
rappelées aujourd'hui : elles ont un autre vélo à nous donner cette fois. Très
bien, on prend !

---

> À propos du livre de Sophie Bernard : Le nouvel esprit du salariat, Paris,
> PUF, 2020.

> Certains auto-entrepreneurs n’ont ainsi qu’un seul client, ce qui les
> assimile à des salariés déguisés.

> Et d’un autre côté, les pratiques managériales favorisant l’autonomie et la
> responsabilisation des salarié.e.s tendent à rapprocher leur condition de
> celle des travailleurs indépendants

[Salariat et capitalisme : la nouvelle donne. Entretien avec Sophie
Bernard](http://www.contretemps.eu/salariat-uber-nouvel-esprit/)

---

Je vais finalement partir en voiture pour Paris, je ramène une machine a laver
et un sèche linge au retour, pas facile en train.

Est-ce que je publie mon trajet sur
[Mobicoop](https://www.mobicoop.fr/covoiturage/proposertrajet) ?

---

> La politique actuelle isole les gens et émiette les collectifs

[Montpellier : après "Indignez-vous", “Le refus de parvenir“
!](https://www.midilibre.fr/2014/03/21/l-anarchisme-c-est-le-refus-de-parvenir,837239.php)

À lire ? Refuser de parvenir idées et pratiques, recueil coordonné par le Cira
de Lausanne, éditons Nada, 205 page, mars 2016, 20 euros

[Nada Editions - REFUSER DE
PARVENIR](https://www.nada-editions.fr/?product=refuser-de-parvenir)

Découverte dans [le dernier post mastodon du nouvelle
essai](https://mamot.fr/@LeNouvelEssai/104969388837892754) (Isabelle Attard et
son compagnon).


---

> L’oppresseur ne se rend pas compte du mal qu’implique l’oppression tant que
> l’opprimé l’accepte. -- Henry David Thoreau


> Comment l’arithmétique de l’entre-soi se met-elle en place ?
> 
> Il y a 3 étapes :
> 
> Étape 1 : la socialisation de l’individu dans l’organisation. Cela commence
> par des moments conviviaux, chaleureux ( “vient boire un coup, je vais te
> présenter à des gens cool qui font des choses sympa”). Je mets un pied dans
> cette organisation, je rencontre de nouvelles personnes.
> 
> Étape 2 : le conformisme. Dans le secteur associatif et l’entrepreneuriat
> social, le conformisme arrive par l’acceptation des codes et des valeurs
> dominantes et du langage créé par et pour ces structures. Le risque majeur
> est que ces pratiques ne soient connues que des personnes engagées : bonne
> chance pour l’individu extérieur qui souhaite pourtant s’investir. De plus,
> le manque de recul vis-à-vis des codes arrive doucement, consciemment ou non,
> et permet d’assurer un contrôle ferme des bénévoles. L’objectif : chacun
> reste dans le rang sans faire trop de vagues.
> 
> Étape 3 : la soumission à l’autorité. Les fondateurs/responsables de ces
> mouvements vous font croire que seule leur cause est juste, que seule elle
> mérite d’être défendue car il y a des valeurs fortes au sein de
> l’organisation, et que vous devez faire votre maximum pour faire triompher
> cette cause. Ces personnes vous font croire d’une façon insidieuse et jamais
> explicite que vous êtes le meilleur et que vous devriez vous sacrifier pour
> la cause. 
> 

> 
> J’ai décidé d’arrêter de changer le monde pour aller vers cette voie de
> l’acceptation de soi. Ensuite, le chemin que vous prenez est le vôtre, il est
> propre à chacun et doit être respecté.
> 
> Voici donc mon conseil à tous ceux qui souhaitent changer le monde et renouer
> avec une vie pleine de sens : prenez le temps d’apprendre à vous connaître,
> cela vous aidera à avoir un impact sur vous-même, ce qui constitue
> aujourd’hui à mes yeux préalable de tout engagement durable.

[source movilab - Réflexions sur la tocixité et les Tiers
Lieux](https://movilab.org/index.php?title=R%C3%A9flexions_sur_la_toxicit%C3%A9_et_les_Tiers_Lieux&fbclid=IwAR1HW4FUqPMkldDvAX5-qQHc4OjosJkqSLUiELJD_g0Jb6cxmfNqCc0pRnI#Pourquoi_j.E2.80.99ai_d.C3.A9cid.C3.A9_d.E2.80.99arr.C3.AAter_de_changer_le_monde)


## Commandes & programmation

`date +%G%U`

Ça m'a aider pour construire ce journal hebdo.

---

Humble Bundle qui propose un bundle NoScratchPress sur l'apprentissage de la
programmation en s'amusant ! Trop chouette.

[Learn to code the fun
way](https://www.humblebundle.com/books/learn-to-code-the-fun-way-no-starch-press-books)

Beaucoup de référence que j'ai déjà, ou pas envie d'avoir. Je ne prendrais pas
le bundle. Par contre, j'en profite pour lister quelques livres qui
m'intéresse.

- à lire ? [Think Like a Programmer](https://nostarch.com/thinklikeaprogrammer)
- à lire ? [Coding in the Classroom](https://nostarch.com/coding-classroom)
- à lire ? [Code Craft](https://nostarch.com/codecraft)
- à lire ? [Algorithms for the
  Adventurous](https://nostarch.com/algorithms-adventurous)
- à lire ? [Algorithmic Thinking: A Problem-Based
  Introduction](https://nostarch.com/algorithmic-thinking)
- à lire ? [Art of R Programming](https://nostarch.com/artofr.htm)
- à lire ? [Learn You a Haskell](http://learnyouahaskell.com/) En ligne ?
- à lire ? [Land of lisp](http://landoflisp.com/)
- à lire ? [Learn You Some Erlang](https://learnyousomeerlang.com/)
- à lire ? [Impractical Python
  Projects](https://nostarch.com/impracticalpythonprojects)
- à lire ? [The Secret Life of
  Programs](https://nostarch.com/foundationsofcomp)

---

La vertu de tout mettre à la poubelle en fin de journée pour recommencer de
zéro le lendemain.

C'est dans un livre sur l'eXtrem Programming que j'ai découverte cette notion.
Aujourd'hui, encore une fois, je me suis retrouvé à vérifier ce principe.

J'ai supprimer tout ce que j'avais fait sur la branche. Au passage, ça a fermé
la PR sur github au moment de partager le code là bas. J'avais espoir que ça
nettoie la branche uniquement... Tant pis.

La partie principale à été rebatie très rapidement. Il me reste justement à ne
pas tomber dans les même pièges que la dernières fois sur les quelques
ajustements que j'aimerais ajouter à cette PR.

---

> En cela SAFe est une redoutable machine de guerre du conseil en entreprise :
> cela nous rappelle que dans la ruée vers l’or ce sont bien plus les vendeurs
> d’outils que les chercheurs qui se sont enrichis. L’analogie avec la ruée
> vers l’eldorado l’agile est saisissante.

[Ce que nous apprend la House of Lean sur
SAFe](https://thehypertextual.com/2020/09/29/ce-que-nous-apprend-la-house-of-lean-sur-safe/)

---

[Des images Debian contenant des firmware non
libre](https://cdimage.debian.org/cdimage/unofficial/non-free/images-including-firmware/current-live/amd64/iso-hybrid/).
Très utile pour les machines dont le driver de carte wifi n'est pas libre :-/

---

Suite à deux expériences pro pas super agréable, une amie a décidé de
s'enfermer pour s'entrainer dur en se disant que c'est ça qu'il faut faire.
Nous discutons ensemble du fait que peut-être que ce n'est pas la bonne
approche.

La compétence qu'elle pourrait faire progresser pour se sentir plus autonome
sur un projet c'est la navigation / recherche dans ce projet.

Peut de personne en parle et encore moins s'en préoccupe au moment
d'accompagner des personnes à la découverte de la programmation. Pourtant c'est
ce que nous faisons chaque jour. En arrivant sur un projet, la première chose à
faire c'est savoir l'executer sur sa propre machine. Ensuite, c'est vérifier
que si nous modifions quelque chose dans le code, ça a un impact. Et déjà là,
nous commençons à devoir comprendre où est le code fait en sorte d'afficher
telle chose à l'utilisateur...

Nous avons parlé de travailler sur des logiciels libre, en lui racontant mon
histoire avec Publify.

J'ai proposé de faire le PO sur des projets que j'ai en tête, voir sur Merci
Edgar ! Puis, nous avons envisagé les projets de beta.gouv.

Et pourquoi pas sur RDV-Solidarité ? Ça pourrait se faire dans le cadre d'un
stage de moins de 2 mois. Ça permettrais aussi qu'elle rencontre les autres de
Scopyleft et que peut-être nous l'embochions ?

À voir.

Nous avons aussi parlé de mon envie de relancer le
[rookie-club](https://rookieclub.org/). Elle ma suggérer d'utiliser Discord.
Après Raphaël, c'est la deuxième personne qui me suggère ça. Alors j'ai
démarrer le serveur. Je vais inviter les personnes qui traine sur la mailling
list de venir, et couper cette mailling list. Je vais aussi poser un message
sur le discourse, et, petit à petit rappatrier sur un site statique dans
l'[organisation github du rookie club](https://github.com/rookie-club).


---

Intéressant de voir David qui répertorie des bouts de code « utile » [en
python](https://gist.github.com/davidbgk/b5989d129dabebea1ce8d253d4b7a6c7) et
en
[javascript](https://gist.github.com/davidbgk/8700969263bdb9d2a31ccc1ec2328f00).

Est-ce que nous sommes en présence de design patterns en quelque sorte ?

---

> La tech n'est pas éthique La tech n'est qu'un "outils" !  C'est nous qui
> rendons son utilisation éthique... ou pas.

> Sobriété Numérique
> 
> 80% des fonctionnalités demandées par l’utilisateur sont peu ou jamais
> utilisées !
> 
> Bing à réduit sa charge serveur jusqu’à 80% en réduisant de 20% le nombre
> d’éléments de sa page de recherche !
> 
> 70% du coût d’un logiciel est constitué par sa dette technique

[Une chouette conférence parisWeb en
ligne](https://slides.com/doc_roms/deck/fullscreen)

---

Amusant, moi aussi je me suis mis à utiliser Pandoc pour
[elsif.fr](http://elsif.fr) (via pana.sh dans un premier temps, puis finalement
j'ai arrêté de construire un outil générique...)

[Pandoc as a site
generator](https://ordecon.com/2020/07/pandoc-as-a-site-generator.html).

[Un Makefile à
étudier](https://github.com/ivanstojic/pandoc-ssg/blob/master/Makefile)

---

[Une api pour manipuler les calendriers dans
NextCloud](https://www.open-collaboration-services.org/) (et peut-être d'autres
?).

Je me dis qu'on pourrais ne pas gérer l'affichage des rdvs dans
RDV-Solidarités, et laisser les personnes qui font des calendrier gérer les
calendriers :)

Philisophie unix ? Ben je crois que oui. RDV-Solidarités apporte quelque chose
dans la « configuration » de règle pour le calcul de créneau. Une fois
selectionné, ça pourrait tomber dans les applications de gestion de calendrier
et puis voilà...  On est la pose de rendez-vous.

