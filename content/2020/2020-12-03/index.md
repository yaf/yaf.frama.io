---
title: Jeudi 3 décembre 2020
---

[Les plantes médicinales : un avenir prometteur ?](https://theconversation.com/les-plantes-medicinales-un-avenir-prometteur-147955)

> On dispose par ailleurs d’inventaires recensant les plantes d’intérêt et les
> modalités de leur utilisation à travers l’Antiquité : ainsi dans le papyrus
> Ebers, daté du XVIe siècle av. J-C, le safran, le lotus bleu, l’encens, le
> chanvre et bien d’autres espèces végétales sont-elles listées parmi les
> quelque 700 substances auxquelles la médecine égyptienne d’alors faisait
> appel.

> En pratique, la plante est en quelque sorte une usine chimique miniature.
> Fonctionnant à partir de l’énergie solaire et des éléments minéraux fournis
> par le sol, elle synthétise et accumule dans ses tissus un ensemble de
> composés d’une grande diversité.

Encore un endroit où la nature nous monte le chemin : accueillir la diversité
comme une richesse, une force.

_Ça me fait pensé à l'article [Du bon usage des barbelés](https://lmsi.net/Du-bon-usage-des-barbeles) que Pierre à partagé_

> Un spectre hante la gauche : le « No border »

> Nous sommes quelques-uns en effet à considérer que les frontières physiques ne constituent pas nécessairement l’horizon indépassable de la condition humaine, et qu’il y a lieu peut-être d’envisager leur démontage.

Parallèle un peu scabreux j'en conviens, mais j'aime imaginer que ce qu'il se passe à petite échelle dans les plantes fonctionner aussi à l'échelle de la planète; une histoire d'écosystème fragile, un équilibre de diversité.

---

[S’inspirer du vivant pour collecter et assainir l’eau](https://www.goodplanet.info/2020/11/18/sinspirer-du-vivant-pour-collecter-et-assainir-leau/)

---

>  Deux arbres d’une même variété étant génétiquement identiques, ils sont
>  incompatibles. Il faut donc planter des arbres appartenant à des variétés
>  différentes pour espérer récolter des fruits.

[Ce que la vie amoureuse du châtaignier nous enseigne de l’agroécologie](https://theconversation.com/ce-que-la-vie-amoureuse-du-chataignier-nous-enseigne-de-lagroecologie-148734)

---

> Écrire devient un exercice de liberté. 

[Processus d’écriture](https://carolineclementblog.wordpress.com/processus-decriture/)

Écriture manuscrite ? Et si j'essayais ?

---

[Le service MySocket, pour donner un accès Internet à ses développements locaux](https://www.bortzmeyer.org/mysocket.html)

Est-ce que cet outil faciliterais l'usage de git en décentralisé ?


---


À lire ? [La Flore des Bonnes Herbes](https://www.tela-botanica.org/2020/11/la-flore-des-bonnes-herbes/)

> La Flore des Bonnes Herbes permet de reconnaître plus de 730 espèces ayant un usage, poussant à proximité des hommes. Une flore visuelle pour une détermination facile.


---

> Commençons par faire exploser un mythe: les coopératives ne peuvent pas être
> racistes, non vraiment ?

> Mais même lorsque j’ai commencé à inviter les Noir·es, en particulier les
> jeunes Noir·es, dans les espaces coopératifs traditionnels, le mouvement
> coopératif n’était pas prêt pour eux et elles : les Noir·es m’ont dit que les
> coûts étaient trop élevés pour participer, ou que les questions discutées
> n’étaient pas pertinentes. Ils et elles se sentaient aliéné·es et beaucoup ne
> sont pas revenu·es – probablement à cause du privilège invisible des blancs
> que cultivent de nombreux membres de coopératives et du manque de
> représentation des Noir·es dans les documents, les études de cas ou les
> préoccupations du mouvement coopératif.

> McIntosh note que les Blancs apprennent à penser à leur vie comme moralement
> neutre, normative et moyenne, et aussi comme un idéal – de sorte que, comme
> elle le dit, lorsque «nous travaillons au bénéfice des autres, et cela est
> considéré comme un travail qui permettra à “eux” d’être comme “nous”. »

[Les coopératives face à l’équité raciale dans : six défis clés et comment les relever](https://autogestion.asso.fr/les-cooperatives-face-a-lequite-raciale-dans-six-defis-cles-et-comment-les-relever/)


