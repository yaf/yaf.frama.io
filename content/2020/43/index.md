---
title: Semaine 43
---

_Du dimanche 25 au samedi 31 octobre_

## Éducation & apprentissage


Un jeu libre à tester ? [Prog&Play](https://framalibre.org/content/progplay)

> Prog&Play est un jeu sérieux sur la programmation. Ce jeu exploite l'univers de Kernel Panic fonctionnant sur le moteur Spring.

---

Ça fait un certain temps que je suis abonné à l'info-lettre du site [Arbustes](https://arbustes.net/). C'est passionnant, mais un peu trop de messages pour moi. Je me désabonne.

Reste à explorer un peu ce site.

> La solution par l'éducation
> Un site pensé pour être utilisé avant tout par les enfants
> Pour les parents
> A destination des 6 - 16 ans, Numéricole favorise les apprentissages autonomes. Deux espaces : l'un pour les enfants, l'autre pour les parents.
>  Pour les enseignants
> Chaque classe dispose au sein du réseau de son propre espace avec de nombreux outils : arbres de connaissances, albums, messagerie, activités etc

J'ai eu l'occasion de découvrir qu'il existe des écoles « parentale » sous contrat ou pas.

Est-ce que les écoles sans contrat c'est encore ok ?

---

[Riposte Créative Pédagogique](https://www.ripostecreativepedagogique.xyz/?PagePrincipale)
> Coopérons pour apprendre et faire apprendre
> dans l'enseignement supérieur et en formation des adultes

>  Pourquoi cet espace ?
> Le confinement nous oblige à réviser dans l'urgence nos pratiques pédagogiques.
> Nous vous proposons un tiers lieu pour encourager la collaboration entre les acteurs du supérieur et de la formation des adultes (étudiants, enseignants, chercheurs, conseillers pédagogiques, personnels de soutien, animateurs de formation...) qui permette à chacun d'avancer dans sa redéfinition pédagogique.
> Sur Riposte Créative Pédagogique nous visons à faciliter,  en coopération ouverte :
> 
> - la collecte collaborative et la diffusion de bonnes pratiques et de ressources ouvertes, pour agir tout de suite ;
> - le partage d'expériences et la constitution de groupes de travail pour donner à voir, inspirer et avancer ensemble ;
> - la documentation et l'analyse de pratiques pour transformer cette crise en opportunité d'études, pour aller plus loin ;
> 
> Cette initiative est complémentaire par sa transversalité des dispositifs mis en place par les établissements d'enseignement supérieur et organismes de formation.

Dans les initiateurs on retrouve Michel Briand. C'est parce qu'on m'a parlé de lui que j'ai découvert Riposte Créative. Un Breton :)

---

[Faire École Ensemble (Fée)](https://wiki.faire-ecole.org/wiki/Accueil)
> Faire École Ensemble est une association - collégiale et à durée de vie limitée - qui facilite le soutien citoyen de la communauté éducative durant l’épidémie de Covid-19. Ses actions s’organisent par programmes collaboratifs et se destinent à être supportées par des coalitions d'organisations et de collectifs pérennes. 

Une bien belle association

---

J'ai redécouvert [Sugarizer](https://sugarizer.org/). J'en ai profité pour jouer un peu avec Aydan. C'était marrant.

---

## Programmation

À lire ? Ce lire me fait envie !

> pre-order an upcoming thriller (that summons the ghosts of ninjas past): [Cyberjutsu](https://nostarch.com/cyberjutsu). Learn ancient approaches to modern security issues based on authentic, formerly classified Ninja scrolls.

source [mailing list de No Starch Press](https://mailchi.mp/nostarch/hackers-and-exploits-and-bugs-oh-my?e=b5a8cab2a6)

---

[This page is a truly naked, brutalist html quine](https://secretgeek.github.io/html_wysiwyg/html.html)

Fabuleux !

Encore un exemple d'usage de [Quine](https://fr.wikipedia.org/wiki/Quine_(informatique)) qui me plait bien. Un jour je prendrais le temps d'explorer à mon tour ce type de construction.

---

Une émission de l'[April](https://www.april.org/) qui m'a fait découvrir le tunnel de verre (merci [Catherine Dufour](https://fr.wikipedia.org/wiki/Catherine_Dufour)) [« Libre à vous ! » Les femmes et l'informatique](https://april.org/73-femmes-informatique).


---

La présidente de l'[April](https://www.april.org/) est une philosophe. [Éthique du libre : une lecture philosophique de Véronique Bonnet](https://www.april.org/ethique-du-libre-une-lecture-philosophique-veronique-bonnet)



> «  Quel est votre philosophe préféré  ?  » Telle est la question posée par Véronique Bonnet à Richard Stallman, lors d’une récente conférence de ce dernier intitulée «  Une société numérique libre  ».

[Richard Stallman, Rousseau et Kant](https://framablog.org/2013/09/22/richard-stallman-rousseau-kant/)


> montrons qu’il serait préjudiciable de le transformer en « Education is Code », c’est-à-dire de confier l’acte éducatif émancipateur à n’importe quel support qui serait porteur, dans ses rouages techniques, de dispositifs de confiscation. Soit de réduire le droit de regard de l’éducateur et de l’éduqué au droit de regard d’une technologie invasive qui introduirait dans ses mécanismes des couches logicielles intrusives. 


>  Il en va de certains pourvoyeurs de logiciels comme des sophistes. Sous couvert de panacée, de remède universel, ils prétendent que l’éducation peut se fier à n’importe quelle informatique, que toute informatique fait du bien à qui l’utilise. Qu’un logiciel ne peut jamais faire de mal puisqu’un élève doit au moins apprendre à s’adapter à lui.
> 
> Préférons alors à ces outils logiciels peu soucieux d’éveiller le libre arbitre une informatique capable de se comporter effectivement en amie prodigieuse. L’informatique libre par laquelle «Code is Education ». 

[Code is Education](https://www.april.org/code-education-un-editorial-de-rentree-de-veronique-bonnet-presidente-de-l-april)



## Transition

[La Brèche](https://makesense.org/paumees-la-breche/)

> Le programme pour les paumé.e.s qui veulent s’engouffrer dans le monde d’après tout de suite maintenant

Une amie suit se programme. C'est intéressant (comme souvent ce que propose MakeSense). Ceci étant, je trouve que c'est un peu violent, et que ça reste sur des éléments de surface.

Sans doute nécessaire pour mettre certaines personnes sur le chemin de la deconstruction ? Un choc violent, puis beaucoup de discussion et de découverte...

J'ai besoin d'un truc avec plus de terre, de ver et d'air.

---

Une belle annonce pour la reprise d'une ferme PAM (Plante Aromatique et Médicinal). Il y a déjà tout de planté, un réseau de distribution installé. Bref, tout est là.

Et pourtant, je ne me sent pas prêt pour ça. Trop gros d'un coup.

Et en même temps, il me faut en parler avec ces personnes. J'imagine qu'elles pourraient justement me raconter comment ça se passe pour elles, que faudrait-il que je fasse pour m'installer dans ce genre d'endroit, réaliser ce genre de production. Ça m'aiderais aussi à me projeter, à vérifier si c'est bien ce que je souhaite.

Et bien voilà, je les aient contacté !

---

## Découvertes

[Stop Amazon](https://www.stop-amazon.fr/Les-alternatives)


---

Laurent évoque les « Wardley Maps ». Je me renseigne un peu pour comprendre.

Apparemment, on peut [faire des Wardley Maps avec Visual Studio](https://twitter.com/owardleymaps/status/1321215623813357568?s=21), mais ça ne vas pas m'aider à comprendre de quoi il s'agit.

> Understand context and diminish risk: How to build your first Wardley Map with Miro
>
> Wardley Maps are all about strategy. To be more precise, they are about building an intuitive and shareable understanding of your context to provide the situational awareness necessary for building a sound strategy. All this is especially helpful if you are starting a business or a non-profit, inspecting the competition, analyzing industries for opportunities or even trying to anticipate what the future might hold. 
>
> What is Wardley Mapping?
>
> A Wardley Map is a representation of the landscape in which a business operates. It consists of a value chain (activities needed to fulfill user needs) graphed against evolution (how individual activities change over time under supply and demand competition). A Wardley Map represents the situational awareness and shared assumptions being made about a context and hints at what strategic options are available.

[Miro permet de faire des Wardley Maps, et explique comment](https://miro.com/blog/wardley-maps-whiteboard-canvas/)

C'est déjà un peu mieux. Est-ce que j'ai besoin d'un outil pour penser à l'environnement « métier » ? À une chaine de valeur ?


Et si je souhaite creuser un peu plus, j'ai trouvé une forme d'[introduction aux carte de Wardley](https://aktiasolutions.com/introduction-to-wardley-maps/). Amusant d'y voir un exemple avec un magasin de thé :).


