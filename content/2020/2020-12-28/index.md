---
title: lundi 28 décembre 2020
---

Je voulais essayer de journaliser les heures et le contenu de mon intervention pour [RDV-Solidarités](https://www.rdv-solidarites.fr/), mais je constate que je n'ai pas réussi ce matin.

Je pense avoir commencé vers 10h.
- intégration des retours pour la page à propos de l'annulation de rdv dans la doc
- création de 2 ticket dans le github
- lecture de commentaire sur le trello
- Réponse à 2 tickets Zammad (c'est pas très pratique pour traiter les demandes, les emails étaient plus simple)
- mise à jour du trello
- début du ticket pour l'ajout de l'information mineur/majeur dans l'export stat de l'organisation

Pause à 12h30.
Reprise 14h30.

- refactoring pour déplacer le service RdvExporterService de app/services à app/service_functions/RdvExporter et modification en conséquence.
- report du ticket sur la tracabilité usager dans le forum, pour repousser la réalisation à « après » avoir trouvé la bonne solution pour la séparation des bases usagers


pause 15h30
reprise à 17h30

- Mail de Clarrise du 92 : erreur sur l'export en prod de stats en prod
- Construction d'une palette pour limité les couleurs et maitriser les contrastes dans l'agenda

arrêt à 19h30

reprise à 21h

- jardinage dans le forum, déplacement de quelques sujets du trello vers le forum

fin à 21h45

---

