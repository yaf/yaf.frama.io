---
title: 22 décembre 2020
---


> J’ai toujours eu l’impression que l’industrie aime traiter les équipes de développement comme une bande d’ados stupides. Permettez-moi de m’étendre là-dessus…

[SCRUM et les mêlées quotidiennes](https://framablog.org/2019/11/08/scrum-et-les-melees-quotidiennes/)

---

> The magical SD-card
> 
> This is the moment to introduce you to my magical SD-card, which is another hardware upgrade that facilitates the use of old (but also new) laptops. Many people have their personal documents stored on their laptop's hard drive and then make backups to external storage media if all goes well. I do it the other way around.
> 
> I have all my data on a 128 GB SD-card, which I can plug into any of the Thinkpads that I own. I then make monthly backups of the SD-card, which I store on an external storage medium, as well as regular backups of the documents that I am working on, which I temporarily store on the drive of the laptop that I am working on. This has proven to be very reliable, at least for me: I have stopped losing work due to computer problems and insufficient backups.
> 
> The other advantage is that I can work on any laptop that I want and that I’m not dependent on a particular machine to access my work. You can get similar advantages when you keep all your data in the cloud, but the SD-card is the more sustainable option, and it works without internet access.
> 
> Hypothetically, I could have up to two hard drive failures in one day and keep working as if nothing happened. Since I am now using both laptops alternately – one with battery, the other one without – I can also leave them at different locations and cycle between these places while carrying only the SD-card in my wallet. Try that with your brand new, expensive laptop. I can also use my laptops together if I need an extra screen.
> 
> A 128 GB SD-card will set you back between 20 and 40 euros, depending on the brand. In combination with a hard disk drive, the SD-card also increases the performance of an old laptop and can be an alternative to installing a solid-state drive. My spare laptop does not have one and it can be slow when browsing heavy-weight websites. However, thanks to the SD-card, opening a map or document happens almost instantly, as does scrolling through a document or saving it. The SD-card also keeps the hard disk running smoothly because it's mostly empty. I don’t know how practical using an SD-card is for other laptops, but all my Thinkpads have a slot for them.
> 

[How and why I stopped buying new laptops](https://www.lowtechmagazine.com/2020/12/how-and-why-i-stopped-buying-new-laptops.html)

Ou l'autre façon de gérer ses backups efficacement ! Je n'utilise toujours pas ce mode, et pourtant, je cois que ça me forcerais à faire du tri dans mes documents, dans les photos.... Et surtout, à être à jour en backup.

