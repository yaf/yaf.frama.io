---
title: Pensée du Samedi 12 décembre 2020
---


la puissance de la mer. Vivre proche de l'océan ? À quel prix ? Rester à porter de vélo ? Ou partir plus loin, quitte à envisager de ne pas voir l'océan facilement (le jour où il n'y pourra plus y avoir de pétrole)...

![océan atlantique, plage du Magouëro, Pays de Lorient](P1010199.JPG)

