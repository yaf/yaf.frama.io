---
title: Jeudi 17 décembre 2020
---

> Femmes pionnières
>
> En l’espace de vingt ans, la place des femmes dans l’informatique a été réduite de moitié. « En 1983, c’est le deuxième secteur comportant le plus de femmes diplômées, avec 20,3 %, soit 6 points au-dessus de la moyenne des femmes ingénieures. Dans les années 2010, les filières STIC (sciences et technologies de l’information et de la communication) diplôment seulement 11 % de femmes », résume la chercheuse.
>

[Les femmes de plus en plus minoritaires dans le secteur de l’informatique](https://www.lemonde.fr/campus/article/2017/12/11/femmes-et-informatique-vingt-ans-de-desamour_5227726_4401467.html)

---

En vrac, issue de la formation UX proposé par [Raphaël](https://www.iergo.fr/)

- 1984 macintosh, début de l'informatique « grand public » utilisable sans compétence en développement.
- L'ergonomie : personne réalisant une tache, dans un contexte donnée, avec un certain nombre d'outils.
- Grosse différence entre une activité prescrite versus une activité réelle.
- Concevoir en fonction d'activité réelle, garder une adaptabilité pour faire évoluer l'outil vers les activités réelles.
- Penser l'accessibilité et le design pour toutes et tous (capacités cognitives et physique à court ou long termes.
- Notre perception s'appuie sur notre expérience
- Loi de proximité, loi de similarité, loi de la cloture, loi de la symétrie
- En haut à gauche, le plus utilisé, en bas à droite, le moins.
- Loi de hick : temps de décision = log (nombre de choix + 1)


---

Journal du jeudi 17

- Formation UX avec Beta Gouv toute la matinée. C'est chouette de revoir des choses, et d'en découvrir plein d'autres.
- réponse à une agent qui nous a pris pour un usager (alors que nous ne faisions que faire suivre le message)
- Réponse à Élise du 14 à propos 1 le visuel d'occupation des agents pour prévoir les vacances, les permanence. 2 - un ticket à propos de statistique. L'idée serait d'ajouter une info pour savoir si l'usager est mineur ou majeur. Il y a un élément sur les IVG également. 3 - Un comportement étonnant (dérangeant) de navigation quand nous ne sommes pas sur une page du menu agenda, vis à vis du changement d'agent qui ne suit pas niveau affichage.
- Appel avec Magalie du 22 à propos de l'affichage des libellés de plage d'ouverture qui informe l'agent sur l'endroit où il travail et avec qui, et le fait que les rdv posé via l'agenda, sur une de ces plages, ne propose pas le lieu/motif de l aplage par défaut. Ce dernier point rejoint une partie de la discussion visio avec Élodie du 62. Nous avons aussi évoqué la formation pour la secto. Le 22 aimerais ouvrir aux usagers à partir de février/mars et souhaite préparer la config avant.
- appel/visio avec Élodie du 62 à propos de la navigation fiche rdv -> fiche usager -> retour fiche RDV. Nous avons conclu que le mieux serait l'ajout un bloc distinct pour afficher, s'il y a, le (ou les ?) rendez-vous en cours, en prenant 1h de marge avant et après. Nous avons aussi parlé de la pose de rdv dans l'agenda, mais sur une plage, et du comportement par défaut. Nous avons aussi parlé de son avenir. Elle va être là encore quelques mois.


