---
title: 29 novembre 2020
---

[How to Do 90% of What Plugins Do (With Just Vim)](https://www.youtube.com/watch?v=XA2WjJbmmoM)

Un vidéo de [Thoughtbot](https://thoughtbot.com/) à propos de Vim.

Voici quelque notes.

```
set path=**
set wildmenu
```


`command! MakeTags !ctags -R .`

`^]` pour y aller ;
`^t` pour retourner ;
`g^]` pour lister toutes les instances de ce marqueur ;

C'est là qu'un clavier qwerty est quand même vachement mieux... Et finalement, je binôme assez peu avec d'autres personnes. Je pourrait sans doute me commander un clavier qwerty à nouveau ?

Pas fan de l'autocomplete. Ça pourrait cependant être utile.
Le parcours des fichiers ne me semble pas très utile non plus, du moins, pas dans ma pratique.
Pas beaucoup d'usage sur les snippets. Mais l'idée pourrait permettre d'autres choses.
Intégration dans vim. Pas beaucoup de sens pour moi. Les tests sont souvent trop long. Par contre, c'est intéressant de voir que la commande `:make` se lance vis à vis du fichier makefile du répertoire courant.

Durant les questions, il est abordé la copie dans le clipboard du système. J'apprécie d'entendre un peu parler de ces différents tampon de Vim que je n'ai jamais vraiment mis en œuvre. Je découvre que le tampon par défaut est `"`. Par contre, je n'ai pas de registre `+` utilisable sur mon Vim. Pourquoi donc ?

> :[range]y[ank] [x]      Yank [range] lines [into register x]. Yanking to the
>                         "* or "+ registers is possible only when the
>                         +clipboard feature is included.

Et bien voilà pourquoi. La feature `+clipboard` ne dois pas être activée.


> User the help system...
`:help <command>`

---

À lire ? À acheter pour soutenir ? [Histoires d’un arbre : le nouveau roman graphique de Mathias Bonneau](https://www.alternativesforestieres.org/Histoires-d-un-arbre-le-nouveau-roman-graphique-de-Mathias-Bonneau)

> C’est en occultant la responsabilité collective de transformation sociale, au nom d’une sortie de l’«  assistanat  », que l’idéologie libérale a dévoyé le concept d’empowerment en le réduisant à une définition individualisante et culpabilisante  : «  Prenez-vous en main  : quand on veut on peut.  »

À lire ? [Féminisme noir : s’autodéfinir, une nécessité vitale](https://www.unioncommunistelibertaire.org/?Feminisme-noir-s-autodefinir-une-necessite-vitale)

> Dans ce premier recueil de textes à paraitre en français, Abdullah Öcalan, fondateur du PKK, aborde avec une grande clarté le concept de confédéralisme démocratique, lié à l’indispensable révolution des femmes.

À lire ? [Öcalan, « La Révolution communaliste »](https://www.unioncommunistelibertaire.org/?Lire-Ocalan-La-Revolution-communaliste-8882)

