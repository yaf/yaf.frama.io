---
title: Semaine 42
---

_Du 18 au 24 octobre 2020_

## Code

De l'inconfort dans le code de l'autre. J'ai du mal avec le code d'un de mes
équipiers actuellement. Et quand j'y regarde pour trouver des éléments factuels,
je n'en trouve pas tant que ça. En dehors du style d'écriture des tests, le
reste me semble plutôt convenir. Alors pourquoi ? Parce que nous ne l'avons pas
construit ensemble ? Du coup, je dépense de l'énergie à le comprendre
après coût, comme un fait accompli. Il y a de la frustration.

_la frustration c'est de la colère avec une dose de peur ?_

## Botanique, potions et jardin

Je crois que les deux arbustes qui sont déjà implanté dans la cour sont des
[_Viburnum sp._,
Caprifoliacées](http://www.snv.jussieu.fr/bmedia/arbres/viorne.htm).

Encore un site plein de bonne info mais qui vieilli mal [Biologie et
Multimedia](http://www.snv.jussieu.fr/bmedia/index.htm)

---

[Du vinaigre de Romarin, à partir de vinaigre de cidre](http://www.lesjardinsdalice.ch/2020/10/vinaigre-de-romarin-un-allie-malin.html).  À tester.

Plus le temps passe, et plus je découvre mon envie de faire ce genre de
préparation : huile essentielle, hydrolat, vinaigre et huile, pommade, crème,
tisane...

D'ailleurs, je me suis inscrit pour un atelier hydrolat de [_Laurus
nobilis_](https://fr.wikipedia.org/wiki/Laurus_nobilis) (laurier noble)
mercredi par l'association Cueillir. J'en profiterais pour adhérer par la même
occasion.

De là à devenir druide, il n'y a qu'un pas.

---

Une recette de l'association Cueillir :

_« la fameuse tisane des centenaires »_

> Le frêne a la réputation de procurer la longévité à ceux qui en consomment régulièrement. Voici une boisson rafraîchissante et tonique pour défier les années !
>
> Mélanger en parts égales des feuilles de Frêne, Cassis, Menthe poivrée et fleurs de Reine des Prés. Infuser 10g du mélange dans 1L d'eau pendant 10 minutes. Boire dans la journée en plusieurs tasses.

Deal !

---

> Obtenu à partir du vesou (jus de canne à sucre pressé), de la mélasse
> (produit dérivé du raffinage du sucre de canne) ou encore du miel de canne
> (jus de canne concentré)

[Miel de canne](https://guide-rhum.com/tag/miel-de-canne/)

J'ai gouté de miel de canne. C'est souvent ce qu'il y a comme miel aux Comores.
Je viens de comprendre ce que c'est. Rien à voir avec les abeilles. Et
ça explique aussi pourquoi je trouve que ça vieilli mal, que ça sent l'alcool, ça fermentation.

---

Des clefs de détermination numérique ?
[Des clés de détermination informatisées interactives pour le genre géant Solanum](https://www.tela-botanica.org/2020/10/des-cles-de-determination-informatisees-interactives-pour-le-genre-geant-solanum-solanaceae/).

[Solanaceae Source](http://solanaceaesource.org/)

Ça semble plus compliqué que ce que j'avais envisagé. Je crois malgré tout que
j'ai envie d'essayer de faire un truc accessible au grand public, à aides
personnes débutantes, amateurs, comme j'ai l'impression d'être :)

---

> Là où le règne animal fonctionne en circuit fermé avec le cœur comme pompe,
> le règne végétal fonctionne en circuit ouvert par différence de pression.

[Le jardinage des racines](https://gardenfab.fr/inspiration/le-jardinage-des-racines)

---


[L'happyColette de l'atelier
Paysan](https://www.latelierpaysan.org/Farming-Soul-fiches-outils). Une
brouette pour les apiculteur. J'avais déjà vu et manipulé une version
commercial de ce type de brouette. C'était pas concluant, pas très pratique. À
voir si l'atelier à réussi à améliorer la version existante.

[Des abris à Coccinelles](http://www.jardinsdenoe.org/fabriquer-des-abris-pour-les-coccinelles/)


---

Atelier Laurier noble (Laurier Sauce) à appeler _Laurus nobilis_ pour éviter
les confusions tant il y a de plante qu'on appel laurier (et qui ne sont pas du
tout de la même famille botanique, et pas comestible).

Je suis le seul homme dans un groupe de 6, plus les deux animatrices. J'espère
ne pas avoir été un éléphant dans un magasin de porcelaine. Dit autrement, un gros
lourdaud qui manque d'écoute.

C'est vraiment passionnant tout ce qu'on peut faire avec des plantes. Là, le
laurier est vraiment riche. Et je découvre qu'on peut bricoler pour faire de
l'hydrolat, pas besoin d'alambic !

Je crois avoir trouvé une Bernard. Elle donne plutôt que de trafiquer, elle
connait beaucoup de chose qu'elle partage très simplement. J'aime beaucoup.
Dommage, elle a apparemment déménagé dans l'Anjou. Elle passe de temps en temps
par ici. J'espère la revoir.

Nous sommes passé dans un petit jardin partagé de l'[association Lucioles](http://www.luciolesriatransition.bzh/). Une
association qui semble bien implanté autour de la Ria d'Étel.

J'ai adhéré à l'association [Cueillir](https://www.facebook.com/cueillirplantessauvages/).

Peut-être devrais-je me faire une petite liste des associations auxquelles je
suis adhérent..


## Informatique

J'ai refait ma configuration de [Redshift](http://doc.ubuntu-fr.org/redshift) sur la nouvelle machine. Encore un fichier à ajouter dans mon repo [dotfiles](https://github.com/yaf/dotfiles).

---

J'avais en tête d'utiliser [YesWiki](https://yeswiki.net/?AccueiL) mais pourquoi
faire ?

Pour les pieds dans le code, je me dit que je peux rester ouvert, mais avec un
générateur de site. Où bien c'est justement pour les pieds dans le code que
j'envisage le wiki, et le générateur pour [mon propre site](https://elsif.fr).

Utiliser un hébergeur associatif Breton ?

- [Infini](https://www.infini.fr)
- [Vulpecula](https://www.vulpecula.fr/)
- [Simple Hosting](https://simplehosting.me/)

Amusant le concept de [gare centrale sur YesWiki](https://yeswiki.net/?LaGareCentrale).

Il me semble avoir entendu [Thomas](https://oncletom.io/) parler de YesWiki. Quel rapport
entretient-il avec ce projet ? Il me semble que Stéphane aussi connait ce projet.

C'était intéressant d'entendre la [présentation de Thomas, codeur et contributeur de YesWiki](https://www.youtube.com/watch?v=4nPZpCATqkg).

Expérimentation de [wiki Le pieds dans la code](https://ferme.yeswiki.net/lespiedsdanslecode/).

Si les pieds dans le code deviens un live, partir d'un wiki n'est peut-être pas
la meilleure idée ?

Avantage d'un wiki : faciliter la mise à jour par toutes les personnes qui le
veulent (moyennant un apprentissage d'utilisation d'un wiki).  Inconvénient
d'un wiki : Structure mon linéaire et moins maîtrisé qu'un site, le design
moins « flexible ».

Pas grand chose de négatif il me semble. Il faut essayer.

Un inconvénient : devoir trouver un hébergeur :-/ Est-ce que Scopyleft serait
prêt à me payer un hébergement pour Les pieds dans le code ?

Est-ce qu'on jardine un site statique comme on jardine un wiki ?

Quand je vois que les [Women On Rails lance un wiki basé sur github](https://women-on-rails.github.io/ressources/docs#comment-lutiliser-), je crois que je vais rester sur ce format. Plus simple à héberger, à personnaliser niveau CSS..


---

La compostabilité... Et ce n'est qu'un élément de tout ce que j'aime tellement
bien beaucoup chez Framasoft <3

[Framasoft, le modèle associatif est-il soluble dans la #StartupNation ? Pierre-Yves Gosset - Ethics by design](https://peertube.designersethiques.org/videos/watch/0e3b464a-3885-4ee4-af76-8b2e8952d548)

À lire ? [« Plutôt couler en beauté que flotter sans grace » de Corinne Morel Darleux](https://www.infolibertaire.net/plutot-couler-en-beaute-que-flotter-sans-grace/)

[Quelque extrait sur Cairn](https://www.cairn.info/revue-projet-2019-6-page-91.htm)

---

> Basthon est l'acronyme de "Bac À Sable pour pyTHON". Il ressemble au mot
> "baston", c'est une allusion à la "lutte" que peut parfois représenter
> l'apprentissage de la programmation, l'écriture d'un code ou son débogage.

[Basthon](https://basthon.fr/about.html)

J'apprécie le produit, qui donne envie de répliquer ce genre de chose avec
d'autres langages; J'apprécie beaucoup cette page « à propos » qui me semble
assez claire et lisible par des personnes qui ne connaissent pas encore la
programmation, mais qui s'y intéresse !

---

Belle article (partagé par Adrien) à propos de [Rails Service Objects](https://www.codewithjason.com/rails-service-objects/). C'est intéressant de voir qu'il évoque le paradigme objet. Je crois que j'ai tendance à apprécier de plus en plus la construction d'un logiciel qui utiliserait des fonctions pour transformer des objets de données. Et peut-être faut-il un savant mélange des deux pour être agréable ?

Ça me donne envie de me replonger dans le domain driven design. Ça pourrait
m'aider dans les rôles produit que je me retrouve à porter plus ou moins
partiellement sur mes dernières missions.

> I don't know why this anti-pattern is so common. I suspect it's due to many
> people who haven't really worked with a proper domain model, particularly if
> they come from a data background. Some technologies encourage it; such as
> J2EE's Entity Beans which is one of the reasons I prefer POJO domain models.

> In general, the more behavior you find in the services, the more likely you
> are to be robbing yourself of the benefits of a domain model. If all your
> logic is in services, you've robbed yourself blind.

[Anemic Domain Model par Martin Fowler](https://martinfowler.com/bliki/AnemicDomainModel.html)

---

Stéphane évoquait le ticket d'entrée vs les possibilités sur
- [Github](https://github.com/) - ticket faible, possibilités nombreuses : beaucoup de projet sont là
  bas
- [Gitlab](https://gitlab.com/) - ticket un peu plus dur, possibilité moins nombreuses;
- auto-hébergement - ...

Quel service utiliser et chez qui ?

Est-ce que je garde mes micro projets sur [git.scopyleft.fr/yaf](https://git.scopyleft.fr/yaf) ? Est-ce que je me crée un truc quelque part pour moi en perso ? Est-ce que j'utilise
github ?

Merci Edgar est sur [Framagit](https://framagit.org). Pas sur que ce soit maintenu. Chez qui poser notre hébergement ?

[chapril](https://www.chapril.org/-services-.html)

Est-ce qu'il manque l'aspect fédération d'identité pour que toutes les forges
puissent être intéressante ? Qu'est-ce qui fait qu'un projet est plus facile
d'accès sur github que sur un système git auto-hébergé ?

Jonathan avait évoqué un jour le fork et ce que ça permet. C'est vrai que ça réduit
de beaucoup la participation à un projet. Et avoir un système auto-hébergé
rends difficile cette partie là (tant que ce n'est pas fédéré).

Je constate que [Bastien](https://bzg.fr/), qui pourrait être une référence dans le monde du
libre, est plus actif sur [github](https://github.com/bzg) que sur [gitlab](https://gitlab.com/bzg). Il affiche malgré tout un lien vers ces deux comptes.

On pourrait aussi se poser la question du coup d'hébergement d'une forge. L'option forge scopyleft me semble intéressante (au delà donc du coup/gain pour faire venir certaines personnes).

Comment choisir ? Sans doute faut-il avoir des trucs un peu des deux cotés.
Pour un projet, s'auto hébergé, ça serait ok en trouvant un moyen simple
d'accéder au code pour les personnes qui souhaite contribuer. À tester sur
Merci Edgar ? Ou tout simplement en partant sur [gitlab](https://gitlab.com/) ?

[Il y a 23 chatons qui proposent l'hébergement de forge logiciel](https://chatons.org/fr/find?title=&field_chaton_services_tid=68)

---

Bientôt l'[assemblée générale de Tela Botanica](https://www.tela-botanica.org/2020/10/invitation-a-notre-assemblee-generale-annuelle/)

Est-ce que [j'adhère à cette association](https://www.tela-botanica.org/presentation/adherez/) aussi ?

---

Suite à mes échanges sur le keybase de [Scopyleft](http://scopyleft.fr/) à propos de [YesWiki](https://yeswiki.net/?AccueiL) et de l'usage d'un moteur de site statique, David me partage un article [zserge.com/posts/awfice](https://zserge.com/posts/awfice/) pour l'aspect « avoir les mains directement dans le web ».

Et je découvre des choses comme

- Text editeur : `data:text/html,<html contenteditable>`
- Diaporama : `data:text/html,<style>@page{size: 6in 8in
  landscape;}</style><body><script>d=document;for(i=0;i<50;i++)d.body.innerHTML+='<div
  style="position:relative;width:90%;padding-top:60%;margin:5%;border:1px solid
  silver;page-break-after:always;"><div contenteditable
  style="outline:none;position:absolute;right:10%;bottom:10%;left:10%;top:10%;font-size:5vmin;"></div></div>';d.querySelectorAll("div>div").forEach(e=>e.onkeydown=e=>{n=e.ctrlKey&&e.altKey&&e.keyCode-49,x=["formatBlock","formatBlock","justifyLeft","justifyCenter","justifyRight","outdent","indent","insertUnorderedList"][n],y=["<h1>","<div>"][n],x&&document.execCommand(x,!1,y)})</script>`
- Dessiner : `data:text/html,<canvas
  id="v"><script>d=document,d.body.style.margin=0,f=0,c=v.getContext("2d"),v.width=innerWidth,v.height=innerHeight,c.lineWidth=2,x=e=>e.clientX||e.touches[0].clientX,y=e=>e.clientY||e.touches[0].clientY,d.onmousedown=d.ontouchstart=e=>{f=1,e.preventDefault(),c.moveTo(x(e),y(e)),c.beginPath()},d.onmousemove=d.ontouchmove=e=>{f&&(c.lineTo(x(e),y(e)),c.stroke())},d.onmouseup=d.ontouchend=e=>f=0</script>`

[github.com/zserge/awfice](https://github.com/zserge/awfice)

## Transition & société

> Au Royaume-Uni, le calcul des prestations sociales s’effectue mensuellement,
> en fonction de l’évolution des revenus mensuels perçus. En ne s’intéressant
> qu’au salaire perçu dans le cours du mois, et pas à la fréquence à laquelle
> ils sont payés, les revenus sont souvent surestimés d’un mois sur l’autre et
> les prestations réduites d’autant.

> Les bénéficiaires doivent gérer seuls les fluctuations de versement de
> prestations, ce qui a des conséquences directes sur leurs endettements.

> la protection sociale et les caisses d’allocations familiales sont parmi les
> principaux organismes où les réclamations sont nourries et où l’excès de zèle
> a tendance à fragiliser les plus démunies. Pire, souligne la journaliste, la
> complexité française crée des non-recours et le zèle des services génère des
> coûts administratifs de traitements souvent plus élevés que les sommes en
> cause.

[De l'automatisation de la précarité](http://www.internetactu.net/a-lire-ailleurs/de-lautomatisation-de-la-precarite/)

---

> Revue et collectif révolutionnaire.  Contre la société cybernétique en
> formation, Véloce œuvre à l’émergence d’une civilisation du jeu, de la
> solidarité matérielle et vivante. 


[À propos de Lisez Véloce](http://www.lisez-veloce.fr/#about)


> L’informatique est le moyen technique par lequel le capitalisme tente de
> résoudre ses contradictions internes, selon ses propres termes.

> Si le capitalisme vend à chacun son moyen d’accès au réseau, c’est pour ne
> pas rendre à tous les moyens de produire.

> Dans ces conditions, le salariat, comme temps exclusivement consacré à la
> production de valeurs, peut disparaître. Mais il ne disparaît qu’en se
> réalisant, en étendant sur toute la vie quotidienne la contrainte de
> valorisation qui le caractérise, la soumission au processus qui l’accompagne,
> sa surveillance et sa misère. Le travail abstrait est partout ; la vie
> devient un salariat permanent.

[Thèses sur l'information (de Lisez Véloce)](http://www.lisez-veloce.fr/veloce-01/theses-sur-linformatique/)

Merci David pour la découverte.

---

Les paysans boulangers de Erdeven ! [Le moulin de saint germain](http://www.lemoulindesaintgermain.fr/commande.html) Il y a moyen de commander pains et farines. À tester !

---

[Dérivation](https://dérivation.fr/) via [Une lecture politique
des Furtifs](https://dérivation.fr/furtifs/), suite à une discussion sur le
sujet chez les Codeurs en libertés...

Très intéressant !

---

> On a tendance à parler de grand public, mais en fait, je suis désolé de vous
> l'annoncer, grand public n'existe pas.

[tester avec vos utilisateurs](https://vimeo.com/421686703)

Une présentation de Raphaël Yharrassarry dans le cadre de l'équipe de Design Transverse.

---

À lire ? [Anarchiste de Philippe Clochepin](https://www.unioncommunistelibertaire.org/?Lire-Clochepin-Anarchiste)

---

Découverte de [l'encyclopédie anarchiste de Sébastien Faure](http://www.encyclopedie-anarchiste.xyz/). Site très moche, mais les ressources
semblent bien intéressantes.

Est-ce que c'est mal venue de proposer une refonte du site bénévolement ?

---

Découverte du [SHOW](https://www.shom.fr/index.php/fr/qui-sommes-nous/missions)
Déjà en lien avec Data.gouv.fr

---

[Graine d'océan](http://grainedocean.over-blog.com/)

---


