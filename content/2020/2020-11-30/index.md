---
title: Lundi 30 novembre 2020
---

[Hyperliens S02E03 - Fablab des 3 Lapins : Fabriquer du réseau et du lien](https://www.youtube.com/watch?v=GlaZba_OZWI&feature=youtu.be)

[Fablab des 3 lapins](https://fablab3lapins.org/)

> la raison de l'être des 3 lapins, c'est l'être ensemble

> Un point de rencontre.

> Y'a eu le passage à l'ordinateur, mais plutôt à les utiliser qu'à comprendre comment ils étaient montés, et ce passage là, on l'a pas ni en formation scolaire, ni nulle part. Et du coup tu as un gap. Et du coup, notre modèle de développement sur l'exploitation qui était d'essayer de gérer tout, d'essayer de se dépanner sois même, et puis de bricoler pour pas que ça coute chère et puis qu'on arrive à s'en sortir financièrement, aujourd'hui, ça ne marche pas.

> c'est sortir de cette logique du guichet, on va faire des démarches à ma place. Se prendre en main, c'est mieux.

> On a pas besoin de grands experts dans ces lieux là, on a besoin de gens qui éclairent les autres.

---

> il est défendu l’idée qu’un dispositif d’inclusion numérique ne peut pas réussir s’il ne donne pas à ses bénéficiaires des moyens de capacitation, d’autonomisation.

_à propos de la capacitation : « Processus par lequel un individu ou un groupe acquiert les moyens de renforcer sa capacité d’action, de s’émanciper » (Bacqué, 2005)_


> Le capital numérique intègre l’idée d’un réservoir qu’il faut alimenter, quasiment continuellement, pour éviter qu’il ne se vide. Cette idée me semble être tout à fait pertinente quand on voit la vitesse à laquelle les technologies numériques transforment notre société, notre droit, nos habitudes. Pour pouvoir suivre, il faut entretenir sa veille.

Autant je suis d'accord sur pas mal d'aspect autour de la fracture numérique qui masque une plus grande complexité, autant je ne suis pas fan du capitale numérique.

- Ça pourrait être parce que je n'aime pas le mot de capitalisation;
- Ça pourrait être parce qu'une personne qui finalement ne s'y connait pas tant que sa en numérique et en être humain l'a utilisé
- mais je crois que c'est surtout parce que je ne suis pas d'accord avec l'aspect accumulation, ou remplissage constant parce qu'il faut se tenir à jour.

Et je n'aime pas trop non plus la conclusion qui suit

> Mais surtout, la notion de capital numérique évite les termes d’inclusion/exclusion. Il faut bien admettre qu’on peut faire le choix de ne pas développer de capital numérique, parce qu’on n’en voit pas l’intérêt, qu’on ne développe pas d’appétence numérique. Et il faudra bien finir par accepter que tous les citoyens ne sont pas intéressés par les technologies numériques, n’en déplaise à certains politiques ou champions de la tech.

À creuser pourquoi :thinking:

[Du concept de « fracture(s) numérique(s) » à celui de capital numérique ?](https://louisderrac.com/2020/11/19/du-concept-de-fractures-numeriques-a-celui-de-capital-numerique/)

---

[laike9m/Cyberbrain Python debugging, redefined](https://github.com/laike9m/Cyberbrain)

Wahou ! Merci David pour la découverte ! Un outil fabuleux pour apprendre et comprendre la programmation. À tester.
