---
title: Mercredi 9 décembre 2020
---

Lecture à la médiathèque aujourd'hui de [En route pour l'autonomie alimentaire](https://boutique.terrevivante.org/librairie/livres/4405/conseils-d-expert/458-en-route-pour-l-autonomie-alimentaire.htm)

Plusieurs points d'action assez intéressant.

## Tenir un journal du jardin

J'avais déjà noté ça comme un point d'amélioration. J'ai vu Bernard tenir son carnet. Le faire de façon numérique, ici ne me semble pas une bonne idée. Bien que je puisse, après coup, partager certaines observations.

Peut-être que je devrait commencer, même si le jardinet que nous avons aujourd'hui me semble vraiment petit...

> Vous noterez ainsi ce que vous avez réalisé, semé, planté ou obtenu comme résultat. Ce cahier vous permettra de vous rappeler à quel moment vous avez si en terre telles graines ou planté tel arbre.

Il est aussi évoqué le fait de tenir un journal lié à ce que l'on mange. C'est assez intéressant. L'idée ici est de noter
> - ce que vous avez cuisiné vous-même à partir d'ingrédients naturels
> - ce qui a eu juste besoin d'être réchauffé, ou cuit, car préparé par l'industrie agroalimentaire ou le traiteur local.

Je passerais bien à une autre étape : noter les quantités préparer, les restes ou pas. J'ai besoin (envie ?) de réfléchir au volume de nourriture de ma famille.

> Distinguez la nourriture qui vous semble être respectueuse du vivant et celle qui ne l'est pas. Pour les viandes et les poissons, observez leur provenance et tentez de savoir si les conditions d'élevage sont attentives au bien-être des animaux.

## Incroyable comestible

C'est le chemin que j'avais suivi pour rencontrer les personnes qui participaient au jardin robinson. Une belle aventure dont je me souviendrais longtemps, et de laquelle je ressort avec des amies qui compte pour moi.

Pourquoi je n'ai pas encore suivi le chemin des incroyables comestibles ici, à Hennebont, ou bien à Lorient ?

> La méthode des Incroyables Comestibles : cinq étapes
> 1 - S'engager
> Se prendre en photo devant la pancarte de la commune avec les écriteaux indiquant « Nourriture à partager » et les outils de jardinier.
> 2 - Communiquer
> Partager des photos sur internet et informer la presse.
> 3 - Participer individuellement à la démarche collective
> Faire des plantations de nourriture à partager sur l'espace privée ouvert au public.
> 4 - Mobiliser des habitants pour fair ensemble
> Réaliser des actions collectives pour devenir une force citoyenne.
> 5 - Sensibiliser les élus de la commune au mouvement participatif
> Tenter d'obtenir l'ouverture de l'espace public pour développer l'agriculture urbaine dans la ville, avec la participation des services municipaux.


## Feuille de route des 21 actions

Une double page propose un dessin en roue, en soleil, avec 4 volets, découpé en 21 zones, chacune proposant une action

> 1. On prend soin (au centre)
> de soi, de l'autre et de la terre
> _On plante - Volet participatif_
> 2. Permis de végétaliser la vile en paysage nourricier
> 3. Agora d'agriculture urbaine
> 4. Pépinières citoyennes participatives
> 5. Circuits comestibles pédagogiques
> 6. Vergers et jardins partagés
> _On jardine - Volet éducatif_
> 7. Potagers pédagogiques dans les écoles
> 8. Grainothèques pour la vie
> 9. Poulaillers participatifs
> 10. Intégration du chemin de l'eau
> 11. Production de biomasse
> _On partage - Volet coopératif_
> 12. Ateliers de cuisine
> 13. Ateliers de conservation des aliments
> 14. Supermarchés coopératifs
> 15. Liens entre producteurs et mangeurs
> 16. Organisation des circuits courts
> _On aggrade - Volet régénératif_
> 17. Jardins de Cocagne
> 18. Zones d'activités nourricières
> 19. Phyto-remédiation
> 20. Réduction des déchets et élimination des polluants
> 21. Plantations d'arbres et forêts comestible

Ces points, bien que numérotés, ne sont pas à suivre dans un ordre particulier. J'éprouve du plaisir à voir que certains points sont déjà en cours ou avancé dans l'endroit où je vis, dans les endroits om j'étais avant. C'est assez intéressant.

Il y a une proposition d'auto-évaluation de nos habitudes d'approvisionnement avec un radar à 8 branches.
- 2 branches opposées (1 et 5) dépendent entièrement de nos décisions. Représentées en vert.
- 4 branches (2, 3, 4 et 8) achats dépendants de l'existence ou pas d'un type d'équipement. Représentées en rouge.
- 2 branches (6 et 7) modes d'approvisionnement alternatif. Ceux que nous pouvons intégrer s'ils sont disponible prêt de chez nous. Représentées en violet.

> 1. Potager familiale ou collectif
> 2. Hyper et supermarchés
> 3. Petit commerce local
> 4. Marchés locaux
> 5. Chasse, pêche, cueillette sauvage, glanage
> 6. Supermarchés coopératifs
> 7. Abonnement paniers hebdo (AMAP, ruches, Cocagnes)
> 8. Vente direct à la ferme, libre cueillette

Note de 0, je ne pratique jamais, à 10, c'est une source majeur et permanente.

La représentation en radar permet de superposer un objectif et une analyse des pratiques courantes. Je ne suis pas sur de comprendre le jeu de couleurs, les branches auraient pu être cote à cote sans doute ? La graduation n'est pas évidente. Est-il possible de trouver un autre moyen de « noter » sur ces mode d'approvisionnement ?

J'aime bien voir apparaître la dimension gestion de l'eau et la phytoremédiation. C'est assez rare dans les analyses et proposition autour de l'alimentation, et pourtant, c'est tellement important.

## Sept clés pour réussir

Le livre se termine par ces clefs de réussite.
> 1. Avoir une vision commune partagée
> 2. Avoir conscience du pouvoir des petites actions
> 3. Éprouver la richesse de ce que nous partageons
> 4. Ne pas oublier que nous sommes les créateurs de notre réalité
> 5. Impliquer les enfants
> 6. Vivre la joie comme essence
> 7. Se connecter les uns aux autres

Je découvre le mot Aggrader

> Désigne l'acte d'enrichir les soles par différents moyens naturels issus de la biomasse. Ce verbe a été peu utilisé jusqu'à récemment. Un des premiers scientifiques à l'avoir évoqué est le Pr Gilles Lemieux de l'université Lava, au Québec, pour qualifier d'aggradant le bois raméal fragmenté (BRF).

Un livre intéressant qui montre qu'il y a des moyens d'agir.

J'ai d'abord pensé que c'était une approche opposé à Deep Green Resistance, mais finalement, je me dit que c'est complémentaire. À condition de metre de coté la dévalorisation des actions « individuelles » que l'on peut trouver (reprocher ?) à l'approche de Deep Green Resistance.


---

Je visualise une vidéo (perdu depuis) sur la confection du papier. Ça serait marrant de fabriquer du papier.

- Papier en papyrus
- Papier en fibre de murier

Ça donne des idées d'activités loisir, plaisir. Je trouve ça magnifique de faire ce genre de chose.

> Selon la légende, durant le règne des Abbassides, le secret de la fabrication du papier est obtenu de deux Hans, faits prisonniers lors de la bataille de Talas en 751. Cette invention permit la fondation de la première papeterie de Samarcande et se diffusa dans le reste du monde islamique et, plus tard, en Europe.

[Samarcande](https://fr.wikipedia.org/wiki/Samarcande)

