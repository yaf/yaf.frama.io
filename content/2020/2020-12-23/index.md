---
title: mercredi 23 décembre 2020
---

> Chaque employé de Captain Train était connecté aux utilisateurs. Sans filtre. En direct. Comment faire un mauvais produit dans ces conditions ? Quand vous savez qu’une fois par mois, vous allez devoir gérer vous-même les clients, cela vous pousse forcément à essayer de régler les problèmes avant qu’ils n’arrivent !
> -- Frédéric Bardolle 

[Faire un bon produit nécessite d'être connecté à ses utilisateurs](https://f14e.fr/2020/12/22/faire-bon-produit-connecter-utilisateurs/)

J’ai toujours beaucoup apprécié le soin porté à la relation client, aux utilisateurs. Chez Capitaine Train, c’était un élément clef, chez Scalingo, je trouve le résultat plutôt bon également. À ma mesure, j’essaie de faire de même dans les équipes où j’évolue.

---

Parce que PostGreSQL est un gestionnaire de base de donnée vraiment bien fait, avec beaucoup de fonctionnalités très pratique quant au stockage de données, j'aime cet outil ;
Parce que PostGreSQL est distribué sous licence libre, j'aime ce logiciel ;
Parce que PostGreSQL est géré par une communauté et non une entreprise, j'aime les personnes qui travail dessus.

Et souvent je m’extasie devant une fonction que j'avais oublié, sur une page de doc redécouverte lors d'une recherche...

Aujourd'hui, c'est les fonctions autour des dates [Date/Time Functions and Operators](https://www.postgresql.org/docs/9.1/functions-datetime.html).

Merci PostGreSQL !

---

[_Malus sylvestris_ le pommier sauvage](https://fr.wikipedia.org/wiki/Malus_sylvestris) et le [_Malus domestica_ pommier commun ou domestique](https://fr.wikipedia.org/wiki/Pommier_domestique). Un arbre fruitier important dans nos régions. Important car bien acclimaté, fournissant des fruits vraiment exceptionnel au niveau nutritif, et relativement simple à faire pousser et entretenir.

Il est intéressant malgré tout de voir la recherche autour de cet arbre. Un projet assez intéressant, à suivre sans doute :
[Un verger de pommier sauvage conservatoire et expérimental sur le plateau de Saclay](https://www.youtube.com/watch?v=QzP_HPDUEbA). Voir également sur le site de l'université : [Un projet de recherche sur le pommier sauvage](https://www.ideev.universite-paris-saclay.fr/fr/le-verger)
