---
title: mercredi 30 décembre 2020
---

Journal de mon activité [RDV-Solidarités](https://www.rdv-solidarites.fr/)

Début de journée Rdv 9h45

- j'entreprends de regarder un peu les tickets qui reste dans le trello brouillon
- Le trello brouillon est vidé
- nettoyage des motifs invalides de la base de données de prod. Il y avait 4 motifs. 2 sans rendez-vous, supprimer directement, 1 avec un rdv pour Aude (un test sans doute), rdv supprimer, et motif supprimer). LE dernier avait 10 rdv. Chacun à été déplacé vers le motifs valide correspondant, puis le motif invalide à été supprimé.

Pause à 12h30

Reprise à 15h

- Après un petit coup d'œil aux premiers ticket, je repense à des problématiques UX, au Natural Language Form (https://tympanus.net/Tutorials/NaturalLanguageForm/), Design System, au déplacement du paramétrage dans un menu spécifique (comme la secto)... Et puis je choisi un plus petit ticket
- exporter ce qui est affiché. Ça fait créer un exporte qui fait sortir des informations lié à l'usager... C'est un point chaud pour le RGPD.

pause de 16h30 à 18h
