require 'test/unit'

DUCK="duck"
ELEPHANT="elephant"
SNAKE="snake"
FILE="test_file"

class TestFileConcurrencyAccess < Test::Unit::TestCase
  def test_concurrency_access
    t1 = Thread.start do
      File.open(FILE, "w") do |file|
        file.puts SNAKE
        file.flush
        sleep 8
        file.puts DUCK
      end
    end
    File.open(FILE,"r") do |f1|
      assert SNAKE, f1.read
    end
    t2 = Thread.start do
      File.open(FILE,"w") do |f|
        sleep 2
        f.puts ELEPHANT
      end
    end
    t2.join
    File.open(FILE,"r") do |f2|
      assert ELEPHANT, f2.read
    end

    t1.join
    File.open(FILE,"r") do |f3|
      assert SNAKE, f3.readline
      assert DUCK, f3.readline
    end

  end
end

