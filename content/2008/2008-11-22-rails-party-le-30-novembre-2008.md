---
layout: post
title: Rails Party le 30 novembre 2008
---

  Avec les feuilles qui tombent vient le temps de "ParisOnRails":http://paris.onrails.info/ . Et cette année, plutôt que d'organiser un apéro Ruby la veille au soir, l'association "RubyFrance":http://rubyfrance.org a décider d'organiser une "Rails Party":http://rubyfrance.org/evenements/rails-party-2008/

*Au programme*

L'après-midi sera consacré à des ateliers, sûrement autour de "Typo":http://typosphere.org, peut-être de "Pictrails":http://pictrails.rubyforge.org/, "RSpec":http://rspec.info/ et sûrement bien d'autres !

La soirée sera ouverte au Lightning talks (présentation courte d'une dixaine de minutes).

L'évènement se veut conviviale, c'est avant tout un moyen de ce retrouver entre raillers avant les conférences du lendemain.

Rendez-vous donc le dimanche 30 novembre 2008 de 16 heures à 22 heures au Dune, 18 avenue Claude Vellefaux, dans le 10ème arrondissement de Paris.

"Cyril y sera (Pictrails, Typo, et plein de rails dans la tête)":http://blog.shingara.fr/2008/11/21/la-before-paris-on-rails-cest-la-rails-party-le-30-novembre-2008, "Frederic viens aussi (Typo, Ergonomie, Web, et plein de chose à dire":http://t37.net/rails-party-le-30-novembre-2008-i-paris, bien sur l'association "RubyFrance":http://rubyfrance.org avec tout plein de membre motivé autour de l'organisateur des évènements Ruby Parisien Jean-François !

A dimanche alors !




  