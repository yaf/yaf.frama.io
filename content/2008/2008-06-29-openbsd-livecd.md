---
layout: post
title: OpenBSD LiveCD
---

  Il existait déjà quelques versions officieuses, mais cette fois, c'est la bonne. Suite à plusieurs discussion sur la _mailling list_ @misc@@ l'équipe de "BSDAnywhere":http://bsdanywhere.org/ propose maintenant une version _live_ d'"OpenBSD":http://www.openbsd.org.

Ce live CD contient, outre OpenBSD, un bureau sous "Enlightenment":http://www.enlightenment.org/, "AbiWord":http://www.abisource.com/, "Firefox":http://www.mozilla.com/en-US/firefox/, "Thunderbird":http://www.mozilla.com/en-US/thunderbird/, "Gimp":http://www.gimp.org/, et toute une liste de paquet que vous pouvez consulter sur leur site: "http://bsdanywhere.org/software":http://bsdanywhere.org/software

_via "Undealy.org, le journal officiel d'OpenBSD_":http://undeadly.org/cgi?action=article&amp;sid=20080629134031


  