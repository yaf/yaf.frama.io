---
layout: post
title: Et pourquoi pas Wmii iiiiiiiii
---

  A force de placer mes fenêtres toujours au même endroit, avec la même taille (ou presque) et de repartir ces _groupes_ sur les divers bureaux, je me suis dit qu'il étais peut-être temps de tester un autre gestionnaire de fenêtre.

"Ion":http://modeemi.fi/~tuomov/ion/ semblait un bon candidat, mais la configuration ne me donnais pas super envie, et les divers lectures au sujet de l'auteur et de la licence de l'application ont fini par me couper l'envie de l'adopté.

Alors j'ai continué à fouiller dans les gestionnaires de fenêtre accès clavier et simplicité. Sur la list misc@ d'"OpenBSD":http://www.openbsd.org il y avait eu une discussion sur le gestionnaire de fenêtre par défaut. Actuellement "FVWM":http://www.fvwm.org/ (premier du nom), certain voulais voir "cwm":http://monkey.org/~marius/pages/?page=cwm prendre la place. Du coup, il fallait que je l'essaie.

Assez sympathique ma fois. Simple, avec quelque raccourci clavier rappelant "Vi(m)":http://www.vim.org/. Très rapide, et très agréable à utiliser.

Cependant, je n'etais pas encore convaincu. J'avais entendu parlé d'un autres gestionnaire qui pourrais remplacer Ion durant mes recherches: "Wmii":http://fr.wikipedia.org/wiki/Wmii

Alors pour être dérouté, on peut l'être. C'est très interessant. Ici point de menu, d'icône, de boutons de fenêtre. Une petite barre, quelques touches et c'est parti. Un des concept est de ne pas avoir à gérer les fenêtre justement: on arrive sur une _frame_, une petite combinaison de touche et hop, un terminal en plein écran. On en lance un deuxième, pour voir, et voilà deux terminaux qui prenne chaucun une moitié d'écran, un troisième et ... non, je vous laisse deviner. Une autre combinaison de touche plus loin et paf, c'est sur deux colonnes que ça se passe, royal. Plus la peine de placer les fenêtre ! :) Plus de place à force de lancer des applications ? Une petite combinaison et paf, je met la nouvelle appli sur une autre _frame_ (on pourrais les apparentés à un bureau virtuel qui s'étend a mesure du besoin).

J'adore :)

Et pour ne rien gacher, je me rends compte qu'un figure du monde "Ruby":http://www.ruby-lang.org , "Mauricio Fernandez":http://eigenclass.org/ aime et utilise (il me semble) lui aussi ce gestionnaire de fenêtre (il a d'ailleurs commencer à faire quelque script et autre bricole en "ruby pour Wmii":http://eigenclass.org/hiki/wmii+ruby , mais c'est une autres histoire).

Et dans toute ces histoires de script et de config, je tombe sur encore une sommité du monde Ruby: "Why":http://redhanded.hobix.com/inspect/aFewWmii3Hacks.html qui apparemment aime et utilise Wmii ! _(D'ailleurs, j'ai découvert qu'il utilisais "DragonFlyBsd":http://www.dragonflybsd.org/index.shtml contrairement à tout les macaddict que l'on *voit* beaucoup dans le monde de Ruby sur les rails ;-) )_

Finalement je ne suis pas trop surpris, je trouve que l'auteur de "Wmii":http://www.suckless.org/wiki/wmii ressemble un peu à ces deux là. "Anselm R Garbe":http://www.suckless.org/~arg/ est un doux dingue du petit code efficace, c'est d'ailleurs le concept du site dont il est fondateur  "sukless.org":http://www.suckless.org/wiki/about :

<blockquote>
Dedicated to software which sucks lessâ€¦

The upper boundary on the size of the software we accept is 10,000 source lines of code (SLOC).
</blockquote>

J'ai encore plein de chose à découvrir, mais si vous voulez essayé un gestionnaire de fenêtre innovant, je vous conseil d'essayer "Wmii":http://www.suckless.org/wiki/wmii

_ps: j'allais oublié. le peu d'intérêt d'un screenshot avec ce genre de gestionnaire de fenêtre explique le manque d'image de ce billet :)_


  