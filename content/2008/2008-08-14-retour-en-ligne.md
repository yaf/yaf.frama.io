---
layout: post
title: Retour en ligne
---

  bq. Toutes les bonnes choses ont une fin

Les vacances ne sont pas encore tout à fait fini pour moi, mais me revoilà avec un ordinateur connecté à internet dans les mains :-)

Quelques images de la Bretagne, les "Côtes d'Armor":http://fr.wikipedia.org/wiki/C%C3%B4tes-d%27Armor pour être plus précis. Cela faisait un moment que je n'avais pas vu la Bretagne, ça fait du bien :-)

!http://farm4.static.flickr.com/3200/2762412663_fcb9282ca0.jpg?v=0!:http://www.flickr.com/photos/yafra/2762412663/

!http://farm4.static.flickr.com/3007/2762412667_e483b9a1fe.jpg?v=0!:http://www.flickr.com/photos/yafra/2762412667/

_C'est très beau la côte de granite rose, nous avons parcouru le GR34 par petit morceau pour la découvrir, je vous le conseil !_



  