---
layout: post
title: 5e apéro rubyFrance
---

  _une semaine plus tard_

Cet session fût très bonne. Pas loin de 30 personnes ont fait le déplacement pour cette "apéro" qui en fait ressemblait plus à une bonne présentation.

Le thème principal de ce lundi était l'agilité, l'extreme programming, les tests. En effet, des membres de l'association "XP-France":http://www.xp-france.net sont venus nous présenter l'agilité, le développement piloté par les tests le tout dans sous la forme d'un *kata*, une des pratiques de dojo.

Ensuite, Jean-François nous a présenter une toute nouvelle librairie ruby qui gagne à être connu: Arel également appelé "ActiveRessource":http://wiki.rubyonrails.org/rails/pages/ActiveResource. Un librairie visant à permettre la création d'ORM(Object Relationnal Mapping). Disont pour résumé que cela enlèverais la couche "concaténation de chaine de caractères" dans ActiveRecord par exemple, et du coup nous aurions le moyen de construire plus joliement des requête SQL. A suivre donc.

Depuis quelque temps déjà je m'interesse aux méthodes agiles, à l'extreme programming, cette présentation à fini de me convaincre qu'il faut absoluement que j'aille en Dojo pour pratiquer le code, échanger avec d'autres personnes aguéri à ces techniques de tests et de façon de voir le code.

On en reparle plus tard ;-)


  