---
layout: post
title: "1er novembre : OpenBSD 4.4"
---

  On y est ! Comme d'habitude, pile à l'heure. "OpenBSD 4.4":http://marc.info/?l=openbsd-announce&amp;m=122547376504077&amp;w=2 est disponible.
Pas mal de nouveautés pour cette version à la numérotation mythique ! Je vous laisse découvrir cela.

!http://zone.typouype.org/openbsd44_cover.gif!:http://openbsd.org/44.html

Je vous ajoute une info paru sur la mailling-list de l'"April":http://www.april.org:

<blockquote>
Bonjour,

Dimanche 2 novembre 2008 de 14H à 15H30, Symbiose recevra Miod Vallat et Marc Espie du projet OpenBSD. Au menu, présentation de ce système d'exploitation et de sa philosophie.

Symbiose est diffusée sur radio libertaire 89.4 en région parisienne, audible également en streaming :

MP3 qualité ADSL : "http://ecoutez.radio-libertaire.org:8080/radiolib.m3u":http://ecoutez.radio-libertaire.org:8080/radiolib.m3u

MP3 qualité modem : "http://ecoutez.radio-libertaire.org:8080/radiolib-modem.m3u":http://ecoutez.radio-libertaire.org:8080/radiolib-modem.m3u

OGG qualité ADSL : "http://ecoutez.radio-libertaire.org:8080/radiolib.ogg.m3u":http://ecoutez.radio-libertaire.org:8080/radiolib.ogg.m3u

OGG qualité modem : "http://ecoutez.radio-libertaire.org:8080/radiolib-low.ogg.m3u":http://ecoutez.radio-libertaire.org:8080/radiolib-low.ogg.m3u

L'enregistrement de l'émission (sous licence CC by-nc-nd) sera disponible au téléchargement et en podcast : "http://archive.symbiose.free.fr":http://archive.symbiose.free.fr "http://www.onirik.net":http://www.onirik.net
</blockquote>

Une émission qui sera sûrement très interessante.


"L'annonce dans le journal officiel d'OpenBSD":http://undeadly.org/cgi?action=article&amp;sid=20081031165518

_a noter: un très bon thème basé sur StarWars pour cette version ! :)_



