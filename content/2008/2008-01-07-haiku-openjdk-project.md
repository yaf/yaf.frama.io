---
layout: post
title: Haiku - OpenJDK Project
---


Le petit OS qui avance dans l’ombre de BeOS commence doucement à s’étoffer. [Après les avancés du port du webkit](http://www.typouype.org/articles/2007/12/19/haiku-webkit) Haiku lance un projet de portage de la <span class="caps">JVM</span> ouverte de Sun: OpenJDK.

Je ne suis pas sur qu’Haiku vise à être une grande plateforme de développement (quoique pourquoi pas ;-)), mais quoiqu’il arrive, avoir une machine virtuel java porté pour votre OS est quasi indispensable ! Et contrairement à Adobe qui garde sont FlashPlayer bien fermé, Sun, en ouvrant la <span class="caps">JVM</span> permet à des équipes divers de la porter sur les OS passé, présent et futur.

!(http://haiku-os.org/files/screenshots/logo_haiku-openjdk.png)

Pour ceux qui serais interessé, je vous laisse lire la news officiel de la création de l’équipe [Haiku-OpenJDK](http://haiku-os.org/news/2008-01-03/new_java_for_haiku_team_formed) pour en savoir plus.

*Un projet bien interessant, je vais m’y interesser de prêt... Peut-être même plus vu mon profil :)*


