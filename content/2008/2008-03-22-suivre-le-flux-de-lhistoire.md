---
layout: post
title: Suivre le flux de l'histoire
---

  Quelle belle invention la "syndication":http://fr.wikipedia.org/wiki/Syndication. Que ce soit au format "RSS":http://fr.wikipedia.org/wiki/Flux_RSS ou "Atom":http://fr.wikipedia.org/wiki/Atom, cela nous permet de  nous tenir au courant. Mais je ne souahite pas épiloguer sur ce point, ni sur la différence entre les deux formats, d'autres le font bien mieux que moi.

Beaucoup d'outil sont disponible pour suivre ces flux, _offline_ bien sur, mais je fais parti de ces gens qui préfère aller voir le nouveau _billet_ sur le site éméteur. J'aime voir, ressentir l'univers de l'auteur durant ma lecture. Bien souvent on comprend mieux un texte quand il est placer dans un contexte.

Pour me permettre de _veiller_ même en dehors de chez moi, même sur une autre machine que la mienne, j'ai choisi d'utiliser un _aggrégateur_ de flux en ligne. Mon choix c'est porté sur "Netvibes":http://www.netvibes.com. Le soucis avec cet outils, c'est que c'est pour ce construire un portail personnalisé, pas forcement pour aggréger 300 flux :). Du coup ça rame, ce n'est pas très pratique à utiliser. En parallèle je me suis donc mis à utiliser "bloglines":http://www.bloglines.com. Mais là encore je ne suis pas super satisfait.

Effectivement, je souhaiterais pouvoir parcourir les titres par _tag_ par exemple, que la liste des billets non lu soit constitué offline. Je ne veux pas, en venant voir les news, attendre que l'application fasse le tour des sites pour vérifier les nouveautés.

Mais est-ce que cela ne va pas à l'encontre du web justement ? Dans l'application que je souhaite mettre en place, je vais me retrouver à centraliser des informations déjà disponible sur des sites distants (la liste des billets non lu). D'un autres coté, comment faire autrement ? Cette application pourrais être vu que une gestion en ligne de marque page doté de fonctionnalitées supplémentaire.

_to be continued_


  