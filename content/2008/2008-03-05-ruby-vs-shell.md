---
layout: post
title: Ruby vs Shell
---


Que je n’aime pas ce genre de comparaison. Ou plutôt ce que je n’aime pas c’est de chercher un vainqueur. En fait je voulais vous raconter une petite histoire informatique.

Un jour, chez mon actuel client, je me suis retrouvé face a un *cas d’école*: faire un programme pour réaliser une purge de la base de donnée, mais pas entièrement, juste quelque table *fonctiono-transactionnelle*.

Une bonne chose pour moi, un programme existe déjà dans le foutoir^la boite à outil de l’équipe en place. Ce programme va me permettre d’extraire la liste des tables dépendantes de celle que je veut purger. J’appel ce programme (réalisé dans un langage propriétaire, comme l’application) en passant en paramètre la table que je souhaite supprimer, et lui va aller déclencher les divers *trigger* en interceptant les erreurs pour pouvoir faire la liste de toutes les tables dépendantes. Bien, je répète l’opération pour toutes les tables que j’ai à *nettoyer*

A la sorti je me retrouve avec un gros fichier texte, contenant plusieurs fois le même nom de table à divers endroit du fichier, pas très pratique. Il faut que je nettoie tout ça. Apparemment mes collègues font ça à la main habituellement... Moi c’est hors de question. L’informatique c’est justement fais pour faciliter le travail non ?

Alors je lance [cygwin](http://cygwin.com/) (heureusement qu’il est autorisé chez mon client celui là, sinon j’aurais pleuré !!) et je regarde quelque manpage pour vérifier les commandes à utiliser. Je trouve mon bonheur, et le dieu pipe va grandement m’aider sur le coup:

* La commande [cat(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=cat&amp;apropos=0&amp;sektion=0&amp;manpath=OpenBSD+Current&amp;arch=i386&amp;format=html) permet de _lire_ le contenu d'un fichier.
* Dieu pipe va ensuite transférer ce contenu à la commande [tr(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=tr&amp;apropos=0&amp;sektion=0&amp;manpath=OpenBSD+Current&amp;arch=i386&amp;format=html) qui elle va me mettre le contenu en minuscule (c'est plus pratique pour trier (c'est le A-Z a-z qui précise le type de transformation à appliquer au contenu).
* Dieu pipe intervient encore pour passer ce contenu minuscule à la commande [sort(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=sort&amp;apropos=0&amp;sektion=0&amp;manpath=OpenBSD+Current&amp;arch=i386&amp;format=html) qui va elle, comme sont nom l'indique, trier le contenu.
* Dieu pipe une dernière fois va passer le contenu minuscule trié à la commande [uniq(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=uniq&amp;apropos=0&amp;sektion=0&amp;manpath=OpenBSD+Current&amp;arch=i386&amp;format=html) qui comme sont nom l'indique encore, va éliminer les doublons.
* Dieu pipe passe la main ici à la reine &gt; qui va me permettre d'écrire le résultat de tout ça dans un fichier et non à l'écran.

Je me retrouve avec un fichier parfait pour faire ma purge.

C'est bien beau, mais une question trottais dans ma tête: Comment faire cela en Ruby ? Est-ce que j'y arriverais en une ligne aussi ?

Et bien comme d'hab, oui c'est faisable, et oui avec Ruby on peut le faire en une ligne.

Quel intérêt ? Dans le mien auccun, à part mon plaisir. Mais si ce script devais être re-utilisé, ou bien intégrer à un outil plus vaste, cela aurait tout de suite plus d'intérêt :)

Voilà ce que ça donne:

*Bon, en une ligne c’est un poil abusé et pas lisible c’est pour cela que je le présente sur plusieurs :),  mais c’est faisable*

Pour faire cela, une bonne lecture des objets [Array](http://www.ruby-doc.org/core/classes/Array.html), [IO](http://www.ruby-doc.org/core/classes/IO.html) et [File](http://www.ruby-doc.org/core/classes/File.html#M002602) iront très bien :)

