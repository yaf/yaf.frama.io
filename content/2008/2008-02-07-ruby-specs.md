---
layout: post
title: Ruby Specs
---

  
Pour les personnes qui s’interesse à [Ruby](http://www.ruby-lang.org), qui comprennent l’anglais, et/ou qui souhaite participer à l’évolution, l’enrichissement d’un langage objet libre, je viens de découvrir un wiki-kil-est-bien:

[Ruby Spec](http://spec.ruby-doc.org/wiki/Main_Page)

Plus ou moins initié par les divers groupes cherchant à implémenter Ruby dans sur d’autres machine virtuel (comme le projet [Rubinius](http://rubini.us/) ou [Jruby](http://jruby.codehaus.org/)) mais assez suivi dans la communauté en général, ce wiki permet de spécifié chaque objet/module.

Selon les pages c’est soit pas encore fait, soit simple (juste du texte), soit bien complet avec des exemple très interessant avec des exempls et des petites comparaisons avec d’autres objets. Comme par exemple sur l’"objet Proc":http://spec.ruby-doc.org/wiki/Proc

**A consulter et enrichir !**

  