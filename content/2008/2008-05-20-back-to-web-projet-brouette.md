---
layout: post
title: Back to web - projet brouette
---

  Non, je n'étais pas vraiment parti (quoique presque), mais surtout très occupé.

C'est un peu l'inconvénient d'être dans chez un client qui pratique le mode projet _brouette_. J'entends par là que l'on developpe en _sous-marin_ et que l'on déverse l'ensemble de l'application, directement en production, aux utilisateurs. Quand en plus ils n'ont pas demandé à avoir une nouvelle application, c'est pire.

Donc voilà, je suis arrivé au moment oÃ¹ l'on vient de verser l'ensemble du projet, et du coup, faut passer un peu la serpière, colmater les fuites... Ca me promet quelque jours bien chargé encore, mais ça commence à aller mieux.

!http://zone.typouype.org/Flag_of_the_Czech_Republic.png!

J'ai eu la chance (si on veut) d'aller avec l'équipe vider la brouette en production sur place: en République Tchèque, a Pragues. Malheureusement, je n'ai pas vu grand chose: L'hotel, la filialle, le chemin entre les deux, et les rare restaurants prêt de l'hotel qui était encore ouvert après 23h30 (quand on rentrais à l'hotel pour dormir un peu).

Ca faisait longtemps que je n'avais pas fait autant d'heures.

Je ne sais pas ce qui me gène le plus dans tout ça, le fait de _gacher_ une bonne équipe en la faisant travailler n'importe comment (grosse pertes d'energie en broutille) ou bien de _fourguer_ aux utilisateurs une application qui ne correspond pas vraiment à leurs besoins :-/ Le deuxième point est le pire je crois.

Je commence à avoir trop de mission dans le genre à mon actif, ça me fatigue. Vivement la prochaine, en espérant que je pourrais influancer un peu plus la façon de travailler au moins, voir la façon de recueillir le besoin et de mettre en place la meilleur solution *pour les utilisateurs*. Ou bien, que je tombe sur une équipe qui soit dans ce genre d'objectif !

!http://zone.typouype.org/Coat_of_arms_of_the_Czech_Republic.png!

_A suivre..._



  