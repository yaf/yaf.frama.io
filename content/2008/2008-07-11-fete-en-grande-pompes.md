---
layout: post
title: Fête en grande Pompe
---

  On connait (ou pas) ce merveilleux petit _framework_ d'interface graphique en "Ruby":http://www.ruby-lang.org :  "Shoes":http://code.whytheluckystiff.net/shoes/ (une oeuvre signé "_Why":http://whytheluckystiff.net/ encore une fois !).

!http://hackety.org/images/nks-small.png!:http://code.whytheluckystiff.net

2 grandes rencontres virtuelles vont avoir lieu pour partager, tester, discuter, découvrir, documenter autour de ce _framework_: l'une à lieu en ce moment (vendredi 11 Juillet) et l'autre aura lieu dans 2 semaines (le vendredi 25 Juillet). C'est toute la journée, ça se passe sur "IRC":http://fr.wikipedia.org/wiki/Irc : #shoes@freenode.net , c'est ouvert à tous: développeur, testeur, documenteur, partageur, découvreur; du _framework_ ou bien d'application l'utilisant. _Vu le coté international, ça se passe en Anglais bien sur._

Si vous ne connaissez pas Shoes, c'est peut-être le moment d'aller découvrir ce _framework_. D'ailleurs, c'est un évènement qui précède la prochaine grosse release qui devrait avoir lieu à la fin du mois.

L'annonce officiel de l'évènement: "7/11 &amp; 7/25 ShoesFests with Why The Lucky Stiff":http://ihack.us/2008/06/27/shoesfests-on-july-11th-and-july-25th/
L'annonce sur RubyInside: "Join Why The Lucky Stiff (And Others) For an Online â€œShoesFestâ€":http://www.rubyinside.com/shoesfest-announcement-948.html


  