---
title: Boîte à idée
---

_ou bien liste énorme de truc que j'aimerais bien faire un jour_

- Transformer les questions de programmation en kata ?
- Retranscrire les écrits sur [la Ruche de Faure](https://gallica.bnf.fr/ark:/12148/bpt6k67585g/f3.image.r=.langEN)
- Poser des bases de projet sur [glitch](https://glitch.com/) à destination des personnes débutantes
- Refaire le site des [éléments](http://elements.wlonk.com/ElementsTable.htm voir http://elements.wlonk.com/ElementsTable.htm) en HTML/CSS/JavaScript vanillia
- Explorer [Racket](http://docs.racket-lang.org/quick/index.html)
- Participer à [Telebotanica](https://github.com/telabotanica)
- Faire un exercice de [fonction calisthenics](https://codurance.com/2017/10/12/functional-calisthenics/)
- Faire un exercice d'[objet calisthenics](https://williamdurand.fr/2013/06/03/object-calisthenics/)
- Creuser [Liberating Structure](https://www.liberatingstructures.fr/principes-des-ls/)
- [Alan Trotter](https://alantrotter.com/) Inspiration, un petit air commun avec [Natural Language Form](https://tympanus.net/Tutorials/NaturalLanguageForm/) ?
- Refaire le site d'[ouverture facile](http://www.ouverture-facile.com/) en logiciel libre et sans flash
- Faire l'exercice de [fabriquer une base de donnée](https://cstack.github.io/db_tutorial/)
- faire une sorte de [Vim adventures](https://vim-adventures.com/) mais en licence libre
- [Semantic](https://github.com/github/semantic), un outil d'analyse de code source
- Sujet d'exercice ? À tester [Quine Relay](https://github.com/mame/quine-relay)
- Rencontré aux RMLL 2018, un lyonnais : Mougeot. Echanger avec le [CEDRATS](http://www.cedrats.org/)
- À tester ? [QuineDB](https://github.com/gfredericks/quinedb), peut-être un peu comme [TinyDB](https://tinydb.readthedocs.io/en/latest/)
- [Liste d'algorithme en go](https://github.com/arnauddri/algorithms), intéressant à étudier ?
- Faire du [Elm](http://elm-lang.org/)
- Savoir filter de l'eau
- Savoir reconnaître 10 plantes utiles
- Étudier Orlov
- Lecture de code suivi
- Un page web pour parler du concept de lecture de code, de mon, approche de la programmation
- Documenter les formats d'apprentissage
- Repérer les code Smell
- Passer sur Sparrow Decks
- Sparrow deck sur les plantes comestibles, sur les arbres, sur les insectes, sur les vers de terre, sur les fleurs, ...
- Analyser le plomb dans le sol, il doit être < 400 ppm si le PH est >= 7 Peut-être faut-il tester le PH avant parce que c'est plus facile ?
- Une plante qui nettoie le sol des métaux lourds http://www.tela-botanica.org/bdtfx-nn-103108-synthese peut-être aussi http://www.humanite-biodiversite.fr/article/des-plantes-hyperaccumulatrices-de-metaux-lourds, la moutarde aussi le fait https://fr.wikipedia.org/wiki/Moutarde_brune
- Plantes indicatrices de polution http://fablabo.net/wiki/Plantes_bio-indicatrices voir aussi http://permaforet.blogspot.fr/2013/04/plantes-bio-indicatrices.html https://sites.google.com/site/tpesurlaphytoremediationgroupe/La-depollution-des-metaux-lourds


## Livres

- Lire Tolstoï, Paul Robin, Francisco Ferrer
- Lire Sébastien Faure, Écrits Pédagogiques , Éd. du Monde Libertaire, réédition 1992

- Lire Roland Lewin, Sébastien Faure et « La Ruche » , ou l'Éducation Libertaire , Éd. Ivan Davy, 1989
- [Sébastien Faure Et "La Ruche" Ou L'éducation Libertaire - Roland Lewin](https://www.priceminister.com/offer/buy/577123/Lewin-Sebastien-Faure-Et-Ruche-Livre.html)

- Lire Édouard Stéphan, La Ruche, une école libertaire au Pâtis à Rambouillet, 1905-1917 , Éd. SHARY, 2000
- Lire « L’Arbre-Monde » de Richard Powers
- Livre à lire ? [La femme et les champignons](https://www.tela-botanica.org/2019/01/la-femme-et-les-champignons-de-long-litt-woon/)
- Lire [Comment la non violence protège l'état](https://editionslibre.org/produit/prevente-comment-la-non-violence-protege-l-etat-peter-gelderloos/)

En attente sur https://www.recyclivre.com :

- "La Botaniste de Damas" également réédité cette année, Simone Lafleuriel-Zakri
- [Surveiller et punir](https://fr.wikipedia.org/wiki/Surveiller_et_punir) de Michel Foucault
- [Une société sans école](https://www.amazon.fr/soci%C3%A9t%C3%A9-sans-%C3%A9cole-Ivan-Illich/dp/2757850083)
- [La fin de l'éducation ? Commencements... ](https://www.babelio.com/livres/Lepri-La-fin-de-leducation--Commencements/521383)
- [La révolte luddite : Briseurs de machines à l'ère de l'industrialisation Kirkpatrick Sale]()

Pas touvé sur https://www.recyclivre.com :

- "L'herboriste andalou"  également réédité cette année, Simone Lafleuriel-Zakri
- [Voyage en misarchie](http://editionsdudetour.com/index.php/les-livres/voyage-en-misarchie/)
- https://www.babelio.com/livres/Thoreau-Walden-ou-La-vie-dans-les-bois/84891
- https://www.placedeslibraires.fr/livre/9782081428577
- https://www.syllepse.net/afro-communautaire-_r_37_i_786.html
- https://www.syllepse.net/les-dispensaires-autogeres-grecs-_r_25_i_673.html
- https://www.syllepse.net/sur-les-chemins-de-l-emancipation-l-autogestion-_r_64_i_759.html
- https://www.syllepse.net/la-liberte-contre-le-destin-_r_76_i_701.html
- https://www.syllepse.net/pour-une-ecole-emancipatrice-_r_98_i_528.html
- https://www.syllepse.net/apprendre-a-transgresser-_r_62_pageid_2_i_776.html
- https://www.syllepse.net/pistes-pour-une-agriculture-ecologique-et-sociale-_r_100_i_597.html
- [La fin de l'éducation ? Commencements... ](https://www.babelio.com/livres/Lepri-La-fin-de-leducation--Commencements/521383)
- [La révolte luddite : Briseurs de machines à l'ère de l'industrialisation Kirkpatrick Sale]()
- https://www.syllepse.net/dictionnaire-des-dominations-_r_76_i_532.html
- Le municipalisme libertaire - La politique de l'écologie sociale Broché – 7 janvier 2014 de Janet Biehl
- https://livre.fnac.com/a9455378/Alexander-samuel-Entropia
- https://www.amazon.fr/dp/226625409X/ref=cm_sw_r_cp_awdb_c_oRmXCbT8CNV87
- L’art de conter nos expériences collectives   Benjamin Roux 
- https://www.babelio.com/livres/Ba-Une-si-longue-lettre/3426
- https://www.babelio.com/livres/Sapienza-Lart-de-la-joie/5719
- https://www.babelio.com/livres/Ba-Un-chant-ecarlate/118778
- https://www.franceculture.fr/oeuvre/le-ventre-des-femmes-capitalisme-racialisation-feminisme
- https://fr.m.wikipedia.org/wiki/Bolo%27bolo
- [Sentir-penser avec la Terre](http://www.seuil.com/ouvrage/sentir-penser-avec-la-terre-arturo-escobar/9782021389852)
- [Vivre ma vie. Une anarchiste au temps des révolutions](https://www.lechappee.org/agenda/vivre-ma-vie-une-anarchiste-au-temps-des-revolutions-7)
- [Le Changement climatique expliqué à ma fille](http://www.seuil.com/ouvrage/le-changement-climatique-explique-a-ma-fille-jean-marc-jancovici/9782020965972)
- [Le capitalisme expliqué à ma petite fille en espérant qu'elle en voie la fin](http://www.seuil.com/ouvrage/le-capitalisme-explique-a-ma-petite-fille-jean-ziegler/9782021397222)
- [Livres enfants éducation](https://www.auxeditionsduphare.com/)
- [Économie Symbiotique](https://www.actes-sud.fr/catalogue/economie/leconomie-symbiotique)
- [Magasin Général](https://regisloisel.com/magasin-general)
- [Femmes, magie et politique Starhawk](http://www.peripheries.net/article215.html)
- [L'évaluation du travail à l'épreuse du réel](https://www.cairn.info/l-evaluation-du-travail-a-l-epreuve-du-reel--9782759224609.htm)
- [Stratégies végétales, petits arrangements et grandes manoeuvres Relié – 15 décembre 2011 de Benoît Garrone (Auteur),‎ Philippe Martin (Auteur),‎ Bertrand Schatz (Auteur),‎ Les Ecologistes de l'Euzière (Auteur)](https://www.amazon.fr/Strat%C3%A9gies-v%C3%A9g%C3%A9tales-arrangements-grandes-manoeuvres/dp/2906128287?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&tag=duc-21&linkCode=xm2&camp=2025&creative=165953&creativeASIN=2906128287)
- [Libres savoirs : les biens communs de la connaissance](https://tempsdescommuns.org/libres-savoirs/)
- http://editionslibertalia.fr/catalogue/nautre-ecole/12-la-joie-du-dehors
- [Stratégies végétales, petits arrangements et grandes manoeuvres](https://www.amazon.fr/Strat%C3%A9gies-v%C3%A9g%C3%A9tales-arrangements-grandes-manoeuvres/dp/2906128287)
- [La symbiose](https://www.amazon.fr/symbiose-Marc-Andr%C3%A9-Selosse/dp/2711752836)
- [Éloge de la plante Francis Hallé](http://www.seuil.com/ouvrage/eloge-de-la-plante-pour-une-nouvelle-biologie-francis-halle/9782020684989)
- [Un monde sans hiver Francis Hallé](http://www.seuil.com/ouvrage/un-monde-sans-hiver-francis-halle/9782757838242)
- [Éducation libertaire par l'auteur Hugues Lenoir](http://www.hugueslenoir.fr/presentation/)
- [Du bon usage de la pédagogie](https://www.icem-pedagogie-freinet.org/node/50934)
- [Fernand Pelloutier et les Origines du syndicalisme d'action directe](http://www.seuil.com/ouvrage/fernand-pelloutier-et-les-origines-du-syndicalisme-d-action-directe-jacques-julliard/9782020026710)
- [Logique de l'action collective](https://www.amazon.fr/Logique-laction-collective-Mancur-Olson/dp/2800415029)
- [Commun village](http://editionsrepas.free.fr/editions-repas-livre-commun-village.html)
- [Introduction à la Permaculture](http://www.passerelleco.info/article.php?id_article=1708)
- [Permaculture : le guide pour bien débuter : Jardiner en imitant la nature](https://www.amazon.fr/Permaculture-d%C3%A9buter-Jardiner-imitant-nature/dp/2815306174)
