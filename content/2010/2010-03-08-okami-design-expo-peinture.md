---
layout: post
title: "OKAMI design: expo peinture"
---

  
Rien à voir avec les pieds dans le code directement: je me permet un peu de pub pour Cyrou, un ami de longue date et Monique, une artiste/designeuse.


Si vous aimez les japonaiserie je vous conseil d’aller faire un tour sur [Okami](http://okami.fr/).


![Les peintures rondes de Monique- Okami design](http://elsif.fr/files/painting_round.jpg)


![Un tigre de Cyrou- Okami design](http://elsif.fr/files/dessin_cyrou_tigre.jpg)


[Monique expose jusqu’à fin mars quelques peintures chez Yin](http://okami.fr/blog/fr/expo-de-peintures/), un restaurant que Cyril nous recommande.

  
