---
layout: post
title: Rework, sans rien changer...
---

  
Après avoir refermé ce nouveau bouquin de chez [37Signals](http://37signals.com/), je suis un peu déçu. [Rework](http://37signals.com/rework/) n’est, de mon point de vue, qu’une version amélioré de [Getting Real](http://gettingreal.37signals.com/). C’était peut-être le but, mais du coup, je n’ai rien appris de nouveau.


![Rework](/files/front-cover.png)


Beaucoup de chose relève du bon sens et sont courament faite dans quelques sociétés. D’autres sont moins évidente, et on voit peu d’entreprise les mettre en place. Je pense notamment au fait de rester *petit*. Je vois trop de personnes pour qui la taille d’une boite indique une certaine puissance, notoriété ou encore tranquillité...


Il y a par contre quelque choses avec lequel je ne suis pas d’accord: le fait de travailler à domicile, avec une équipe réparti. Je pense: pour pouvoir recruter les meilleurs des meilleurs il faut accepter de recruter à travers la planête. Pour moi, le fait de travailler en équipe, dans une même pièce est beaucoup plus efficace et important que de travailler avec la crème de la crème mondiale. Mon coté vert me dit que c’est pourtant bien de proner le travail à domicile... mais je préfère pouvoir discuter tout de suite avec quelqu’un quand j’ai besoin de le faire, et surtout voir quand son visage ce tord de douleur sur un bout de code, pour pouvoir lui venir en aide. Et j’aime à penser que les défaut de chaque membre de l’équipe servent a rendre l’équipe plus forte.


En gros, lisez Rework plutôt que Getting Real si vous devez en choisir un. Et surtout (c’est d’ailleur dit dans un des premiers chapitres) restez vous même !

  
