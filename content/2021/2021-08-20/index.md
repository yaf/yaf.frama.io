---
title: Vendredi 20 août 2021
---

Reprise de temps devant l'ordinateur en soirée, après une longue pose lié à un Covid.

Il m'a bien cloué au sol pendant 3 jours, bien réduit mes défenses immunitaires. Ensuite, j'avais l'impression que ça allait mieux, il me restait à évacué ce rhume. Sauf que je n'arrêtais pas de me moucher... Conduit auditif qui se bouchent, début de douleur dans les sinus, narine très irrité, maux de tête. J'ai commencé à avoir du mal à dormir.

Sans repos, sans anticorps, difficile de prendre le dessus. Une sinusite à fini par s'installer. Le toubib a remplacé les fluidifiants par un antibiotique. Ça m'a fait du bien assez rapidement.

Niveau date, 
- 5 août : premier symptômes grippaux (courbature, fièvre, grosse fatigue)
- 8 août : allègement des symptômes (encore un peu de fièvre, un peu de fatigue, et le nez qui coule)
- 9 août : j'apprends que la personne avec qui je partage ma vie est positive
- 10 août : je suis testé positif également, c'est l'isolement pour toute la famille
- 11 août : Plus du tout de courbature, mais grosse irritation nasale, conduit auditif bouché, extrémité des narines irrité au sang. J'utilise prêt de 7 paquets de mouchoir en papier par jours
- 12 août : une infirmière viens tester les enfants : 2 sur 3 sont positif. Supposition que la 3eme l'est aussi en fait
- 16 août : date à laquelle le protocole d'isolement nous autorise à sortir, je passe à la pharmacie prendre de quoi me faire des inhalations, et des conseils. Je me retrouve avec un fluidifiant et un spray nettoyant (eau de mer) évidement
- 18 août : ça fait plusieurs jours que je n'arrive pas à dormir, la fièvre monte, je vais voir le médecin. Ordonnance et équilibrage de médicament
- 19 août : j'ai passé ma première nuit complète, ça fait du bien.
- 20 août : j'ai passé ma deuxième nuit complète, je me sent presque reparti comme avant :)

Méfiance malgré tout. Il reste un peu de fatigue, de la toux (apparemment, ça pourrait rester jusqu'en octobre, c'est lié au Covid). Pour la sinusite et le reste des parties ORL, ça va beaucoup mieux. C'est pas complètement revenue, mais ça fonctionne assez correctement pour que la fièvre soit descendu et que je puisse me reposer, ce qui aide beaucoup.

Nous avons eu beaucoup de chance. Pas d'hospitalisation. Pas de cas complexe... Après plus d'un an et demi que cette maladie tourne autour de nous... C'est un peu dommage que ça tombe sur nous l'été, pendant les vacances, avec un déménagement à préparer ... Et malgré tout, nous avons beaucoup de chance.

---

[Cascade and inheritance](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance)

Dans la zone plus généraliste de [la doc de mozilla à propos de l'apprentissage du web](https://developer.mozilla.org/en-US/docs/Learn)

Il y a vraiment de belles ressources pour apprendre à programmer sur internet... À se demander ce qu'apporte un cours avec un prof ?

Question rhétorique, j'ai pu discuter avec plusieurs personnes apprenant la programmation : une personne externe, qui connait un peu le sujet, ça aide à prendre confiance dans ces apprentissages, ça aiguille, ça motif, conforte, reoriente, confirme... Peut-être qu'il serait temps d'ailleurs de lister ce que ça apporte et de concentrer les rencontres sur ce genre de points plutôt que sur les aspects qui peuvent être vue seul ou en petit groupe autonome d'apprentissage.

---

> Si tu bosses pas tous les soirs sur un side project, t’es pas un vrai dev. C’est honteux ton attitude. Du coup, tu le dis à personne que le dev c’est pas toute ta vie. C’est ton petit secret.

> L’argument principal c’est que développeur, c’est un métier compliqué. Pour surmonter ces difficultés, il faudrait une véritable passion. Et il faudrait encore plus de passion pour être “un bon développeur”.

> Alors oui, je suis fan, mais pas fanatique. Le développement, c’est pas toute ma vie. J’utilise le même mot, passion, mais la nuance c’est que pour moi c’est un centre d’intérêt qui ne domine pas les autres. 

> La maîtrise de la programmation ne nécessite pas que tu sois un passionné. La programmation est un ensemble de savoirs et de techniques informatiques. Ça vient avec de l’expérience et de la pratique sur des projets différents et progressivement complexes. Le seul point commun de tous les développeurs qui ont du succès, c’est le travail. Pas la passion. 

> Ouvrant alors le fameux cercle vertueux : pratique => maîtrise => intérêt => pratique => maîtrise => intérêt, etc etc.

> Tu pratiques pendant des heures et ça marche pas. Tu bosses, tu cherches, t’essayes de comprendre, tu galères pendant des heures. C’est mission impossible cette affaire. Tout d’un coup, tu percutes ! Tu changes un truc, tu relances ton app, et BOUM, ÇA MARCHE !

> La passion est utilisée à outrance par les entreprises. Ce ne sont pas les développeurs qui en parlent le plus. Ce sont les entreprises qui en parlent le plus. Et la passion est souvent pervertie dans le process. Un développeur va parler de plaisir. Une entreprise va parler de performance.

> Non, ça va pas. La passion doit rester une affaire personnelle, pas un prérequis pour un travail. Aucune entreprise ne devrait l’exiger.

> L’autoroute qui mène à l’excellence est la même qui mène au burnout. Arrête-toi dans une aire de temps en temps.

[Je suis un dev](https://www.jesuisundev.com/passion-developpeur/)
