---
title: Mardi 6 avril 2021
---

Pour avoir de la prévisibilité sur les tickets... 

https://kanbantool.com/cumulative-flow-diagram

Ça nécessite d'avoir des tickets de taille similaire (ou bien au moins, sur trois taille de t-shirt, faire en sorte que les S soit identique, les M, et L également... Au moins à priori).

Comment l'automatiser ?


---

[Reading Code Is a Skill](https://dzone.com/articles/reading-code-is-a-skill)

> The problem is not that we shouldn't write readable code.
> The problem is that these two issues are not mutually exclusive. It's not "write readable code" or "learn to read code". That's like saying, "I'm going to drive really economically so I don't need to put petrol in the car". No. You're still going to need to put fuel in the car (if it's not electric!) at some point no matter how economically you drive.


> No one writes unreadable code on purpose. You can see this for yourself if you have a project that only you contribute code to - every time you come back to it after an absence of months, weeks, or even days, you wonder what on Earth you were thinking when you wrote it, because now you know a much better way of doing the same thing, or you have a name which is much clearer for this method.

> The narrative of "unreadable code is the fault of the code author, you should be writing readable code" focuses on placing blame rather than looking at how to deal with the issue.
> We should also be kind and caring to the authors of the code, and learn to accept the code as it is.

> Readable Code Is Subjective

---

[Reading Code Is Harder Than Writing It](https://trishagee.com/presentations/reading_code/)

> Even those who promote software as a craft sometimes fall into the trap of often talking about writing clean code that people can read, yet not placing much emphasis on the skill of reading the code.


---

[Lydia et Claude BOURGUIGNON : Comprendre Le Sol, La Terre et l'Humus](https://www.youtube.com/watch?v=tf2zNgrzWg0)

---

> [ Figuration dû sentir ]
> Bâton de fumigation, plantes purificatrices ou bien être, encens de plantes... parfois simple ou complexe; il nous vient des Grecs ( avec le thym) , des Amérindiens ( avec la sauge blanche )...
> 💡La fumigation est l’action d’utiliser la fumée des plantes brûlées ou chauffées pour assainir un lieu, désinfecter, faire fuir lés insectes, comme action médicamenteuse et spirituelle.
> Ces bâtons de fumigation à l’approche sensorielle, à l’acte dû sentir, de l’agréable, de part les plantes choisies nous raconte une histoire.
> Nos ressentis olfactifs seront liés à nos expériences individuelles.
> Autrefois utilisé pour chasser les mauvais esprits, purifier un lieu comme les personnes, on l’utilise aujourd’hui aussi pour parfumer, décorer.
> Après avoir déposé le bâton sur une coupelle, dans un pot, on le laisse se consumer quelques instants, pour laisser la fumée parfumer, purifier son intérieur.
> ( on évite pour les bébés et femmes enceintes )
> Utiliser des plantes locales à l’avantage d’une autonomie 🙂
> Le bâton est charmant, on pourrait mettre les mêmes plantes à brûler en vrac, dans une coupelle...
> 🌷Soyons créateurs
> 🍃La sauge purifie, intuition, chance, guérison, contre les cauchemars
> 🍃Le romarin purifie, concentration, paix, guérison
> 🍃Le cyprès purifie, pratique du spiritisme
>
> -- Karine Pytlak

[faire_son_temps (instagram)](https://www.instagram.com/faire_son_temps/)
