---
title: Mercredi 7 juillet 2021
---

Seul à la maison. La famille au pays. Du coup je travail.

Réunion, finalement en visio, avec le groupe Communication & co de Terre de liens Bretagne.

On parle du nouveau site, de la proposition de l'association Defi, de l'utilisation de wordpress. Mon discours n'est pas assez solide pour proposer des alternatives.

---

Où noter des adresses, des contacts ? J'ai des cartes de visites empilé, je les recopies ici pour garder la trace. Est-ce que j'ai vraiment besoin de les noter ?

- Atelier de Céramique Marie Compagnon 06 63 54 25 48 Le Faouët (56) Fabrication artisanale d'objets beaux, utiles et durables. Instagramm @bud_ceramiques
- Nathalie Haaff Apicultrice 6 rue du Maros 56640 Nostang 02 97 02 61 42 mobile 06 83 80 88 60
- Marion Garnier Productrice de champignons et compagnie 06 6178 29 00 Pen ar roz 29560 Argol champigninsetcompagnie29@gmail.com facebook @ champignonsetcompagnie
- Henri Ackermann potier en Bretagne 56860 Séné http://ackermannpottery.monsite-orange.fr visite & acquisitions sur rendez-vous au 06 08 35 20 69

---

J'utilise toujours vim. Est-ce bien nécessaire ? J'y ais mes habitudes, mais est-ce qu'il y a autre chose ? Dans le mode écriture, journal, ça n'a pas beaucoup d'intérêt. C'est dans l'ouverture d'un code source qu'il puise son intérêt, pour la lecture, la navigation.

Mais avec les raccourcis proposé par un éditeur plus récent, est-ce que nous n'arrivons pas à un résultat similaire ?

---

Premier contributeur sur le [open collective de Merci Edgar](https://opencollective.com/merci-edgar#category-BUDGET) \o/


