---
title: Vendredi 12 mars 2021
---

Note de lecture de « Chère Ijeawele, ou un manifeste pour une éducation féministe » de Chimamanda Ngozi Adichie.

> En réponse à la sollicitation de mon amie, j'ai décidé de lui écrire une lettre que je souhaits sincère et pragmatique, tout en me permettant aussi de structurer, en quelque sorte, ma propre pensée féministe. Ce livre est une version de cette lettre, légèrement remaniée.

> je suis convaincue de l'urgence morale qu'il y a à nous atteler à imaginer ensemble une éducation différente pour nos enfants, pour tenter de créer un monde plus juste à l'égard des femmes et des hommes.

> Faites les choses ensemble (les deux parents). Tu te souviens qu'à l'école nous avons appris qu'un verbe est un mot d'« action » ? Eh bien, un père est un verbe autant qu'une mère.

> si les tâches liées à l'éducation de l'enfant sont équitablement réparties, tu le sauras. Tu le sauras parce que tu n'auras pas la moindre rancœur. Parce que quand l'égalité est réelle, la rancœur n'existe pas.

> Chudi ne t'« aide » pas quand il s'occupe de son enfant. Il fait ce qu'il est censé faire.


> « Parce que tu es une fille » ne sera jamais une bonne raison pour quoi que ce soit. Jamais.

> Je ne peux pas m'empêcher de m'interroger au sujet du petit génie marketing qui a inventé cette distinction  binaire entre rose et bleu. Il y avait aussi un rayon « unisexe », avec tout un tas de gris blafard. Le concept d'« unisexe » est idiot, puisqu'il se fonde sur l'idée que le masculin est bleu, que le féminin est rose et qu'unisexe est une catégorie à part.

> Nous sommes si profondément conditionnés aux rôles de genre que nous leur obéissons souvent même quand il contrarient nos désirs véritables, nos besoins, notre bien-être. C'est très difficile de les désapprendre.

> Dis-lui (à ta fille) que c'est important de pouvoir faire les choses par elle-même, de savoir se débrouiller seule. Apprends-lui à essayer de réparer les objets quand ils se cassent. Nous avons tendance à juger trpo vite que les filles ne peuvent pas faire plein de choses. Laisse-la essayer. C'est très difficile de les désapprendre.

> Dis-lui (à ta fille) que c'est important de pouvoir faire les choses par elle-même, de savoir se débrouiller seule. Apprends-lui à essayer de réparer les objets quand ils se cassent. Nous avons tendance à juger trop vite que les filles ne peuvent pas faire plein de choses. Laisse-la essayer.

> Le féminisme _light_ utilise le vocabulaire de la « permission ».
> Permettre est un terme problématique. Permettre renvoie au pouvoir.
> Les expressions « permettre » et « autoriser à », quand on les utilise ainsi de façon unilatérale ne devraient jamais appartenir au vocabulaire d'un mariage égalitaire.

> Apprends-lui à questionner les mots. Les mots sont le réceptacle de nos préjugés, de nos croyances et de nos présupposés. Mais pour lui enseigner cela, tu devras toi-même questionner ton propre langage.

_Est-ce que c'est une des raisons pour laquelle les échanges part écrit rendent les choses si compliqué parfois ?_

> La puissance des modèles alternatifs ne pourra jamais être exagérée.

> Apprends-lui à questionner la façon dont notre culture utilise la biologie de manière sélective, comme « argument » pour justifier des normes sociales.

> Apprends donc à Chizalum que la biologie est un sujet intéressant et fascinant, mais qu'elle ne doit jamais accepter qu'on s'en serve pour justifier une norme sociale, quelle qu'elle soit. Parce que les normes sociales sont créées par les êtres humains, et qu'il n'y a pas de norme sociale qu'on ne puisse changer.


