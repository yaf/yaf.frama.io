---
title: Vendredi 21 mai 2021
---

[Ressources pour concevoir des services utiles et utilisables pour toutes et tous](https://design-accessible.fr/).

---

> Le mentorat en France est plus qu’un espoir, c’est une réalité. 
>
> Au cœur des quartiers comme dans les campagnes, des structures créent et animent des programmes de mentorat. Elles s’adressent aux enfants, aux adolescents et aux jeunes adultes, et visent à favoriser la réussite scolaire et à améliorer l’insertion professionnelle. Le mentorat bénéficie ainsi à ce public, mais également aux mentors ainsi qu’aux territoires concernés.
>
> Nous sommes déjà près de 30 000 mentors. Avec la crise sanitaire, des milliers de personnes supplémentaires nous ont rejoints via #MentoratdUrgence pour accompagner à distance des enfants et des jeunes, afin de garantir la continuité éducative. Cette période a plus que jamais montré l’efficacité du mentorat comme moyen de lutte contre les inégalités éducatives.
>
> Ensemble, atteignons l’objectif de 200 000 mentors, accompagnons toujours plus d’enfants et de jeunes vers LEUR réussite !

[Collectif Mentorat](https://www.lementorat.fr/)

et 

> LE MENTORAT TELEMAQUE
>
> Association créée en 2005, Télémaque relance l’ascenseur social, dès le collège, en accompagnant des jeunes investis et motivés de territoires fragiles par le biais d’un double mentorat “école-entreprise”.

[Telemaque](https://www.telemaque.org/)


Découvert via une personne candidate au programme [EIG5](https://eig.etalab.gouv.fr/)

---

> Enfin, je pense qu’on a tous le cerveau branché différement, et donc que l’on va tomber sous le charme de certains langages pour des critères subjectifs.

[Choix du langage de programmation pour un projet: vraiment important ?](https://www.mcorbin.fr/posts/2021-05-12-langage-prog-important/)


---

6 chatons en Bretagne

- [Allella](https://allella.fr/)
- [SimpleHosting](https://simplehosting.me/) (une seule personne)
- [Madata Defi](https://madata.defis.info) association Défi, mais pas de forge logiciel
- [Leprette.fr](https://leprette.fr/site) pas de forge
- [Infini](https://www.infini.fr/) déjà entendu parlé dans des émissions. Bien implenté en bretagne. Peut-être le meilleur candidat pour TDL BZH ?
- [Vulpecula Constellation](https://www.vulpecula.fr/)

