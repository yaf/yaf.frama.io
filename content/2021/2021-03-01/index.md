---
title: Lundi 1er mars 2021
---

Exploration de la différence entre les [Jacinthes](https://fr.wikipedia.org/wiki/Jacinthe) et les [Narcisses](https://fr.wikipedia.org/wiki/Narcissus)

Nous sommes dans les [Causses](https://fr.wikipedia.org/wiki/Causse) dans l'arrière pays de Montpellier. Très particulier comme sol.

J'ai trouvé un site [spécialisé dans les plantes des Causses Cévennoles](http://fleurscaussescevennes.fr/), avec un page spécifique pour les [fleurs](http://fleurscaussescevennes.fr/fleurs.html)

---

Il serait peut-être le moment que je m'essaie à [Svelte](https://svelte.dev/). Encore un framework JS, mais celui-ci sonne un peu particulièrement. Il faut essayer pour se faire un avis. Celui-ci semble moins lourd à découvrir.

---

> Le libre et l'open source permettent cette variété de projets et ne s'accordent que sur un principe commun : les utilisateurs de programmes informatiques ne doivent jamais avoir leurs libertés limitées par la volonté des propriétaires d'un code source.

> Ne nous trompons pas sur les nouveaux risques d'enclosure : si des entreprises participent de plus en plus à la gouvernance et à la production de logiciels libres, cela ne rend pas ces codes sources moins libres. Cela rend l'informatique libre plus dépendante des intentions de ces acteurs, mais les codes sources restent bien des ressources informationnelles librement réutilisables.

> Proposer de nouvelles licences ne permettra pas de faire l'économie des façons de lutter contre l'hégémonie d'entreprises aux valeurs douteuses. C'est la forces des initiatives concurrentes qui engagera la lutte, que ces initiatives s'appuient ou non sur des licences libres.

[Au delà des licences lires](https://bzg.fr/au-dela-des-licences-libres.html/)

---

Pour ne pas oublier, [Brut de thé](https://github.com/brutdethe)

---

[A Rennes, Mikaël a fait le pari de vivre de l'agriculture urbaine ](https://france3-regions.francetvinfo.fr/bretagne/ille-et-vilaine/rennes/a-rennes-mickael-a-fait-le-pari-de-vivre-de-l-agriculture-urbaine-1969249.html)

> Eviter la mécanisation, tout travailler à la main, à hauteur d’homme. C’est le choix de Mikaël Hardy à Rennes. Dès la première rencontre, le ton enjoué de sa voix traduit la passion qui l’a guidé dans son projet complètement fou.

> J'en avais marre de donner des conseils aux autres. Je me suis dit que j'allais les mettre en œuvre moi-même.

> "La première année, je n’ai vendu que des plantes sauvages : pissenlits, orties pour faire des soupes. Ça m’a permis d’acheter une première serre dans laquelle j’ai fait pousser des plants que j’ai vendus aux particuliers." 

> "En camion, je perdais du temps à cause des bouchons. A vélo je mets un quart d’heure." La coopérative de livraison à vélo Toutenvélo lui a même fabriqué une remorque qui se transforme en étal. "C’est plein de petits trucs, il faut s’adapter à l’agriculture urbaine", s’amuse Mikael.


