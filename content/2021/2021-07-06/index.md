---
title: mardi 6 juillet 2021
---

- À lire ? [« Le langage C norme ANSI » de Dennis Ritchie & Brian W. Kernighan](https://www.dunod.com/sciences-techniques/langage-c-norme-ansi)
- À lire ? [« jeu de Go Le langage des pierres » de Motoki Noguchi](https://praxeo-fr.blogspot.com/2005/10/jeu-de-go-le-langage-des-pierres-motoki.html)
- À lire ? [« La grande transformation » de Karl Polanyi](https://fr.wikipedia.org/wiki/La_Grande_Transformation)
- À lire ? [« L'homme qui apprenait lentement » de Thomas Pynchon](https://www.seuil.com/ouvrage/l-homme-qui-apprenait-lentement-thomas-pynchon/9782021090765)
- À lire ? [« Le travail n'est pas une marchandise » Alain Supiot](https://journals.openedition.org/lectures/37832)
- À lire ? [« Pour l'intersectionnalité » de Éléonore Lépinard & Sarah Mazouz](https://anamosa.fr/livre/pour-lintersectionnalite/)
- À lire ? [« Fabriquer l'égalité pour en finir avec le sexisme dans l'économie sociale et solidaire »](https://hal.archives-ouvertes.fr/hal-02337127
- À lire ? [« L’insoutenable subordination des salariés » Par Danièle Linhart](https://www.cairn.info/l-insoutenable-subordination-des-salaries--978274926849.htm)

---

> N’est-ce pas un peu problématique? Nous avons complètement renoncé à être, je ne dis pas les protagonistes, mais au moins partie prenante de nos actions.

> Le non fonctionnement est la condition de la pensée critique.

[Ce qui pourrait être autrement: éloge du non fonctionnement](http://blog.sens-public.org/marcellovitalirosati/cequipourrait/fonctionnement.html)
