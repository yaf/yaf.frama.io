---
title: Mercredi 22 septembre 2021
---

[Comment lire un pneu de vélo](https://www.cycletyres.fr/blog/comment-lire-un-pneu-velo.html)

> L’unité de mesure Anglo-saxonne en pouces.
> Le pouce est utilisé pour les pneus BMX et les pneus VTT, par exemple 29 x 2,50, qui correspond à un pneu de diamètre 29″ dont la section est de 2,50″.

> L’unité de mesure anglo-saxonne, exprimée en pouces, prend en compte le diamètre extérieur du pneumatique, hauteur des flancs compris. Autrement dit, il inclut également la distance entre la jante et la bande de roulement du pneu. Ce diamètre est donc supérieur au diamètre au niveau de la jante.

> Cette unité de mesure n’est pas réglementée et les dimensions peuvent alors varier d’une marque à l’autre. Ce n’est donc pas la plus fiable. Un pneu VTT de dimension 27,5 x 2,35 d’une certaine marque peut donc s’avérer plus étroit qu’un pneu 27,5 x 2,35 d’une autre marque.

> L’unité de mesure Française, exprimée en millimètres

> Elle est utilisée pour les pneus pour vélo de route et pour les pneus de vélo de ville. L’unité de mesure Française est exprimée en millimètres, par exemple 700 x 23C. Cette dimensions désigne un pneu de 700 mm de diamètre et de 23 mm de section. La lettre « C » fait référence à la largeur de la jante correspondante. En savoir plus sur la signification des lettres dans les dimensions de pneu vélo.

> Cette unité de mesure n’est pas non plus réglementée.

> L’unité de mesure ETRTO, exprimée en millimètres

> L’unité de mesure ETRTO est exprimée en millimètres. Il s’agit d’une unité normalisée au niveau européen et qui est reconnue dans le monde entier pour les dimensions des pneumatiques. La norme est régie par la European Tyres and Rim Technical Organization, d’où l’acronyme ETRTO. Son format est de type XX-XXX (section-diamètre) ; par exemple 35-622.

> Le diamètre ETRTO correspond au diamètre intérieur du pneu ainsi qu’au diamètre extérieur de la jante sur laquelle il est monté.

[Quelle taille de pneu pour vos jantes](https://www.cycletyres.fr/blog/quelle-taille-de-pneu-pour-vos-jantes.html)
[Comment mesurer un pneu et une jante de vélo](https://www.cycletyres.fr/blog/comment-mesurer-un-pneu-et-une-jante-de-velo.html)


**700x38 38c 622**
=> ETRTO : 40-622 ?

