---
title: Samedi 2 octobre 2021
---

Petite visite et inscription à la Coopérative de Consommateur La Coquille. Prochaine réunion vendredi, j'ai hate.

En plus des produits commandé en gros, ou des fournitures déniché par les coopérateur, il y a aussi deux dépots :
- [Les paysans solidaires](http://www.lespaysanssolidaires.fr/index.php?route=product/depot/info&depot_id=46)
- [la cagette - Ellé bio](https://app.cagette.net/amap)

---

Dans le cadre du Grand Manger, je regarde un peu les chatons bretons qui propose de l'hébergement.

- [Libreon](https://libreon.fr/)
- [Infini](https://www.infini.fr/hebergement)


