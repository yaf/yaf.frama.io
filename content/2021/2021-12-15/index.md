---
title: Mercredi 15 décembre 2021
---

Beaucoup de questions à propos de [SQL](https://fr.wikipedia.org/wiki/Structured_Query_Language) aujourd'hui lors d'une journée de jury à Nantes...

CEtte journée en tant que jury d'un titre pro en développement, et le trajet associé (c'était à Nantes) m'on amené queluqes élément de réfléxion.

- Et si l'axe c'était de partir d'un rappel **l'informatique sert à transformer de l'information** ?
- Avoir un pan parlant de mon histoire, partir de l'histoire, s'appuyer sur les anciens.
- Informatique indus: on part de l'interrupteur, on avance sur la fonction (dessin) représentation, schema.
- Puis algorithme, et implémentation.
- Jeu de recherche d'un bout de code qui implémente un élément d'algorithme
- Schema graphset ?
- Soumettre ce qui a été trouvé ou fait sur un site (pour lister et envisager une revue par une autre personne... Moi au début ?)
- Axe du livre avec lecture pédagogique d'un exercice.
- Comment validé, vérifier, auto évaluer ? Soumettre au tier pour validation lors que l'on pense avoir réussi ?

