---
title: Lundi 4 janvier 2021
---

Début : 9h

- email de réponse à Benoit (12)
- réflexion sur un mail pour annoncer l'ouverture du forum et la fermeture du trello brouillon
- débug d'un cas pas vu sur l'exporter de données de RDV

pause à 9h45
reprise à 10h

- fin de correction du cas sur l'export des données
- mise à jour du fichier budget avec les infos de Camille sur Octobre/Novembre 2020
- écriture d'un mail pour les personnes référentes à propos du forum
- correction d'une erreur en prod lié au couleur de motif. Il y a au moins une personne admin qui n'a pas le color picker et qui du coup à écris «bleu » et « rose » sur deux motifs qu'elle a créée. Mails d'annonce lié à ce bug et la correction. Création d'un ticket qui reparle de la palette de couleur..
- Ajout des jours fériées de 2022 un moyen comme un autre de faire passer les tests sur le service des jours fériées.
- découverte d'un soucis avec les couleurs pour une autre orga du 64 pour un autre agent. Correction puis email. Même admin ?

Pause à 12h
Reprise à 14h40

- rebase et reprise du ticket pour avoir un export spécifique des rdv depuis la vue liste des rdvs
- préparation du message et de la réunion référentes de demain matin
- écriture d'un courrier pour vérifier si les Côtes d'Armor souhaitent toujours utiliser Mobitel
- grooming du backlog github. Que faire de celui de trello ? On le fini ou on le supprime avant ?

in 18h30

Noter le temps est vraiment horrible. Par contre, ça me permet de voir que je prends des libertés, et ce ce sont ces libertés qui utilise le temps de transport que j'ai gagné. C'est sur ces libertés qu'il faut que je prenne pour faire du yoga ou du vélo... Ou bien je dois créer de nouvelles libertés ?

---

Ajouter un bout de JavaScript pour ouvrir les commentaires d'un billet de blog sur mastodon ? C'est plutôt intéressant !

[Adding comments to your static blog with Mastodon](https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/)

---

[Licoornes, les coopératives du monde d’après](https://www.enercoop.fr/blog/actualites/nationale/licoornes-les-cooperatives-du-monde-dapres#)

J'y découvre
- [Commown, incarnez le changement vers une électronique responsable](https://commown.coop/)
- [Label Ëmmaus, la boutique en ligne](https://www.label-emmaus.co/fr/)


---

[Ce qui nous nourrit principalement : l’autonomie alimentaire et les limites du maraîchage](Ce qui nous nourrit principalement : l’autonomie alimentaire et les limites du maraîchage)

> Notez aussi que les 35kg de maïs en grain représentent environ 30% de l’apport en calories fourni par le potager, pour à peine plus de 5% de la masse produite. On entrevoit ici l’importance des plantes à graines, que je développerai plus tard.

> Même en doublant la production, nous ne serions pas encore à un million de calories et je crois que nous serions à notre « limite » en termes d’ingestion de légumes !
>
> Mais même en approchant le kilo de légumes ingérés, nous n’aurions couvert que 20% de nos besoins estimés, les fameux « 5 millions de kcal ». (ou 25% si on se cantonne aux estimations plus frugales).
>
> Crouler sous les légumes pourrait paraitre séduisant, mais cela ne nous « nourrirait » pas.
>
> Mais alors, qu’est-ce qui nous nourrit vraiment ?
>
> Je crois que vous l’avez deviné d’après le début du propos : les plantes à graines. Directement (en les mangeant) ou indirectement (en les donnant à des animaux). L’herbe aussi nous nourrit, indirectement, en la donnant à des animaux ruminants qui savent la « transformer » en lait ou en viande. La plupart des « herbes » de la prairie sont d’ailleurs soit des graminées (des poacées, cousines du blé) ou des légumineuses (des fabacées, cousines du haricot).
>
> De fait, nous achetons, comme la plupart des Français, de la farine, des pâtes, du riz, de la semoule, du boulgour, des lentilles… Bref, autant de « graines ».
>
> Dans les « plantes à graines », nous avons les céréales les plus courantes (blé, orge, maïs, riz), les céréales secondaires (seigle, avoine, triticale, épeautre, sorgho), les pseudo-céréales (sarrasin, quinoa), les légumineuses comestibles (haricots secs, pois et pois chiches, lentilles, lupin, fève et fèverole) et les oléagineuses (tournesol, colza principalement).
>
> Les graines de toutes ces plantes ont des caractéristiques qui les rendent uniques et vitales pour l’alimentation et la sécurité alimentaire
>


> En contrepartie, leur culture est plus difficile et objectivement hors de portée du « potager familial », à l’exception notable des haricots secs et du maïs dont les rendements au m² peuvent être assez satisfaisants et qui sont toutes deux des cultures « sarclées » qui peuvent se mener au potager, jusqu’à la récolte grâce à la taille de leurs graines. Les valeurs énergétiques moyennes parlent d’elles-mêmes :


> Mais les propriétaires du Bec Hellouin admettent, avec une transparence que les journalistes et thuriféraires ne mettent pas toujours en avant, qu’ils ont recourt au fumier du haras voisin. Ce « recourt » est tout à fait massif et déterminant. L’étude menée par l’institut Sylva avec AgroParisTech sur les flux de matière au Bec Hellouin confirme que ce fumier représente… 70% des apports de fertilisation et que ses apports se chiffrent en tonnes. On est loin de l’autosuffisance par le tas de compost[11].
>
> Et là encore, ce qui est la base de ce cycle, ce ne sont pas les chevaux, mais les prés qui les nourrissent (en foin) et les champs de céréales qui les alimentent (en grain et en paille). Prés et champs dont on ne peut pas présumer de l’éventuelle fertilisation non biologique, mais qui en tous cas doivent être exploités de manière totalement mécanisée et motorisée.
>
> Le maraichage est donc le « sous-produit d’une autre agriculture » (ou d’une sylviculture si votre intrant est un bois raméal fragmenté). Ce n’est pas grave en soit. Il faut juste l’admettre.


> Dans un cas comme dans l’autre, je sais que je suis dans une situation assez particulière d’expérimentateur et je ne prétends pas que mon expérience soit généralisable. En revanche, elle montre les « limites » de l’agriculture familiale : on ne nourrit pas une famille avec les légumes du potager. Ou, plus précisément, on la nourrit « mieux », mais pas « assez ».

>  il faut admettre, de la même façon, que le maraichage ne peut pas « nourrir » les populations ni assurer leur sécurité alimentaire en cas de pénurie. Il faut produire des céréales et les stocker en vue de problèmes, partout, sans attendre de l’Etat qu’il le fasse.


Ça fait du bien de lire un texte, avec éléments factuels, source, évoquant le fait que non, c'est pas en faisant en sorte que chacune et chacun fasse un petit potager que nous serons autonome collectivement en nourriture.

Je suis bien d'accord avec pas mal d'aspect de cette article. Et pourtant, quelques points me grattent un peu.
- Pourquoi vouloir être autonome au niveau familiale ? J'aime bien l'approche de l'autonomie au niveau d'une bio région ou d'un village au moins.
- Ça me fait penser au fait que l'ensemble des personnes d'un village passaient de ferme en ferme pour la moisson, à la main, avant. C'était une grande fête, tout le monde participait.
- Je devrais peut-être relire [La Révolution d'un seul brin de paille](https://fr.wikipedia.org/wiki/La_r%C3%A9volution_d%27un_seul_brin_de_paille) de [Masanobu Fukuoka](https://fr.wikipedia.org/wiki/Masanobu_Fukuoka). Il me semble qu'il fait pousser beaucoup de céréales (alternance de riz et d'orge de mémoire), sans mécanisation...

J'imagine qu'on peut évoqué l'autonomie d'une région; envisager de réduire la mécanisation des dites grandes cultures de céréales. Est-ce que la traction animal ferait le taf ?

Et d'un coup, l'arboriculture dans les grands champs de céréales prend une dimension tout autre pour moi... Et si, au milieu des vergers on faisait pousser des céréales ?


