---
title: Mardi 13 avril 2021
---

À lire ? [Contre la résilience](https://www.lechappee.org/collections/pour-en-finir-avec/contre-la-resilience)

---

Continuer à se former après un bootcamp ?

Après avoir suivi la formation d’un bootcamp en 2019, Kirsty nous confie qu’elle s’est sentie un peu submergée par ce qui se présentait devant elle désormais en tant que développeuse. On est sûres que ça vous rappelle quelque chose….

Elle partage dans son article les étapes et les ressources qui lui ont permis de s’y retrouver et de continuer à progresser dans son métier:

    Identifier les compétences clés à acquérir pour son “dream job”
    Trouver un équilibre pour développer ces compétences entre le travail en entreprise et le travail personnel
    Se fixer des objectifs clairs, mesurables et réalistes compatibles avec son rythme
    Trouver un mentor et/ou un buddy à qui on rend compte de son avancée
    Se détendre ! Au final, peu importe le rythme ou la façon dont on s’y prend, on avance et on apprend toujours un peu plus chaque jour quoiqu’il arrive

[Building Your Own Personal Learning Curriculum - Kirsty Simmonds](https://www.smashingmagazine.com/2021/02/building-personal-learning-curriculum/)

---

Sororitech

Le premier meetup Sororitech aura lieu demain à 18h, un moment d’échange dans lequel des intervenantes 5 étoiles parlerons du rôle de CTO, de manager mais aussi d’expert.

🚂 The path to CTO, avec Ludi Akue & Marie Terrier
👩‍💼 Becoming a manager, avec Mélyna Boniface & Mélanie Bérard
🔥 Expert developer as a woman in tech, rencontre avec Paola Ducolin !

[ Meet-up sororitech le 14 avril](https://www.meetup.com/fr-FR/doctolib/events/276730561/)

