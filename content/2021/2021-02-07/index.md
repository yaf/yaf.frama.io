---
title: Dimanche 7 février 2021
---

Redécouverte de la [charte des Codeurs en libertés](https://www.codeursenliberté.fr/entreprise/ethique/charte/).

> 1. Introduction
>
> Nous appliquons notre charte d’éthique aux projet auxquels nous participons.
>
> Il s’agit d’idéaux, d’objectifs à atteindre, pas d’une liste à cocher. Nous sommes faillibles, et ce texte est imparfait. Nous sommes conscient·e·s que cette charte ne répond pas à la complexité de toutes les questions éthiques, et nous nous engageons à questionner nos pratiques, et à adapter cette charte à mesure.

