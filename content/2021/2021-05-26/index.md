---
title: Mercredi 26 mai 2021
---

>  Quelles sont ces pratiques ordinaires qui génèrent tant de souffrance chez les salarié·e·s et de pertes pour les organisations ?

> On apprend en famille et à l'école ce qu'il faudra rejouer dans la vie professionnelle : renoncer à exercer son intelligence parce qu'on incarne déjà soit l'autorité, soit le/la subalterne.

> On a l'habitude de considérer l'optimisation du travail sous ce prisme, qui dissocie les petites mains des esprits qui les commandent. Cette approche bureaucratique peut bien favoriser la docilité, mais non la qualité du résultat à long terme.

Séparer les mains de la tête n'est vraiment pas une bonne idée. Il serait bon d'arrêter de construire des équipes, de bâtir des société sur ce type de principe.

> la volonté de contrôler les personnes en les occupant conduit à augmenter les tâches inutiles.

[5 façons ordinaires de perdre l'intelligence des salarié·e·s](https://noparking.net/post-5-facons-ordinaires-de-perdre-l-intelligence-des-salariees)

---

J'essaie d'utiliser Element. Il y a quelque chose d'IRC dans cette outil. La console en moins :'(

---

> Learning Vim is not unlike studying a foreign language, where adding a word to your vocabulary increases the number of things that you can say. It takes time and practice to pick up Vim’s motions, but every time you add a motion to your repertoire you’ll discover scenarios where it can save you time and keystrokes.

Ne plus utiliser les touches `h, j, k, l` avec Vim, c'est plutôt une approche intéressante ! J'ai déjà usé de l'astuce de verrouiller les touches flèches pour que des élèves apprennent à vraiment utiliser Vim. Là, c'est le stade d'après. Une contrainte pour apprendre.

> Start off by adding w and b to your repertoire. These move forward and back to the start of a word. When you find yourself wanting to get to the end of a word, add e and ge to your repertoire.

J'aime bien l'approche une nouvelle commande à la fois. C'est comme ça que j'ai appris Vim.

_Mais ne suis-je pas en train d'en partir là en utilisant des outils comme apostrophe pour écrire du markdown ?_

> Use <NOP> mappings to break bad habits



[Habit breaking, habit making](http://vimcasts.org/blog/2013/02/habit-breaking-habit-making/)

---

> Pour elle (Danah Boyd), cependant, la polarisation et la haine sont d’abord les conséquences sociales d’une société fracturée, de personnes qui ne sont pas connectées les unes aux autres de manière significative ou profonde.

> Former des groupes est aussi important que la pédagogie

>  Le problème est que lorsque nous en voulons à une personne différente de nous pour une injustice perçue comme le fait de ne pas faire sa part, nous commençons à en vouloir à la catégorie de personnes que cette personne représente pour nous. En d’autres termes, nous pouvons accroître l’intolérance par des efforts mal accompagnés pour constituer des équipes diversifiées.

> Si tout le monde partage le même objectif, ils peuvent se lier sans beaucoup plus que la co-présence. C’est la beauté d’un club scolaire ou d’une équipe sportive

J'ai l'impression d'avoir vécu des choses similaires quand j'étais prof chez Simplon, quand j'animais le Rookie Club, pendant l'encadrement du programme À mon tour de programmer... Les contextes de reconversion amène les personnes à partager un objectif commun, et nous pouvons y créer ce « club », cette équipe.

> Lorsque les gens sont vulnérables les uns envers les autres, ces liens deviennent plus importants.

Les rétrospectives et autres moment où l'on abaisse les barrières, où l'on essaie de parler de nos sentiments, de nos ressentis, sont des moments importants pour travailler dans cette axe.

>  Mais pour que cela fonctionne, il faut bien sûr que les personnes soient consentantes. « Forcer une personne à rompre ses liens sociaux simplement parce que vous pensez que c’est bon pour elle a tendance à avoir l’effet inverse » Refaire des réseaux de sociabilité est un projet qui se déploie sans cesse. « La rupture des relations sociales change la vie. » Elle peut aider les gens à sortir d’un traumatisme, mais elle peut aussi être traumatisante. Le sociologue Paul Willis dans son livre Learning to labor (1977, L’école des ouvriers, Agone, 2011) a montré par exemple que les jeunes de la classe ouvrière qui bénéficiaient d’interventions éducatives considérables, refusaient bien souvent les nouvelles opportunités qui s’offraient à eux en préférant occuper des emplois ouvriers. De fait, ils ne souhaitent pas laisser derrière eux leur famille et leurs amis. Ils ne veulent pas que leurs réseaux sociaux soient brisés. Ceux qui partent sont souvent ceux qui sont en difficultés dans ces communautés, comme les jeunes LGBTQ qui cherchent à échapper à l’homophobie.

Tellement important et tellement vrai.

> Cela m’évoque d’une certaine manière le dernier livre de l’essayiste américain Malcom Gladwell (Wikipédia, @gladwell), Quiproquos (Kero, 2020), qui explique combien nous sommes nuls à interpréter les autres et notamment les inconnus. La plupart du temps, nous nous trompons à leur égard. Comme Chamberlain quand il rencontre Hitler, la plupart des juges pensent pouvoir confondre la vérité d’un accusé juste en perçant leur cœur de leur regard. Hélas, ça ne fonctionne pas si bien. Nous sommes, contrairement à ce que l’on croit, de mauvaises machines à lire les autres.

> Pire, nos erreurs sont renforcées par nos jugements trop rapides sur les autres qui nous font prendre du bruit pour des signaux, l’apparence pour de l’information…

> Le chemin pour apprendre à vivre avec ceux qui ne nous ressemblent pas ne sera pas si simple, mais il serait intéressant de commencer à le regarder plus concrètement que nous ne le faisons actuellement, pour tenter de préciser comment avancer et comment sortir d’une forme de crise du social que l’individualisation n’aide pas du tout à dépasser.

[Pour apaiser nos réseaux, il est temps de prendre soin de la construction du tissu social](https://www.internetactu.net/2021/05/26/pour-apaiser-nos-reseaux-il-est-temps-de-prendre-soin-de-la-construction-du-tissu-social/)

