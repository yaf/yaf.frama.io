---
title: 26 avril 2021
---

Aujourd'hui, j'apprends (enfin) la commande `:hide` dans VIm.

---

> Pourquoi parle-t-on de tépales ? alors que le terme pétale vient plutôt en tête... 
>
> On emploie le mot tépale quand on ne sait pas faire la différence entre les sépales (les pièces généralement vertes situées sous les pétales) et les pétales (les pièces souvent colorées).

[Luzule de Forster](https://notesdeterrain.over-blog.com/2021/04/luzule-de-forster.html)

