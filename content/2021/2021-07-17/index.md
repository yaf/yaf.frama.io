---
title: Samedi 17 juillet 2021
---

[[Lecture] « Failure to disrupt » : les technologies numériques ne transformeront pas l’éducation](https://louisderrac.com/2021/07/09/lecture-failure-to-disrupt-les-technologies-numeriques-ne-transformeront-pas-leducation/)

Je ne pense pas que j'aurais le courage de lire ce livre. Du coup, je trouve que c'est très confortable de lire les notes de Louis Derrac. Ça créée une interpertation, mais tant pis.

Je note que les MOOC connectiviste seraient les mieux, mais qu'ils se sont fait bouffé par les MOOC Institutionnel.


Ce que je trouve intéressant, c'est que le livre semble évoquer la communauté d'apprentissage comme une bonne piste

Ça me fait plaisir. Là où je bloque un peu plus, c'est sur l'aspect « outilé » de l'approche (MOOC Connectiviste)...
￼

En même temps, c'est un livre écrit par un américain à propos des EdTech. C'est donc assez logique

---

Plus j'y pense et plus je me dit que l'expérience À mon tour de programmé était plutôt bonne :
- avoir un projet par semaine, 
- s'arranger plus avoir assez de semaine pour pouvoir refaire un projet dans un autre langage (ou juste le refaire),
- avoir une introduction / démo de comment résoudre ce problème le lundi, même si ça crée un biais, tant pis. Par contre, vu le biais, cette démo est capitale
- avoir un temps de célébration, mise en commun en fin de semaine (vendredi après-midi ?)
- organiser des rencontres avec des pro, recruteur
- organiser un rapprochement, une introduction, dans la communauté (c'était avec Ladies of code pour À mon tour de programmer)...
- Tenir un journal. Sur À mon tour de programmer, le journal était individuel, un temps perso. Je crois que j'ai une préférence forte pour le journal en commun, comme nous faisions au rookie club. Il faut prévoir du temps par contre. Mais une demie-journée par semaine, c'est peut-être pas mal ?

Sur À mon tour de programmer, il y avait une semaine de creux entre deux semaines projet...

Peut-être qu'il serait intéressant d'y intercaller des temps d'apprentissage sur des plus petits exercices, peut-être en préparation du sujet suivant ?

Il faut aussi ajouter une introduction (sur 2 semaines ?) pour essayer d'accueillir les vraies débutantes. À mon tour de programmé n'avait recruté que des fausses débutantes.

Ces deux semaines d'introduction pourraient être basées sur le même mécanisme qu'une semaine sur deux : des petits exercices à piocher, chacune en fonction de sont rythme.

Chez Ada, ça a été nommé level up. Nous avons revue 6 familles il y a peu (deux on fusionné si j'ai bien compris, il en reste 5). Dans chacune il y a des niveau de profondeur.

Il faut sans doute y ajouter un autre découpage, très lié au 3 niveau d'autonomie vis-à-vis des apprentissages :
- je comprend ce que l'on manipule
- j'arrive à manipuler avec de l'aide
- j'arrive à manipuler sans aide

Peut-être faut-il un dernier niveau : je sais l'expliquer ? Le spectacle est là pour ça. Comment intrégrer le spectacle sur les semaines avec des petits exercices ? Faut-il proposer un spectacle par jour du coup ?


---

> Minsky proposait en outre de «prendre les chômeurs tels qu’ils sont et adapter les emplois publics à leurs compétences»3 afin d’assurer une création immédiate (on the spot) d’emplois. Plutôt que d’attendre que la formation rende les chômeurs «employables», Minsky et ses disciples proposent d’inverser le calendrier: créons des emplois d’abord, formons les travailleurs ensuite. C’est ce principe que l’on retrouve aujourd’hui en France, au cœur de l’expérimentation passionnante des «Territoires zéro chômeur de longue durée».

J'ai l'impression d'avoir bénéficier d'un mécanisme de cet ordre quand j'ai commencé dans l'informatique. J'ai été recruté en alternance, alors que je faisait une formation en « Gestion de Production Assistée par Ordinateur » après mon BTS Productique. L'entreprise qui m'a accueilli m'a formé à la programmation. Pendant les 3 premiers mois, tout seul avec un livre, et un jeu de questions-réponses d'une heure ou deux par semaine. Puis, j'ai suivi un consultant sur le terrain. J'ai appris en le regardant faire, j'ai appris sur le terrain.

[La garantie d’emploi : une proposition anticapitaliste ?](https://www.contretemps.eu/garantie-emploi-transformation-sociale-capitalisme/)

