---
title: Samedi 4 décembre 2021
---

Je ne sais plus si je l'ai écrit... 
21 kg de kiwi sur la 2eme récolte il y a quelques semaines.

Aujourd'hui j'ai passé du temps en forêt. Dans le bois de Rémi. C'était bien. C'est agréable d'être en forêt. Il y a une ambiance particulière dans son bois et j'ai compris pourquoi : il est exposé Nord... Très peu de soleil.

J'ai croisé l'agriculteur d'à coté. Il est venu vérifier si je n'étais pas un voleur de bois. J'ai trouvé ça chouette.

Hier quand j'ai parlé bois et formation tronçonneuse avec Gwendal, il m'a dit que j'étais sur tout en même temps. Et pourtant, je crois que ces histoires de forêt, formation tronçonneuse sont très très cohérente avec le reste. Je m'était intéressé à l'apiculture, et donc aux arbres, c'est là que j'ai commencé à regarder comment fonctionne les bois, l'achat, l'entretient. Découverte du RAF. Rencontre de Bernard, apprentissage de la taille de fruitier, de la greffe. Balade en forêt avec Bernard pour récolter des pieds de châtaignier à greffer justement...

si je faisait une reconverstion, ça serais avec les arbres. Fruitier à priori, mais pourquoi pas les autres aussi. Je vais avancer, j'ai l'impression de ne pas trop me tromper.

---

Nicolas m'a chambré une ou deux fois sur le fait que je suis tout le temps en console. Lui utilise un outil graphique pour Git (dispo uniquement sur Mac) et Rubymine.

J'ai refait un essaie avec VSCodium, mais il y a un truc qui cloche au moment de faire du git ou de lancer des tests.

Je me suis remémoré qu'une des raisons pour un système épuré (vim sans trop d'option et bash le plus basique possible), c'est pour pouvoir basculer sur une autre machine sans problème, pendant une formation... Et finalement, je ne fais plus vraiment ça. Et même en formation, je crois que nous pouvons faire autrement... Alors pourquoi maintenir cette contrainte ? Je pourrais remettre un peu de personnalisation, d'alias, de raccourci.

Pour commencé j'ai repris [Fish](https://fishshell.com/). Ça me fait penser que je n'ai sans doute pas mis à jours mon repo de dotfiles depuis un moment :) [yaf/dotfiles](https://github.com/yaf/dotfiles)

J'inaugure Fish en devant recréer la commande « journal » que j'avais pris l'habitude d'utiliser.

Tiens d'ailleurs, pourquoi pas ouvrir apostrophe ?

un site qui a l'air plutôt chouette pour la programmation avec Fish [tutorial fish](https://fishshell.com/docs/3.0/tutorial.html).

Et bien sur, [la page fish sur learnxinyminutes](https://learnxinyminutes.com/docs/fish/) :)

---

Lors d'un échange sur mastodon, à propos des moyens de chauffage, de poêle et de chaudière, une personne m'a proposé d'aller discuter avec une association de Lorient : [Aloen](https://aloen.fr/). Spécialisé sur les énergies et de bon conseil.

---

Est-ce que l'oiseau qui tape au carreau de la véranda est un [geai des chênes](https://fr.wikipedia.org/wiki/Geai_des_ch%C3%AAnes) ?

![](les_oiseaux_du_jardin.jpg)
