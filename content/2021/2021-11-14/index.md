---
title: Dimanche 14 novembre 2021
---

À lire ? [Coopératives contre le capitalisme](http://www.regards.fr/archives/nos-selections/article/cooperatives-contre-capitalisme)

> Au lieu de courir après la croissance ou de chercher une nouvelle recette keynésienne, l’auteur de Coopératives contre le capitalisme propose une utopie concrète : l’entreprise sans patron. Le propos de ce court et vif essai ne relève pas de l’intention romantique éthérée : il repose sur l’expérience en marche des Scop et sur une analyse économique étayée et argumentée.

---

19,2 kg de Kiwi récolté dans le jardin, avec 3 ans. Il en reste à récolter. J'ai bien fait de passer discuter chez l'arboriculteur bio qui fait des pomme et des kiwis, ça m'a permis de me rassurer sur deux ou trois trucs
- le kiwi, comme les pommes et les poires, se cueille avant d'être mûre (donc dur)
- En ouvrir une : si les graines sont noires, c'est ok pour cueillir
- la taille se fait pendant l'hiver, même pas peur du gel
- la taille se fait façon vigne. Laissant des yeux sur les pousses à partir des charpentières
- Si taillé trop tôt ou trop tard, ça va couler (est-ce que ça signifie qu'il faut tailler les cerisiers et abricotier aussi pendant l'hiver, pour par que ça coule ? Lié à la gommose ?)

J'ai bien fait de n'acheter que 3 kilos chez cette dame, ça nous aurais faire vraiment beaucoup de kiwi :)


Installation du garage. Finalement, c'est là que vont aller mes outils et un espace pour stocker les fruits justement. C'est frais et relativement aérer. Je pense que je vais laisser les fenêtres ouverte pour m'en assurer. Nous allons manquer d'étagère pour stocker les fruits, et de cagette. Ou bien d'un meuble spécifique.

J'ai tester le coin et la masse pour fendre les grosses bûches. C'est plutôt ok. Je dois sans doute revoir ma position par contre, je n'étais pas confortable au niveau du dos... J'ai encore beaucoup à apprendre.

Mon cahier de jardin est coincé dans mes caisses de graines, dans un coin du sous-sol. Est-ce que je note ici ou bien là bas ?

Pour mémoire, nous avons aussi récolté autour de 3 Kg de noix (beaucoup partagé avec les animaux du coin, nous avons trouvé beaucoup de noix ouverte sous l'arbre. Écureuils ? Oiseaux ?)

Je n'ai pas pesé la recolte de nefles par contre. Nous en avons beaucoup. J'ai déjà fait 1 kg de gelée (rattrapé comme il faut avec de l'agar-agar) et un crumble. J'ai eu la flème d'enlever les noyaux (pépins ?) du coup je suis le seul à en avoir mangé :D. J'aimerais essayer de faire une sorte de jus et de la pate de fruit. J'imagine que ça me faciliterais le boulot vis-à-vis des noyaux (?).

---

J'ai rejoint 3 communautés autour de la programmation sur discord. C'est très actif ! J'ai bien fait de ne pas essayer d'en faire une de plus en prolongeant le rookie club sur discord.

---

Après un petit temps à utiliser l'ordinateur au salon, je reintègre mon bureau.
Depuis le retour des vacances, je me suis retrouvé à accompagner Sitina à Lorient presque toute la semaine. Je n'ai pas fait beaucoup d'exploration de lieu de travail : j'ai squaté au Café Code 0. Le nom, bien qu'évoquant le code, est en fait uniquement lié à un concept de 0 déchet... Faire un tour à la colloc et surtout à la maison Glaz me branche bien malgré tout... Je pense que j'accompagnerais un peu Sitina en décembre et en janvier. Je vais m'organiser ça.
Au phénomène qui m'a amené à trainer en bas : l'achat de l'ordi fixe. Ça pose un écran dans un coin salon, avec un accès plus facile pour les enfants. Aydan y passe d'ailleurs un peu trop de temps je crois. Nous allons travailler sur le sujet. J'avais oublié de prendre les cables d'alimentation, ce qui m'a privé d'écran externe pendant un temps, et donc d'intérêt pour mon bureau. À cela s'ajoute l'utilisation du boitier CPL pour relier l'ordi fixe au réseau, et je perds encore en qualité de connexion depuis mon bureau.
Raison ultime (comme si j'avais besoin de me justifier), la période de froid, combiné à la récupération d'un stère de bois et la grosse envie de tester le poil... Ça fait de bonne session de travail au chaud dans le salon :D

À voir par la suite si dans mes journées de travail, seul à la maison, je reste dans mon bureau ou m'installe dans le salon. La première journée de ce type aura lieu demain. Au moins sur une bonne partie de la journée. Nous verrons bien :)

ps: sans chauffage, le bureau là haut n'est pas super accueillant quand il fait froid

---

Twitch, discord et slack...

Beaucoup de communauté d'entre aide, d'outil pour apprendre à coder, d'outil pour partager des connaissances, pour s'encourager.

> Hello all :yellow_heart: Petit rappel: On a monté une communauté de dev pour les femmes avec @lixiviatio qui s'appelle Dev Safe.
> Le but c'est de s'entraider et de partager/échanger en sécurité !
>
> Si ca vous interesse hésitez pas à mp @InhaeeT ou @lixiviatio  pour le lien du discord
> 
> -- https://twitter.com/InhaeeT/status/1459088760658927621

ça pleut des communautés en ce moment... J'en viens à me demander si c'est bien ou pas :thinking_face: Entre Electronic Tales et Motiv'Her, j'ai également découvert ce soir devs.coffee et sans prise de tech. Il y a aussi Azot... Bon, toutes ne sont pas poru les femmes, c'est principalement dev junior.
J'ajoute celle-ci, malgré tout
J'imagine que si ça se crée, c'est que c'est nécessaire.
Mon questionnement est plutôt sur le pourquoi c'est nécessaire ? Sans doute parce que notre univers de dev n'est vraiment vraiment pas accueillant

Ça va vite, très vite. Est-ce que ce que j'ai à partager à encore un sens ? Est-ce que mon point de vue sur l'apprentissage de la programmation à un sens ? J'ai l'impression de me retrouver dans la posture de certaines personnes cherchant à créer une boite, avec une idée en tête qu'elles en souhaitent pas partager... Je leurs disaient que la différence entre elles et les autres c'était que les autres réalisent l'idée, la même en action.

J'ai des idées sur l'apprentissage de la programmation. Mais je ne les mets plus du tout en actions !
