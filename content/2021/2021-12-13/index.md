---
title: Lundi 13 décembre 2021
---

> Pour chaque magicien auto-proclamé, il y a de potentielles nouvelles recrues qui sont démotivées par l’ampleur d’une tâche qui est devenue un mythe. Celui créé par une élite qui en profite allègrement.
>
> J’aimerais participer à son piétinement.

[ Incompétence ](https://larlet.fr/david/2021/11/26/)

Vive les amateurs !
