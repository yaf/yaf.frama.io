---
title: Dimanche 10 janvier 2021
---

Découverte d'une maëstro du Ukulele [Taimane Gardnet](https://en.wikipedia.org/wiki/Taimane_Gardner)

Je découvre aussi le [Yi Jing](https://fr.wikipedia.org/wiki/Yi_Jing), et le fait que l'on peu [utiliser des baguettes d'Achillée pour le Yi Jing]( http://taosophie.free.fr/hxg/achillee.htm)

---

À lire ? [Le retour a la terre (cometbus)](https://www.la-petroleuse.com/livres-punk-rock/4787-livre-le-retour-a-la-terre-cometbus.html)
À lire ? [À bas l’empire, vive le printemps !](https://www.la-petroleuse.com/luttes-revolutions/4788-livre-a-bas-lempire-vive-le-printemps-earth-first.html)

---

> Tech is always political. The way data is collected and handled is often biased, and many products are neither accessible nor inclusive. Ethical Design Guide is made to share resources on how to create ethical products that don't cause harm. More links will be added continuously, and shared through a monthly newsletter.

[Ethical Design](https://ethicaldesign.guide/)

