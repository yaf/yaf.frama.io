---
title: Dimanche 18 avril 2021
---

Après l'écoute de l'émission l'[octet vert de
Nitot](https://www.standblog.org/blog/post/2021/02/26/Je-lance-un-podcast-l-Octet-Vert),
[avec Maël Thomas comme
invité](https://www.standblog.org/blog/post/2021/04/09/L-Octet-Vert-06-avec-Mael-Thomas),
je note les références de lecture

- À lire ? [Écotopia - Le roman visionnaire qui annonce l’émergence d’une
  société véritablement écologique. L’envers de 1984
!](https://www.ruedelechiquier.net/fiction/195-ecotopia.html)
- À lire ? [Ma transition écologique - Comment je me suis
  radicalisé](https://www.radiofrance.com/les-editions/livres/ma-transition-ecologique)
- À lire ? [Utopique!, un livre qui imagine la révolution
  écologique](https://www.bdnet.com/catalogue_detail_Utopique--9782955203941)

Merci Tristan de proposer un lien mp3 pour l'écoute :)

---

À lire ? [Reclaim](https://www.cambourakis.com/tout/sorcieres/reclaim/)

---

L'océan c'est bien, la forêt c'est nécessaire. Je me rappel m'être déjà fait
cette conclusion à une époque où nous explorions l'idée de partir vivre dans la
petite montagne (massif central ? Vallée des échelles ?). Je m'étais dit : tant
qu'il y a une forêt, je crois que j'aurais ce qu'il me faut.

Une petite balade dans le bois du Talhouët, ça fait du bien, et ça permet de ce
rappeler de ce genre de chose.

En cherchant un pourquoi ce mot reviens souvent dans le coin, je découvre
l'existence de [la famille
Talhouët](https://fr.wikipedia.org/wiki/Famille_de_Talhou%C3%ABt) de la
noblesse... Ils semblent originaire d'un peu plus loin dans le Morbihan, et
pourtant pas mal d'endroit d'appel Talhouët en Bretagne. Est-ce que c'est lié à
certains bois ? Peut-être.

Je découvre dans [le bois du
Talhouët](https://www.openstreetmap.org/node/6142455178#map=16/47.8184/-3.2673&layers=N)
une plantation bien étrange, au milieu de la forêt. En regardant sur la carte
d'OpenStreetMap, je découvre que ça correspond à une expérimentation, [le
programme
Willwater](https://ec.europa.eu/environment/life/project/Projects/index.cfm?fuseaction=home.showFile&rep=file&fil=Wilwater_Resultats_FR.pdf).
C'est un programme Européen de production d'énergie à partir de saule. L'idée
est de profiter de la croissance rapide des saules pour les couper tout les 3
ou 4 ans. On parle de TTCR : Taillis à Très Courte Rotation. C'est intéressant
pour [la production de
biomasse](https://www.econologie.com/file/biomasse/4rb_leplus_saule_courte_rotation.pdf),
c'est très vert, très clair, mais c'est tellement triste de voir toutes ces
essences d'arbre identique, aligné.

Bon, c'est mieux que du pétrole. Si on peut se chauffer avec ça en attendant de
faire autrement, de réduire nos besoins... C'est toute la difficulté face à ce
genre de projet. J'ai envie de dire non, et en même temps, c'est quand même
mieux que d'autres trucs.

Je découvre au passage [la Coopérative Oxymore qui semble avoir accompagnée le
projet](http://www.oxymore.coop/references/programme-life-environnement-wilwater/).
Ils sont basés à Rennes.

---

Je rattrapais le backlog le week-end. On avait le lundi du code propre.

Intéressant de commencer le lundi avec du code à regarder.

Réaliser son propre niveau.

Pas de commentaire dans le code. C'était ma posture pendant longtemps.

> Si tu pose un commentaire ; c'est que ton code n'est pas clair !

Un dogme bien génant par certains aspect. 

> Il est tant de changer, et de trouver ; comment faire le bon commentaire,
> lorsque c'est nécessaire.


---

Quelques lectures pédagogiques

- [Page wikipédia de Jean Piaget](https://fr.wikipedia.org/wiki/Jean_Piaget)
- [Céline Alvarez - Les fonctions exécutives, 3 compétences
  clés](https://www.celinealvarez.org/les-fonctions-executives-3-competences-cles)
- [Céline Alvarez - Être
  autonome](https://www.celinealvarez.org/entrainement-des-fonctions-executives-premiers-pas-vers-lautonomie)

