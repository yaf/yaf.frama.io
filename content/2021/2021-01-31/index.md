---
title: Dimanche 31 janvier 2021
---

En lisant [Le fediverse et l'avenir des réseaux décentralisés](https://framablog.org/2021/01/26/le-fediverse-et-lavenir-des-reseaux-decentralises/), je ne peux pas m'empêcher de penser à Gopher, Gemini.

Au delà des protocoles, j'ai également en tête le développement. Depuis que je suis arrivé dans l'informatique, il y a un peu plus de 20 ans, la direction à été du client dit « lourd » vers le web. Nous parlions beaucoup de « webiser » les applications. Et si nous revenions à des applications qui s'exécute localement, avec des données locales. Bien souvent, c'est tout à fait envisageable.

Il me reste une tracasserie : un des gros avantage de passer par un navigateur, c'est que c'est déjà installé sur toute les machines. C'est un moteur de rendu générique. Est-ce que je devrait explorer le « offline firt » ? Est-ce que je devrait regarder ce qu'il y a comme [outils pour construire des applications](https://hackernoon.com/9-popular-cross-platform-tools-for-app-development-in-2019-53765004761b (amusant de voit que QT est toujours de la partie :)) (à la [ElectronJS](https://www.electronjs.org/)).

Et que ce passe-t-il pour les mobiles ? Faut-il continuer à les prendre en compte ? Sans doute que oui.

Peut-être que ce qui compte, c'est de ne pas systématiser l'utilisation d'un navigateur ? Et si je commençait par me l'appliquer ? Un lecture de flux RSS extérieur au navigateur par exemple. Utilisation du client github en ligne de commande ? Est-ce bien pertinent ?

Pour reprendre ma lecture...

> Si le Fédiverse nous apprend quelque chose, c’est que le réseau et les composants de logiciel libres de son infrastructure n’ont jamais été aussi politisés qu’aujourd’hui. 

Peut-être que le plus important est justement l'aspect politique de l'application ou du site développer. Comment rester accessible à toutes et à tous...

>  Les débats sur les mérites des fonctionnalités et des logiciels modifiés qu’elles génèrent nourrissent de plus amples discussions sur l’orientation de ces projets, qui à leur tour conduisent à une attention accrue autour de leur gouvernance.

Faire participer les utilisatrices et utilisateurs...

> Qu’ils soient motivés par l’éthique ou l’économie, les logiciels libres et les logiciels open source partagent l’idéal selon lequel leur position est supérieure à celle des logiciels fermés et aux modes de production propriétaires. Toutefois, dans les deux cas, le moteur libéral à la base de ces perspectives éthiques et économiques est rarement remis en question. Il est profondément enraciné dans un contexte occidental qui, au cours des dernières décennies, a favorisé la liberté comme le conçoivent les libéraux et les libertariens aux dépens de l’égalité et du social.
> 
> Remettre en question ce principe est une étape cruciale, car cela ouvrirait des discussions sur d’autres façons d’aborder l’écriture, la diffusion et la disponibilité du code source. Par extension, cela mettrait fin à la prétention selon laquelle ces pratiques seraient soit apolitiques, soit universelles, soit neutres.


> Tout d’abord, l’apparition de ce nouveau type d’usager a entraîné une nouvelle remise en cause des modèles archétypaux de gouvernance des projets de FLOSS, comme celui du « dictateur bienveillant ». En conséquence, plusieurs projets FLOSS de longue date ont été poussés à créer des structures de compte-rendu et à migrer vers des formes de gouvernance orientées vers la communauté, telles que les coopératives ou les associations.
> Deuxièmement, les licences tendent maintenant à être combinées avec d’autres documents textuels tels que les accords de transfert de droits d’auteur, les conditions de service et les codes de conduite. Ces documents sont utilisés pour façonner la communauté, rendre leur cohérence idéologique plus claire et tenter d’empêcher manipulations et malentendus autour de notions vagues comme l’ouverture, la transparence et la liberté.
>
> Troisièmement, la forte coloration politique du code source remet en question la conception actuelle des FLOSS. Comme indiqué précédemment, certains de ces efforts sont motivés par le désir d’éviter la censure et le contrôle des plateformes sociales des entreprises, tandis que d’autres cherchent explicitement à développer des logiciels à usage antifasciste. Ces objectifs interrogent non seulement l’universalité et l’utilité globale des grandes plateformes de médias sociaux, ils questionnent également la supposée universalité et la neutralité des logiciels. Cela est particulièrement vrai lorsque les logiciels présentent des conditions, codes et accords complémentaires explicites pour leurs utilisateurs et les développeuses.

---

À lire ? La dette publique, [Précis d’économie citoyenne](http://www.contretemps.eu/dette-publique-economie-citoyenne/) (Seuil, janvier 2021), sous le label des Économistes atterrés

---

> Nul doute, donc, qu’au jeu de la data science, les acteurs fortunés et hyperconnectés partent vainqueurs face aux acteurs du Sud, moins équipés en infrastructures numériques

> Un autre chemin est possible. Celui privilégiant, à la technoscience, la praxis, la voix de l’autonomie et de l’apprentissage par l’écoute du terrain. Là, le savoir des caféicultrices et caféiculteurs tutoie sans sourciller celui des multinationales. 

> Nous devons décider aujourd’hui quel modèle de société nous choisirons dans un futur proche. La démocratie ne peut se faire en pariant sur des techniques qui dépossèdent les producteurs et productrices de leurs outils de travail.

[Culture du café : la technologie contre les paysans](https://reporterre.net/Culture-du-cafe-la-technologie-contre-les-paysans)

---

> Le Service national universel, c’est :
>
> - Une opération de soumission de la jeunesse
> - La remise en cause des droits des travailleurs et travailleuses
> - Des dépenses considérables
> - Le renforcement de la militarisation
>
> La « cohésion sociale » vantée par les promoteurs du SNU est une dangereuse fumisterie dans le cadre d’un système qui repose sur l’exploitation de la majorité de la population par une minorité, le sexisme, le racisme ou le militarisme. La cohésion sociale n’est envisageable que dans le cadre d’une société reposant sur les valeurs de la démocratie et de la solidarité, la culture de la paix.


[Non au Service national universel !](https://www.unioncommunistelibertaire.org/?Non-au-Service-national-universel)


