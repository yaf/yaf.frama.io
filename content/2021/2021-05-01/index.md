---
title: Samedi 1er mai 2021
---

> L’originalité d’Elizabeth Blackwell est d’avoir édité elle-même son œuvre et d’en avoir tiré des revenus qui lui ont permis de vivre et de tirer son mari de sa geôle. Elle fait partie d’un ensemble d’illustratrices au 18ème siècle qui restent à redécouvrir, à l’instar de Maria Sibylla Merian, Barbara Regina Dietzsch ou Françoise-Madeleine Basseporte.

[Elizabeth Blackwell ou la botanique comme instrument de libération](https://gallica.bnf.fr/blog/13042021/elizabeth-blackwell-ou-la-botanique-comme-instrument-de-liberation?mode=desktop)

---

Note de lecture du libre « croire aux fauves » de Nastassja Martin.

Dans ce livre, Nastassja raconte sa rencontre avec un ours et surtout le chemin quel parcours après y avoir survécu. C'est un livre très intéressant, que j’ai lu facilement tant c’était captivant.

Cependant, j'ai peu de note à en faire sortir. Pourquoi ? Je n’ai peut-être pas tout saisi. Ou j’ai peut-être besoin de laisser la poussière retomber.

J’extrait des éléments qui ne sont peut-être pas important vis-à-vis de cette histoire, mais vis-à-vis de la mienne avec elle.

> Sur une échelle de un à dix , vous avez mal comment ? La fameuse question que tous les patients des hôpitaux français connaissent m’est posée. Au début j’hésite, je bafouille, j’ai peur d’abuser, peur de me faire mal voir. Je m’interroge sur cette étonnante échelle. Je me la fais expliquer plusieurs fois. À partir de quel chiffre peut-on être prise au sérieux ? Cinq ? Six ? Ne par être trop gourmande, je me dis au début, si j’annonce un nombre trop élevé, après je ne pourrai pas monter plus haut s’ils résistent à me fournir la drogue.

_Passage où elle est rentré en france, à la Salpétrière, après avoir subit une autre opération pour changer la plaque qui tient sa machoire_

C'est vrai que cette échelle est surprenante, et complexe. Comment y réagir ? Je comprend que la douleur est quelque chose à prendre en compte et que c'est un des moyens trouvé pour essayer. Mais ça reste complexe. Il y a une histoire de confiance derrière et de cachoterie un peu je pense.

Pourquoi ne pas nous expliquer les options plutôt. Si vous dites 6 alors on vous donne ça, si vous dite 2 alors vous mangerez votre soupe de poireau, si vous dites 10, c'est bloc opératoire direct...

> Il y a trois ans, Daria m’a raconté l’effondrement de l’Union soviétique. Elle m’a dit Nastia un jour la lumière s’est éteinte et les esprits sont revenus. Et nous sommes repartis en forêt.

_La bagarre avec l'ours à eu lieu en Russie_

> Les personnes comme Daria savent qu‘elles ne sont pas seules à vvre, sentir penser, écouter dans la forêt, et que d‘autres forces sont à l‘œuvre autour d‘elles.

> Cela fait quelques jours que nous sommes arrivés à Tvaïan, je m‘applique à ne rien faire, je voudrais même essayer d‘arrêter de penser. Ce matin, je me dis qu’il faut surtout que je cesse de vouloir - comprendre guérir voir savoir prévoir tout de suite. Au fond des bois gelées, on ne « trouve » pas de réponses : on apprend d‘abord à suspendre son raisonnement et à se laisser prendre par le rythme, celui de la vie qui s‘organise pour rester vivants dans une forêt en hiver.

En lisant cela, j'ai pensé à David et ses excursion forestière... Est-ce qu'il s’arrête de penser là bas ?

> À Tvaïan, la vieille idée selon laquelle les hommes chassent et les femmes cuisinent est un leurre absolu, une jolie fiction d’Occidentaux qui peuvent dès lors être fiers de l’évolution de leur société et du dépassement des présumés rôles genrés. Ici, tout le monde sait tout faire. Chasser, pêcher, cuisiner, laver, poser des pièges, chercher de l’eau, cueillir des baies, couper du bois, faire du feu. Pour vivre en forêt au quotidien, l‘impératif _est_ la fluidité des rôles ; le mouvement incessant des uns et des autres, leur nomadisme journalier implique qu’il faut pouvoir tout faire à tout moment car la survie concrète dépend des capacitées partagées lorsqu’un membre de la famille s’absente.

J'aime bien ce petit tackle aux occidentaux dont je suis. Comme si nous progression plus vite ou mieux que d'autres. C'est la ville qui à construit les genres et le cloisonnement ?

---

> Existe-t-il un parcours pour enseigner l’informatique et pourquoi parler "d’éducation à l’informatique" ? s’interroge Gérard Berry. Alors que l'hyper-puissance de l'informatique change notre monde de manière profonde, le chercheur analyse les enjeux de l’enseignement de l’informatique.

> La société informatisée qui nous attend dépendra directement de nos choix conscients ou inconscients. La persistance d’une mauvaise compréhension des raisons de la puissance des mouvements actuels nous conduirait à subir les choix faits par les autres plutôt que d’organiser nous-mêmes notre évolution. Cela se voit déjà clairement au fait que notre pays, qui a longtemps considéré l’informatique comme une activité secondaire, est loin de faire partie des leaders du domaine (sauf pour sa recherche, qui est de niveau mondial). Mais, pour faire des choix sensés, il faut d’abord comprendre. Mon objectif sera donc d’expliquer les ressorts de l’informatique moderne au grand public, afin de lui permettre de mieux saisir ses évolutions actuelles et apprécier leurs effets positifs ou négatifs.

Éduquer, parce que pour enseigner il faut des élèves. Or il faut éduquer plus largement à l'information (formé les politiques par exemple).

Arrive fin du 19e l'information et l'algorithme.

L'information se stock, se transfert, se copie facilement. Rien à voir avec la matière.

Piliers de l'informatique, très important pour l'éducation :
- données
- algorithme
- programme
- machine
- IHM (Interface | Interaction)

On parle autant au objet qu'aux hommes.

Pas une science naturel, c'est une science de construction.

L'information est la même partout, pour tout les usages.
Il y a qu'un type d'algorithme;
Il existe une machine universel (machine à laver ou centre de calcul fonctionne sur les mêmes principes, seul change la performance).

3 niveaux d'éducation distinct:
- literacie (savoir lire et écrire, savoir se service d'un ordi, d'un appareil informatisé)
- compréhension de la pensée informatique (on sait pas forcement ce que ça fait, on sait pas forcement le faire, mais on comprend un peu comment c'est fait).
- pratique plus fine pour devenir acteur de la création.


issue du rapport [MISSION INFORMATIQUE FONDAMENTALE ET PROGRAMMATION - Nivat Berry 1983](Rapport-Nivat-Berry-1983-ocr.pdf):

> Il  faut  former  au  meilleur  niveau  possible  les  techniciens,  ingénieurs,  cadres administratifs, commerciaux, dirigeants, etc. Il faut reconnaître à l’informatique son caractère et l’importance de son rôle à part entière formateur et utilitaire. Il faut cesser de prétendre que l’informatique est facile et s’apprend quand on en a besoin. Il faut au contraire accepter de consacrer dans les cursus le temps nécessaire à l’apprentissage et au mûrissement des concepts informatiques, ce qui exige autant de temps que pour toute autre discipline

> l’informatique est maintenant une véritable discipline scientifique, qui s’appuie sur des  concepts  et  techniques  propres,  et  elle  doit  être  considérée  comme  telle  à  tousles niveaux d’enseignement. C’est une discipline importante pour beaucoup d’activités professionnelles, car elle fournit des outils d’usage très général qui permettent de  mieux  résoudre  certains  problèmes  et  surtout  d’en  aborder  d’autres  auparavant inattaquables.

> nous distinguons quatre niveaux homogènes : niveau de base que devrait posséder tout  technicien  ou  cadre  amené  à  être  en  contact  avec  l’informatique,  niveau  des utilisateurs intensifs mais non informaticiens professionnels (ingénieurs, techniciens,cadres  supérieurs,  etc...),  niveau  des  informaticiens  professionnels,enfin  niveau  formation pour et par la recherche.

> Son niveau est bien en deçà des besoins : l’informatique est souvent enseignée de manière totalement utilitaire pour les besoins d’autres disciplines, sans réflexion sur sa nature

> sur une tablette, on peut pas apprendre l'informatique.

> Il faut vraiment que la communauté informatique progresse là dessus (image de l'informatique auprès du grand public) parce qu'elle est très très loin de la physique et de la biologie. C'est à dire il y a très très peu de gens qui s'implique et les informaticiens sont resté longtemps à dire « Les autres n'ont cas comprendre ». Ça se paie chère cette attitude, ça se paie chère.

Deux profiles prioritaire à former : médecin et juriste.

[Les cours du Collège de France avec Gérard Berry Série « Où va l'informatique? » Épisode 3/8 : L'éducation à l'informatique](https://www.franceculture.fr/emissions/les-cours-du-college-de-france/ou-va-linformatique-58-leducation-a-linformatique)

Et si je devenais prof ?

- [Société Informatique de France - CAPES informatique / NSI](https://www.societe-informatique-de-france.fr/capes-informatique/)
- [Création d'un Capes Numérique et sciences informatiques à partir de la session 2020](https://www.devenirenseignant.gouv.fr/cid137910/creation-d-un-capes-numerique-et-sciences-informatiques.html)
- [SNT - NSI - INFORMATIQUE LYCÉE](https://pixees.fr/informatiquelycee/)

Des ressources pour apprendre
- [La Main À La Patte - 1,2,3... codez !](https://www.fondation-lamap.org/fr/123codez)
- [Classe Code v2](https://pixees.fr/classcode-v2/)
- [Classe Code](https://project.inria.fr/classcode/)
- [Classe Code Open Classe Room](https://openclassrooms.com/fr/partners/class-code) (?)
- [L'Informatique sans ordinateur](https://www.csunplugged.org/fr/)

---

> Mais notre humanité est actuellement confrontée quelques petits problèmes qui nous obligent à une autre urgence pour que les problèmes des uns, les réussites des autres puissent être beaucoup plus partagés, échangés. Si quelqu’un produit des contenus, des réalisations intelligentes, il est urgentissime que ces expérimentations, ces questions puissent être visibles, accessibles par d’autres personnes que celles du petit groupe avec qui il a coopéré.

> Mais ces structures s’ignoraient les unes, les autres. En prenant plaisir à inventer des mots, on a parlé de "tragédie du LSD". La tragédie du Libre Solidaire et Durable c’est le constat que le silo des Libristes, celui des Solidaristes et celui des Durabilistes ont tendance à travailler les uns à côté des autres, parce que chacun a son histoire, son vocabulaire, sa culture. Et en côtoyant une diversité de structures on voyait que le problème d’un silo était résolu dans le silo d’à côté. Si on arrivait à établir un peu plus de lien, de porosité entre ces silos, il serait possible que cela permette à la fois à ces silos d’avancer mais surtout de passer à l’échelle au delà de leur propre cercle. Les sujets auxquels nous sommes confrontés, sont trop vaste pour que nous les traitions seuls.

[![carte illustrant l’archipel des lowtech en france publié par le lowtechlab un site sous licence CC by par défaut](archipel_lowd6be-9fbc1.jpg)](https://lowtechlab.org/en/news-blog/l-archipel-low-tech-en-france-illustre)

> La notion d’archipel est un apport un peu poétique d’une idée travaillée par Édouard Glissant, poéte et philosophe proche d’aimé Césaire et qui a beaucoup parlé des notions d’archipelisation en contre-pied de la centralisation, produit historique de l’état français.

> Donc déjà deux notions :
> - Pas de centralité et être plutôt dans une distribution des savoirs et des pouvoirs
> - Des identités racine travaillées et visibles et des identités relations fécondes de transformations.

> « J’appelle créolisation la rencontre, l’interférence, le choc, les harmonies et les dysharmonies entre les cultures. » Par ces mots, Édouard Glissant fait de la « créolisation » une décontinentalisation, qu’il nomme archipélisation, et qu’il corrèle à ce qu’il appelle le « tout-monde ». Le monde entier, pour lui, se créolise et s’archipélise.

[Partage sincère, "tragédie du LSD", fonctionnement en archipel : dialogue autour de la coopération ouverte avec Laurent Marseault](https://www.innovation-pedagogique.fr/article9092.html)

---

[L’Association Fédérative des Universités Populaires de Parents et des Initiatives Parentales Citoyenne](http://www.upp-acepp.com/)

---

> J’aime assez l’idée du développeur en scribe à l’envers : une élite qui déguise son pouvoir, non sous le sérieux de sa mission, mais sous sa fausse légèreté. Et voilà que tout à coup, la coolitude de la Silicon Valley prend un tout autre sens. Rendre son sérieux à cette activité est aussi un moyen de lutter contre les abus du pouvoir qu’elle s’octroie. Voilà qui est aussi politique.

[Coder, ce n'est ni facile, ni marrant](https://www.franceculture.fr/emissions/la-vie-numerique/coder-ce-nest-ni-facile-ni-marrant)

---

[Common Mark - Each lesson introduces a single Markdown concept with an example.](https://commonmark.org/help/tutorial/)

---

Une nouvelle « forme » de ruche. Assez intéressant ! Ça reprends des principes de l'abbé Warré et de la ruche kenyane, mais en les faisant progrésser.

[Présentation de la ruche de la Forêt Nourricière](https://www.laforetnourriciere.org/la-ruche-de-la-foret-nourriciere/)

Je décrouvre l'histoire de l'accarien, et l'astuce du grillage pour le frelon est vraiment chouette.

