---
title: Mardi 14 septembre 2021
---

Je viens de supprimer [Abricotine](http://abricotine.brrd.fr/) après 2 mois d'essai. Je le trouve moins bien qu'[Apostrophe](https://gitlab.gnome.org/World/apostrophe). Je pense que c'est la présentation du texte, le orange des marques qui est trop présent ?

Place à quelques jours avec [Ghostwriter](https://wereturtle.github.io/ghostwriter/) maintenant.
