---
title: Vendredi 29 janvier 2021
---

Des opérateurs de logiciel libre pour les collectivités.

- [Association Declic : la fédération des Opérateurs Publics de Services Numériques (OPSN](https://www.asso-declic.fr/)
- [Adulact](https://adullact.org/)

Une piste pour RDV-Solidarités ? Des associations où je pourrais m'investir après RDV-Solidarités ?

---


[Post sur un groupe facebook à propos de la préservation des abeilles](https://www.facebook.com/groups/188955998580548/permalink/905352940274180/)

_Faisant référence à [12 Tenets of Preservation Beekeeping](https://whatbeeswant.com/12-tenets-of-preservation-beekeeping/) - 12 clés pour la préservaion des abeilles (sur le site de Jacqueline Freeman : What bees want)_

> Nous sommes en phase avec les recherches de deux apiculteurs dévoués, Tom Seeley (États-Unis) et Torben Schiffer (Allemagne). Nous avons combiné des éléments de leur travail avec nos propres principes et observations et avons créé cette liste de lignes directrices qui illustrent l'apiculture de conservation. Elles constituent la pierre angulaire de "ce que veulent les abeilles". Plus vous serez en mesure de mettre en œuvre ces lignes directrices, mieux ce sera pour vos abeilles.
>
> 1 - Avoir de petites ruches
>
> Les abeilles veulent une maison plus petite, qu'elles peuvent remplir entièrement de nourriture et de rayons. Dans les ruches traditionnelles, les abeilles vivent dans des espaces qui changent constamment, car des hausses vides sont ajoutées au sommet des ruches ou des hausses de miel sont enlevées. Les abeilles de ces ruches s'efforcent toujours de suivre les changements constants apportés par l'éleveur par le biais de la gestion et de l'intervention dans les ruches.
> Les petites colonies sauvages passent 60 % de leur temps à préparer la nourriture, à élever leurs petits et à entretenir les ruches. Les petites colonies passent les 40 % restants de leur journée à se reposer et à se soigner mutuellement. Les ruches de petite capacité accomplissent plus facilement leur travail saisonnier, ce qui leur laisse du temps pour se reposer et prendre soin d'elles. Lorsque les abeilles ont besoin de travailler en continu, les soins personnels et le toilettage mutuel sont les premières tâches abandonnées.
> Les abeilles sont naturellement frugales. Dans la nature, elles stockent le miel pour les années à venir. En période de sécheresse, d'incendies et de fluctuations climatiques, les abeilles qui vivent dans de petites ruches dotées de bonnes réserves de miel peuvent survivre à presque tout. La petite taille est vraiment mieux pour les abeilles.
>
> 2 - Rugosité de l'intérieur de la ruche
>
> Les abeilles utilisent la propolis pour lisser les murs rugueux. Les preuves scientifiques montrent que les abeilles qui propolisent fortement sont en meilleure santé. La propolis est la médecine des abeilles. Lorsqu'elle est utilisée en grande quantité dans une ruche, les abeilles survivent mieux.
>
> 3 - Placez un éco-plancher sous chaque ruche
>
> Les planchers écologiques sont de simples boîtes vides (ou rondins de bois creux) placées sous la ruche. Ils sont entièrement ouverts à la colonie située au-dessus. Nous plaçons du paillis de jardin dans cet espace vide, offrant un foyer à tous les symbiotes des abeilles. Les planchers écologiques accueillent des colonies de fourmis, des fausse teignes, des punaises, des perce-oreilles, des collemboles et même des acariens bénéfiques, ainsi que des milliers de bactéries et de levures qui aident à maintenir une colonie florissante. Les abeilles veulent vivre dans un village, et l'éco-plancher leur permet de le faire.
>
> 4 - Nourriture : uniquement du miel
>
> Si vos abeilles ont besoin de nourriture (et seulement si elles en ont vraiment vraiment besoin), offrez du miel pur. Les solutions sucrées ne sont pas naturelles et sont dures pour les intestins des abeilles. Nous leur donnons cela et rien d'autre. Il est toujours préférable que vous sachiez d'où vient le miel : de vos abeilles ou des abeilles d'un ami qui fait aussi de l'apiculture propre, sans produits chimiques ni médicaments.
>
> 5 - Espacer les colonies
>
> Dans la nature, les abeilles préfèrent vivre à au moins 400 m les unes des autres. Nous n'avons peut-être pas ce genre de terrain à notre disposition, mais il est facile de maintenir les colonies séparées grâce à un treillis ou à des écrans installés entre les ruches, ou même simplement par les différentes directions d'entrée. Cela empêche les abeilles de dériver par erreur dans une ruche voisine et peut contribuer à limiter la transmission de maladies entre les ruches.
>
> 6 - Ne pas utiliser de produits chimiques dans la ruche
>
> La science prouve que les produits chimiques utilisés pour traiter les abeilles contre les varroas ont l'effet involontaire de créer des acariens plus forts et plus mortels, tout comme l'utilisation excessive d'antibiotiques qui, chez l'homme, ont maintenant créé des maladies résistantes aux traitements. Ces traitements tuent également les symbiotes que nous avons invités avec nos planchers écologiques. Les traitements chimiques persistants ne permettent pas aux abeilles de faire l'expérience de la sélection naturelle, qui s'est avérée être le "traitement" le plus efficace de tous. Le chercheur Thomas Seeley a étudié pendant 30 ans les abeilles sauvages qui ont été autorisées à survivre par leurs propres moyens dans le nord de l'État de New York. Ces abeilles ont présenté plus de 600 modifications de leur ADN au cours de cette période. Ces modifications de l'ADN ont permis aux abeilles sauvages de rebondir après l'introduction dévastatrice des acariens varroa au début des années 2000. Les produits chimiques empêchent les abeilles lourdement gérées de progresser dans leur évolution de santé.
>
> 7 - Une ruche très bien isolée en été comme en hiver
>
> Les abeilles veulent une ruche qui soit stable lors des changements de température. L'élevage du couvain et l'évaporation du nectar nécessitent des températures très spécifiques, et les abeilles sont des maîtres du chauffage-ventilation-climatisation lorsque les parois de leur ruche offrent une stabilité thermique importante. Toute ruche, avec un peu de créativité, peut être reconstruite pour fournir une isolation optimale.
>
> 8 - Stop à l'utilisation des cadres
>
> Les ruches Dadant et Langstroth utilisent des cadres, qui sont un réel avantage pour l'apiculteur en raison de la facilité avec laquelle les rayons peuvent être inspectés, déplacés ou retirés. Mais les abeilles veulent et doivent pouvoir traiter chaque zone entre les rayons comme une pièce séparée qu'elles peuvent chauffer ou refroidir selon leurs besoins. Seuls quelques degrés de température en plus ou en moins peuvent nuire au couvain d'abeilles. Dans les ruches à cadres, l'air circule librement dans toute la colonie, et les abeilles doivent travailler sans relâche pour maintenir des températures correctes dans la ruche.
>
> Les cadres obligent aussi les abeilles à construire en ligne droite, comme un livre, rangée par rangée. Selon la façon dont les vents dominants entrent dans la colonie, les abeilles peuvent avoir besoin de fabriquer leurs rayons en cercles ou en demi-lunes pour maintenir des températures optimales dans la ruche.
>
> Au lieu de cadres rectangulaires, utilisons des barrettes - de simples lattes de bois - sur le dessus des ruches. Les barrettes permettent aux abeilles de construire comme elles le souhaitent pour tirer le meilleur parti de la circulation de l'air, empêcher l'humidité et maximiser le chauffage et le refroidissement. Dans les ruches Warrés, à barrettes supérieures, les barrettes sont déjà la norme. Dans les ruches tronc et les ruches en paille, le haut de la ruche est soit une grosse plaque de bois, soit de la paille épaisse tissée, et les abeilles construisent selon leur volonté.
>
> 9 - Pas d'intrusion dans la ruche
>
> Il peut être amusant d'"inspecter" vos abeilles, mais ce n'est amusant que pour vous. Les abeilles n'apprécient aucune intrusion dans leur colonie. Les visites entraînent la rupture des boucliers de propolis, la libération ou la contamination de l'air de la ruche et des températures qui deviennent folles. Souvent, du miel est renversé des rayons, ce qui crée un désordre que les abeilles doivent nettoyer. Parfois, les inspections entraînent la mort accidentelle ou les blessures des abeilles, même de la reine.
> Apprenez à connaître vos abeilles par l'observation au trou de vol. Avec le temps, vous vous rendrez compte de ce qui se passe à l'intérieur de votre colonie en observant de l'extérieur. Ce niveau d'intelligence des abeilles est merveilleux !
> La préservation de l'apiculture est au cœur d'un appel à faire confiance aux abeilles pour qu'elles fassent les bons choix pour elles-mêmes. En n'interrompant pas les colonies, nous leur faisons savoir que nous respectons le mystère de la vie des abeilles.
>
> 10 - Des ruches peuplées avec les abeilles de notre région
>
> Les abeilles locales sont acclimatées à la région. Elles sont les plus aptes à comprendre et à travailler avec les variations saisonnières et la flore locale unique. Les abeilles qui viennent de loin ne sont pas aussi bien adaptées à votre région que les abeilles que vous récoltez d'un essaim dans votre propre secteur.
> Les abeilles achetées dans des "paquets" à des centaines ou des milliers de kilomètres de distance arrivent souvent malades, mourantes et pleines de maladies et de parasites. Ne soutenez pas la cruelle industrie des abeilles en paquets. Rejoignez un groupe local d'apiculteurs partageant les mêmes idées et partagez les essaims et les abeilles supplémentaires avec votre groupe. Personne ne devrait avoir à acheter des abeilles.
>
> 11 - Laissons nos abeilles essaimer
>
> Les abeilles veulent essaimer, mais l'élevage conventionnel croit que les essaims d'abeilles sont mauvais pour la communauté car ils font peur aux gens. Cela n'est pas inéluctable. Nous avons constaté que les essaims actifs sont des moments d'enseignement exceptionnels. Lorsque nous arrivons pour recueillir une boule d'abeilles dans la baignoire d'un oiseau, sur le pare-chocs d'une voiture ou sur le guidon d'un vélo, nous pouvons montrer à quel point les abeilles sont douces. Nous invitons les personnes qui se trouvent à proximité à toucher l'essaim et à sentir la chaleur. Une fois qu'ils ont pris leurs photos avec leurs mains sur les abeilles, ils sont changés à jamais et sont impatients de partager leurs nouvelles connaissances avec tous leurs amis.
> En plus d'offrir de grands moments d'enseignement, il n'y a rien de tel que l'essaimage pour revitaliser les abeilles. Les éleveurs traditionnels contrecarrent l'essaimage en créant de "faux essaims" ou des "divisions", en divisant une colonie en deux, mais ce n'est pas un véritable essaim. Dans un essaim, la colonie décide qui reste, qui part et le moment de l'événement.
> Les essaims réduisent également les populations d'acariens (varroas). Les acariens ont besoin d'alvéoles pour y pondre leurs œufs et un essaim nouvellement débarqué n'a pas encore construit sa nurserie, ce qui signifie que les acariens peuvent mourir en attendant. L'essaimage permet d'éviter l'accumulation des acariens.
> Les essaims sont un glorieux mystère de la nature, une guérison pour la ruche, et une façon mémorable d'enseigner à la communauté la douce merveille des abeilles.
> 12 - Protéger les ruches de l'humidité
> Les abeilles sont par nature arboricoles, ayant évolué dans les arbres pendant des millions d'années. Elles ne sont pas adaptées à l'humidité du sol. Il faut mettre les ruches à l'abri d'une manière ou d'une autre pour les protéger des intempéries et les garder au sec. Nous plaçons les ruches sur des tables et des étagères à au moins 90 cm du sol. Plus haut, c'est toujours mieux, même un balcon au deuxième étage ferait le bonheur des abeilles. Si vous êtes aventureux, suivez l'exemple des anciens apiculteurs européens qui ont mis au point des moyens de placer leurs ruches en toute sécurité en hauteur dans les arbres.

---

[The First Question To Ask When Building Teams – Is This Really A Team?](https://www.viktorcessan.com/the-first-question-to-ask-when-building-teams-is-this-really-a-team/)

> You might assume that if you put together a bunch of people and give them a goal, they’re a team. But that’s not how it works. While all constellations are working groups of some sort, in order for a working group to be considered a team two criteria need to be met.

> Different types of working groups
>
> - Team is when we need each other and have a common goal
> - Pseudo-Team is when we don't need each other but have a common goal
> - Temporary Alliance is when we need each other but doesn't have a common goal
- - Co-worker is when we don't need each other and doesn't have common goal

---

[Qualité Web : la checklist de référence](https://checklists.opquast.com/fr/assurance-qualite-web/)

> 240 règles pour améliorer vos sites et mieux prendre en compte vos utilisateurs - Version 4 - 2020-2025


> Les bonnes pratiques Opquast ne sont pas que des bonnes pratiques. Ce sont des règles. 

[La fin des « bonnes pratiques » Opquast ?](https://www.opquast.com/la-fin-des-bonnes-pratiques-opquast/)



