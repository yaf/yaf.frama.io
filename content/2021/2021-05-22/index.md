---
title: Samedi 22 mai 2021
---

Je me relance dans l'installation d'éditeur de texte en markdown. Je devrait peut-être regarder si ces éditeurs savent aussi faire du ascidoc tiens ?

[Vim](https://www.vim.org/) pour le code c'est bien, pour le texte, ça a peut-être moins d'intérêt. Je n'utilise quasiment aucune commande vim durant la saisi de journaux ou de texte. Est-ce que c'est le début d'un arrêt d'utilisation de vim ? Je ne crois pas. J'ai l'impression d'être prisonnier d'une habitude avec Vim. Et surtout, ce n'est pas une souffrance pour moi. Il y avait parfois un leger inconfort à utiliser un autre éditeur, mais ça passe toujours très vite. Mais depuis le travail à distance, je n'ai plus trop l'occasion d'utiliser l'éditeur de quelque d'autre. Par contre, je met des `:w` de partout :)

J'avais déjà un peu utilisé [Typora](https://typora.io/), mais ce n'est pas libre. Je vais donc essayer différent outils libre pour voir.

- [Abriotine](http://abricotine.brrd.fr/)
- [Ghostwriter](https://wereturtle.github.io/ghostwriter)
- [Apostrophe](https://gitlab.gnome.org/World/apostrophe)

Pour Abricotine, c'est un `.deb` à téléchargé. Il n'est pas (encore ?) dans les repos officiel. Les deux autres utilise le format flat.

Il faut que je regarde un peu ce nouveau système d'installation de paquet sour linux. J'ai l'impression qu'il y a quelque chose d'intéressant. Mais je me demande si c'est un élément libérateur ou bien une porte ouverte vers du grand n'importe quoi ?

[FlatPak](https://flatpak.org/setup/Debian/)

Abricotine à l'air sympa. Il manque peut-être une intégration avec Git tiens.

---

Journaux d'équipe.

J'ai commencé à le faire, via un [gitbook](https://www.gitbook.com/) privée. C'est un peu étonnant, mais ça me fait du bien de le faire. Il y a peut-être (sans doute) trop d'information. L'objectif pour moi c'est de partager, transmettre ce qui est dans ma tête. Pour que d'autres personnes de l'équipe, actuelle ou future, puisse accéder à des informations.
