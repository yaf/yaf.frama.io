---
title: Dimanche 4 avril 2021
---

Découverte de l'[ail à trois angles, ail triquètre, _Allium triquetrum_](https://fr.wikipedia.org/wiki/Ail_%C3%A0_trois_angles)

Un article sur l'ail triquètre sur l'asso cueillir [Zoom sur l'ail triquètre](http://asso-cueillir.over-blog.com/2015/02/l-ail-sauvage.html)

[Recette de préfou à l'ail triquètre](https://cuisinealouest.com/recettes/prefou-ail-triquetre/)

Mercredi est prévu une formation sur les aulx à l'association Cueillir. Je ne sais pas si ça va être maintenu.

---

Sur le portail de la chambre de l'agriculture, une page qui liste des articles sur l'[Organisation du travail en agriculture : comment bien s'organiser ?]http://www.chambres-agriculture-bretagne.fr/synagri/organisation-du-travail-et-relations-humaines-dans-les-exploitations
