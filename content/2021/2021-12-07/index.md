---
title: mardi 7 décembre 2021
---

[Le guide complet du pneu de vélo](Le-guide-complet-du-pneu-de-velo.pdf) _via [lesrookies.com](lesrookies.com)_

Ça me fait penser que je n'ai toujours pas monter les nouveaux pneus sur mon vélo... Il fait trop froid et humide pour que j'ai envie de faire un tour ?

Et le nom de domaine me travaille fort aussi sur mon envie d'animer un atelier d'apprentissage. Je suis content de pouvoir refaire quelques « ateliers » avec les descodeuses. Et je me demande bien de quelle manière je vais participer aux futures promo.

---

[Le logiciel de gestion de la CAE29](https://framagit.org/endi/endi) À croire que chaque CAE à un outil dédié ? Celui-ci semble « sponsorisé » par Coopéré pour entreprendre.

---

> Engager son entreprise, sa fondation
>
> Parce que la forêt, c’est le temps et la force de la nature, source d’humilité et de sérénité

[Engager son entreprise, sa fondation](https://www.alternativesforestieres.org/-Engager-son-entreprise-sa-fondation-)

---

> There is an alignment between writing really good software and being good people together. 

[Software development pushes us to get better as people](https://jessitron.com/2021/11/28/software-development-pushes-us-to-get-better-as-people/)

---

Pour acheter des forêts en commun ?
- [LegiFrance, Groupement foncier Agricole](https://www.legifrance.gouv.fr/codes/id/LEGISCTA000006152231/2008-05-05/)
- [ENR - Mutation à titre gratuit - Successions - Champ d'application des droits de mutation par décès - Exonérations en raison de la nature des biens transmis - Parts de groupements fonciers ruraux ](https://bofip.impots.gouv.fr/bofip/2039-PGP.html/identifiant=BOI-ENR-DMTG-10-20-30-40-20140318)

