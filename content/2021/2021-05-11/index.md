---
title: Mardi 11 mai 2021
---

> Il faut faire particulièrement attention aux exécutables avec la propriété setuid, en particulier setuid root. Ne les accordez que si besoin est, et remplacez-les autant que possible par des capabilities (cf. plus bas).

> doas est un outil conçu par et pour OpenBSD, qui fonctionne dans la même veine que sudo mais qui se veut être bien plus simple et moins compliqué à configurer.

[Touche pas à mon root](https://wonderfall.space/root-usage/)

