---
title: Samedi 11 septembre 2021
---

> Apprendre à désinnover

[Produire mieux ou moins : quel développement économique face à l'urgence climatique](https://www.franceculture.fr/emissions/la-bulle-economique/la-bulle-economique-du-samedi-11-septembre-2021)

---

La pédagogie noire, c'est mal.

[Et tout le monde s'en fout #74 - L'éducation](https://www.youtube.com/watch?v=3PqL9A84uBk) (vidéo Youtube sur l'éducation).

Vidéo où je découvre [Alice Mille](https://fr.wikipedia.org/wiki/Alice_Miller) et la notion de [pédagogie noire](https://fr.wikipedia.org/wiki/P%C3%A9dagogie_noire)

---

Bientôt le retour du [Hacktober Fest](https://hacktoberfest.digitalocean.com/resources).

Un bon moment pour contribuer et faciliter la contribution !
Il y a du boulot pour RDV-Solidarités et pour Merci Edgar. Ça serait intéressant de faire quelque chose.

---

> « Interdit aux adultes » : Biblø Toyen, le tiers-lieu réservé aux 10–15 ans
>
> A Oslo, en Norvège, dans le quartier de Toyen existe un lieu où il fait bon vivre pour les enfants, la bibliothèque Biblø Toyen. Sa spécificité ? Elle est interdite aux adultes.

En voilà une idée intéressante...

> Après l’école qui termine tôt par rapport aux standards français, en début d’après-midi, beaucoup d’enfants du coin ne savent pas où aller. « Un tiers des enfants vivent sous le seuil de pauvreté établi par la ville, ils vivent dans des petits appartements. Ils ne peuvent donc pas recevoir leurs copains ».

[Article sur Usberetrica « Interdit aux adultes » : Biblø Toyen, le tiers-lieu réservé aux 10–15 ans](https://usbeketrica.com/fr/article/interdit-aux-adultes-biblo-toyen-le-tiers-lieu-reserve-aux-enfants)

---

Une page bien pratique, à ne pas oublier [Déggoglisons Interne, les enjeux](https://degooglisons-internet.org/fr/#enjeux)

