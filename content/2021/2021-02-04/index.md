---
title: Jeudi 4 février 2021
---

> - [OpenKeys.science](https://openkeys.science/) est un service en ligne qui permet de produire et diffuser des clés de détermination sous la forme de petit sites web autonomes.
> - Hé hé ! Non, ce n’est pas du tout l’idée. Les contenus sources sont dans des formats libres et ouverts. Il est possible d’importer et exporter ses productions. Le service OpenKeys.science est construit avec des technologies libres (la suite logicielle Scenari et le modèle associé IDKey). Il est donc possible d’héberger ce genre de service ailleurs…
> On peut considérer OpenKeys.science comme une îlot en interaction avec d’autres dans l’archipel des connaissances libres. Chez nous, on fabrique et diffuse des ressources pour comprendre la nature qui nous entoure. On utilise des technologies open source et les ressources produites sont sous licence libre pour favoriser la circulation des connaissances.
> - Je suis nul en smartphone, mais un pote m’a parlé de Pl@ntnet, ça va pas vous couper l’herbe sous le pied ? Est-ce que des IA dans la blockchain avec des drones autonomes connectés 5.0 ne seraient pas plus efficaces que des humains qui se promènent avec des sites statiques ? C’est pas un peu old tech votre histoire ?
> - Mais non :-) ! Ce n’est juste pas du tout les mêmes approches ! Une clé de détermination permet de guider le regard, d’apprendre à observer, de prendre son temps, bref de s’intéresser et de découvrir un être vivant… c’est certainement un peu plus long que de prendre une photo et d’attendre qu’une IA du Web 3.0 fasse tout le travail mais tellement plus valorisant !
> - Et puis, quand on a pris le temps d’observer à fond une espèce, lors de la prochaine rencontre vous vous en rappellerez tout seul et sans aide !

[OpenKeys.science, des clés de détermination pour ouvrir les portes de la biodiversité](https://framablog.org/2021/01/21/openkeys-science-des-cles-de-determination-pour-ouvrir-les-portes-de-la-biodiversite/)

Participer ?

- [Viginateur : SPIPOLL - insectes pollinisateurs](https://vigienature.openkeys.science/spipoll/?state=[]&tab=keys)
- [Éditeur de la suite logicielle Scenari, Kelis conçoit des solutions éditoriales et documentaires dédiées à chaque métier et à chaque client.](https://scenari.kelis.fr/)
- [Bienvenue sur OpenKeys.science](https://openkeys.science/)

