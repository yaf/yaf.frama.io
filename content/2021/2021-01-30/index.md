---
title: Samedi 30 janvier 2021
---

La communauté d'OpenStreetMap s'installe sur [mobilizzon.openstreemap.fr](https://mobilizon.openstreetmap.fr/). C'est peut-être l'occasion de voir s'il y a des rencontres en Bretagne ?

---

[Fréquence Commune](https://www.frequencecommune.fr/)

> Nous sommes un groupe de militant·e·s, d’activistes, de citoyen·ne·s, de professionnel·le·s, de chercheur·euse·s et d’élu·e·s qui agissent depuis plusieurs années, convaincu·e·s que la réponse aux enjeux environnementaux, sociaux et économiques est fondamentalement liée au fonctionnement politique.
>
> Nous agissons en faveur d’une démocratie plus directe, ancrée dans les communes, pratiquée et auto-organisée directement par les habitant·e·s, pour l’intérêt commun.

> Fréquence Commune est une Société Coopérative d’Intérêt Collectif (SCIC).
>
> C’est une entreprise portée par ses sociétaires (salarié·e·s, formateurs·trices, municipalités, collectifs, bénévoles, chercheur·euse·s et associations) qui sont toutes et tous financeurs et décisionnaires lors des assemblées générales de la coopérative. Ils partagent une vision et un objectif commun: transformer la démocratie à partir de l’action locale.
>
> L’avantage de cette structure juridique est qu’elle permet à ses membres de définir démocratiquement une stratégie commune et d’organiser et financer les actions nécessaires pour la mettre en œuvre  : actions collectives coordonnées, organisation d’évènements, achat de matériel commun, campagne de communication, travaux de recherche, etc.
>
> Cette coopérative est ouverte à toutes celles et ceux qui agissent pour créer une nouvelle manière de faire de la politique au niveau local. Bienvenue !

---

La FAQ de l'autogestion :)

> L’autogestion, c’est avant tout la gestion par toutes et tous des affaires qui les concernent, de l’ensemble des décisions qui régissent leur vie. En un mot, l’autogestion, c’est la démocratie. Des décisions politiques, économiques, territoriales, à celles qui régissent les entreprises, l’autogestion défend l’émancipation de toutes et tous à travers l’implication quotidienne dans tous les domaines de la vie collective.

[L'autogestion, qu'est-ce que c'est ?](https://autogestion.asso.fr/lautogestion-quest-cest/)

---


[Animer un atelier de désintoxication de la langue de bois](https://www.youtube.com/watch?v=8oSIq5mxhv8)

[Scop le contrepied](http://www.lecontrepied.org/)

J'hésite à proposer d'organiser un atelier de désintoxication de la langue de bois chez Beta gouv 🚎  https://www.youtube.com/watch?v=8oSIq5mxhv8
J'aime bien l'idée de se « re-approprier » notre métier 🤔
ça me fait me poser des questions sur les entreprises d'insertion 🤔

