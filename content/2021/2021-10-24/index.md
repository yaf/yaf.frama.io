---
title: Dimanche 24 octobre 2021
---

À lire ? [Prendre soin de l’informatique et des générations. Hommage à Bernard Stiegler](https://boutique.fypeditions.com/products/prendre-soin-de-l-informatique-et-des-generations-hommage-a-bernard-stiegler)

> Lorsque les technologies numériques sont mises au service de l’économie des données, leur design et leur fonctionnement exploitent les attentions, afin d’orienter, voire de contrôler, les comportements des utilisateurs. Réduits à un ensemble de processus cognitifs et de réactions réflexes, ils se voient dépossédés de leurs savoirs, alors même que, dans nos sociétés en situation de crise sanitaire, sociale, politique et écologique, le partage et la transmission des savoir-faire, des savoir-vivre et des savoir-penser sont plus que jamais nécessaires.
>
> Comment concevoir et réaliser des plateformes numériques au service des relations sociales et intergénérationnelles, aujourd’hui menacées par les applications addictives et l’économie des données ? Comment intégrer dans les dispositifs computationnels des fonctions délibératives et interprétatives ? Comment transformer les technologies numériques en supports de mémoire et de savoirs ? Comment mettre les algorithmes au service de l’intelligence collective ? En un mot, comment prendre soin de l’informatique pour les générations actuelles et à venir ? Ce livre interroge la manière dont les supports techniques configurent nos capacités psychiques et nos relations collectives, et propose des solutions pour concevoir de nouveaux dispositifs et de nouvelles pratiques, afin de mettre les technologies numériques au service de la production et de la transmission de savoirs, ainsi que des liens entre les générations.


---

> Le Self-Garage Solidaire du Pays de Lorient est ouvert à tout public.
> Contrairement à un garage solidaire, notre garage associatif fonctionne en auto gestion grâce aux dons et au résultat d’exploitation de son fonctionnement et aux adhésions. Nous ne recevons aucune subvention
> Nous payons 100% du loyer et 100 % des salaires de notre personnel. Les investissements sont directs et payés grâce à vous.

> Bien que le self-garage soit ouvert à tout public, il s’adresse surtout aux personnes qui ont besoin de leur véhicule pour travailler ou pour se déplacer et qui n’ont pas les moyens de l’entretenir ou de le réparer chez un concessionnaire.

Est-ce que ça signifie que ce n'est pas une bonne idée d'y aller ? Est-ce que je vais prendre la place de quelqu'un d'autre ? Ou au contraire, est-ce que je vais pouvoir financer un peu plus que les autres pour aider ? Donner de mon temps aussi ?

J'ai deux ou trois bricole sur lesquelles j'aimerais savoir quoi faire (faut il acheter des pièces de rechanges ?) et j'aimerais installer une attache pour attelage, en vue d'acheter une remorque d'occasion.

[Self-Garage Solidaire](http://www.sgs-lorient.com/le-projet/)


---

Note pour plus tard

[Finacoop](https://www.finacoop.fr/) propose aussi de l'accompagnement à la création de SCOP/SCIC.o

> Accompagnement à la création et au développement
>
> Parce que nous avons la conviction qu’une entreprise ne peut réussir qu’avec des copilotes engagé.e.s, nous vous accompagnons étape après étape, de l’amorçage au changement d’échelle


---

Vendredi, j'ai découvert que l'[Ursaaf](https://fr.wikipedia.org/wiki/Union_de_recouvrement_des_cotisations_de_s%C3%A9curit%C3%A9_sociale_et_d'allocations_familiales) est en fait un groupe privée avec une mission de service publique.

Nous avons ensuite vérifie. La mission de service publique les obligent bien à publier le code source de leurs services :)

