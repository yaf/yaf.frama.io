---
title: mardi 13 juillet 2021
---

[Création de la ruche d'un seul tenant ?](https://www.youtube.com/watch?v=ZMVAY6UKzmU)

J'apprécie le personnage, mais la ruche Tanzanienne fait déjà ça depuis longtemps. 

> Jusqu'aux années 1960, les apiculteurs de la Tanzanie rurale utilisaient principalement des rondins de bois taillés en cylindre à extrémités fermées avec un trou de récolte au centre. Le fait que la récolte soit effectuée à partir du milieu de la bûche signifiait que les rayons étaient détruits chaque fois que le miel était récolté. Le gouvernement tanzanien a ensuite promu deux autres types de ruche, à savoir une ruche de bois rond qui pourrait être récoltée de chaque extrémité (de sorte que le couvain dans le centre demeure inchangé) et une ruche de planche, qui était une ruche simple. La ruche de planches n'utilisaient pas de barres mobiles, cependant les abeilles attachaient les rayons en motifs naturels au toit. L'avantage de la ruche de planches était qu'elle permettait une certaine inspection avant la récolte.

[Ruche horizontal à barre](https://fr.wikipedia.org/wiki/Ruche_horizontale_%C3%A0_barres)

La différence c'est que Norbert, l'allemand, fait une ruche à cadre, horizontal, à bord droit. Les cadres font la différence.

Il m'épate à visiter les ruches sans aucun équipement. Et les abeilles semblent si paisible malgré l'ouverture de la ruche ! Et la chute de l'essaim dans la ruchette ! Sans équipement ! C'est fou, complétement fou !

---

Apprendre à ne pas tous noter ici, ne pas tout partager... Pourquoi faire ce bruit. Parce que je suis content d'avoir découvert quelque chose ? Ma joie se nourri d'elle même, pas besoin de crier partout.

J'ai enfin pris le temps d'aller manger au café Code 0 (pour 0 déchet, rien à voir avec la programmation :D). Plutôt bien mangé, dans un cadre agréable. J'aime bien.

La voiture posé par loin de la gare, j'ai traversé Lorient centre en vélo. C'était plutôt chouette ! Après le repas, je suis resté faire deux ou trois bricole sur l'ordi. C'est ok.

Par contre le téléphone perd vite de la batterie quand il sert de routeur. C'est pour ça que c'est bien d'avoir une boite à coté, une boite qui est conçu pour faire le boulot. C'est chouette d'avoir revisité pourquoi c'est bon d'avoir un routeur 4g à part.

C'est bon parfois de revisiter, de remettre en cause, pour vérifier si c'est la décision est toujours justifié.

Ensuite, c'est reparti vers la gare; je vais tester l'abri syklett. J'y a passé tout l'après-midi ! Accueilli par Simon, j'ai installé mon vélo, et commencé par démonter, nettoyer, remonter, ajuster les freins. Changement de patin également. C'est ensuite au tour du cylindre de pédalier. Plus compliqué. Un schéma sur le mur nous aide un peu, mais ça ne reste pas très clair. Quel suprise de découvrir que le jeu viens du fait qu'un des roulement à explosé dans le boitier ! Remplacement, nettoyage, remontage. C'est chouette.

À l'abri sont passé plusieurs type de personne, toutes accueillis correctement avec un deal clair : c'est toi qui fait ta réparation. J'apprécie.

Je ne me suis pas inscrit comme bénévole pour l'abri ou les balades, je ne suis pas sur d'arriver à faire le trajet le Faouët-Lorient régulièrement. À voir plus tard.
