---
title: Samedi 26 juin 2021
---

_Sur Paris depuis quelques jours_

Je constate que je n'écris plus ici...

En même temps, j'écrivais surtout pour noter des liens, des références, que je ne voulais pas perdre. À partir du moment où je ne lis plus beaucoup, je n'ai plus rien à noter ici.

Sauf que de revoir toutes ces personnes avec de beaux projets, d'écouter toutes ces idées, de sortir de sont contexte, ça aide à la réfléxion. Ça ouvre des horizons... Ça amène beaucoup de questions.

J'éprouve le besoin d'en écrire quelques unes.

Pourquoi faire une formation à la programmation ?

Souvent, les apprenantes ne s'y retrouve pas avec la personne encadrante. Souvent elles se retrouvent à suivre un autre cours, en ligne, pendant leurs formation.

Les entreprises devraient faire leur part. Peut-être que ça serait plus simple d'envisager que chaque entreprise accueillant plus de 5 personnes faisant du logiciel devrait forcement accueillir une personne junior, et s'occuper de sa formation. Cette personne pourrait suivre des cours en ligne (il y a tellement de matière déjà), et l'aider à mettre en œuvre.

Parce que je reste persuadé que pour apprendre la programmation, il faut alterner
- des petits exercices, simple, mais permettant d'aiguiser ses reflexes, d'apprendre à se servir des outils ;
- des mises en situations, qui sont tellement plus efficace quand c'est pour de vrai.

À défaut, comment répondre aux questions ? Comment rassurer ? Comment penser un cursus en s'appuyant sur ces contenu en lignes, en apportant ce que les contenus ne peuvent pas proposer : une prise de recule, un échange, et éventuellement, des mises en situation ?

Une autre question me gratte : est-ce que je peux m'investir dans une formation en présenciel à Paris depuis Lorient ? Est-ce que je ne ferais pas mieux de m'investir localement ? Est-ce que je peux faire les deux ?

Est-ce que j'arriverais à poser une pédagogie, un cursus, en étant à distance, pour que des personnes sur place s'en empare, l'utilise, pour arriver à faire une « formation » de bonne qualité ?

C'est un peu ce que j'essaie avec Ada Tech School. C'est ce que j'aimerais faire, et même plus, pour les DesCodeuses.

L'histoire de l'écriture d'un livre me trotte encore dans la tête... Et pourquoi pas animer des formations de formateurs.

Il y a quelque chose qui ne fonctionne pas aujourd'hui dans toutes ces formations, et ça tourne autour des personnes encadrantes. Pas forcement très sur d'elles, pas formé à la pédagogie, ... C'est sans doute une piste intéressante.

---

Rémy m'a parlé de [Gabor Maté](https://fr.wikipedia.org/wiki/Gabor_Mat%C3%A9) à propos des addictions.

---

Un groupe / projet à suivre sur Le Faouët ?

[Le Grand Manger](https://www.facebook.com/legrandmanger/)

> LE GRAND MANGER est un projet de lieu de vie participatif & coopératif autour de l'alimentation et de la transition écologique et sociétale sur la commune du Faouët.

---

[Liste de 52 biais cognitifs](https://uxinlux.github.io/cognitive-biases/52-liste-fr/)

> Les biais cognitifs sont des mécanismes de pensée qui poussent le cerveau humain à tirer des conclusions incorrectes.
> 
> Voici une liste de 52 biais classés en 5 catégories. Elles permettent aux équipes de conception de prendre conscience de leurs biais et des différents biais qu’elles peuvent induire, qu’elles le veuillent ou non, aux utilisatrices et utilisateurs

---

> Les cours sont articulés à chaque séance autour de 3 thématiques :
>
> - Un atelier « papier-crayon » permettant de comprendre et de mettre en pratique ce que représente une consigne ou une commande.
>
> - La mise en pratique sur un ordinateur au travers d’activités sur un écran.
>
> - Le travail sur une problématique répondant à la question « comment faire pour… » ou a l’idée « et si on faisait … » et sa mise en pratique concrète.
>
> L’objectif de ces cours est de les mettre en situation d’apprentissage à travers l’expérimentation et la déduction logique.
>
> Au fur et à mesure de leur apprentissage, et de leur aisance avec les projets, ils cherchent naturellement à faire de nouvelles choses, laissant libre cours à toute leur créativité.

[Code Code Codec, l'écode de code informatique](https://codecodecodec.com/qui-sommes-nous/)

Le nouveau Magic Maker ?

---

[Lætitia Vitaud - I'm a writer and speaker about the future of work with a feminist perspective
](https://laetitiavitaud.com/)

---

À lire ? [« Faire avec. Conflits, coalitions, contagions » de Yves Citton](http://www.editionslesliensquiliberent.fr/livre-Faire_avec-9791020909619-1-1-0-1.html)

---

> Abeilles sauvages
> 
> À Bruxelles, près de 140 espèces d’abeilles sauvages ont été observées. Ces espèces d’abeilles sont généralement peu connues, car elles ne produisent pas de miel et n’ont donc pas été élevées et « domestiquées ». Les études révèlent toutefois qu’elles sont indispensables à nos écosystèmes.


[Abeilles sauvages](https://environnement.brussels/thematiques/espaces-verts-et-biodiversite/la-biodiversite/faune/abeilles-et-pollinisateurs/abeilles)


- [Abeilles de printemps](https://environnement.brussels/sites/default/files/abeilles_printemps.pdf)
- [Abeilles d'été](https://environnement.brussels/sites/default/files/abeilles_ete.pdf)

---

[ Huile essentielle de Tea Tree : comment l'utiliser correctement?  ](https://www.compagnie-des-sens.fr/huile-essentielle-tea-tree/)

> Les variétés de l’arbre à thé sont diverses. On y distingue quelque 150 espèces d’arbres et arbustes dont le Cajeput (melaleuca cajeputii) et le Niaouli (melaleuca quinquenervia).
> 
> Souvent confondu avec le théier, la particularité de cette plante est liée à son entretien.
> En effet, son maintien à des températures humides et fraîches surtout la nuit, est primordial. Ceci dans une pièce lumineuse, mais à l’abri des rayons directs du soleil, de sorte à le maintenir en période végétative permanente.
> 
> L’arbre à thé nécessite énormément de surveillance. Il est recommandé de l’arroser dès qu’il montre des feuilles pendantes ou que le substrat est sec au toucher. Il bourgeonne très facilement à partir de l’arrière et les jeunes pousses qui apparaissent permettent de lui donner la forme souhaitée. Cette forme sera maintenue pendant toute la période végétative.

[ Huile essentielle de tea tree : bienfaits, comment l'utiliser ?  ](https://sante.journaldesfemmes.fr/fiches-sante-du-quotidien/2639663-huile-essentielle-tea-tree-arbre-a-the-bienfaits-cheveux-enceinte-virus-bouton-comment-utiliser/)


[Hélichryse : bienfaits, utilisation, propriétés](https://www.medisite.fr/dictionnaire-des-plantes-medicinales-helichryse.48683.8.html)
[L’hélichrysum : variétés, plantation et entretien](https://www.truffaut.com/helichrysum-varietes-plantation-entretien.html)
[Qu'est-ce que je soigne avec l'huile essentielle d'hélichryse ?](https://www.topsante.com/medecines-douces/huiles-essentielles/huile-essentielle-d-helichryse/qu-est-ce-que-je-soigne-avec-l-huile-essentielle-d-helichryse-634901)
[Huile essentielle d'hélichryse italienne](https://www.passeportsante.net/fr/Solutions/HuilesEssentielles/Fiche.aspx?doc=huile-essentielle-helichryse-italienne)


[Baume Shanti](https://www.baume-shanti.fr/)
