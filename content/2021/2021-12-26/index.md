---
title: Dimanche 26 décembre 2021
---

- À lire ? [« Soigner la technologie » par le collectif Stasis](https://cfeditions.com/stasis/)
- À lire ? [3 BD pour parler d'agriculture](https://blog.ecologie-politique.eu/post/Trois-BD-pour-parler-d-agriculture)
- À lire ? [ Daniel Zamora et Nic Görtz, Être radical. Réflexions made in USA pour radicaux pragmatiques](RDL-5global-3-12.pdf)
- À lire ? [Roberto Nigro, L’insubordination radicale de l’Autonomie italienne](RDL-5global-19-22.pdf)
- À lire ? [Anne Clerval, Gentrification et droit à la ville. La lutte des classes dans l’espace urbain Entretien](RDL-5global-29-42.pdf)
- À lire ? [La Fascinante Démocratie du Rojava](https://www.unioncommunistelibertaire.org/?Sciences-politiques-La-Fascinante-Democratie-du-Rojava)
- À lire ? [Au nom des droits des femmes de Sara R. Farris](https://www.contretemps.eu/racisme-femmes-femonationalisme-farris-extrait/)

---

Sur mastodon, on m'indique que [Néovim](http://neovim.io/) est dépassé, et que depuis la version 8, Vim intègre un gestionnaire de paquet et que c'est reparti avec le vim standard, pas besoin d'allé voir néovim.

---

> [Statuts collégiaux de l’association Roule ma Poule](https://www.passerelleco.info/article.php?id_article=1958)
> 
> Voici un nouvel exemple de statuts juridiques pour une association loi de 1901. En tant qu’association collégiale, elle n’a ni président, ni trésorier, ni secrétaire.
> 
> Vous pouvez vous inspirer de ces status, ou de l’un des multiples exemples de statuts d’associations collégiales disponibles ici, pour créer les statuts de votre propre association collégiale.

---

> J’ai participé à une Assemblée Générale de ma coopérative. J’y ai présenté la démarche d’entretiens utilisateurs menés pour le logiciel FormaSol, ainsi que l’approche pas à pas de la refonte du site web de la coopérative.

> On m’a remercié plusieurs fois pour le regard et la manière de travailler singulière que j’apportais dans les projets relatifs à la coopérative Solstice. J’ai éprouvé de la fierté, et de la joie à offrir ces savoir-faire/savoir-être.

[Détour Studio - Weeknotes #104](https://détour.studio/weeknotes/104/)

J'avais l'intention de repartir en CAE, pour retrouver le contact avec des personnes local, qui font d'autres choses que de l'informatique. Retrouver un groupe, de temps en temps. Ce que vit [Thomas](https://détour.studio/about/) avec [Solstice](https://solstice.coop/) m'encourage à avoir une approche entière, plus que je n'ai pris le soin de le faire durant mes passages chez Port Parallèle (maintenant Omnicité)

---

> Robin du Bois est une association fondée en 2019 qui agit selon les principes de non-violence pour lutter contre l’industrialisation forcenée de nos forêts et ses cortèges de coupes rases. Située dans le Pilat, elle promeut un modèle de gestion durable pour des forêts vivantes.

https://www.alternativesforestieres.org/Robin-du-bois

https://www.ethykprod.com/robindubois

---

Pourquoi je ne publie plus mes notes sur mon site ? Je me freine parce qu'il n'y a pas de flux, parce que ça n'est pas organisé comme je l'aimerais... Et alors ?

Pourquoi je ne partage plus ?
