---
title: Dimanche 31 octobre 2021
---

_Nous sommes chez ma mère, à Six-fours-les-plages, prêt de Toulon._

--- 

[Roblox](https://corp.roblox.com/fr/)

> Roblox a pour mission de rassembler les gens du monde entier à travers le jeu. Nous permettons à chacun d'imaginer, de créer et de s'amuser avec ses amis en explorant des millions d'expériences immersives en 3D, toutes construites par une communauté mondiale de développeurs 

[Roblox - pour les parents](https://corp.roblox.com/fr/pour-les-parents/)

> Roblox est une plateforme globale où des millions de gens se rassemblent tous les jours pour imaginer, créer et partager des expériences ensemble dans des mondes en 3D immersifs créés par les utilisateurs. Les genres d’expériences de jeu sur Roblox sont sans limite tout comme l’imagination des créateurs.

---

À lire ? [The Missing README](https://nostarch.com/missing-readme?mc_cid=021c2dad06&mc_eid=b5a8cab2a6) Ça fait longtemps que je n'ai pas lu un livre sur la programmation.

Est-ce que j'ai envie de replonger dans ce domaine ? Ça serait en mode accompagnement toujours. Steph parle de CAE, j'ai évoqué une entreprise d'insertion, des formations...

---

[Quickly create and run optimised Windows, macOS and Linux desktop virtual machines.](https://github.com/wimpysworld/quickemu) un nouvel outil d'émulation / virtualisation ?

