---
title: Dimanche 23 mai 2021
---

> Il manque la formation des professeurs d'informatique !

Informatique comme support de nouvelle méthode vs comme un nouveau fonctionnement du monde à enseigner

Les enfants, c'est pas des intello, ils aiment bien toucher, sentir, entendre

On attrape l'abstraction en manipulant

Il faut enseigner l'art (STEAM !)

Enseigner l'abstraction ça veut rien dire, ce qu'on peut enseigner c'est une progression vers l'abstraction.

On apprend l'abstraction en essayant d'identifier les mécanismes qui font que les choses marches

Toutes les technologies puissantes sont à double tranchant. Si c'est pas à double tranchant, c'est pas puissant...

[Épisode 6/8 : Retour sur quelques questions de recherche en informatique](https://www.franceculture.fr/emissions/les-cours-du-college-de-france/ou-va-linformatique-68-retour-sur-quelques-questions-de-recherche-en-informatique)
