---
title: Vendredi 4 juin 2021
---

Quelques découverte partagées par l'association [Interhop](https://interhop.org/)

> Autodéfense contre la surveillance : astuces, outils et guides pratiques pour des communications en ligne plus sécurisées
[Surveillance Self-Defense - EFF](https://ssd.eff.org/fr)


> Construisons l'avenir du système de santé 
> Objectif : libérer du temps pharmaceutique en anticipant les parcours de soins
[P4pillon.org](https://www.p4pillon.org/)

---

> L’aire d’attraction d’une ville est un ensemble de communes, d’un seul tenant et sans enclave, qui définit l’étendue de l’influence d’un pôle de population et d’emploi sur les communes environnantes, cette influence étant mesurée par l’intensité des déplacements domicile-travail. 

[https://www.insee.fr/fr/information/4803954](https://www.insee.fr/fr/information/4803954)

C'est en regardant la page wikipedia du [Faouet](https://fr.wikipedia.org/wiki/Le_Faou%C3%ABt_(Morbihan)) que j'ai découvert « Commune hors attraction des villes ». J'ai voulu savoir ce que a signifi.

---

> First you have to know where you want to fit in. Do you want to focus on frontend or backend? Are you interested in devops, game development, data analysis, mobile development, web development — which is right for you?

Je ne suis pas super d'accord avec ce découpage des activités. Et bien qu'elles existent, je pense que chaque personnes peut prendre le temps de les découvrir, de les essayer, et peut-être d'en faire plusieurs si elle souhaite !

> Next you might wonder what language you should learn.
> (...)
> None of it matters, you don’t know how to program.

> Syntax is easy to learn once you have a foundation.

Oui !

> Knowledge of data types, and object oriented principles and design patterns don’t change, and are used no matter what you do in the field. Don’t ever delay learning in an effort to find the right thing to learn.

> Before you pick a language, learn to speak.

[Learn to Program, not a Programming Language](https://codeburst.io/learn-to-program-not-a-programming-language-9c0f56c21935)

---

> Technology, like fashion, is changing at the speed of light. To catch up, we need to run very fast. This race has no winners because it has no end.

> I bought a set of evergreen books. These books took 80% of my learning time:
>
> - The Pragmatic Programmer
> - Clean Code
> - The Clean Coder
> - Domain-Driven Design
> - Growing Object-Oriented Software, Guided by Tests
> - Continuous Delivery

> [Check out the full list here](https://sizovs.net/2019/03/17/the-best-books-all-software-developers-must-read/)

> Don’t rush to learn new technology – it has a high probability of dying.

> All projects seem different unless you look under the surface:
>
> - Programming languages are different, but design smells are alike.
> - Frameworks are different, but the same design patterns shine through.
> - Developers are different, but rules of dealing with people are uniform.
>
> Remember – frameworks, libraries and tools come and go. Time is precious.


[Stop learning frameworks](https://sizovs.net/2018/12/17/stop-learning-frameworks/)

