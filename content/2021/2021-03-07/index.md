---
title: Dimanche 7 mars 2021
---


Note de lecture du livre « les bons mariages au potager » de Philippe Asseray, Rustica Éditions.

C'est un livre qui, après une rapide introduction sur les bienfaits de la rotation et de l'association de plantes, passe en revue 25 associations bénéfique, en détaillant un peu le mode de semis ou de plantation, le type de sol, les avantages de cette association et les modes de récolte. C'est un livre de recette.

Je vais relever la liste des associations ici, et je reprendrais ensuite quelques éléments du livre.

> - Artichaut & asperge
> - Chou-fleur et chicorée
> - Haricot à rames et maïs
> - Laitue, carotte et radis
> - Pomme de terre, céleri-rave et mâche
> - Fraisier et bourrache
> - Haricot nain, pois ridé et lentille
> - Oignon, échalote et poirée
> - Poireau et céleri à côtes
> - Panais et salsifis
> - Ail et pêcher
> - Chou de Bruxelles et souci
> - Chou pommé et menthe
> - Fève et aneth
> - Navet et fenouil
> - Poivron et géranium rosat
> - Tomate et œillet d'Inde
> - Betterave et crosne
> - Courgette et dahlia
> - Épinard et tétragone
> - Petits pois et minicarottes
> - Phacélie et aubergine
> - Tomate, persil et basilic
> - Thym, sarriette et absinthe
> - Concombre et tournesol


Ce que j'apprécie dans ce livre, c'est l'utilisation de fleurs dans les associations. C'est classique, et pourtant assez rarement évoqué.

Le découpage pour la rotation est proposé en 6 parcelles. J'avais déjà lu à propos de 4 parcelles avec des rotations par familles. Voyons ce qui est proposé ici.

> La première année, procédez comme suit. 
  >
> - Installez dans la première parcelle haricot, pois, lentille, etc., mais aussi les engrais verts comme le trèfle ou la vesce.
> - Dans la seconde, semez laitue, chicorée, persil, cerfeuil, épinard, etc.
> - Poursuivez avec les courges, concombres, melons, aubergines, tomates, etc., dans la troisième parcelle.
> - Carottes, panais, salsifis, radis d'hiver, etc., occupent la quatrième partie.
> - Ils sont suivis par l'ail, l'oignon et l'échalote.
> - Terminez enfin par la pomme de terre, le crosne, le poireau, le chou...


Une astuce pour que les artichauts ne fleurissent pas.

> Effectuez une fente sous le bouton d'artichaut avec un couteau bien affûté et introduisez un petit morceau de bois pour l'empêcher de fleurir.

Les artichauts semblent se dupliquer par rejets.

L'association carotte, radis et laitue est intéressant pour la succession dans la récolte.

> Ces trois légumes ont les mêmes exigences de sol et d'humidité. Ils se sèment donc en même temps sur le même rang, mais se récoltent successivement, laissant chaque fois la place aux suivantes pour se développer. L'arrachage des radis fait office d'éclaircissage pour les carottes et les laitues. La récolte des carottes libère à son tour le terrain pour le développement des laitues. Les trois cultures assurent un couvert permanent du sol, limitant ainsi les opérations d'arrosage et de désherbage. En outre la carotte adoucit la saveur des radis, et ce dernier éloignerait l'araignée rouge de la carotte.

> Un pied de fraisier voit sa production évoluer en déclinant jusqu'à l'âge de 3 ans, moment où il faut renouveler la fraiseraie. La multiplication par stolons ne doit se pratiquer qu'une seule fois. En effet, elle risque toujours de propager une maladie.

Ça me fait penser qu'il y a des pieds de fraisier qui reparte de partout dans la cour. Les pieds doivent avori plus de 3 ans sans doute. Est-ce que l'on peut renouveller avec les graines ? Ça fait le boulot ?

> Malheureusement célèbre, la cloque dite du pêcher est due à un champignon qui sévit lors des printemps sans soleil. Celui-ci provoque un enroulement des feuilles, empêche la croissance des jeunes rameaux, et perturbe enfin la mise à fruits. Si les traitements sont efficaces contre ce fléau, sachez qu'il suffit de planter de l'ail au pied de l'arbre pour obtenir le même résultat, sans fatigue et de manière naturelle.

> Irrésistiblement attirés par les fèves, les pucerons noirs sont heureusement faciles à éliminer sans sortir l'arsenal chimique. Coupez toutes les jeunes pousses tendres sur lesquelles ils s'agglutinent et brûlez-les.
> Pour décourager les pucerons et si les coccinelles ne les ont pas déjà mises à leur menu, semez entre chaque rang de fève un rang d'aneth dont l'odeur les ferait fuir. La sarriette aurait le même effet.

C'est plus cool de planter d'autres plantes que de les bruler ces pucerons :-/

> Le feuillage du _Geranium capitatum_ (plante qui fera d'agréables potées fleuries et odorantes) dégage un parfum répulsif contre les parasites du poivron.

> Très odorante, l'absinthe n'est pas difficile et accepte les situations les plus ensoleillées dans n'importe quel sol. Plantez-la au printemps, voire en automne. Récoltez les feuilles ainsi que les extrémités fleuries en été, et faites-les sécher pour conservation. Utilisez-la en tisane ou bien à la maison comme répulsif pour les mites.

> La sarriette donne un parfum méditerranéen à vos salades estivales. Préférez l'annuelle, plus épicées.

> Le cuisinier utilise deux types de sarriette. L'une, plus parfumée, parfois appelée sarriette commune, est une plante annuelle, tandis que l'autre est une vivace.


---

Ces dernier temps, j'utilise mon journal pour consigner mes lectures. Je n'y pose plus mes réflexions. Pourquoi ? Est-ce que c'est un moyen pour moi de réduire le nombre de question que je me pose ? De mettre sous silence mes pensées ?

Est-ce que ça fonctionne ? Sans doute pas. Poser mes idées me permettent de mieux les observer. Je pense que je vais recommencer à penser à haute voix ; de penser à écriture visible.

---

Hier, pour j'ai entamé un cahier pour le jardin. Ce type de journaux semble indispensable, et plus bien efficace en papier que sur l'ordinateur. Faciliter l'écriture plus que l'utilisation.

J'ai consigné :

- la date,
- l'adresse (j'envisage de prendre ce genre de note sur chaque terrain où je me rends pour œuvrer avec les plantes),
- un petit plan du jardin où j'ai noté ce que j'avais semez, les variétés, et où je les aient achetées.
- le travail du sol que j'ai fait,
- l'arrosage.

Est-ce que ça ne va pas être pénible de faire le plan à chaque fois ? J'imagine que ce n'est peut-être plus nécessaire quand il y a plus de place, ou bien que les parcelles sont délimité nettement, et numérotées ou nommées.

J'ai beaucoup pensé à Bernard et ces petits cahiers en le faisant. Je l'imagine en train de sourire.

