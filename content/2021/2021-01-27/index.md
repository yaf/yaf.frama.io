---
title: Mercredi 27 janvier 2021
---

Petite révision : [cultiver avec les ronces](https://permaforet.blogspot.com/2014/09/cultiver-avec-les-ronces.html?m=1)

---

[Pour un savoir libre et pluriel](https://www.lapresse.ca/debats/opinions/2021-01-26/logiciels-proprietaires/pour-un-savoir-libre-et-pluriel.php)

_Les auteurs s’adressent aux recteurs et administrateurs universitaires_

> Nous vous écrivons aujourd’hui pour vous faire part d’une inquiétude croissante sur l’avenir de nos universités, de la recherche et de l’enseignement. L’adoption massive de logiciels propriétaires par les universités a dénaturé le travail de nos communautés de recherche. Désormais, l’espace de notre pensée, de notre recherche et de notre enseignement est devenu l’espace privé de Microsoft, de Zoom, de Google, d’Apple et d’une poignée d’autres entreprises. 

> 1. Les logiciels ne sont pas neutres. Il ne sont pas des « solutions ». Comme l’expliquait déjà McLuhan il y a plusieurs décennies : le média est le message. Lors d’une vidéoconférence Zoom, c’est Zoom qui pense, un texte écrit avec Word exprime une « pensée Word ».

> 2. Nos activités sont financées par l’argent public et devraient rester publiques.

---

> LE NOMBRIL DE VÉNUS.
> C'est une plante comestible et médicinale, aromatique , consommé par les promeneurs en forêt , ajouter les les feuilles aux salades ( relèvent la saveur ) les feuilles sont riches en fer et autres sel minéraux.
> Propriété : Diurétique, calculs rénaux, cicatrisante, anti inflammatoire, ulcère, hémorroïdes, plaies.
> Utilisation interne : Jus (utiliser presse jus) prendre 1 cuillère à café par jour.
> Utilisation externe : Cataplasme, appliquer le jus sur les plaies et les brûlures.
> Contre indication : Aucune. Par mesure de précaution femmes enceintes et allaitantes .

Via le groupe [plantes sauvages et médicinales de nos jardins et chemins / nombril de venus](https://www.facebook.com/groups/530773417857627/permalink/740599783541655/) sur Facebook.

C'est la saison des récoltes.


