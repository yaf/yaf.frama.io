---
title: Samedi 12 juin 2021
---

Plantes utilisées

- Frêne commun
- Pissenlit

Pour 1L de Frênette

- Jeunes feuilles de frênes	50 gr
- Eau	1 L
- Jus de citron	50 ml
- Sucre	200 gr
- Levure fraîche	8 mg
- Racines de pissenlit	10 gr

1. Préparer les racines de pissenlit : laver, peler et hacher les racines. Torréfier (sans matière grasse donc) les racines de pissenlit.
2. Préparer la frênette : porter à ébullition l’eau et les feuilles de frêne. Laisser refroidir et filtrer. Mélanger le jus de citron, le sucre, la levure et les racines de pissenlit.
3. Laisser fermenter 2 semaines.
4. Repos : filtrer et laisser reposer (idéalement) 2 mois hermétiquement.
5. Finition : mettre en bouteille.

[Frenettes](https://cuisinesauvage.org/recipe/frenettes/)

Ou la version avec de la chicorée

[Recettes de Frenettes](https://montagne-hautlanguedoc.com/recettes-de-cuisine-traditionnelle-de-france/recettes-de-boissons-naturelles/recettes-de-frenette/)

---

> Error messages have a lot of ground to cover.

> Ask three key questions:
>
> - Who triggered the error?
>   - User: if the user caused the error, like mistyping an email address, don’t apologise. It just takes more time and effort to read and process. And it gets old fast.
>   - System: if it’s our fault, say “sorry”.
>
> - Do we know what caused it?
>   - Yes: explain what happened, or why something didn’t work.
>   - No: if we don’t know what went wrong, own up and tell them. Reassure them that we’re working to fix it.
>
> - Can we fix it now?
>   - Yes, we can: resolve it, and tell them what you’re doing to help
>   - Yes, the user can: give them clear instructions to fix the problem
>   - No: if there’s no way forward, offer the best suggestion or guide them back a step to try again. Only take people to help sections or live/web chats if it helps them solve the problem.

[How to write useful error messages](https://www.bbc.co.uk/gel/guidelines/how-to-write-useful-error-messages)

---

À lire ? [LE CONSTRUCTIVISME Tome I Les enracinements par Jean-Louis Le Moigne](https://www.editions-harmattan.fr/index.asp?navig=catalogue&obj=livre&no=5118)

---

Parce que j'ai tendance à croire que c'est une bonne approche de la programmation : [Processing](https://openprocessing.org/)

Loin des tracas de l'informatique de gestion... Une première approche plus amusante, visuel, artistque.

---

Un coopérative en guise de fédération, pour mutualiser l'administratif dans l'univers des formations ? [Formations.coop](https://formations.coop/) Un SCIC aurait été intéressante... Ou simplement une association, sorte de bannière, un peu comme Happy Dev pour la prestation.

C'est une belle idée en tout cas.
