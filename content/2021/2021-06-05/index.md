---
title: Samedi 5 juin 2021
---

[No Meetings, No Deadlines, No Full-Time Employees](https://sahillavingia.com/work)

> I started Gumroad in 2011. In 2015, we reached a peak of 23 full-time employees. In 2016, after failing to raise more money, I ended up back where I began: a one-person company.
>
> Today, when I’m asked how many people work at Gumroad, I respond with “ten or so.” That’s how I convert the number of people we have into what others expect. But the truth is more complicated:

> Because I was burned out and didn’t want to think about working any more than I needed to, I instituted a no-meeting, no-deadline culture.
>
> For me, it was no longer about growth at all costs, but “freedom at all costs.”

> Instead of having meetings, people “talk” to each other via GitHub, Notion, and (occasionally) Slack, expecting responses within 24 hours. Because there are no standups or “syncs” and some projects can involve expensive feedback loops to collaborate, working this way requires clear and thoughtful communication.
>
> Everyone writes well, and writes a lot.

Qu'est-ce que nous appelons écrire bien ?

> But we don’t prioritize ruthlessly.
>
> People can work on what’s fun or rely on their intuition, because as long as we remain profitable and keep shipping, we tend to get to the important stuff eventually. Our public roadmap helps Gumroad's creators hold us accountable


> [Our public roadmap](https://www.notion.so/Roadmap-ce2ad07c483046e7941227ad7810730d) helps Gumroad's creators hold us accountable.

> Minimum viable culture


Ça me fait penser à cette idée folle : créer une association (ou boite ?) sans salarié pour chaque produit / service... Peut-être en utilisant [OpenCollective](https://opencollective.com/) tiens. Ça aurais certainement certains avantage. Il y a aussi un goût de truc un peu trop capitaliste / chacun pour soi peut-être :thinking:.


