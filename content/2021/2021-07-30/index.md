---
title: Vendredi 30 juillet 2021
---

Après un séminaire de 4 jours avec les descodeuses, j'ai revue encore une fois pas mal de chose sur ma réflexion pédagogique. Quelques notes en vrac pour le moment

- Ce que j'ai dans la tête est difficile à mettre en œuvre, principalement parce qu'il faut une disposition d'encadrement qui n'est pas facile ou habituelle : pouvoir animer une communauté d'apprentissage autour de différents langage de programmation, défendre l'intérêt de ces orientations
- Apprendre à programmer plutôt que d'apprendre un langage
- Faire plutôt que d'écouter
- Faire plutôt qu'expliquer
- Avoir plusieurs personnes qui interviennent pour multiplier les contexte, les façons de voir
- Le journal associé au spectacle (comme au rookie club 2eme génération) ça me semble vraiment une bonne approche
- Partenariat avec des associations ou collectifs pour le montage/réparation/installation d'ordinateur ou de serveur (Le Garage Numérique, Ëmmaus Connect, Association Défi, ...)
- Un exercice de programation à de la valeur sur une journée
- un projet sur pas plus d'une semaine (si construction de 0)
- le journal collectif peut également service à lister du vocabulaire, des ressources, et constituer une boite à outils

Et quelques questions

- Comment intégrer le fait que certaines personnes sont habituées aux cours magistraux, à avoir une grosse par de théorie avant d'appliquer ?
- Comment former des formateurs ?
- Comment trouver les bonnes personnes pour l'encadrement ?
- Est-ce que ça fonctionnerais à distance ?
- Chaque personnes qui intervient doit-elle respecter l'intégralité des principes pédagogiques ? Est-ce que finalement, les laisser faire comme elles souhaites c'est pas un plus ?
- Une personne pour l'animation doit-elle être présente tout le temps ? Une grande partie du temps ? Très peu ?
- Place de l'open source ?
- Apprentissage des outils ? Phases de théorie ?
- la contribution à un projet libre serait vraiment très très très très bien à refaire. Pas vraiment re-expérimenté depuis Simplon, sauf au rookie club où nous avons fait quelques belles tentative



Piste à explorer ?

- Bascule sur un aspect beaucoup plus communautaire, sans formation longue ? Comment financer ?
- Avoir des POEC/POEI pour accompagner à l'emploi (spécifique à descodeuses ?)
- Est-ce que l'approche « apprendre la programmation plutôt qu'un langage » peut se faire en restant malgré tout sur un seul langage ?

---

Échange sur Beta gouv à propos des lois (pas de l'assemblée) utilisé en construction de service informatique
- https://fr.wikipedia.org/wiki/Principe_de_Peter
- https://meta.wikimedia.org/wiki/Cunningham%27s_Law/fr
- https://fr.wikipedia.org/wiki/Loi_de_Parkinson
- https://fr.wikipedia.org/wiki/Loi_de_Conway
- https://www.timsommer.be/famous-laws-of-software-development/
- https://en.wikipedia.org/wiki/Arrow%27s_impossibility_theorem
- https://fr.wikipedia.org/wiki/Loi_de_Reed
- https://fr.wikipedia.org/wiki/Loi_de_Goodhart


