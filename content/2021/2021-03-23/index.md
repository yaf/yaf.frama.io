---
title: Mardi 23 mars 2021
---

> Nous avons reçu énormément de témoignages de personnes qui se sont lancées, soit en rachetant des forêts pour les préserver, soit en faisant fonctionner des terres agricoles pour les aggrader. Les gens ont bien compris que l’avenir n’était pas de vivre enfermé par quatre murs de béton.

> On comprend que tout ce qui nous entoure vit, et vibre. Et à son contact nous vibrons et nous changeons notre fréquence personnelle. C’est pour cela que j’invite les gens à devenir gardiens d’un territoire, que ce soit un petit bout de jardin qu’on se fait prêter ou une zone agricole ou forestière que l’on achète. Pour préserver un territoire quel qu’il soit, on a besoin de cet attachement-là.

> Heureusement, il y a plusieurs sources sur le terrain. Nous avons donc mis des bidons de 20L dans une brouette et fait de nombreux allers/retours pour nous approvisionner et pallier à cette situation de crise. Pour la première fois de ma vie, j’ai pris conscience de ce que vivent les personnes qui ne disposent pas d’eau potable au sein de leur habitation.
>
> Cet épisode m’a créé un vrai stress, depuis j’observe très attentivement la pluviométrie. Je crée des réserves d’eau douce et nous économisons l’eau. Nous avons un besoin journalier de 5L/jour/habitant alors qu’en moyenne pour un Français c’est 150L.
>
> J’ai développé un respect pour l’eau que je n’avais pas jusqu’alors. Depuis, nous nous sommes raccordés à une autre source. Je sais dorénavant que c’est une question d’années avant qu’il ne se reproduise la même chose, je préserve donc chaque goutte d’eau.

> Intégrez-vous dans les territoires pour que les habitants vous apprécient à travers des activités culturelles et pédagogiques. Face au manque de liens, nous devons développer une culture commune et un bien-vivre ensemble.

> Pour conclure, j’aimerais parler d’une initiative qu’on a lancé : « les gardiens des territoires » pour aider les gens qui n’ont pas les moyens de le faire à se lancer malgré tout. On ne veut pas que ce type de démarche soit réservé à une population aisée. Il faut donc mettre à disposition des terrains pour que ceux qui sont intéressés par cette démarche en prennent soin.
>
> Enfin, on a racheté avec l’association un terrain pour le mettre dans les communs et le sortir de la propriété individuelle. La propriété privée n’est pas une fin en soi.

[Plutôt que de dégrader, aggrader les écosystèmes nous permettra de faire un grand pas dans notre humanité](https://lareleveetlapeste.fr/plutot-que-de-degrader-aggrader-les-ecosystemes-nous-permettra-de-faire-un-grand-pas-dans-notre-humanite)

---

[Discord serait à vendre pour plus de 10 milliards de dollars, Microsoft serait intéressée](https://www.nextinpact.com/lebrief/46522/discord-serait-a-vendre-pour-plus-10-milliards-dollars-microsoft-serait-interessee)

Moi qui pensait m'y installer pour relancer le rookie club. Peut-être qu'un serveur mumble ferait le job ? Ce dont j'ai besoin c'est l'audio et le partage d'écran. Les salles pré-disponible, c'est pas mal, mais pour être honnête, pour le moment, je n'en ai pas vraiment besoin.

---

Est-ce qu'Aydan a l'âge pour jouer à [Minecraft](https://www.minecraft.net/fr-fr) ? Quand je vois certains petit jeu auxquels il joue, je crois que je préfèrerais le voir faire du minecraft. Mais je ne suis pas certains qu'il puisse appréhender les concepts du jeu ? À tester sans doute.

---

> Il est difficile de déterminer le jour exact où les gens ont commencé à danser sur des codes informatiques.

> Les live coders sont très axés sur le côté « open-source et logiciel libre » du domaine informatique. En général, les hackers sont du même côté de la barricade. Comme je l’explique dans mon livre, il semble que les hackers aient anticipé de nombreux concepts qui ont ensuite été repris par les live coders. Certains live coders définissent leur activité comme un hacking de la musique, ou du moins de son processus de fabrication, et je suis d’accord avec cette définition.

[« Pour devenir live coder, vous devez pratiquer, pratiquer, pratiquer » : Entretien avec Giovanni Mori](https://www.makery.info/2021/02/26/english-to-become-a-live-coder-you-need-to-practice-practice-practice-interview-with-giovanni-mori/)

