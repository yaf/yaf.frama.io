---
title: Jeudi 18 mars 2021
---

Un EIG voulant parler de formation, et on se retrouve à une poignée sur une visio à discuter de nos parcours et de comment on trouve des opportunités pour donner des formations.

Très sympa, une belle ambiance et beaucoup de chose intéressante.

Je crois que je suis encore un des seuls qui n'a pas fait de syllabus, qui ne passe pas du temps à préparer un cours.

Dans les échanges j'ai eu envie de partagé le compte [twitter de Félienne](https://twitter.com/felienne). Un peu avant, Élisabeth a partagé un lien pour un livre qu'elle trouve passionnant sur le sujet de l'apprentissage de la programmation [the programmers brain](https://livebook.manning.com/book/the-programmers-brain/welcome/v-3/). Ce livre est de Félienne. Je n'avais pas suivi qu'elle l'avait sorti !

---

Whaou, [un forum ouvert sur le logiciel libre](https://dérivation.fr/evenement/forum-ouvert-faut-il-en-finir-avec-le-libre/)

Dommage que ce soit un week-end.
