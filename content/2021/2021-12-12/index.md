---
title: Dimanche 12 décembre 2021
---

Et si c'était deux livres ou bien un livre qui se lit d'un coté puis de l'autre que j'écrivais ?

Un coté pour apprendre la programmation, un coté pour accompagner la transmission.

Il faudrait avoir des éléments d'autoévaluation coté apprentissage. Coté accompagnement, des conseils et autres tips pour accompagner aux mieux.

Une intro pour parle du cadre : un journal sans doute. Proposer que les personnes ajoute un lien vers leur journals et répo d'exercices ?

Faire une intro sur le faite qu'il y a plein de ressource en ligne et que ce livre proposera de s'en servir. Que c'est un complément à la formation, un outil pour mieux faire le lien avec l'ensemble des connaissances. Voir remplir des manques.

Peut-être que l'écriture de ce livre me permettra de mettre en évidence qu'il manque des ressources en lignes, et donc de les créer.

Quitte à être dans ma camapagne, et ne pas vraiment pouvoir participer à des formations, et si j'écrivais enfin « ma méthode » ?

Un projet à mettre en avant dans ma fiche pour la CAE ? J'ai peur de la remplir, peur d'être refusé...

---

Récupération de fiche et outils du RAF pour la gestion d'une forêt, d'un « bois-bûche ».

[Bois de chauffage en circuit-court](https://www.alternativesforestieres.org/-Bois-de-chauffage-en-circuit-court-)

---

> Basée à Augan dans le Morbihan, l’association Sylv n’ Co s’intéresse à :
>
> - créer et privilégier des espaces de sylviculture écologiquement responsables et socialement solidaires,
> - mettre en place des chantiers participatifs de réalisation de bois de chauffage et d’œuvre en circuit court,
> - organiser des événements pour associer et informer la population sur les questions et pratiques forestières.
>
> L’association fait du bois de chauffage en collectif, de la sensibilisation grand public et on réfléchit sur une valorisation de la filière bois d’œuvre de l’amont à l’aval pour essayer de relocaliser son utilisation.

[ Association Sylv n’Co ](https://www.alternativesforestieres.org/Sylvn-Co)

> hortie@riseup.net
> le Vaubossard
> 56430 Concoret

À contacter pour une formation tronçonnneuse ?

Contacté !

---

> La forêt est avant tout un organisme vivant, qui entretient son équilibre par une dynamique complexe mêlant la vie à la mort, la symbiose et la coopération à la prédation et au parasitisme. En permanence exposée à la puissance des éléments (vent, feu, eau, terre), la forêt en bonne santé s’adapte, revient perpétuellement à un état d’équilibre.

[ Regards sur la forêt ](https://www.alternativesforestieres.org/-foret-france-)

---

Quelques liens pour apprendre QGIS

- [Système d'Information Géographique Libre et Open Source ](https://www.qgis.org/fr/site/index.html)
- [ [QGIS3] Boîte à outils ](https://lofurol.fr/joomla/logiciels-libres/188-qgis3-boite-a-outils)
- [Tutoriel QGIS 3.16](https://ouvrir.passages.cnrs.fr/tutoqgis/index.php)

---

[OpenBSD Amsterdam - hébergement de serveur sour openbsd](https://openbsd.amsterdam/)

