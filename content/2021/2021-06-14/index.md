---
title: Lundi 14 juin 2021
---

J'utilise l'outil [apostrophe](https://opensourcemusings.com/writing-in-markdown-with-apostrophe) pour faire un journal pour [RDV-Solidarités](https://www.rdv-solidarites.fr/). C'est plutôt pas mal.

Par contre, la manipulation de fichier y est pénible. Pour RDV, je n'utilise qu'un fichier par mois, c'est plus simple.

Mais pourquoi je fais un journal ? Pourquoi ne pas utiliser mastodon pour partager mes reflexions et utilise mon site pour rédiger des billets plus construit. C'est ce que font la plus part des personnes je crois.

Pourquoi publier mon journal brut ? Peut-être pourrais-je commiter mon journal, un peu comme un brouillon, comme de la matière première, me servant à réfléchir, à construire des articles plus construit.

---

Nous allons aller au Faouët.

Je suis content de pouvoir explorer ce que signifie de vivre dans une petite ville. À peine plus de 2 000 habitants. J'ai quelques petites craintes, très lié à mes habitudes et à ma vision de urbaine de la vie. Et c'est justement pour démonter ces craintes, déconstruire mes apriori que je suis content d'essayer.

Pour ça la location à du bon, l'engagement est faible.

Ici, à Hennebont, il y a la facilité de pouvoir tout faire à vélo, voir à pieds. Comme à Poissy. Et il y a la grosse ville où il se passe pas mal de chose : Lorient pas loin. Un peu comme Paris n'était pas loin de Poissy.

Là, ce n'est pas la même histoire.

Dans les premières réflexions, il y a la voiture... Et peut-être que je vais commencer à imaginer qu'avoir 2 voitures est peut-être utile dans ces endroits.

Je commence à explorer, par internet, la découverte des environs, des associations et lieux qui semble intéressant à allé découvrir.
- [Le Violon Vert](http://leviolonvert.fr/)
- [Collectif Tomahawk](https://www.facebook.com/associationtomahawk/)
- [La Bascule Argoat](http://argoat.la-bascule.org/)
- [La Ferme de Rouzen](https://biotoutcourt.com/la-ferme-de-rouzen)
- [Les Paysans Solidaires](http://lespaysanssolidaires.fr/)
- [Minoterie Dréan](https://www.moulinderestaudran.fr/)

Il va falloir explorer, découvrir et peut-être créer !

---

« Au-delà de l’invention, les low tech entraînent spontanément un mode de vie plus doux, et une attention nouvelle portée à la nature chaque jour. Ceux qui prennent soin de leur jardin le savent, c’est une façon idéale d’être présent et apaisé. »

[La diffusion des low tech avance dans l’habitat!](https://france.makerfaire.com/2021/06/08/la-diffusion-des-low-tech-avance-dans-lhabitat/)

---

À lire ? [« Reprendre la terre aux machines » par l'atelier paysan](https://www.seuil.com/ouvrage/reprendre-la-terre-aux-machines-l-atelier-paysan/9782021478174)
---

Amusant comme l'exemple simple d'une boutique peut servir à expliquer beaucoup de code.

[JavaScript Async/Await Tutorial – Learn Callbacks, Promises, and Async/Await in JS by Making Ice Cream 🍧🍨🍦](https://www.freecodecamp.org/news/javascript-async-await-tutorial-learn-callbacks-promises-async-await-by-making-icecream/)

Je me rappel que j'ai appris avec une base de code existante, et un bouquin qui m'invitait à explorer le langage et les outils liés à la plateforme en me servant de cette base de code pour m'aider... Et c'était une boutique de sport.

