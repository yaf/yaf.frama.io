---
title: Jeudi 7 janvier 2021
---

Je note des jeux collaboratif à tester avec les enfants

- [Yokai](https://www.espritjeu.com/yokai.html)
- [Deep sea adventure](https://www.philibertnet.com/fr/oink-games/37973-deep-sea-adventure-4271394090427.html)

Découvert avec David B. Merci :)

---

> La Scop apparaît comme le moyen de transmettre l’entreprise sans transmettre le capital, mais en améliorant le niveau de protection sociale. « L’outil de travail reste la propriété de la coopérative, les parts sociales mises au début dans la société restent à leur valeur nominale. » Autrement dit, l’apport initial des associé.es ne peut pas faire l’objet d’une plus-value.

> S’ils avaient par exemple choisi le statut de Gaec (groupement agricole d’exploitation en commun), ils auraient à l’inverse pu bénéficier jusqu’à 100 000 euros de DJA... « Si on ajoute les aides économiques ou fiscales qui seraient multipliées par le nombre d’associés grâce à la transparence, ça fait beaucoup... À l’heure actuelle, s’installer en Scop en agriculture est très pertinent politiquement, mais une connerie sur le plan économique » déplore Mathieu. Adhérent de la Confédération paysanne, il milite pour l’accès aux mêmes droits et aides que sous un statut agricole classique.

[Comment une petite société coopérative tente de changer radicalement le monde paysan](https://www.bastamag.net/la-premiere-SCOP-en-agriculture-ferme-Beletre-Indre-et-Loire-transmettre-une-ferme-sans-que-l-argent-ne-soit-un-obstacle-economie-sociale-et-solidaire)

---

[Guide RGPD du développeur](https://github.com/LINCnil/Guide-RGPD-du-developpeur#guide-rgpd-du-d%C3%A9veloppeur)

Il y a des PRs à faire :)


---

Déplacer mes repo éparpillé ?
J'en ai sur
- [github/yaf](https://github.com/yaf?tab=repositories)
- [gitlab/yaf](https://gitlab.com/yaf)
- [framagit/yaf](https://framagit.org/yaf)
- [git.scopyleft.fr/yaf](https://git.scopyleft.fr/yaf)

Ça fait un peu partout non ? Où rassembler le tout ? Faut-il rassembler le tout ?

---

> Qu’on considère comme « obsolète » un outil de 2015 est un problème. Cette course permanente à l’armement, à la toute dernière nouveauté, est justement un des problèmes du numérique. Le choix de Gemini de faire un protocole qui n’évoluera pas (ou, plus exactement, qui a été conçu pour être difficile à faire évoluer) est un choix délibéré, justement pour éviter cette obsolescence programmée.

Dans un commentaire du [Le web est-iil devenu trop compliqué](https://framablog.org/2020/12/30/le-web-est-il-devenu-trop-complique/)

---

[Entreprise d'insertion EI](https://travail-emploi.gouv.fr/emploi/insertion-activite-economique/article/entreprises-d-insertion-ei)

Et si je montais une structure d'insertion ?

---

Prise de conscience du soir [Statu-Quo](https://oncletom.io/2021/01/05/statu-quo/)

> Le statu quo n’a rien de passif. C’est même très actif. Dire “c’est comme ça”, “on n’y peut rien” ; c’est faire un choix actif en se protégeant, par la validation d’une norme, d’un regard silencieux qui cautionne ce choix actif.

