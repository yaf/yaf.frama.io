---
title: Mercredi 5 mai 2021
---

> Les myosotis ornementaux ( M. sylvatica ) sont en effet comestibles. Ils poussent dans les zones USDA 5-9. Si vous êtes sûr qu'aucun pesticide n'a été utilisé, ils ajoutent de la belle couleur aux salades ou même aux produits de boulangerie et font d'excellentes fleurs confites. Cela dit, ils contiennent de la pyrrolizidine, un produit chimique légèrement toxique qui, s'il est ingéré en grande quantité, peut causer des dommages. Les espèces de M. sylvatica sont vraiment les plus comestibles des myosotis et ne causeront probablement aucun problème avec les enfants ou les animaux domestiques qui les ingèrent.

[Sont Oubliez-Moi-Nots Comestible: Conseils Pour Manger Des Fleurs De Myosotis](https://fr.haenselblatt.com/are-forget-me-nots-edible) (site plein de pub)

> Le myosotis des champs a encore bien d’autres qualités, et mériterait d’être mieux connu pour ses bienfaits sur la santé.
Véritables réservoirs de potassium, ses pétales et ses feuilles renferment des propriétés anti-inflammatoires, très appréciables notamment en cas de rhumatismes, d’arthrite ou de douleurs diverses, mais aussi sédatives et toniques, dont on peut profiter aussi bien en infusion qu’en décoction.
>
> Ainsi, en infusion ses feuille aident à calmer les inflammations des bronches, y compris en cas de la redoutable BPCO.
>
> La décoction de fleurs et feuilles, elle, est stimulante pour tout l’organisme, et donc recommandée en cas de fatigue chronique.
L’infusion des mêmes fleurs et feuilles a plutôt une action sédative : extra pour se relaxer en cas de nervosité !
>
> Et en usage externe ?
>
> Oui, là aussi le myosotis des champs est parfois un bon allié. La preuve : longtemps utilisées en remplacement du mélilot pour les soins oculaires, des compresses trempées dans une infusion de myosotis puis posées sur les paupières fermées aident à soulager les yeux irrités et fatigués, ainsi que la conjonctivite.

> Si vous vous demandez si le myosotis des champs est une fleur comestible, là encore la réponse est oui ! Autant vous le dire tout de suite : les fleurs n’ont pas beaucoup de goût, mais rien ne vous empêche d’en cueillir (dans un endroit le moins souillé et le plus loin de la pollution possible), de les faire sécher, et de les ajouter en décoration de salade, de fromage ou de gâteaux !


[Le myosotis ? Une fleur qui ne s’oublie pas !](https://www.consoglobe.com/le-myosotis-bienfaits-vertus-cg)

> Le myosotis est l’emblème de plusieurs organisations ou associations à travers le monde.
> 
> Parmi les plus remarquables, on retrouve les  Grandes Loges unies d’Allemagne. La fleur, rappelant à tous que certains ont souffert de la guerre, notamment la franc-maçonnerie.
> 
> Le myosotis est aussi le symbole de la Société Alzheimer, il symbolise la perte progressive de la mémoire.
> 
> Le myositis fût également associé à des grandes causes comme le génocide arménien pour en célébrer le centenaire ou à la mémoire des enfants disparus.

[Myosotis : jolie petite fleur](https://www.jardiner-malin.fr/fiche/myosotis-arrosage-planter-semer.html)

