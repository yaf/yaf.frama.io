---
title: 28 avril 2021
---

> « Outil informatique » il faut bannir de son vocabulaire.

> Va avec « outil informatique », « il suffit de savoir s’en servir », et là
> c’est très simple, le résultat de cette phrase c'est qu’on est devenu des
> esclaves de ce qui est fabriqué aux États-unis et en Asie même si on essaie
> de faire autrement.

Moi qui utilise souvent cette terminologie pour évoquer le fait que c'est un
outil au service d'autres chose. Peut-être faut-il que je modifie ma façon
d'exprimer cette idée.

> L’information est universel. Il n’y a qu’une seul sorte d’information.  Il
> n’y a qu'une seule sorte d’algorithme

> Le levier de l'information est très efficace.

> Données numérisé Algorithme (procédé intellectuel de calcul) Programme : truc
> très détaillé pour décrire les algo au machine. Grande distance entre algo et
> programme (ça veut dire langage aussi) Machine (Nous ?)

> Interface est pour faire des interactions.

> Ce qui rends l'informatique très différentes des autres sciences, c'est que
> c'est une science de construction, c'est pas une science naturelle. Les
> biologistes ont énormément de travail, c'est une grande chance, mais ils
> n'ont rien choisi dans leur travail. C'est pas eux qui ont consu l'ADN, c'est
> pas eux qui ont conçu la cellule, c'est pas eux qui ont conçu les être vivant
> ils n'y sont pour rien encore; Ils essayent, (...), mais par rapport à la
> nature, c'est quand même des débutant intégraux.

> La limite de l'informatique, ça reste nos têtes.

> Les langages de programmation, il y a deux camps assez distinct : le camp des
> langages typées, de la puissance d'expression, ...  les langages dynamiques,
> très souple, comme python, JavaScript

[France Culture - Où va
l'informatique](https://www.franceculture.fr/emissions/series/ou-va-linformatique)

