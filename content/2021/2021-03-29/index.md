---
title: Lundi 29 mars 2021
---

Balqis est dans une classe avec une instit formidable je trouve.

Balqis viens de me montrer des plantes, en fleurs actuellement, en me disant : ce sont des « ruines de Rome ». Après une petite recherche, je tombe effectivement sur [_Cymbalaria muralis_](https://fr.wikipedia.org/wiki/Cymbalaire_des_murs) (anciennement _Linaria cymbalaria_), aussi appelé Linaire cymbalaire, Linaire des murs, Ruine de Rome, Cymbalaire.

> Elle permettrait de soigner la gale et le scorbut.

Il est fort possible que l'autre arbre que je cherche à identifier sois une variété de [_Magnolia ×soulangeana_](https://fr.wikipedia.org/wiki/Magnolia_%C3%97soulangeana), mais je n'arrive pas à avoir assez d'information et d'image pour m'en assurer. La période de floraison semble la bonne, le fait qu'il y ait les fleurs avant les feuilles aussi. Autant je vois beaucoup de magnolia avec des pétales rose, au moins sur une face, autant là, la fleur que j'ai vu a des pétales blancs sur les deux faces, et beaucoup plus fin.

---



[David partage un article où Le SNC documente son mode de fonctionnement](https://larlet.fr/david/2021/03/20). Une approche intéressante. Qu'est-ce que ça donnerais si j'essayais de faire le mieux ?

> - les conditions dans lesquelles j’aime travailler ;
> - les heures et les moments de la journée pendant lesquels j’aime travailler ;
> - les meilleures façons de communiquer avec moi ;
> - les façons dont j’aimerais recevoir des commentaires ;
> - les choses dont j’ai besoin ;
> - les choses avec lesquelles j’ai de la difficulté ;
> - les choses que j’aime ;
> - les autres choses à savoir à mon sujet.

