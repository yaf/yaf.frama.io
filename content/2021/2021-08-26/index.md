---
title: Jeudi 26 août 2021
---

Hier, à la réunion pour [le Grand manger](https://www.legrandmanger.bzh/), j'ai entendu parlé de beaucoup de choses chouette dans le coin:

- [FolAvoine épicerie rurale et café](https://folavoine.fr/)
- [La bascule](https://la-bascule.org/)
- [Les sœurs A'Drac](https://www.letelegramme.fr/morbihan/le-faouet/de-l-epicerie-en-vrac-au-marche-bio-de-restalgon-06-11-2020-12652306.php)

Le travail d'exploration va être intéressant. Avec le Grand Manger, je me retrouve déjà au bon endroit pour entendre parlé des bons contacts et aller rencontrer des personnes intéressantes :)

- [L'espace Kdoret, un tiers lieu coopératif à Rostrenen en Centre Bretagne proposé par la SCIC "Gens de la Fontaine"](https://gensdelafontaine.bzh/)
- [Esprit Fablab](https://esprit-fablab.org/)
- [Espace Kdoret](https://asso.espace-kdoret.org/)
- [ Espace Kdoret tiers lieu coopératif en centre bretagne ](http://www.bretagne-creative.net/article23905.html)
- [La Fourmi-e]https://assolafourmie.wordpress.com/) est une association dont le but est de développer des projets autour de la création contemporaine et de la recherche artistique, associant la médiation à la création et à la diffusion d’œuvres, dans le domaine des arts visuels et plastiques. L’association s’inscrit également dans une démarche territoriale
- [Tomahawk. « Une histoire de vies »](https://www.letelegramme.fr/finistere/quimperle/tomahawk-une-histoire-de-vies-07-09-2017-11654201.php)

Voir aussi [MoviLab Bretagne](https://movilab.org/wiki/MoviLab_Bretagne)

---

> Alors que la structure derrière un gros projet open source comme Rust peut sembler similaire (à une grosse entreprise), vue de loin, c’est souvent complètement l’inverse. Dans un tel projet, les objectifs et buts ne sont pas ceux des équipes d’en haut, mais effectivement ceux des contributeurs.

> Au lieu de cela, un contributeur passionné d’algorithmes de formatage pourrait se manifester, et commencer à travailler sur le problème. Notre travail en tant qu’équipe library est de faire en sorte que cette personne puisse travailler. S’assurer que son projet est en accord avec le reste de la bibliothèque standard, relire son travail et fournir un retour utile et constructif. Si davantage de personnes interviennent pour collaborer sur ce projet, mettre en place un groupe de travail pour aider à tout organiser, etc.

> Les entreprises ne font pas travailler le premier venu sur quelque chose. C’est ce qui fait que l’open source est particulier et si génial, si on s’y prend bien.

> Ça peut impliquer le fait de dire non à des idées quand elles seraient incompatibles avec d’autres idées, ou ça peut impliquer beaucoup de discussions pour harmoniser les idées afin qu’elles soient compatibles. C’est exactement l’opposé de la manière dont fonctionne une entreprise typique, où les objectifs viennent d’en haut, et où le management décide de la manière de les répartir et les assigner aux personnes qui effectuent le travail technique.

[Le projet Rust et sa gestion collaborative](https://framablog.org/2021/08/23/le-projet-rust-et-sa-gestion-collaborative/)
