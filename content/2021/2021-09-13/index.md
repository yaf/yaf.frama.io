---
title: Lundi 13 septembre 2021
---

> Le troisième déclencheur, est venu, lui, des lectures d’Édouard Glissant.
> Édouard Glissant (1928-2011), écrivain, poète, philosophe martiniquais, est considéré comme l’un des penseurs les plus importants au monde du concept d’archipélisation. C’est aussi un théoricien de la relation, qui est le lieu par excellence de la lutte comme celui de la prédation.

> « J’appelle créolisation la rencontre, l’interférence, le choc, les harmonies et les dysharmonies entre les cultures. » Par ces mots, Édouard Glissant fait de la « créolisation » une décontinentalisation, qu’il nomme archipélisation, et qu’il corrèle à ce qu’il appelle le « tout-monde ». Le monde entier, pour lui, se créolise et s’archipélise.

Ça me fait penser à une réfléxion personnelle lorsque j'ai découvert que les plats Comoriens avaient beaucoup de ressemblance avec les plats Indiens. Il a toujours (?) été plus simple de se déplacer en bateau sur de longue distance (en longeant la cote dans un premier temps ?) que traversant de grande étendu de terre/montagne. Ça entraine plus de proximité entre des côtes pourtant éloigné qu'avec l'autre bout du continent terrestre...

Un lien avec l'archipelisation dont Édouard Glissant parle ?

[Archipelisation, comment Framasoft conçoit les relations quelle tisse](https://framablog.org/2019/12/10/archipelisation-comment-framasoft-concoit-les-relations-quelle-tisse/)

À lire ? [Édouard Glissant](https://fr.wikipedia.org/wiki/%C3%89douard_Glissant)

---

Le W3C propose maintenant [un design system pour faire un meilleur web](https://design-system.w3.org/), et [le code](https://github.com/w3c/w3c-website-templates-bundle/tree/design-system-docs) est libre bien sur. Il semblerait que ce soir accessible par défaut \o/


---

[Marp - Des présentation avec Markdown](https://blog.zwindler.fr/2021/09/13/marp-presentations-rapides-et-jolies-avec-markdown/)

