---
title: Mercredi 31 mars 2021
---

À lire ? [Petit guide illustré de botanique](https://www.tela-botanica.org/2021/03/petit-guide-illustre-de-botanique/). Une approche simple et par l'image. Sans doute très accessible pour les enfants, mais pour les grands aussi ! Commander [sur le site des éditions Ulmer](https://www.editions-ulmer.fr/editions-ulmer/petit-guide-illustre-de-botanique-772-cl.htm)

---

À lire ? [Sous terre](https://www.tela-botanica.org/2021/03/sous-terre/) À acheter sur le site de l'auter [Dargaud](https://www.dargaud.com/bd/sous-terre-bda5375020)

> Une BD digne de science fiction sur ce qui se passe sous nos pieds... En compagnie de Suzanne, une ado qui a répondu à cette petite annonce pas comme les autres et qui se trouve réduite à une taille minuscule, le lecteur explore le monde passionnant du sol

---

À lire ? [Les Apprentis sorciers de l’azote. La Face cachée des engrais chimiques, Terre vivante](http://blog.ecologie-politique.eu/post/Les-Apprentis-sorciers-de-l-azote)

> L’azote est le principal composant de l’air mais ni nous ni les plantes ne sommes capables d’en tirer directement profit, bien que ce soit un élément indispensable à la vie, la brique principale des protéines.

---

À lire pour Aydan ? [Super fonceuse](http://livres.onpk.net/?9782844554017)

> Goliath était un jeune bouc super costaud. D'une seule patte, il pouvait soulever une masse de mille kilos Mais il estimait qu'aucun travail n'était digne de sa grande force.

---

> Les chiffres mentionnés de rapidité de croissance (10 fois plus rapide, mais selon quels paramètres ?) et d’importance de biodiversité (20 à 100 fois plus importante ou plus dense selon les documents, mais comment est-ce calculé ?) laissent plutôt perplexe, surtout pour des surfaces plantées de si petites dimensions, et devraient faire l’objet de suivis scientifiques et d’études plus précises.

> Plutôt que le nombre d’arbres (qui va forcément évoluer), ne serait-il pas plus juste de comptabiliser les m2 boisés par cette méthode ?

> Mais il est souhaitable que cette nouvelle approche, séduisante et mobilisatrice d’enthousiasme et d’adhésion citoyenne dans le cadre de projets participatifs, prenne toute sa place dans la panoplie des actions à développer, au côté des plantations d’arbres en ligne dans les rues, de l’extension des squares et parcs boisés, ainsi que de la création de « forêts urbaines » moins denses mais plus étendues, afin d’assurer une végétalisation accrue des zones urbaines.The Conversation

[Microforêts urbaines : que penser de la « méthode Miyawaki » ?](https://www.tela-botanica.org/2021/03/microforets-urbaines-que-penser-de-la-methode-miyawaki/)

---

Me former ?

- [Calendrier de formation de l'école bretonne d'herboristerie](http://www.capsante.net/wordpress/?page_id=357)
- [Stages & Formations 2021, stages et formation Terres de tisanes](https://terresdetisanes.fr/stage-et-formation/)

Ce sont des endroits où est passé Kristell de l'association Cueillir.

---

[Tech Moms and curious parents](https://www.techmoms.co)

> Pourquoi initier ce projet Tech Mom ? Voici les raisons principales de vouloir créer ce blog, et ce qu’il peut apporter.
> 1. Découvrir des Roles Models
> 2. Etre parents est sous-estimé
> 3. Inspirer les mamans
> 4. Créer notre documentation
> 5. Utiliser le pouvoir de la communauté

[Pourquoi initier ce projet Tech Mom ? Voici les raisons principales de vouloir créer ce blog, et ce qu’il peut apporter. (en français)](https://www.techmoms.co/community/2020/10/20/tech-moms.html)

---

[Fabrique des Communs Pédagogiques](http://fabpeda.org/)

> La Fabrique des Communs Pédagogiques est un incubateur francophone à #communs (ressources, communautés) dans l'éducation et la recherche. 
> La Fab_Peda est une association loi 1901.

