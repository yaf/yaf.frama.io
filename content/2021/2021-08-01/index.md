---
title: Dimanche 1er août 2021
---

[Sauvages, journal botanique](https://www.tela-botanica.org/2021/07/sauvages-journal-botanique/)

> Ce travail d’attention au monde végétal est riche d’enseignements.
En commençant, je ne pense pas avoir assez de diversité pour assurer une régularité quotidienne. Pourtant, à certains moments, la profusion est telle que je n’ai pas assez de temps.

> Par mon travail d’artisan de la laine, je fais des teintures végétales.
> J’utilise un nombre limité de plantes, celles qui ont le meilleur pouvoir tinctorial.
> En menant ce travail, je constate que, d’une manière ou d’une autre, toutes les plantes teignent.
> Elles contiennent toutes des pigments (jaunes en général) qui les protègent des rayons UV émis par le soleil.
> L’alignement de toutes les pelotes teintes met à l’évidence cette couleur et pourtant, chaque nuance de jaune est unique.

![](Sans-titre5-700x394.png)

> L’inattendu est également de la partie. Les belles fleurs blanches de l’aubépine teignent la laine en un roux joyeux et les pétales de coquelicot donnent un gris profond.


![](pl-700x514.png)

---

J'ai confondu le [Séneçon commun](https://fr.wikipedia.org/wiki/S%C3%A9ne%C3%A7on_commun) et le [Millepertuis](https://fr.wikipedia.org/wiki/Millepertuis). Je découvre au passage la liste énorme d'espèce de Millepertuis !

C'est pas demain que je vais m'amuser à faire une huile rouge avec du millepertuis 

---

> non, il n’y a pas d’intuition et il n’y a pas d’outils intuitifs. La notion d’intuition est dangereuse car elle pousse à oublier le fait qu’un outil porte des valeurs et que le sens est le résultat de l’ensemble des intra-actions - comme les appelle Karen Barad dans son Meeting the Universe halfway - desquelles surgissent après - comme des après-coups - les outils et aussi les êtres humains.

[Ce qui pourrait être autrement: réflexions ultérieures sur l'intuition](http://blog.sens-public.org/marcellovitalirosati/cequipourrait/reflexionsintuition.html)

---

> La démocratie représentative est probablement parmi les pires formes d’organisation politique.
> En premier lieu, le fait que quelqu’un cherche le pouvoir - en se portant candidat - est une excellente raison pour ne pas le lui donner. 

> Que serait beau un monde sans politicien.ne.s.

[ Ce qui pourrait être autrement: contre la démocratie représentative](http://blog.sens-public.org/marcellovitalirosati/cequipourrait/democratie.html)


---

À lire (Aydan) ? Lire : [le Renard et la couronne. Roman ado](https://www.unioncommunistelibertaire.org/?Lire-le-Renard-et-la-couronne-Roman-ado)

---

[L’art enfantin est un art de la liberté](https://www.icem-pedagogie-freinet.org/node/11088)

> - Concrètement ? Par le tâtonnement expérimental!
> - La liberté devient aussi source d’émancipation, d’autonomie et d’efficacité. 
> - Il n’y a donc pas de « méthode » pour apprendre à créer,
> - Il n’y a plus de raison d’imposer des modèles à reproduire
> - Il s’agit de laisser s’épanouir « l’être esthétique » chez l’enfant
> - Ne plus pousser les enfants à représenter le monde de manière « réaliste » ou «objective»:
> - Et assumer cette part, c’est avant tout admettre que pour créer, il faut laisser rêver!
> - La vérité est imaginative
> - Il s’agit donc pour le maître d’accueillir la singularité de son regard et de son geste créateur


[L’art enfantin est un art d'atelier](https://www.icem-pedagogie-freinet.org/node/11089)

> Quelques aspects de ce milieu qui font de l’art enfantin un « art d’atelier »:
> - Un climat rassurant
> - Un climat d’échange et de construction mutuelle des savoirs
> - La part du maître pour encourager la pratique créative singulière
> - Un milieu riche, grâce à l’ancrage culturel
> - Un milieu qui donne accès à des modes d’expression diversifiés
