---
title: dimanche 11 juillet
---

Anne-Sophie fait la promotion de [Kirby](https://getkirby.com/). Un CMS basé sur des fichiers, et accessible... À teste sans doute ?

Peut-être pour le site de terre de liens ?

---

[Une chouette série de vidéo à propos des plantes](https://www.diyseeds.org/fr/films/).

---

[À propos des problèmes d'amoniac en Bretagne](https://splann.org/)... Culture intensive :-/

---

> I posted this drawing on Twitter a while back, and wanted to put it here too because I think it’s an important idea. “No feigning surprise” is probably the social rule from the Recurse Center Manual that’s had the biggest impact on me over the years. 

[No feigning surprise](https://jvns.ca/blog/2017/04/27/no-feigning-surprise/)

Tellement de chose bien dans ce [manuel du recurse center](https://www.recurse.com/manual) !

Il y a sans doute des adaptations à faire pour une formation en ligne.

---

[La fédération des cafés librairies en Bretagne](http://www.lafederationdescafeslibrairiesbretagne.fr/)


