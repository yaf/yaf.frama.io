---
title: Lundi 6 septembre 2021
---

Le déménagement est terminé. Disons, le gros est fait. Nous dormons au Faouët, les enfants vont à l'école d'ici. Il ne reste que les plantes en pot à transporter ici.

La maison est grande, le jardin encore plus. C'est presque trop. Et en même temps, ça nous plonge dans une végétation luxuriante. C'est agréable.

Pneu éclaté sur mon vélo habituel, je roule avec l'ancien vélo de mamie Bretonne, sans vitesse. C'est un peu dur ici, il y a quelque bosse quand même. Le bourg est agréable, le marché bio de Restalgon est sympatique...

Nous n'avons pas encore gouté le pain de Gwendal, rencontré dans le cadre de l'association de préfiguration de la SCIC du grand manger, une cantine coopérative. Il est boulanger bio depuis 15 ans au Faouët ! [Le pain de la semaine](http://www.lepaindelasemaine.bzh/). Une personne très sympa avec qui je vais sans doute découvrir et apprendre pas mal de chose dans la région !

Pour le reste, je n'ai pas encore pris le temps de tout explorer, et pourtant, il y en a. Je vais poser ici quelques liens, pour mémoire.

- [nourriciers.tierslieux.net](https://nourriciers.tierslieux.net/?PagePrincipale) Un webinaire à propos des tiers-lieux autour de l'alimentation. Découvert via Claire du Grand Manger.
- [Ressourcerie Récup'R](https://rmcom.bzh/ressourcerie-recupr), sur la commune du Faouët, et dans toute l’agglomération du Roi Morvan. Il y a également un PDF sur le site de l’agglomération, un [guide de la seconde vie](https://www.lefaouet.fr/wp-content/uploads/2021/06/Guide-Seconde-Vie-RMCom-2021-06-web.pdf) (pour les objets :)).
- [Théatre du laid cru](https://www.laidcru.com/) « Clown, Bouffon et autres travaux publics », une belle compagnie que nous pourrons découvrir bientôt j'espère.
- juste à coté de la maison, il y a un batiment vide depuis peu sans doute. Enseigne encore propre. [Liv'édition](https://www.liv-editions.com/). Dommage, ça avait l'air d'une chouette maison d'édition...
- [LA JARDINERIE SIMORIN](https://lajardinerie.site-solocal.com/), fleuriste, jardinerie, pépinière ? C'est à coté de la maison. J'essayerais d'y faire un tour bientôt pour voir si c'est aussi bien que ça en a l'air...
- [Marché Bio, vente à la ferme](https://www.marchebio.bzh/). Dans une commune toute proche, trois valeurs : faire du bio, expérimenter, faire breton... À découvrir également du mardi au samedi de 15h à 20h. Le dimanche du 10h à 12h.
- [Voisins du Faouët](https://voisinsdufaouet.wordpress.com/jardin-partage/). Toujours d'actualité ?
- « Ellé bio et locale, ma cagette ! », c’est le nom du nouveau collectif de treize producteurs locaux, installés aux alentours du Faouët, qui se sont regroupés afin de proposer leur production en direct aux consommateurs. https://www.facebook.com/Ell%C3%A9-bio-et-locale-ma-cagette-100832485172033/ https://app.cagette.net/home
- [Ferme de Rouzen](https://www.facebook.com/lafermederouzen/posts/1418540965197514)
- [Entretien avec Gilles Kervot, apiculteur, producteur de miel pour EcoTree](https://ecotree.green/blog/entretien-avec-gilles-kervot-apiculteur-producteur-de-miel-pour-ecotree). À rencontrer au musée de l'abeille vivante du Faouët.
- De son côté Maël a travaillé dix ans dans l’informatique avant de revenir à ce qui a bercé son enfance : « L’apiculture est un métier qui me convient mieux. Je ne me vois plus aller passer des journées enfermé dans un bureau. J’avais un besoin de concret, de travailler avec mes mains, de travailler dehors. Il y a l’aspect saisonnier également, l’été on fait des grosses journées et l’hiver on est plus tranquille. Je trouve que c’est un plus beau et noble métier. » https://larucheceltique.fr/jose-mael-nadan/ Ils vendent tous les deux leur production annuelle à La Ruche Celtique, que Maël a connu par l’intermédiaire de son père. Même si ce dernier connaissait la coopérative depuis ses débuts en 1984, il n’a pas toujours vendu sa production par ce circuit. [La ruche Celtique](https://larucheceltique.fr/) Coopérative d'apiculteur !
- [Mange Tes Fleurs : Paysanne Herboriste / Tisanes et Cosmétiques naturels](https://www.facebook.com/profile.php?id=100037915553067)
- [Ferme du Samedy](https://fermedusamedy.com/) lait, fromage, yahourt


---

Apprendre le Breton... J'ai envie, mais pourquoi ? Est-ce que j'ai le temps ? La capacités ?

> Être dans le **lagen**
> _être fatigué-e, avoir la gueule de bois_

> Manger comme une gouelle
> _avoir de l'apétit_

> Une ribine
> _un chemin, une petite route_

> En distribil
> _en désordre, en vrac_

> Un pignou
> _un pleurnichard_

> Une lichouserie
> _une sucrerie_

> Partir a-dreuz
> _partir de travers_

> un pochon
> _un sac plastique_

Quelques expressions Bretonne sympa extraites de [12 expressions Bretonnes que vous devriez connaître (sur facebook)](https://www.facebook.com/laurence.lemoeligou/posts/4877670185579975)


