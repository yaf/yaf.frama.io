---
title: Vendredi 16 avril 2021
---

[Why Does a Design Look Good?](https://www.nngroup.com/articles/why-does-design-look-good/)

> Designs do not look good by chance. Each decision in a design should be made with intention, ideally backed up by a visual-design system. While there is not one single thing that explains whether a design looks good or not, following a few principles increases the likelihood that it does:
> - Align typography (and other graphical elements) to a grid. Anchor each element to a line in your grid system.
> - Establish a clear hierarchy and color palette. Decide what is most important in your design and intentionally apply a specific visual treatment (size, color, placement) to it, so that users see it first.
> - Stay consistent. Define clear visual rules and apply them consistently throughout your design.


Je suis un peu partagé. Où commence la personnalité ? Est-ce que ne pas aligner peu être un choix ? C'est une esthétique différente, mais elle plait sans doute à certaines personnes ? Je comprends bien par contre qu'il y a des « normes » esthétique, une majorité qui trouve que c'est plus beau. Est-ce qu'il faut s'y plier ? Ça dépends sans doute de ce que l'on fait.

J'ai l'impression de me laisser de la liberté sur mon site à moi ;
J'aurais tendance à suivre ce type de principe pour les sites sur lesquels je travaillent.

_Où je place ma limite d'ouverture et d'inclusion ?_

---

> C’est ainsi que surgit l’idée de renverser la perspective, en dissociant les tâches accomplies de la rémunération obtenue. Autrement dit, décorréler le salaire du travail, en ne se focalisant plus sur la production et l’activité exercée, mais plutôt sur les besoins revendiqués du salarié. 

> « On partait du principe que les montants allaient rester relativement raisonnables, dans la mesure où tout le monde était associé et investi dans la boulangerie et qu’à ce titre, personne n’allait prendre le risque de couler la boîte en faisant des demandes farfelues »

[Salaire unique ou « salaire au besoin » : une coopérative boulangère repense la notion de rémunération](https://www.bastamag.net/salaire-au-besoin-egalite-salariale-alternative-cooperative-Scop-boulangerie-Le-Pain-des-Cairns-reconnaissance-du-travail)

---

> La Pèlerine des Étoiles
>
> Buvez de l'eau où le cheval boit. Un cheval ne boit jamais de mauvaise eau.
> Fais ton lit où le chat dort paisiblement.
> Cueille les champignons sans crainte où les insectes atterrissent.
> Plantez un arbre où la taupe creuse.
> Construisez une maison où les serpents se réchauffent.
> Creusez un puits où les oiseaux se cachent de la chaleur.
> Va te coucher et lève-toi en même temps que les oiseaux, tu récolteras les grains d'or de la vie.
> Mangez plus vert, vous aurez des jambes fortes et un cœur résistant, comme l'âme de la forêt.
> Regarde le ciel plus souvent et parle moins, pour que le silence entre dans ton cœur, ton esprit reste calme et que ta vie se remplisse de paix ′′
>
> Seraphin de Sarov (1754-1833)

Il me reste pas mal de chose à mettre en place...

---

> LES SARDINES DE PLANTAIN
>
> Des 3 plantains, le grand ( Plantago major), le moyen ( Plantago medium) et le lancéolé ( Plantago lanceolata) de la photo, j’ai choisi ce dernier pour sa tendreté et sa propreté...
> En effet, le grand plantain qui pousse dans les chemins fréquenté par les plantes des pieds est souvent poussiéreux et coriace.
> Comestible avec son goût particulier de champignon et doué de vertus reminéralisantes et médicinales, il constitue un vrai trésor sous son apparente humilité...✨
> Je commence par une recette facile qui régalera grands et petits ...
>
> 🍃RECETTE DES SARDINES :
> non elles ne sentent pas le poisson mais ressemblent par leur forme à des petits poissons grillés.
> Cueillir des feuilles de plantain lancéolé ( une dizaine par personne)
> Les rincer dans une eau vinaigrée ou citronnée.
> Préparer une pâte liquide façon tempura, avec de l’eau gazeuse et de la farine de blé bio tamisée....ou sa version sans gluten: 1/4 farine de riz, 3/4 farine de sarrasin.
> Un peu de sel de l’Atlantique.
> Certains mettent un peu de levure et une cuillère à café de maïzena; moi non.
> La consistance doit rester liquide.
> Tremper les feuilles une par une dans la pâte et les déposer dans une huile à friture bio très chaude.
> Laisser 15-20 secondes, les sortir, les égoutter sur papier absorbant et saler...
> C’est prêt...!
> Vous pourrez les accompagner d’un pesto de plantain bien citronné pour équilibrer la friture.
>
> 🍃 LE PESTO DE PLANTAIN
> Mixer feuilles de plantain, huile d’olive bio, sel, poivre, un citron sans zeste ni pépins, un peu de poudre ou crème d’amande.
> Rectifier jusqu’à consistance et goût souhaités.
> Un apéro sauvage qui «  déchire »...🍃🍃🍃
>
>
> La Nature by Michel Rostalski
