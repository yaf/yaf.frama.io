---
title: Vendredi 11 juin 2021
---

> Selon l'enquête, les trois principales raisons pour lesquelles les responsables apprécient leur travail sont les suivantes :
>
> - « avoir un impact positif sur le monde » (71 %) ;
> - « permettre de répondre à un besoin de travail créatif, stimulant et/ou agréable » (63 %) ;
> - « travailler sur des projets qui me tiennent à cœur » (59 %).

> Il n'en reste pas moins important d'être payé, que l'on soit développeur, réviseur ou mainteneur. La joie du bénévolat ne permet pas de garder un toit sur la tête, après tout.

> Cependant, si le fait d'être rémunéré pour le travail de maintenance arrive en dernière position dans la liste des choses que les responsables apprécient aujourd'hui (21 %), un examen plus approfondi des données suggère que c'est parce qu'ils n'y ont pas beaucoup réfléchi, faute d'avoir été payés pour cela. Ainsi, alors que seulement 18 % des personnes rémunérées moins de 1 000 dollars par an affirment que la rémunération est une raison pour laquelle elles aiment être responsables, celles qui sont payées davantage voient les choses différemment. Par exemple, 61 % de ceux qui sont payés plus de 10 000 dollars par an considèrent la rémunération comme importante.


> Certains développeurs, comme Salvatore Sanfilippo, créateur de la populaire base de données NoSQL Redis, ont cessé d'être mainteneurs parce qu'ils préfèrent être développeurs plutôt que gestionnaires. Mais la première raison la plus probable de démissionner, citée par 60 % des personnes interrogées, est que « d'autres choses dans ma vie et mon travail ont pris la priorité ». Cette autre chose est souvent de gagner de l'argent avec leur emploi principal.

> Il reste beaucoup à faire pour faciliter la vie des mainteneurs, mais leur verser un vrai salaire pour leur vrai travail serait un bon point de départ.

[ Pourquoi le travail acharné et la faible rémunération stressent les mainteneurs de logiciels libres ? ](https://www.zdnet.fr/actualites/pourquoi-le-travail-acharne-et-la-faible-remuneration-stressent-les-mainteneurs-de-logiciels-libres-39924021.htm)

---

La famille AliSoilihi-François va déménager à la fin de l'été.
Nous venons de faire un an pas trop loin de la mer, très prêt de Lorient.
Nous allons expérimenter (en location toujours) de passer un bout de temps dans les terres, au nord de Lorient : [Ar Faoued (Le Faouët en français :p)](https://fr.wikipedia.org/wiki/Le_Faou%C3%ABt_(Morbihan))
Je vous tiens au courant quand j'ai les adresses & co

Nous allons expérimenter une unité urbaine de l'insee aussi : Unité urbaine : Le Faouët (ville isolée) 🤣 
Aire d'attraction - Commune hors attraction des villes 

Découverte associée : [Marion du Faouët (1717-1755), bandit et chef de bande qui sévissait dans la région](https://fr.wikipedia.org/wiki/Marie_Tromel,_dite_Marion_du_Faouet)

