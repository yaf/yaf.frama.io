---
title: Dimanche 16 mai 2021
---

Série de font à tester sur [Elsif.fr](http://elsif.fr) ? `"Literata", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", serif`

---

Où placer un journal d'équipe ?

Je viens de relire une entrée de journal du 15 décembre où j'évoque le fait de faire un journal d'équipe, un journal pro. Je trouve ça assez intéressant. C'est vrai que sur DossierSCO, c'était un outil intéressant.

Je me demande où le placer ? Faut-il qu'il soit privée pour pouvoir y écrire des choses sensibles ? Où bien je me restreint à y écrire des choses publics ?

Un jekyll dans le github de betaGouv ? Ça pourrait être une pratique intéressante à partager avec la communauté. Peut-être sur le github de Rdv-Solidarités ?

