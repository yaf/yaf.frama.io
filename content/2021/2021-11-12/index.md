---
title: Vendredi 12 novembre 2021
---

Que faire de la cendre ?

> Trempez du papier journal humidifié dans de la cendre de bois. Frottez la vitre, et la crasse s'enlève très facilement. Ça marche aussi avec la vitre d'un insert de cheminée.

> La lessive, telle qu'on la connaît maintenant, n'existe que depuis les années 30 avec l'invention des tensioactifs de synthèse.
> 
> Ingrédients
> - de la cendre de bois fine et propre (de couleur blanche ou grise) 
> - de la mousseline 
> - un vieux seau que vous pouvez percer
> - un deuxième seau bien propre Comment faire
> 
> 1. Percez des trous dans un vieux seau. 
> 2. Recouvrez le fond du seau avec de la mousseline et ajoutez une bonne couche de cendres (à peu près 3 verres par litre d'eau). 
> 3. Suspendez le seau percé. Puis, placez un autre seau, bien propre, en dessous du seau percé. 
> 4. Versez de l'eau dans le seau percé. Ce procédé va extraire la potasse de la cendre.
> 5. La mousseline va filtrer les cendres : vous obtenez une lessive à la cendre de bois dans le seau propre ! Note : La lessive à la cendre de bois est de couleur brune. Par conséquent, quand l'eau qui coule du seau suspendu commence à devenir claire, vous pouvez arrêter le procédé de filtration.
> 6. Enfin, versez le liquide dans une bouteille bien propre : votre lessive est prête !
> 7. Secouez pour mélanger. Vous allez voir que la lessive mousse déjà.
> 8. Versez à peu près 100 ml de cette lessive dans la machine à laver (voire moins pour les machines récentes).
> Résultat
> 
> Et voilà, vous savez maintenant fabriquer votre propre lessive à la cendre de bois :-)
> 
> Vous pouvez aussi ajouter quelques gouttes d'huile essentielle (10 gouttes pour 1 l d'eau) pour que ça sente bon.
> 
> Avec cette eau à base de lessive de cendres, vous pouvez nettoyer et désinfecter le linge, les surfaces, les assiettes et couverts et même les traces de rouille sur les surfaces en marbre.
> 
> Attention : il faut utiliser seulement de la cendre fine et propre, de couleur blanche ou grise.
> 
> N'oubliez pas que la lessive à la cendre est un produit caustique qui peut abîmer les mains.
> 
> Vous pouvez aussi réutiliser les cendres restantes dans la mousseline. Mettez-les dans votre compost ou au pied des plantes, car la potasse est un excellent engrais.
> 

---

Visité le bois du pépé d'Audrey aujourd'hui. Est-ce que j'ai le courage d'apprendre à me servir d'une tronçonneuse ? Au delà de son aspect peu écologique (essence, bruit), c'est un instrument dangereux qui me fait peur...


