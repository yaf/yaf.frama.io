---
title: jeudi 30 octobre 2021
---

Tiens, ça fait longtemps que je n'ai pas écrit ici...
Le déménagement ? Le covid ? L'envie de structurer ce site et ma prise de note, mon journal  autrement ?

Je suis de passage pour poser un lien vers une référence que j'apprécie

[Appliquer une pédagogie de l’égalité dans les enseignements d’informatique](https://interstices.info/appliquer-une-pedagogie-de-legalite-dans-les-enseignements-dinformatique/)

Un article d'Isabelle Collet à propos de la pédagogie de l'égalité. Partagé par Souad.

---

[Nocodb - Open Source Airtable Alternative - turns any MySQL, Postgres, SQLite into a Spreadsheet with REST APIs. ](https://github.com/nocodb/nocodb/)
