---
title: Dimanche 21 mars 2021
---

> C'est très difficile d'être un vrai scientifique et littéraire, et bien faite de l'histoire scientifique et vous serez les deux.

J'aime bien cette petite phrase du générique de l'émission « la marche des sciences » de France Culture.

[Émission à propos de « qui se niche derrière la hulotte »](https://www.franceculture.fr/emissions/la-marche-des-sciences/qui-se-niche-derriere-la-hulotte)

> [La Hulotte](https://www.lahulotte.fr/), le journal le plus lu dans les terriers

> Né de l'envie et de la passion d'un homme, alors instituteur, Pierre Déom , ce journal naturaliste visait à l'origine le jeune public, les enfants dont la conscience et l'éveil à leur environnement étaient vus comme un enrichissement pour ces futurs citoyens.

> Chaque fois que j'aborde un sujet, en général je le connais très très mal, je le découvre en même temps que je travail, et 6 mois avant mes lectures, ce qui fait que j'ai une fraicheur, une curiosité.

La Hulotte est né en tant que bulletin de liaison de club « connaissance et protection de la nature ». Recueille d'activité, compte rendu d'enquête, etc.

Ça me fait penser aux journaux, genre Rookie Club.

Ça me fait également penser au travail de Célestin Freinet.

> - Et toujours sans publicité.
> - Ah oui, jamais.

Belle émission, belle personne, ça donne envie de s'abonner.

---

Note de lecture « 55 plantes médicinales dans mon jardin » de Virginie Peytavi


Je trouve ce livre tellement intéressant que je vais l'acheter.

> Préambule
> À l'heure d'une nouvelle - et énième - bataille pour le rétablissement du métier d'herboriste, supprimé en 1941 par le gouvernement de Vichy, les plantes médicinales sont de nouveau au cœur de polémiques passionnées.


> Le moyen âge, une période obscure ?
> Dans les pages marquantes de l'histoire des usages médicinaux des plantes, **l'école de Salerne** ne peut pas ne pas être mentionnée. Dès le IXe siècle, et jusqu'au XIIIe, se rassemblent des hommes de toutes confessions, des juifs, des musulmans, des chrétiens et des femmes (oui, des femmes !), érudits, savants et poètes... Qui mettent en commun leurs savoirs et leurs expériences. De cet exemple de coexistence intelligente et tolérante, il nous reste aujourd'hui d'admirable poèmes. Les Salerniens, dotés de talent et d'humour, consignaient entre autres les vertus des plantes en vers léonins, ce qui en facilite grandement la mémorisation. En voici deux exemples.
>
> La Mauve
> _Elle amollit le ventre avec son suc vanté_
> _Et ce don lui valut le nom qu'elle a porté_
> _Ce suc de l'intestin expulse la matière,_
> _Excite l'utérus et son flux ordinaire._
>
> Le fenouil
> _La graine du fenouil dans le vin détrempé_
> _Ranime, excite une âme à l'amour occupée_
> _Du vieillard rajeuni sait réveiller l'ardeur_
> _Du foie et du poumon disipe la douleur_
> _De la semence encore le salutaire usage_
> _Bannit de l'intestin le vent qui faisait rage._

Je n'ose pas reproduire les longues liste de plantes rangé en classification thérapeutique.

Même les annexes sont riches d'informations.
