---
title: Mercredi 21 juillet 2021
---

- À lire ? [Nadia Yala Kisukidi](https://fr.wikipedia.org/wiki/Nadia_Yala_Kisukidi)
- À lire ? [Basculements. Mondes émergents, possibles désirables, Paris, La Découverte, 2021, 256 p.](https://www.editionsladecouverte.fr/basculements-9782348066733) par [Jérôme Baschet](https://fr.wikipedia.org/wiki/J%C3%A9r%C3%B4me_Baschet)
- À lire ? [Nous ne sommes pas seuls](https://www.terrestres.org/2021/05/17/nous-ne-sommes-pas-seuls-entretien-avec-lena-balaud-et-antoine-chopot/)
- À lire ? [Julia Laïnae et Nicolas Alep, Contre l’alternumérisme](https://www.levelesyeux.com/julia-lainae-et-nicolas-alep-contre-lalternumerisme/) 
- À lire ? [Grèves et joie pure, Libertalia, 2016, 80 p. (ISBN 9782918059875)](https://editionslibertalia.com/catalogue/a-boulets-rouges/simone-weil-greves-et-joie-pure).
