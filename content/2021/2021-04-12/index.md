---
title: Lundi 12 avril 2021
---

Découverte d'un [mouvement libertaire des potagers](https://www.facebook.com/MouvementLibertaireDesPotagers/). Dommage que ce soit sur Facebook !

> Plus de légumes et moins de bitume

> Rendre son jardin résistant à la sécheresse en fabriquant soi même des oyas
> Oubliez la colle. Autant remplacer par la résine, le miel etc

![](158731681_1645946245605168_2684408321054990058_n.jpg)
![](158822712_1645946215605171_972128996041716247_n.jpg)
![](158753705_1645946452271814_1686574548549260915_n.jpg)
![](159067324_1645946385605154_1634549862599800016_n.jpg)
![](159615385_1645946278938498_5157878562677401075_n.jpg)
![](158667286_1645946378938488_7675112034430554529_n.jpg)
![](158996145_1645946315605161_105506034495782680_n.jpg)
![](158959434_1645946332271826_1291079938350766294_n.jpg)


---

[Le W3C propose un curriculum pour aborder l'apprentissage de l'accessibilité](https://www.w3.org/WAI/curricula/#curricula-modules).
