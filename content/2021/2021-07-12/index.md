---
title: Lundi 12 juillet 2021
---

Les pétroleuses propose un livre qui semble très très intéressant dans leur newsletter : [MAKING STUFF & DOING THINGS](https://www.la-petroleuse.com/fanzines-zine-culture/4841-livre-making-stuff-doing-things-kyle-bravo.html), mais actuellement indisponible.

J'ai donc fouillé un peu et je ne le trouve pas. Par contre, je vais reposer (encore ?) ici une liste de site de libraire pour trouver ou commander des livre, sans passer par Amazon.

- https://www.placedeslibraires.fr/
- https://www.librairiesindependantes.com/
- https://www.libraires-ensemble.com/
- https://www.leslibraires.fr/
- https://www.initiales.org/
- https://www.lalibrairie.com/
- https://www.librairies-sorcieres.fr/ (spécial jeunesse)

---

Je suis encore à faire mon journal dans le terminal. J'ai fait « l'effort » d'ouvrir VSCodium ce matin pour voir si je passe la journée dessus ou pas.

---

> TrueCactus  10 h 51
> Comment vous faites pour gérer le mal être du a la recherche d'emploi?
> J'avoue que ça me pèse beaucoup et m'épuise quotidiennement. J'ai beaucoup de mauvaises pensées. La seule chose qui me fait tenir c'est mon amoureux.
> Les remarques culpabilisantes non stop des gens parce que eux ont trouvé ils sont expert en recrutement... Les faut dire ci ça a marché pour moi alors que je dis la même chose, faut faire ci faut faire ça , j'ai l'impression qu'ils pensent qu'on fait rien... Les conseils qui se contre disent... Et qui bien souvent viennent des même personnes. Outre tous ces gens qui se veulent bienveillants (et encore des fois je doutes...)
> Outre ça, il y a les recruteurs qui vous méprisent comme de la merde, des gens me contactent pour me laisser en vu, quel est l'intérêt ?  on m'a déjà raccroché en pleine conversation téléphonique, les réponses auto, les candidatures qui donnent quasiment jamais lieu à des retours... Et tellement d'autres choses.
> Je n'ai déjà que très peu d'estime pour moi. Je n'aime pas la personne que je suis. Je suis stupide, et cette reconversion et recherche d'emploi me le rappellent tous les jours. J'en ai marre de subir que des échecs, je suis épuisée, épuisée de toujours avoir cette angoisse... Épuisée de ne pas pouvoir me projeter dans l'avenir... Épuisée d'être médiocre, Épuisée de me lancer dans des choses qui n'aboutissent pas. Épuisée d'être stupide. Épuisée d'être moi.
> Voilà j'en ai marre. En ce moment je pleure beaucoup, je pense que j'arrive au bout du bout.
> Merci d'avoir lu, si vous avez tout lu.

---

Des personnes intéressantes danas les partenaires du café C0DE. J'ai hate d'allé y faire un tour pour mon repas de demain.

- [Jardin de Brière](https://www.jardinsdebriere.com/)
- [Ethic et Troc](https://www.facebook.com/ethic.troc)
- [Tea & Ty](https://teaandty.com/)
- [Kreiz Breizh Terre Paysanne](https://www.kbtp.fr/)
- [Café d'Oriant](https://www.cafedoriant.bzh/)

[Le thé de Keriado](https://www.facebook.com/permalink.php?story_fbid=125098579235337&id=113695010375694) !?!?? 

---

[CMS avec fichiers static Grav](https://getgrav.org/)
