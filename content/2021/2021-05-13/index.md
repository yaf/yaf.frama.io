---
title: Jeudi 13 mai 2021
---

[Une pratique intéressante de PIX pour lister les outils qu'ils et elles utilisent](https://intranet.pix.fr/)

Je place ça dans le README bien souvent, ou dans la doc. Mais là, c'est clair :)

---

> Similarly, you can delete local branches that have been merged into the master by running this code:

`git branch --merged master | grep -v "master" | xargs -n 1 git branch -d`

_À tester, j'ai souvent le souci, et je n'avais pas encore trouver de combinaison de commande intéresant_

[Top Ten Git Tips & Tricks](https://www.honeybadger.io/blog/git-tricks/)

---

> Elevating others and making open source approachable
>
> Monica nurtures inclusive spaces to educate, learn, be creative, and collaborate.

[Elevating others and making open source approachable ](https://github.com/readme/monica-powell)

Et je découvre en même temps [The Readme Project de Github](https://github.com/readme)

> The ReadME Project amplifies the voices of the developer community by telling stories about: Open Source, Culture, Security, DevOps, and more. 

---

> How can we get people to review pull requests faster??

> We use pull requests to ensure code is understandable by the whole team.

> To improve flow, eliminate queues.

> Pull requests are an improvement on working alone. But not on working together.

[Those pesky pull request reviews](https://jessitron.com/2021/03/27/those-pesky-pull-request-reviews/)

Je suis tellement d'accord.

J'ai beaucoup apprécié la façon de fonctionner que nous avions sur DossierSCO. C'était parfois fatiguant, mais tellement efficace il me semble. Est-ce que ça a été possible parce que nous étions au début dans les mêmes pièces, et que nous nous sommes éloigner petit à petit, après avoir testé des outils de travail à distance ?

Aujourd'hui, je tire aussi quelque bénéfice à travailler avec des pull resquests : je travail de manière très très asynchrone. C'est un confort.

Est-ce que les projets sous licence libre devraient tenter de faire un peu de mob programming de temps en temps ? Sans doute. Est-ce qu'il faut arrêter de faire des PR et de l'asynchrone ? Je ne suis pas sur encore.

Avec l'asynchrone, je peux coder le soir et traiter les demandes utilisateurs en journée. Sur dossierSco, nous les traitions ensemble souvent, et ça nous permettait de faire évoluer l'outil. Actuellement, il se peut que certaines choses ne se mettent pas en place parce que je suis seul le nez dedans. C'est à réfléchir. Pourquoi maintenir ce fonctionnement ? Je devrait peut-être proposer de faire une journée en commun avec Nicolas, ou au moins une demi journée pour voir.

---

[Manifeste CodeWorks : un paradigme d’ESN alternatif, équitable et solidaire.](https://medium.com/codeworksparis/codeworks-un-paradigme-desn-alternatif-%C3%A9quitable-et-solidaire-e6759a19b703)

