---
title: Vendredi 12 février 2021
---

Note de lecture du livre « Philosopher et méditer avec les enfants » de Frédéric Lenoir

> Comme le rappelait Montaigne on devrait surtout proposer aux enfants, dès le plus jeune âge, d'avoir une tête « bien faite » et non pas une tête « bien pleine ». Plutôt que d'assimiler des concepts (ce qui se fait actuellement en terminale), les enfants pourraient apprendre à débattre en respectant des règles, à développer l'esprit critique, le discernement, ainsi qu'une pensée personnelle reposant sur des arguments rationnels, et pas sur des croyances ou des opinions.

> Les ateliers philo passionnent les enfants pour plusieurs raisons. D'abord parce que c'est un des seuls espaces où ils peuvent dire ce qu'ils pensent sans répéter un savoir appris, ni se sentir jugés ou notés.

> À l'inverse, d'autres enfants, brillants dans l'apprentissage des matières habituelles, se révèlent plus embarrassés quand il s'agit d'exprimer un avis personnel et d'argumenter pour le défendre.

> Mais ne peut-on pas aussi concevoir cette discipline (la philosophie) à la manière socratique, soit comme un questionnement exigeant qui permet à la raison de progresser et à la pensée de s'affiner ? Il n'est pas question, dans ce cas, d'acquérir un savoir, mais d'apprendre à penser.

Je découvre aussi [Matthew Lipman](https://fr.wikipedia.org/wiki/Matthew_Lipman).

> Ses travaux visent à promouvoir un enseignement généralisé et adapté de la capacité à penser par soi-même. Il a mis au point un système théorique et pratique, tout d'abord inspiré de John Dewey puis devenu complètement novateur : susciter la pensée rationnelle et créative, à travers des ateliers de discussion à caractère philosophique (pour les enfants ou les adultes). Le tout est soutenu par des romans philosophiques ("manuel narratif") et des manuels d'exercices ("guides pédagogiques"). 
> (citation de wikipedia)

À lire ? [La Découverte de Harry Stottlemeier](https://fr.wikipedia.org/wiki/La_D%C3%A9couverte_de_Harry_Stottlemeier) de Lipman
À lire ? À l'école de la pensée, 2e EDITION -Enseigner une pensée holistique-, Bruxelles, Pédagogies En Développement, De Boeck Université, 2006 (traduction de Nicole Decostre)


Les 10 recommandations:
1. Aménager un espace qui favorise la discussion entre les enfants
2. Demander l'opinion des enfants sur la philosophie
3. Dire aux enfants les règles de l'atelier
4. Choisir une entrée en matière qui favorise le débat
5. Donner le moins possible son point de vue personnel, apporter un éclairage notionnel
6. Appuyer sur les réponses des enfants pour lancer ou relancer le débat
7. Recadrer le débat quand il se perd dans des anecdotes
8. Donner la parole aux enfants qui ne parlent pas
9. Synthétiser les réponses et reformuler
10. Garder une trace écrite des ateliers

---

Découverte d'une association active autour des TIC sur le pays de Lorient et le Morbihan plus largement [CampTIC](https://www.camptic.fr/).

> Nous sommes une association mobilisée pour l’apprentissage du numérique tout au long de la vie. Ces technologies sont au service de l’Humain et non l’inverse. Une culture de clefs de compréhension sur la logique d’Internet et des usages est nécessaire au plus grand nombre aujourd’hui pour ne pas se laisser dépasser.

C'est bon ça :)

---

À lire ? [Jeu et théorie du duende](https://www.babelio.com/livres/Garcia-Lorca-Jeu-et-theorie-du-duende/140061) de Federico Garcia Lorca.

Le livre qu'Henri Verdier donne à ses collègues quand il part d'un endroit. Découvert sur le podcast [HackersPublic #6](https://f14e.fr/hackerspublics/ep6_henri_verdier/)

Henri parle aussi d'une [version audio de Jacques Higelin](d'une audio de ce livre https://www.franceculture.fr/emissions/fictions-theatre-et-cie/jeu-et-theorie-du-duende-de-federico-garcia-lorca) enregistré en 2014.

---

[BuildingConsentfulTech](http://www.consentfultech.io/wp-content/uploads/2019/10/Building-Consentful-Tech.pdf)

> What does consent have to do with technology?A lot of us know about consent with regard to our physical bodies, like in the context of medical decisions or sexual activities. But when it comes to our digital lives, there’s a lack of discussion about what consent means for our data, our identities, and our online interactions.

> UNDERSTANDING CONSENT IS AS EASY AS F.R.I.E.S.
> - Freely given.
> - Reversible.
> - Informed.
> - Enthusiastic.
> - Specific.

> Consent is an ongoing process
> The process of asking for consent does not stop at the first yes.

> It isn’t enough to iterate features in response to harm — we must also iterate the process that lead to those features being released. What would that process look like if it was centered around the privacy and security of survivors of violence? Of people from communities that are regularly subject to state surveillance?

---

> Je rêve d’un client mail qui serait un véritable logiciel d’écriture. Pas d’options et de fioriture. Pas de code HTML. Écrire un email comme on écrit une lettre. En mettant l’adresse du destinataire en dernier, comme on le fait pour une enveloppe.
>
> Un logiciel d’écriture d’email qui nous aiderait à retrouver un contact avec sa correspondance plutôt qu’à permettre l’accomplissement d’une tâche mécanique. Un logiciel qui nous encouragerait à nous désabonner de tout ce qui n’est pas sollicité, qui marquerait des mails les correspondances en attente d’une réponse. Qui nous encouragerait à archiver un mail où à le marquer comme nécessitant une action plutôt qu’à le laisser moisir dans notre boîte aux lettres.

> De même, mes mails rédigés ne seraient pas envoyés avant une heure fixe du soir, me permettant de les modifier, de les corriger. Mieux, je devrais être forcé de passer en revue ce que j ‘envoie, comme si je me rendais au bureau de poste.

> En poussant le bouchon un peu plus loin, les mails envoyés pourraient prendre une durée aléatoire pour être remis. Un lecteur de mon blog a même imaginé que cette durée soit proportionnelle à la distance, comme si le courriel était remis à pied, à cheval ou en bateau.

> Nous en oublions la possibilité d’avoir des échanges lents, profonds, réfléchis.

[Pour un logiciel de correspondance plutôt qu’un client mail](https://ploum.net/pour-un-logiciel-de-correspondance-plutot-quun-client-mail/)

---

Découvert [Coop.Tech](https://cooptech.fr/).

> Les sociétés coopératives du numérique se mobilisent pour développer un écosystème économique vertueux !

> La CoopTech est une association qui a pour objectifs de :
> - Diffuser le statut d’entreprises coopératives auprès des créateurs d’entreprises,
> - Développer l’activité commerciale entre les scops mais également avec toutes entreprises désireuses de découvrir notre modèle et travailler avec nous.

---

> Elle sont bientôt deux mille à se réunir en syndicat, à manifester derrière le drapeau rouge. "Nous ne demandons pas à habiter des hôtels somptueux comme nos patrons, à avoir de belles robes comme leurs femmes, nous ne demandons pas à passer notre temps à la chasse ou en voyage à Paris ou ailleurs; nous demandons du pain, et encore, comme quantité, le strict nécessaire... Nous affirmons que si les usiniers veulent maintenir le travail au mille, c'est que ce mode de travail leur permet de nous voler de 30 à 40% de notre travail", peut on lire dans Le Matin du 20 juillet 1905. 

["Saluez riches heureux ces pauvres en haillons". Le jour où les sardinières de Douarnenez mirent en boîte les conserveurs.](https://lhistgeobox.blogspot.com/2021/01/saluez-riches-heureux-ces-pauvres-en.html?spref=tw&m=1)
