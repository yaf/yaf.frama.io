---
title: Mercredi 28 Juillet 2021
---

- À lire ? Seymour Papert avec Marvin Minsky, Perceptrons, MIT Press, 1988 (ISBN 0-262-63111-3)
- À lire ? Papert, S. et Harel, I., Constructionism : research reports and essays 1985 - 1990, Norwood, New Jersey, Epistemology and Learning Research Group, the Media Lab, Massachusetts Institute of Technology, Ablex Pub. Corp, 1991.
- À lire ? Connected Family : Bridging the Digital Generation Gap, 1996, 211 p.
- À lire ? The Children's Machine: Rethinking School in the Age of the Computer (Traduction française : « L'enfant et la machine à connaître. Repenser l'école à l'ère de l'ordinateur »), Dunod, 1994, 241 p. (ISBN 0-465-01063-6).
- À lire ? Jean Piaget, Cybernétique et épistémologie, PUF, 1968, 142 p.,

[Logo fondation](https://el.media.mit.edu/logo-foundation/)

