---
title: Vendredi 5 mars 2021
---

Note de lecture [« L'enracinement »  de Simone Weil](https://fr.wikipedia.org/wiki/L'Enracinement).

> Une nourriture indispensable à l'âme humaine est la liberté. La liberté, au sens concret du mot, consiste dans une possibilité de choix. Il s'agit bien entendu, d'une possibilité réelle. Partout où il y a vie commune, il est inévitable que des règles, imposées par l'utilité commune, limitent le choix.
  >
> Mais la liberté n'est pas plus ou moins grande selon que les limites sont plus étroites ou plus larges.

> En faisant de l'argent le mobile unique ou presque de tous les actes, la mesure unique ou presque de toutes choses, on a mis le poison de l'inégalité partout. Il est vrai que cette inégalité est mobile ; elle n'est pas attachée aux personnes, car l'argent se gagne et se perd ; elle n'en est pas moins réelle.

> Il y a deux espèces d'inégalités, auxquelles correspondent deux stimulantes différents. L'inégalité à peu près stable, comme celle de l'ancienne France, suscite l'idolâtrie des supérieurs - non sans un mélange de haine refoulée - et la soumission à leurs ordres. L'inégalité mobile, fluide suscite le désir de s'élever. Elle n'est pas plus proche de l'égalité que l'inégalité stable, et elle est tout aussi malsaine. La Révolution de 1789, en mettant en avant l'égalité, n'a fait en réalité que consacrer la substitution d'une forme d'inégalité à l'autre.

> L'enracinement est peut-être le besoin le plus important et le plus méconnu de l'âme humaine. C'est un des plus difficiles à définir. Un être humain a une racine par sa participation réelle, active et naturelle à l'existence d'une collectivité qui conserve vivants certains trésors du passé et certains pressentiments d'avenir. Participation naturelle, c'est-à-dire amenée automatiquement par le lieu, la naissance, la profession, l'entourage. Chaque être humain a besoin d'avoir de multiples racines. Il a besoin de recevoir la presque totalité de sa vie morale, intellectuelle, spirituelle, par l'intermédiaire des milieux dont il fait naturellement partie.

> Les échanges d'influences entre milieux très différents ne sont pas moins indispensables que l'enracinement dans l'entourage naturel. Mais un milieu déterminé doit recevoir une influence extérieure non pas comme un apport, mais comme un stimulant qui rende sa vie propre plus intense.

> Il y a déracinement toutes les fois qu'il y a conquête militaire, et en ce sens la conquête est presque toujours un mal.
> (...)
> quand le conquérant reste étranger au territoire dont il est devenu possesseur, le déracinement est une maladie presque mortelle pour les population soumises. Il atteint le degré le plus aigu quand il y a déportation massives, comme dans l'Europe occupée par l'Allemagne ou dans la boucle du Niger, ou quand il y a suppression brutale de toutes les traditions locales, comme dans les possessions françaises d'Océanie.

> Même sans conquête militaire, le pouvoir de l'argent et la et la domination économique peuvent imposer une influence étrangère au point de provoquer la maladie du déracinement.

> Il est une condition sociale entièrement et perpétuellement suspendue à l'argent, c'est le salariat, surtout depuis que le salaire aux pièces oblige chaque ouvrier à avoir l'attention toujours fixée sur le compte des sous.

> Il est donc urgent d'examiner un plan de réenracinement ouvrier, dont voici, en résumé, une esquisse possible.
> (...)
> Les machines n'appartiendraient pas à l'entreprise. Elles appartiendraient aux minuscules ateliers dispersés partout, et ceux-ci à leur tour seraient soit individuellement, soit collectivement la propriété des ouvriers. Chaque ouvrier possèderait en plus une maison et un peu de terre.
> (...)
> Cette triple propriété (machine, maison et terre) ne pourrait être ni transmise par héritage, ni vendue, ni aliénée d'aucune manière.

> Le déracinement paysan a été, au cour des dernières années, un danger aussi mortel pour le pays que le déracinement ouvrier. Un des symptômes les plus graves a été, il y a sept ou huit ans, le dépeuplement des campagnes se poursuivant en pleine crise de chômages.

> (...) les Blancs transportent partout où il vont le déracinement paysan. La maladie a gagné l'Afrique noire, qui pourtant était sans doute depuis des milliers d'années un continent fait de villages. Ces gens-là au moins, quand on ne venait pas les massacrer, les torturer ou les réduire en esclavage, savaient vivre heureux sur leur terre. Notre contact est en train de leur faire perdre cette capacité. Cela pourrait faire douter si même les Noirs d'Afrique, quoique les plus primitifs parmi les colonisés, n'avaient pas somme toute plus à nous apprendre qu'à apprendre de nous. Nos bienfaits envers eux ressemblent à celui du financier envers le savetier. Rien au monde ne compense la perte de la joie au travail.

> La première condition d'un réenracinement moral de la paysannerie dans le pays, c'est que le métier d'instituteur rural soit quelque chose de distinct, de spécifique, dont la formation soit non seulement partiellement, mais totalement autre que celle d'un instituteur des villes. Il est absurde au pus haut point de fabriquer dans un même moule des instituteurs pour Belleville ou pour un petit village.

> Une fois les corporations disparues, le travail est devenu, dans la vie individuelle des hommes, un moyen ayant pour fin correspondantes l'argent.


