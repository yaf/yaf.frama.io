---
title: Samedi 27 février 2021
---


[Forum d'OpenStreetMap France](https://discourse.openstreetmap.fr/)

---

Découverte de [Thiga, product Academy](https://medium.com/product-academy-thiga)

> Le guide des Product Managers et des Product Owners d’élite !

_Hmm, pas à l'aise avec cette histoire d'élite :-/_

- [Priorisez votre backlog](https://medium.com/product-academy-thiga/priorisez-votre-backlog-le%C3%A7on-6-6795c006326e)
- [#4. Construisez votre Roadmap](https://medium.com/product-academy-thiga/construisez-votre-roadmap-product-academy-le%C3%A7on-4-24732eeaff75#.2tga1f3ep)

Un autre article à propos d'élément de priorisation : [Scrum Part 6: Kano Model – Product Backlog Prioritisation](https://www.jeppstones.com/scrum-part-6-kano-model-product-backlog-prioritisation/)


---

> A l’occasion du colloque Global Race de l’INED (17.12.2020), Patrick Simon a fait remarquer que « l’intersectionnalité est l’avenir des politiques publiques touchant aux discriminations ». Toutes les personnes enquêtées ont un positionnement analogue. Tous et toutes manient les termes d’intersectionnalité, de racisation, racialisation, et insistent sur le caractère préoccupant que prend l’hostilité à l’islam et aux musulmans, qu’ils ou elles rechignent ou non à utiliser le terme d’« islamophobie ».

[Sur la cosmétique ministérielle](https://mouvements.info/sur-la-cosmetique-ministerielle/)

---

[Étêtage des conifères, la fausse bonne idée…](https://lezarbres.wordpress.com/2021/02/26/etetage-des-coniferes-la-fausse-bonne-idee/)

> L’étêtage consiste à supprimer la cime d’un arbre pour le redescendre en hauteur. Si pour la plupart des feuillus (angiosperme) cette opération est relativement simple, en revanche pour les résineux (gymnosperme) le résultat est plus dangereux. Pourquoi ?


