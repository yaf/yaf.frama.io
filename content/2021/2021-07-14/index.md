---
title: Mercredi 14 juillet 2021
---

[Kill the newsletter](https://kill-the-newsletter.com/)

---

Quels sont les différents stade de compréhension (d'autonomie ?) autour de la programmation.

J'ai l'impression de voir des personnes qui ne comprenne pas ce qu'elles manipule. D'autres comprenne mais n'arrive pas à l'organiser. Viens enfin le stade où certaines personnes comprenne ce qu'elles manipule, arrive à le faire, au moins pour arriver a un résultat. Reste donc à optimiser ce résultat.

Cette dernière étape viens plutôt avec l'expérience, le temps. Comment franchir les deux autres ? Est-ce qu'il y a un découpage plus fin des niveaux ?

Difficile d'animer un atelier avec des personnes de ces deux stades. L'autonomie est intéressante, voir nécessaire pour ces aspects.

Quel épreuve, auto évaluation mettre en place pour permettre à chacune de savoir où elle se situe ?

---

Un très chouette article à propos des agrumes en russie

[Faire pousser des plantes subtropicales malgré le gel hivernal : La culture de fruitiers en tranchées](https://solar.lowtechmagazine.com/fr/2020/07/fruit-trenches-cultivating-subtropical-plants-in-freezing-temperatures.html)

---

> On prolétarise les personnes non-érudites à coups de docker-compose up en cachant une technique qui serait la voie vers leur autonomie.

> Ce ne sont pas nos produits que l’on met en boîte mais nos pairs que l’on enferme dans cette façon de (dé)penser pour le confort de quelques uns.

[Fullstack](https://larlet.fr/david/2021/06/23/)



