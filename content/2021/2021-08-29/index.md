---
title: Dimanche 29 août 2021
---

[(Re)lire les féminismes noirs ](https://www.erudit.org/en/journals/ela/2021-n51-ela06216/)

Liste d'œuvre de féministes noires.

---

Et s'il fallait revenir à des clients « lourd », sortir du web, au moins pour certains type d'application ?

[So you want to write a GUI framework](https://www.cmyr.net/blog/gui-framework-ingredients.html)

Typiquement, RDV-Solidarités mériterais un site web pour les usagers, prendre un rendez-vous rapidement (peut-être aussi pour certains agents d'ailleurs ?), et une vrai application local pour la configuration, l'organisation et la gestion au quotidien pour les agents.

---

> C’est complètement catastrophique de n’avoir que des critiques nourries par la haine et aucune honnête, basée sur le travail lui-même.

> Si j’ai décidé de revenir sur cette expérience c’est parce qu’elle entre dans le cadre d’un ouvrage que je construis actuellement dont je dévoilerais sans doute des petits bouts, comme ici même. Un ouvrage qui fait suite au dossier F, qui part en quête d’un profil contraire au « haut score », c’est-à-dire centré sur l’autodétermination, la flexibilité mentale, l’orientation vers l’intrinsèque, des modes d’organisations horizontaux ou d’autogouvernance, l’intelligence sociale + émotionnelle + cognitive et la créativité, hors monde pseudolibéral ou autoritaire. A mon sens, l’expérience d’Alvarez remplit ce cahier des charges pour les enfants de l’âge de la maternelle, et non pas parce que les enfants y apprendraient à lire plus tôt, mais parce que la façon dont ils ont appris à apprendre (grâce à l’environnement créé) nourrit clairement l’humain et ses potentiels plutôt que de le soumettre, le résigner ou le manipuler.

> Cette liberté offerte aux enfants était incluse dans un cadre structuré, aux règles clairement énoncées afin d’assurer leur sécurité, la liberté et l’ordre nécessaire aux activités. Par exemple, une règle était de ne « jamais déranger l’activité de l’autre ».

Ça me rappel la loi des deux pieds...

> À 11heures, il y avait un temps de regroupement de toute la classe ; si certains étaient si concentrés par leur activité qu’ils n’entendaient même pas l’appel : Alvarez et Bisch les laissaient faire leur activité, parce que c’était signe d’un apprentissage d’une grande importance. Lors du regroupement, Alvarez discutait avec eux de la joie d’avoir aperçu untel réussir une tâche difficile, et les enfants rebondissaient en expliquant à leur tour les réussites qu’ils avaient vues chez autrui. C’était un court moment pour créer une confiance, de la bienveillance et de l’émulation

L'équivalent du journal ? Pas tout à fait. C'est sans doute plutôt la cloture de fin de journée l'activité la plus équivalente que j'ai expérimenté.

> Ici l’enseignement créé un environnement où l’enfant a des expériences optimales d’apprentissage, et la source de savoir est dans l’interaction enfant→ environnement → apprentissage, le prof étant un pont facilitant cette rencontre.

Un des piliers du constructivisme ?

> À la place des leçons qui sont une somme de savoir que l’enfant va devoir incorporer pour répéter (par l’action ou en mémoire), il y a des présentations qui guident l’interaction. Elles se déroulent comme ceci :
> - nommer chacun des objets présents et faire répéter leur nomination à l’enfant.
> - montrer lentement et précisément chaque geste de l’activité.
> - montrer les indicateurs pourvoyeurs de feed-back permettant de comprendre que l’activité est réussie ou qu’il y a une correction à apporter pour réussir.
>
> L’enfant ne doit pas toucher à l’activité tant que la présentation n’est pas finie, pour exercer ses capacités à l’inhibition.

Montrer plutôt qu'expliquer... Et là, il s'agit de montrer une activités, une action concrete.

[[E1] La méthode Montessori, réactualisée : l’expérience de Céline Alvarez](https://www.hacking-social.com/2017/09/25/e1-la-methode-montessori-reactualisee-lexperience-de-celine-alvarez/)

> la motivation n’est pas une denrée rare chez eux, par contre ce qui peut la bloquer c’est la peur d’échouer, la peur de déplaire, l’angoisse de la situation sociale, la peur liée aux autres enfants, etc. Ce qui bloque la motivation ou l’élan des enfants est de notre ressort à nous, les adultes ; à nous de créer un environnement safe et de confiance pour que l’enfant puisse se sentir en sécurité d’exprimer ses élans de curiosité.

> « Tout ce temps en individuel, c’est inquiétant… »
> Cette remarque vient du fait qu’il a été dit (ou peut-être l’ont-ils lu dans les médias) à ces enseignants que la classe expérimentale de Gennevilliers passait 30 minutes en rassemblement. Lorsqu’on y regarde de plus près, les enfants ne sont pas esseulés : il y a d’abord l’accueil, où il y a un échange personnel avec chaque enfant, puis il y a les présentations en individuel qui ont souvent pour « public » d’autres enfants ; les enfants pouvaient demander de l’aide à Alvarez et Bisch, et lorsqu’elles les voyaient en difficulté elles les aidaient.

> Travailler en équipe, coopérer n’est pas être groupé et former une masse uniforme avec le même comportement. Au contraire, la coopération me semble former un organisme vivant, évolutif, où toutes les différences sont acceptées et sont des forces où chacun danse à sa manière pour former une œuvre commune riche et complexe qui n’aurait pas pu être possible seul.

Est-ce qu'obliger les groupes ou le binômage est une mauvaise idée ? J'ai souvent utilisé ce principe, en insistant très lourdement... Et les rare fois où j'ai laissé filé, beaucoup de personnes se retrouvais seules. Certaines arrivaient à avancer, apprenant à leur rythme. D'autres par contre restaient vraiment bloquées. Est-ce qu'elles auraient mieux appris en groupe ? Pas sur.

> Par contre, leur imposer de se faire des amis, de s’exposer devant autrui, de sociabiliser et de considérer sa solitude comme un lourd problème peut vite se transformer en cauchemar la situation pour des profils non-extravertis.

[[E2] Diktat de la motivation, individualisme et Valeurs : critiques de l’expérience de Gennevilliers](https://www.hacking-social.com/2017/10/03/diktat-de-la-motivation-individualisme-et-valeurs-critiques-de-lexperience-de-gennevilliers/)

> Si cette volonté de « différenciation » est entendue comme « il faut prendre en compte les difficultés liées aux problèmes sociaux ou de langage » alors Alvarez et Bisch l’ont fait, dans l’échange individuel du matin, l’enfant pouvait partager ses émotions liées à sa vie hors école, et elles prenaient en compte cela, par exemple en autorisant les enfants à dormir ou se reposer lorsqu’ils en avaient besoin. Il y avait des enfants qui ne connaissaient pas la langue française, et ils ont pu l’apprendre à leur rythme tout comme les autres avec les activités qui le permettaient.

Un petit morceau de Paolo Freire ? :)

> Il me semble terriblement discriminant de penser que les enfants pauvres sont différents des enfants riches, dans leur mental et leur capacité à se développer.

Voir Jacotot dans « le maitre ignorant ». Il y a un axe de réfléxion sur ce point.

[[E3] Le mythe Alvarez par les médias, et autres critiques](https://www.hacking-social.com/2017/10/16/e3-le-mythe-alvarez-par-les-medias-et-autres-critiques/)

> À mon avis, à partir du moment où il y a internalité non-allégeante (appelée aussi internalité rebelle), cela ne va pas plaire aux intérêts économiques d’être en présence d’individus qui, en toute simplicité, refusent les ordres parce qu’ils estiment qu’ils sont inappropriés, que ce soit pour soi ou pour tous.

> Il est évident que les attitudes d’Alvarez et de Bisch ont été déterminantes, sans aucun doute, bien plus que le matériel ou les différentes méthodes. Mais pourquoi séparer l’attitude, le comportement du professeur de la méthode ? 

Est-ce une part de difficulté au moment où j'essaie d'exposer ma manière de faire, d'accompagner l'apprentissage de la programmation ? Sans doute.

> Je trouve presque inquiétant qu’on sépare la méthode du comportement : évidemment que rien ne marchera si l’instituteur parle avec brutalité, soupire d’exaspération, n’éprouve absolument rien à l’égard d’un élève qui entre dans la lecture.

> Aucune méthode n’est généralisable, la méthode augmentée de Montessori que propose Alvarez est très pâte à modelable, elle ne cesse d’insister d’ailleurs sur l’initiative des instituteurs à changer en fonction des appétits et habitudes culturelles des enfants, à tester, et à mettre à jour en fonction des situations, des contextes, de l’avancée de la recherche.

[[E4] La pédagogie Alvarez et Montessori à la solde du néolibéralisme ?… et autres critiques…](https://www.hacking-social.com/2017/10/23/e4-la-pedagogie-alvarez-et-montessori-a-la-solde-du-neoliberalisme-et-autres-critiques/)

> Cependant, la femme adulte que je suis a eu quelques réticences au fait de voir des petites filles faire des tâches ménagères. Même si, certes les garçons le font tout autant, avec tout autant d’entrain, et qu’ils sont tout aussi stimulés à ces activités, que se passerait-il avec un.e enseignant.e qui n’aurait pas pensé/déconstruit ses stéréotypes ?
>
> C’est une question compliquée que de trouver des activités qui ne soient pas « corrompues » de stéréotypes, ainsi c’est vraiment à l’enseignant de ne pas implicitement pousser les filles au ménage plus que les garçons.

Quelques références pour pousser un peu plus loin

> - Liberté pour apprendre, Carl Rogers (À lire ?)
> - Application of flow in human developement, Mihaly Csikszentmihalyi (À lire ?)
> - Institute for play : https://www.instituteofplay.org/
> - Quest To learn : http://www.q2l.org/
> - Expérithèque, bibliothèque des expérimentations pédagogique : http://eduscol.education.fr/experitheque/carte.php
> - Edukey, une chaîne qui explore les pédagogies ; ici la pédagogie Freinet https://www.youtube.com/watch?v=cKAUx8IWfg4
> - Reinventing organizations, Frederic Laloux (disponible en français) Un grand aperçu en conférence, il y parle d’un école allemande (ESBZ) où les enfants s’autogouvernent, ou il y a un fort accent mis sur le prosocial https://www.youtube.com/watch?v=NZKqPoQiaDE
> - « Une idée folle » ce documentaire rassemble plusieurs initiatives françaises de pédagogie alternatives  : https://www.youtube.com/watch?v=cqeKM-SGpZo
> - [Les enfants de Summerhill (1997)](https://www.youtube.com/watch?v=_xqFSHa1FE8)
> - Hygiène mentale a fait toute une série sur l’enseignement de la pensée critique en CM2  assez originale (ici c’est l’intro, il y a 4 épisodes) : [ EMI 1 - Education aux médias et à l'information à l'école (Introduction) ](https://www.youtube.com/watch?v=__DVwG9oiuU)
> - Un professeur raconte comment il a gamifié sa classe (c’est sous-titré en français) : [ Classroom Game Design: Paul Andersen at TEDxBozeman ](https://www.youtube.com/watch?v=4qlYGX0H6Ec)
> - François Taddei biologiste, directeur du CRI (Centre de recherches interdisciplinaires)  à milite activement pour l’innovation dans l’éducation ; a suivre également sur twitter (il relaie beaucoup d’informations passionnantes sur l’éducation) ; ici le projet du CRI d’[une école de la recherche, les savanturiers](https://les-savanturiers.cri-paris.org/).

[[E5] Enfants libres, adultes autodéterminés ? Notre critique de l’expérience d’Alvarez](https://www.hacking-social.com/2017/11/06/e5-enfants-libres-adultes-autodetermines-notre-critique-de-lexperience-dalvarez/)

