---
title: Mardi 30 mars 2021
---

[Entr'EEDD](https://entreedd.bzh/?CherChe)

> Vous êtes en Bretagne et vous avez un projet dans le champ de l'éducation à l'environnement et au développement durable (EEDD) ?
> Nous recensons pour vous les aides pour l'EEDD en Bretagne


> 1. Pourquoi ?
>
> Ce site collaboratif a été crée afin de faciliter le partage des informations concernant la recherche d'aides financières ou organisationnelles dans le secteur de l’Éducation à l'Environnement et au Développement Durable en Bretagne : optimiser le temps - faciliter le recensement.
> Les aides présentes sont en cours ou à venir et à différentes échelles : locale, régionale, nationale, européenne.

---

[Déclarations des entrepreneurs de spectacles vivants](https://data.culture.gouv.fr/explore/dataset/declarations-des-entrepreneurs-de-spectacles-vivants/table/?disjunctive.region&disjunctive.departement)

Liste des entrepreneurs du spectacle vivant comme base de données pour Merci Edgar ?
