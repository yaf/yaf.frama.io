---
layout: post
title: JRuby sur Git
---

Après avoir [déménagé le gestionnaire de projet](/2009/03/24/jruby-demenage) de [CodeHaus](http://codehaus.org) à [Kenai](http://kenai.com) (la forge selon [Sun](http://fr.sun.com/)), JRuby change de gestionnaire de version pour passer de [Subversion](http://subversion.tigris.org/) à [Git](http://git-scm.com/)

L'annonce de Headius: [JRuby moves to Git](http://blog.headius.com/2009/04/jruby-moves-to-git.html).

_ça va forker !_

