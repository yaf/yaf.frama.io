---
layout: post
title: Ne soyez pas un développeur Ruby
---

C'est se que l'on pourrait extraire de ce qu'il se passe en ce moment autour de [Twitter](http://twitter.com). C'est aussi une phrase de [Russ Olsen](http://jroller.com/rolsen/) lors de sont intervention à l'édition 2008 de [Paris On Rails](http://paris.onrails.info/).

Et je suis tout à fait d'accord avec lui.

Apparemment, l'équipe de développement de [Twitter va refaire l'outil en Scala](http://www.artima.com/scalazine/articles/twitter_on_scala.html). [Scala](http://www.scala-lang.org/) est un langage de script tournant sur une JVM(Java Virtual Machine). Bien, c'est un langage de script que je ne connais pas, mais le sujet n'est pas de savoir si Scala fera mieux que Ruby sur le sujet, la question c'est plutôt pourquoi une bonne partie de la communauté s'indigne de ce changement.

[Dave Thomas](http://pragdave.blogs.pragprog.com/pragdave/2009/04/twitter-should-move-away-from-ruby.html) le dit bien.

Ruby est un langage de script, tout le monde commence à le savoir, et comme tous les langages de script, c'est forcement moins performant qu'un langage compilé (même si c'est du pseudo compilé comme Java). Il faut savoir utiliser la bonne techno, le bon langage au bon moment, pour les bonnes raisons.

Pour reprendre encore Mr Olsen, Ruby est une bonne solution aujourd'hui pour certain problème, mais ce n'est pas LA solution. Il y en aura d'autres et il y en a déjà d'autres qui répondent mieux à certaine problématique.

