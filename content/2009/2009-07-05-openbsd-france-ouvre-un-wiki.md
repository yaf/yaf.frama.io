---
layout: post
title: OpenBSD-France ouvre un wiki
---


Une petite nouveauté dans le ciel francophone des fans de [puffy](http://openbsd.org), la communauté [OpenBSD-France](http://www.openbsd-france.org) lance un wiki pour permettre à tous de partager des informations lié à notre <acronym title="Opérating System"><span class="caps">OS</span></acronym> préféré.

Je vous invite à parcourir, participer, corriger, améliorer, ajouter vos infos sur [wiki.openbsd-france.org](http://wiki.openbsd-france.org)

[Wiki OpenBSD-France](http://wiki.openbsd-france.org)


