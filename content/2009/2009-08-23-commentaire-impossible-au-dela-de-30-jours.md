---
layout: post
title: Commentaire impossible au delà de 30 jours
---


Bon, après avoir autorisé les commentaires sur tout les billets, même les plus anciens, je me retrouve avec gavé de spam...

Du coup je remet en place le vérouillage des commantaires après 30 jours. Si jamais vous souhaitez commenter un vieux billets, passé par [http://identi.ca/yaf/](yaf@identi.ca) , [http://twitter.com/pouype](pouype@twitter) , ou encore par mail yaf@elsif.fr.

Merci de votre compréhension ;-)


