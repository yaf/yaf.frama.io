---
layout: post
title: RailsCampParis #2
---

Hier avait lieu le [2ième RailsCampParis](http://barcamp.org/RailsCampParis2) dans les locaux de [Sun](http://fr.sun.com/).

Très beau locaux d'ailleurs, dommage que Sun déménage. On verra ou se passera le prochain.

Pas mal de participant, pas autant que d'inscrit finalement, mais quand même une bonne 60aine je crois. Merci à tous, ce fut un RailsCamp très sympa, très riche en échange. On regrette quand même l'absence de quelques figures de la communauté Rails de Paris, j'espère que l'on vous verras au prochain ;-)

Quelques belles sessions, mais peu nombreuses je trouve. Ceci dit tout le monde semble y avoir trouvé sont compte. Manque peut-être quelques sessions un peu originale, à voir pour la prochaine fois.

Aujourd'hui ce déroule un mashpit Merb au Dune. J'espère que vous vous y amuserais bien (ça commence dans une demi heure ;-)).

A quand le prochain ? (peut-être un RubyCamp cette fois ?)



