---
layout: post
title: Passage à Typo 5.3 (en retard)
---

  
Alors que la version 5.3.1 avance, je viens juste de me mettre à jour.

Ce blog tourne maintenant avec “Typo 5.3 Robert Franck”. Principalement dédié au portage de l’application pour  [RubyOnRails 2.2.2](http://rubyonrails.org) , cette version amène tout de même un paquet de corrections de bug et d’évolutions.

Il y en a une que j’attendais (par flème de la faire) depuis longtemps: la redéfinition du format des urls. Je pense que d’ici peu, les urls ici passeront d’un format /année/mois/jour/titre à /titre :-)

**Merci à l’équipe, comme d’habitude, pour le bon boulot !**

  