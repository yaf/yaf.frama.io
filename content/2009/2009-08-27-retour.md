---
layout: post
title: Retour
---


Retour de vacances d’abord, enfin, ça commence à faire loin... 2 semaines :-). L’Italie, c’est beau. Nous avons fait le tour des lacs: [Lac Majeur](http://fr.wikipedia.org/wiki/Lac_majeur) , [Lac de Côme](http://fr.wikipedia.org/wiki/Lac_de_C%C3%B4me) , puis [Lac de Garde](http://fr.wikipedia.org/wiki/Lac_de_garde) . Pour finir, nous avons passé 2 jours à Venise (c’était l’occasion). Très sympa, très reposant, une belle balade.

Retour d’une première expérience. En effet, avant de partir nous avons fait l’acquisition d’un [Canon EOS 450D](http://fr.wikipedia.org/wiki/Canon_EOS_450D). C’est très agréable. Bien plus réactif que l’APN Nikkon que j’avais jusqu’ici. Bien plus beau également au niveau des rendus couleurs. Ceci dit, l’appareil ne fait pas le photographe, et je suis vraiment novice en la matière. Mais je crois que je vais prendre plaisir à apprendre.

Retour en ligne. Pas mal de sujet me trottent dans la tête depuis 6 mois riche en évènement (professionnel entre autres) et j’aimerais partager tout ceci avec vous.

Retour à [OpenBSD](http://openbsd.org). Mais ça me fait le coup à chaque fois que je m’éloigne de puffy, j’y revient une semaine après ;-)


