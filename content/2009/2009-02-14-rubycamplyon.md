---
layout: post
title: RubyCampLyon
---

Juste pour vous rappeler que le samedi 21 février aura lieu un [RubyCamp à Lyon](http://barcamp.org/RubyCampLyon). L'évènement aura lieu au 3ième étage du département informatique de l'INSA.

Merci à [Damien](http://www.dmathieu.com/) d'organisé cet évènement.

*Je suis déçu de ne pouvoir aller à ce RubyCamp !*

