---
layout: post
title: Changement de thème
---


Après pas mal de temps passé sur [Scribbish](http://quotedprintable.com/pages/scribbish) j’ai repris un petit thème maison. C’est pas forcement aussi bien léché, mais au moins, _C’est moi qui l’ai fait_ :-)

![theme scribbish](/files/preview.png)

Et ça me permet également de faire mon premier repository public sur [GitHub](http://github.com) histoire de voir comment ça marche. Retrouvé donc les sources de ce thème sur [github.com/yaf/to_the_left](http://github.com/yaf/to_the_left).

Maintenant il faut que je vois si c’est assez *beau* pour être proposé sur le [typogarden](http://typogarden.org/)


