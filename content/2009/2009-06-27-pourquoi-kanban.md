---
layout: post
title: Pourquoi Kanban ?
---


Durant mes études de génie mécanique puis de productique, j’ai eu l’occassion d’étudier le [TPS](http://fr.wikipedia.org/wiki/Syst%C3%A8me_de_production_de_Toyota) et le système [Kanban](http://fr.wikipedia.org/wiki/Kanban).

Après m’être interessé à l’agilité dans le monde du développement logiciel (domaine m’ayant recueilli après mes études), j’ai vu il y a quelque temps arrivé le [Lean](http://fr.wikipedia.org/wiki/Lean) courant de pensé issu du <span class="caps">TPS</span>, et lu quelques billet très interessant sur l’utilisation de Kanban dans le développement logiciel.

Je vous conseil d’ailleurs la lecture de cet essai très interessant:
[![Scrumban essays system software develpment (lien amazon)](http://ecx.images-amazon.com/images/I/41ycs5LMlbL._SL500_AA300_.jpg)](http://www.amazon.fr/Scrumban-Essays-Systems-Software-Development/dp/0578002140/ref=sr_1_1?ie=UTF8&s=english-books&qid=1246105916&sr=8-1)

De mon point de vue je trouve que Kanban est plus approprié aux équipes que j’ai eu l’occassion de rencontrer: Multiprojet, maintenance. Scrum, XP et les diver pratique agile basé sur les itération courte ne sont pas forcement très simple, voir très adapté à une équipe travaillant sur de la maintenance, des évolutions, un nouveau projet, le tout en même temps...

Pour illustré l’utilisation de Kanban, [Henrik Kniberg](http://blog.crisp.se/henrikkniberg/) à réalisé un petit *strip* qui reflete assez bien le mode de fonctionnement d’une équipe utilisant Kanban: [One day in Kanban land](http://blog.crisp.se/henrikkniberg/2009/06/26/1246053060000.html)

J’espère bien pouvoir convaincre l’équipe avec laquelle je vais travailler quelques temps de mettre en place un tableau Kanban pour mieux gérer nos projets.


