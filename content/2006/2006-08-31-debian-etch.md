---
layout: post
title: Debian Etch
---

  

Debian prépare la sortie de sa version <b>3.2</b> (debian Etch). Normalement prévu pour décembre, une page sur le wiki est désormais disponible et [contient toute les nouveautés de Etch](http://wiki.debian.org/NewInEtch).



On peut voir aussi [quelque copie d'écran du nouvel installeur graphique et en couleur :)](http://shots.osdir.com/slideshows/slideshow.php?release=724&slide=2).



[Debian](debian.org) prend le temps de faire les choses, mais les faits bien. C'est vraiment ma distribution préférée !!


Edit: On m'a souffler qu'une erreur c'est glissé dans le contenu de ce billet... C'est evidemment la 3.2 et non la 3.1 de debian qui vas sortir. La 3.1 etant la version stable actuelle (Sarge) Merci à [LordPhoenix.
](http://www.lordphoenix.info/) pour la correction ;-)

  