---
layout: post
title: "Window Maker Theme : Tango"
---


J’avais très envie d’adapter le [projet Tango](http://tango.freedesktop.org/Tango_Desktop_Project) a Window Maker. C’est un projet très interessant à propos de l’interface utilisateur. Sous le couvert de [freedesktop](http://freedesktop.org/wiki/), ce projet regroupe une série d’icones et une charte graphique.

J’ai donc basé mon theme windowmaker dessus en récupérant les icones officiel, et d’autres non officiel pour compléter (surtout au niveau des applications). voilà ce que ça donne:

[![screenshot de bureau sous windowmaker](/files/tango_windowmaker_1.jpg)](/files/tango_windowmaker_1.jpg)

[![un autre screenshot de bureau sous windowmaker](/files/tango_windowmaker_2.jpg)](/files/tango_windowmaker_2.jpg)

J’utilise deux bureau sous WindowMaker. L’un pour tout ce qui touche au web: Firefox, Xchat, gAim, gftp... L’autre pour le reste: console, gimp, scite, rox...

Pour dans les paquets d’icones officiel il n’y avais pas d’icone en 48×48 ni en 64×64 en png. J’ai donc ecrit un petit quelque chose pour passer de <span class="caps">SVG</span> à 64×64 et 48×48. je l’ai mis dans ma zone. Ca contient un script sh pour l’appel à inkscape (c’est via inkscape de que je transforme du svg au png avec une taille donné en paramètre) et un petit script ruby pour parcourir un repertoire, appeler le script qui vas bien et créer le repertoire avec les nouveaux icones. C’est un peu brut de fonderie, c’est sans license, donc completement public (enfin sauf si on me dit le contraire). Pour lancer il suffit d’ouvrir le script ruby et de modifier les 4 premières variables :). J’ai aussi mis dedans toutes les icones que j’ai trouvé sur le theme de Tango...

[Le paquet des scripts+icones](http://zone.typouype.org/tango_wmaker.tar.gz)

Pour ce qui est de windowmaker, je suis pas encore un professionnel de la construction de theme. Alors j’ai compacté ce qui à été généré dans le repertoire theme de windowmaker.

[tango.themed.tar.gz](http://zone.typouype.org/tango.themed.tar.gz)

Bon maintenant que c’est fait, j’ai eu des pépins (encore..) sous windowmaker. Ca m’était déjà arrivé, Xorg Freeze et pas possible de revenir ou tué x. Donc je suis de retour sous xfce :). Vivement que j’ai plus de place pour avoir plusieurs machine, ça me permettras d’avoir sur chacune un des window managers que j’adore :-p.

