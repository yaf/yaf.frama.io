---
layout: post
title: Microsoft et Mozilla
---

En voilà une nouvelle... Microsoft invite Mozilla à Redmond [par le biais de The Inquirer FR](http://fr.theinquirer.net/2006/08/22/microsoft_veut_firefox_sur_win.html) et chez [Greg](http://greg.rubyfr.net/pub/?p=192)].

Le but est d'essayer de permettre à [Firefox ](http://www.mozilla.com/firefox/) ainsi que [thunderbird](http://www.mozilla.com/thunderbird/) de fonctionner le mieux possible.

C'est très interessant de voir comme Miscrosoft change de position face au monde libre. Les premières réaction était de dénigrer le "mouvement", de cracher sur ce mode de developpement qui d'après eux était le mal incarné (enfin surtout pour [Steve Ballmer](http://en.wikipedia.org/wiki/Steve_Ballmer)).

Maintenant les developpeur de Redmond veulent s'arranger pour que Firefox fonctionne bien :-). une manoeuvre pour pomper de bonne idée pour améliorer IE7 ? Un signe de la mort d'IE ? Ou tout simplement une ouverture d'esprit ?

La 3eme option est sûrement la version officielle, et la plus souhaitable. J'espère que ça n'annonce pas la mort d'IE. Ce n'est pas souhaitable. J'aimerais au contraire que le nombre de navigateur augmente... Ou plutot les moteur. Il en faudrait au moins 2 ou 3. Les navigateurs basé sur Gecko c'est bien (j'utilise Flock là, au boulot c'est windows...) mais ça ne suffit pas. (enfin de mon point de vue).

*Wait and see*

_Edit._

Je me permet une petite édition suite au commentaire de [LordPhoenix](http://www.lordphoenix.info/) qui dans sont billet sur [un Microsoft 2.0](http://www.lordphoenix.info/general/vers-un-microsoft-20) fait référence à deux exellent (comme d'hab en fait ;-)) billets de Louis Naugèe: [Bill,  Ray et Steve](http://nauges.typepad.com/my_weblog/2006/06/bill_ray_et_ste.html) et [Bill,  Ray et Steve (la suite)](http://nauges.typepad.com/my_weblog/2006/06/bill_ray_et_ste_1.html). A lire Absoluement. Merci LordPhoenix pour l'info ;-)

