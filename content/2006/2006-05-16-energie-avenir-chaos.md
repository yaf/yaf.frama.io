---
layout: post
title: Energie, Avenir, Chaos
---

On le sait déjà, mais rien ne change. Nous utilisons beaucoup trop d'énergie, nous la gaspillons. La planête sera surement marqué à vie de notre passage.

C'est en lisant [cette article sur le monde.fr](http://www.lemonde.fr/web/article/0,1-0@2-3232,36-762847,0.html), co-signé par  Nicolas Hulot (Un grand monsieur qui depuis que je suis tout petit nous montre la planête et nous explique que ce que nous faisons tout les jours l'abime) et Jean-Marc Jancovici un expert en énergie, que je me décide à faire plus de bruit autour de tout ça.

Je ne sais pas quoi faire, comment le faire. J'essaie de changer mes habitudes de consommation, les réduires. J'en suis arrivé à préférer les transports en commun à la voiture (au début pour éviter les bouchons, mais maintenant par gouts).

En fait ce qui m'inquiète le plus c'est que je ne vois pas beaucoup d'entreprise de recyclage. Je veux dire par la que la plupart de nos objets quotidien contiennent du pétrole (si je ne me trompe pas). Alors ou sont les entreprises qui refonde le plastique jeté (et il y en a beaucoup) pour le re-utilisé ? Ou sont les entreprises qui fournissent des ordinateurs pas cher fait à partir de vieux ordinateur jeté ?

J'avoue que j'aimerais avoir des enfants, mais ça m'ennuie enormement, en fait, j'ai honte, de leurs laisser la  planête dans cette état.

Nous sommes les animaux les plus destructeurs que j'ai jamais vu.

Ce billet est à fort tendance négative. J'essaie de positiver mais j'avoue avoir du mal quand on parle de planête et d'environnements. Le peut que je vois ce sont des horreurs.

