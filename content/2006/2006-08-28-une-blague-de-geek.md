---
layout: post
title: Une blague de geek ?
---


Pendant que l’on est au comptoir des libriste, *Sert une pression à l’ami [Silvyn!](http://www.silvyn.net/blog/) (voir le poste précédent)*, une petite blagounette en dessin trouvé sur le [blog netBsd d’hubertf](http://www.feyrer.de/NetBSD/bx/blosxom.cgi/index.front). Son blog par ailleurs contient bien souvent de très interessante informations autour des \*BSD, enfin surtout NetBSD :)

![Sudo make me a sandwich - xkcd](http://imgs.xkcd.com/comics/sandwich.png)

Et comme par a hasard, deux onglet plus loin, [Niko sur Prendre un café à lui aussi retenu cette image](http://www.prendreuncafe.com/blog/2006/08/28/488-sudo-me). Même référence, même humour ? ;-).

