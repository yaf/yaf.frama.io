---
layout: post
title: OpenBSD - Debian :-/
---

Bon c'est pour moi une claque monumentale, mais ça fait du bien des fois. Je crois que je l'ai bien cherché ;-)

Je n'ai pas réussi à faire tout ce que je souhaitais faire pour me mettre à l'aise sur ce système d'exploitation (OpenBSD). J'ai donc craqué pour remettre ma debian cet après midi.

Non pas qu'OpenBSD ne me plaise pas, au contraire, plus je lit à son propos et plus je colle à sa philosophie ! Mais je ne connais pas Unix (pas assez) et mon expérience GNU/Linux ne ma pas assez enseigner (c'est aussi pour ça que je souhaitais découvrir la famille des systèmes d'exploitation \*BSD).

Bref, tout ça pour dire donc que j'attend mon livre sur BSD avec impatience, que j'espère y apprendre, et je retournerais sous les nageoires du poisson piquant le plus vite possible.

J'aime Debian, j'aime la philosophie debian egalement, mais j'aime la façon de voir le libre du coté des BSD et surtout OpenBSD. Peut-être qu'une distribution Debian/Bsd plutot que Debian/Linux pourrais m'aller ? A méditer.



