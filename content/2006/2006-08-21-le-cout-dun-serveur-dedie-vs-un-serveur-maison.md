---
layout: post
title: Le coût d'un serveur dédié vs un serveur maison
---

Bon, puisque c'est le principal frein pour moi j'ai décidé d'essayer de comparer avec le cout d'un serveur maison... Histoire de me convaincre. C'est une étude faites maison, mais si ça peut aider certaines personne à prendre leur décision (moi le premier) c'est déjà ça.

*Je considère que j'ai déjà une machine, ma machine de bureau. Je vais ramener les tarifs à l'année.*


## Coût Financier

### Dédibox:

C'est le plus simple. De quoi on a besoin ?

* Une connexion internet. Aujourd'hui elle tourne pour la plupart autour de 30 euro/mois (free, neuf, alice). Donc 30\*12=~360 euro/an

* La dédibox. 29.99 euro ht/mois. soit:(29.99\*1.196)\*12=*430.5 euro /an*

La dédibox couterais un total de 790 euro/an. Je ne compte pas les frais électrique de ma machine personnel.

<u>Coût total /an: 800 euro</u>

### Le serveur maison

J'ai fait une rapide config à peut prêt équivalente à la machine dédibox sur ldlc.fr: il faut compter environ <b>450 euros</b> la machine. Pas de carte graphique (pas besoin c'est un serveur), pas d'écran, pas de clavier/souris.

La connexion internet. C'est le même tarif 30 euros/mois soit en gros, 360 euros/an.

Pour l'obtention d'une IP fixe, je crois que le plus simple c'est de passer par les services comme [DynDNS](http://www.dyndns.com/) ou [no-ip.com](http://www.no-ip.com/).
Ce sont des services gratuits (à ce que j'ai pu voir) donc aucun coùt financier.

Pour ce qui est de l'électricité, j'ai trouvé sur le site d'ernergystar.org [un calculateur de consomation d'énergie](http://www.eu-energystar.org/fr/fr_008.htm). J'ai été assez surpris de voir que ça me reviendrais à seulement <b>126 euros/an</b>... Bon je me suis peut-être trompé quelque part (attention sur la somme final il prend en compte l'achat ou la location de la machine...)

<u>Coùt total / an = 900 euros la première année puis 500 euros</u>

j'ai arrondi au supérieur, on vas dire une marge de sécurité sur mes estimations.

## Coût Temps

A l'installation, j'estime que je mettrais autant de temps. Pour la dédibox, je compte installer OpenBSD et à la maison aussi. Ca devrais me prendre le même temps. Même pour la maintenance, je passerais autant de temps. Même OS, dans les deux cas je serais en SSH dans une console (je pense) donc vraiment je vois pas la différence... Les kilomêtres ?  Pour de la lignes de commandes je suis pas sur que ça soit si flagrant que ça.

Au niveau des différence sur le temps, c'est le temps passer avec le hardware qui peut être compté. Combien de temps pour monter une machine ? combien de temps pour changer un disque dur ? combien de temps pour mettre de la ram en plus ? combien de temps pour remettre en place une carte réseau ? une alimentation ?

On vas dire que ça vas de 5 minutes à 4 heures. Soyons large :)


## Conclusion Personnel

Effectivement, même si la première année coûte moins cher avec une dédibox, ça deviens plus interessant d'héberger un serveur à la maison. Cependant pour une différence de 300 euros par an ça reste une somme.

Mais il manque les éléments qui a priori ne coute pas plus de temps ou d'argent mais qui peuvent être génant quand on héberge.

*Le bruit* déjà. Pour le moment je n'ai pas la place pour mettre une machine qui tourne en permanence chez moi. Trop bruyant. Mais c'est un critère assez personnel, je pense que certaine personnes peuvent s'en accomoder.

*La disponibilitée*. C'est sur ça fait classe d'afficher un UpTime de 2 ans :-D. Mais le truc c'est qu'il ne faut pas avoir de coupure de courant/connexion entre temps. Ne pas se prendre les pieds dans le fil, que le chat ne mange pas la prise. Et surtout laissé tourner la machine pendant qu'on est en vacances... Je fais parti des gens qui coupe toute l'électricité et l'eau quand ils partent en vacances. Encore une fois c'est un critères très personnel.

*La bande passante*. Je ne pense pas avoir des "tétra chier" (j'adore cette expression) de visiteurs, mais c'est toujours désagréable de mettre du temps pour accéder à un site, et puis ça jouerais aussi sur mon utilisation quotidienne... Alors les zones dégroupés avec gros débits c'est bien, mais c'est pas partout ;-). Encore un critère personnel.

Finalement j'en arrive à une conclusion que j'ai en tête depuis un moment: je vais prendre une dédibox. Suis-je pret à payer  70 euros /mois (au lieu de 30 euros aujourd'hui) ?

Je crois que mon compte en banque le l'autorise, il me reste plus qu'a franchir le cap, la barrière psychologique, de dépenser de l'argent pour avoir une machine supplémentaire en location et distante, car finalemetn c'est ça la dédibox: Une machine de location distante ;-)

## Conclusion Général

D'un point de vue plus général je crois que la différence est assez faible.

Il y a le point financier, mais il faudrais prendre en compte l'étude sur plusieurs année (ce qui est délicat en informatique) car je pense que le serveur d'hébergement à besoin d'évoluer parfois, ne serait-ce qu'en matériel. Et c'est un coup zéro pour un hébergement distant (on change d'ofrfe, voir elle évolue) alors qu'a la maison faut aller racheter des pièces.

Donc en gros, si c'est une question de moyen financier, autant essayer d'héberger sur une petite machine maison, avec du bricolage maison. C'est valable aussi pour ceux qui veulents justement s'amuser avec une machine à la maison, la bichoner, avec un gros uptime, ou tout simplement apprendre à le faire.

Pour ceux sans problème financier (disons pas à 100 euros pret de retirer à la fin du mois), qui n'ont pas envie de passer du temps à bichoner une machine, je conseillerais une dédibox, ou tout autre solution d'hébergement type serveur dédié ou virtuel.

---

*la voie de la passion: Aller courage... prend ta carte bleu*

*la voie de la raison: Nons attend réfléchi un peu, est-tu sur d'en avoir besoin ? attend unpeu, y'a surement d'autres option*

*la voie de la passion: La carte ! c'est facile c'est rapide tu sentiras rien ... aller !!! *

