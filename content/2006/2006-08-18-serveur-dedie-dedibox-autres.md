---
layout: post
title: "Serveur dédié: Dédibox ? Autres ?"
---

Ca fait quelque mois que je me pose la question. Est-ce que je doit prendre un serveur dédié ?

Juste pour mon blog c'est clair que c'est pas la peine, mais j'aimerais mettre certaine bricole à disposition, et en gérer un peu l'historique par le biais d'un [SubVersioN](http://subversion.tigris.org/) ou d'un [CVS](http://www.nongnu.org/cvs/) (hmm tiens encore un choix qu'il faudras faire :) ). J'aimerais aussi tester certaine bricole en Ruby, pourquoi ma developper certain service, bref, des activité qui nécessite d'avoir un accès plus poussé qu'un simple FTP.

Le problème majeur c'est le tarif ! c'est tout de suite autres chose, disons que c'est le tarif du mutualisé mais par mois au lieu de par an :-/

En mettant de coté l'aspect financié, il y a plusieurs options interessante. La [dédibox](http://www.dedibox.fr/) semble interessante, un accès root, la possibilité [d'installer un OpenBSD](http://opendedibox.fatbsd.com/) (bon c'est pas dans les installation par défaut, mais c'est faisable ;-) ). D'autres [Serveur Dédié Virtuel chez 1and1](http://commander.1and1.fr/xml/order/ServeursEconomiquesLinux;jsessionid=7CCF0D112EE6F27F92DB88AE91F70E35.TC30a?__frame=_top&__lf=Static) semble interessant, mais plus cher que la dédibox pour un service un peu en dessous.

Une autre option prise par certain est d'avoir un serveur à la maison... même si je pense que ça me plairait à mettre en place, ça me saoulera très vite de l'administrer... Et puis la bande passante... c'est une option que j'ai rejeté.

Et comme pour beaucoup de chose, la possibilité d'abstinence. Bon pour un serveur online, c'est faisable contrairement à d'autres truc ;-).


Bref, je suis dans le flou depuis 3 mois, je ne n'arrive pas à me décider. C'est horrible !
A force de voir des gens mettre en place des dédibox ([prendreuncafé](http://www.prendreuncafe.com/blog/2006/08/18/486-vacances-again-vrac),[un groupement d'éponge activiste y'a un moment](http://www.gougueule.com/2006/07/12/128-location-d-une-dedibox)) , je vais craquer :)


_Edit:_ J'oubliais l'option hebergement chez un ami à titre gracieux :) mais je me demande si ça ne peut pas créer des conflits. "Quoi tu déconne ton chat à débranché le serveur et il est resté down pendant toute ta semaine de vacances !!! "



