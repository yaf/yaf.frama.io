---
layout: post
title: Nos libertés électronique en danger
---

Maintenant que le web se répends, que de plus en plus de personnes sont connecté à la toile, les industriels commence à très sérieusement s'interessé à ce petit monde.
Cherchant toujours plus de profit, d'argent, pourquoi finalement, ça on ne sais pas.

Pour preuve, ce petit billet sur [burninghat au sujet de la "cloture d'internet" au état unis](http://blog.burninghat.net/?2006/06/17/96-quand-les-etats-unis-cloturent-internet). Ca fait peur ! J'espère qu'on arriveras jamais à ça.

Et pourtant, on en prend peut-être doucement le chemin. Déjà on veux nous privée de notre liberté d'utilisation des support numérique. Voici encore un exemple [de DVD qui ne fonctionne pas après l'achat](http://linuxfr.org/~Schwarzy/21911.html). Ce qui me frappe ici c'est que maintenant on à la journée pour utiliser les 20 euros (et donc perdre d'office les 16 euros restant, car un dvd à la FNAC c'est bien souvent 36 euros :-/), et que tout les autres dvd sont eux aussi proteger.

Ce systême vas pousser les gens à télécharger encore plus et à moins acheter.

Enfin, ce que j'en dit moi.
  
