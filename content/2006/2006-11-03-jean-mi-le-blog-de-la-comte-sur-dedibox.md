---
layout: post
title: "Jean-mi : le blog de la comté sur dédibox"
---


A force de trainer et de tester les configurations ça devait arriver: mise en ligne d'un site sur la dédibox que je partage avec Jean-mi (appelé pour l'occassion PouMiBox).



La PouMiBox est donc heureuse d'accueillir [le blog de la comte (www.lacomte.net)](http://www.lacomte.net) :). Le blog de jean-mi parle de musique/film distribuées sous license libre (en général), un peu de phisyque des étoiles (;-)) et d'actualité Ruby. Mais qui sais, demain peut-être nous expliquera-t-il enfin comment est fabriqué le comté (avec la marmotte et tout, ah non c'est autres chose ça :p )



Je pensais être le premier à migrer pour essuyer les platres, mais finalement en une petite soirée tout c'est bien passé. Je vais avoir plus de mal avec le tyPouypOblOg car ma version de typo est plus ancienne (donc la migration des bases est moins simple, surtout que je ne connais pas ma version de typo :-( ).



Enfin bon, ça as l'air de tenir la route Je ne devrais pas tarder à le rejoindre :)



*Bon ensuite faudras commencer à voir pour un subversion ou autres outil de versionning, et d'autres joyeuseté que nous avons en tête :)*



