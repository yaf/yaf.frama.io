---
layout: post
title: Java en GPL ?
---

[Java](http://java.sun.com/) : ([definition wikipdiesque](http://fr.wikipedia.org/wiki/Java_%28technologie%29)) langage informatique objet développé par [Sun Microsystems](http://fr.sun.com/). C'est aussi la technologie qui depuis quelque année me permet de manger. Je n'en parles pas souvent car je ne suis pas un fan absolue, j'aime bien, ça permet de travailler, mais d'autres je préfére [la philosophie](http://www.37signals.com/svn/) [d'autres technologie](http://www.ruby-lang.org/), ainsi que de [participer à sa "promotion"](http://www.rubyfrance.org/). Je vais tout de même essayer de vous conter une histoire qui n'as pas encore de fin, mais qui ne devrais pas mal finir. En tout cas ça me fait plaisir de voir cette techno évoluer dans ce sens.

L'histoire commence par la venue d'un troll velu: la [JVM java](http://fr.wikipedia.org/wiki/Machine_virtuelle_Java) libre ou pas libre (on parle aussi de plateforme Java, pour l'ensemble de l'API standard). Pendant longtemps le garde manger du troll ne désempli pas, mais voilà qu'au mois de Mai 2006, [lors de la JavaOne Conference](http://java.sun.com/javaone/sf/2006/wrap_up.jsp) , les officiels annonce l'ouverture prochaine de la plateforme java

> Green wouldn't give any details on making Java technology available as an open source platform just yet, but he said, <b>"It's not a matter of Whether, but a matter of How."</b> More news and developments will continue to flow on this topic, but for now, it's one step at a time. Stay tuned, but get ready for changes and new opportunities for all.

*Le gras, c'est mes doigts qui l'ont fait ;)*

Globalement, on range le troll, on le renvoie dans sa caverne, et on en sort un autre, celui de savoir qu'elle licence prendre... C'est un troll embauché à durée déterminé, effectivement, fin Octobre 2006, lors de la Oracle OpenWorld conference (oui oui tout les grand tente de se méler à l'open Source) Sun annonce que [la décision sera prise avant noël](http://fr.theinquirer.net/2006/10/28/java_en_open_source_avant_noee.html). Un troll licencié juste avant les fête de noël, c'est pas sympa :D

Et qu'apprend-je aujourd'hui même ? Le troll n'auras bientôt plus à manger, avant même le mois de décembre, [on parle de licence GPL](http://fr.theinquirer.net/2006/11/08/java_sous_license_gpl.html). Oui oui la [GNU GENERAL PUBLIC LICENSE](http://fr.wikipedia.org/wiki/GNU_GPL), celle qui est une bonne représentante du monde libre (oui on sait tous qu'il en existe beaucoup aujourd'hui). Sun ne sortiras (apparemment pas) une licence maison un peu étrange (comme le fait Microsoft), mais belle et bien une licence du monde libre.

Je ne pense pas que ça modifieras ma façon de penser au travail, je ne pense pas que ça me feras installer une JVM à la maison, je continuerais à penser [Ruby](http://www.ruby-lang.org), manger [Ruby](http://www.ruby-lang.org) et travailler avec [RubyFrance](http://www.rubyfrance.org) :D. Cependant, je travaillerais peut-être avec un sourire plus important qu'à l'accoutumé ;-).

*Attention c'est une fiction, ne vendons pas la peau <s>du troll</s> de l'ours avant de l'avoir tué :D.  Ps: quelqu'un pourrais me dire comment on met les petite lettre genre TM et autre Copyright ? c'est balot mais je sais même pas faire ça :-/*

