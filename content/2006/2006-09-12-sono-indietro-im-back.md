---
layout: post
title: Sono indietro (I'm back)
---


Dur retour. Hier encore j’etais sur la piazza della Republica en train de boire (enfin une gorgée quoi) mon expresso... Et aujourd’hui j’etais au boulot :-/

Florence (Firenze) est une bien belle ville. Certes très accès sur les musées et les églises, ce qui n’étant pas mon cas était un handicap. Mais la ville ne manque pas d’interêt pour quelqu’un qui aime découvrir des batiments originaux, des parcs riche en recoin, quelque statue pour le moins interessantes, bref c’est tout de même très sympa.

Bien dépaysé, je tiens quand même à confirmer plusieurs chose:

* Les italiens conduisent comme des fous ce n’est pas une légende
* Les italiens sont très bavards et parlent forts
* Les métiers du batiement sont surement les plus gros employeurs. (avantage: tout les batiements sont refait à neuf régulièrement)
* Les italiens sont plus chauvin que les autres peuple que j’ai pu visiter. Apparement c’est même régionnal: un italien de la région de Rome est un étrangé pour un italien de la Toscane...
* Le chauvinisme pousse même les italiens à ne manger qu’italien. Fautes de marché, je pense que tout autres restaurants est voué à fermer. J’ai vu 2 chinois et 3 Kebabs... Pour une ville grande comme Florence ça fait peut.
* Les italiennes sont jolies mais c’est pas mon style.
* Les italiens n’ont les 3 boutons du haut de leurs chemises et sont en général très poilu (presque autant que moi c’est pour dire...)
* Leur petite habitude de se retrouvé à la place du quartier tout les soirs entre 18 et 19 h est assez amusante, et assez sympathique. On devrais faire ça aussi :)
* Paris vu d’haut c’est beau


  Et pour viter le poste qui ne sert à rien, j’ajoute quelque photos rapidement:

[![Voiture de carabinieri](/files/florence_carabinieri.jpg)](/files/florence_carabinieri.jpg)

[![le Duomo de Florence](/files/florence_duomo.jpg)](/files/florence_duomo.jpg)

[![Kioske à journaux avec beaucoup de maillot de foot à vendre](/files/florence_maillot_foot.jpg)](/files/florence_maillot_foot.jpg)

[![Le pont des marchands de bijoux](/files/florence_pont.jpg)](/files/florence_pont.jpg)

  J’ai un peu cartonné avec le numérique, et d’ailleurs j’ai du me retenir par manque de place... je vais peut-être aller acheter une autre carte mémoire en plus... Un fois les impôts payés :-/

  Dès que j’ai le temps, je met tout ce que j’ai sur [mon compte flickr](http://www.flickr.com/photos/yafra/)

