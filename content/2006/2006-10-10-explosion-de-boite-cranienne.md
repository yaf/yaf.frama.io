---
layout: post
title: Explosion de boite cranienne
---

En ce moment j'explose, ça fuse beaucoup trop. Peut-être parce que je blog plus assez. C'est vrai après tout, mon blog me sert à ça normalement, défouloir, bloc-note...

En ce moment l'activité numéro *uno* c'est [trouver du boulot dans le sud](http://typouype.org/articles/2006/09/07/recherche-demploi). Tiens d'ailleurs ce précédent billet me rappel que ça fait un mois que j'ai commencé. C'est une sorte de bilan alors que je fait ? ah ben oui.

Je n'ai toujours rien trouvé. C'est pas faute d'avoir cherché, mais il faut bien avouer qu'il y a beaucoup moins de choix qu'à Paris. Bon ça je m'en doutais, mais peut-être pas à ce point. Ce qui est rassurant c'est de mettre son CV sur monster et d'être dès le lendemain contacté par un paquet de <del>vendeurs de viande</del> SSII. Malheuresement ils ont mal lu, et me proposent des missions parigo-parisienne.

J'ai bien postulé aux quelques postes trouvé de ci de là, mais aucune réponse positive (voir aucune réponse tout court). Alors, c'est le syndrome du parigo en tongue qui descend piquer le boulot des gens du sud ?

Je sais qu'habituellement on part en province par rapport à des propositions toute faites, j'en avais pour Toulouse, mais non, mon choix c'est Montpellier.

Pourquoi on choisi toujours le plus dur ? *Parce que plus facile, c'est le chemin du coté obscur ?*

La semaine dernière par contre, j'ai eu la surprise d'avoir deux boites au bout du fil:

> rh: "Bonjour, j'ai vu votre CV sur le monstre-plein-de-cv et je suis interessé par votre profil, vous en etes où de vos recherche ? "
>
> Moi: "Et bien je suis toujours à la recherche d'un poste dans le languedoc roussillon"
> rh: "Hmm .........."
> rh: "Et bien écouté, nous avons une agence dans cette région, pret de Montpellier ça vous irais ? "
>
> moi: "C'est parfait" :)

Et me voilà avec deux rendez vous pour des entretiens pour des postes sur Montpellier, mais proposé par des boites parisienne :-/

C'est mieux que rien, en plus ils ont l'air sympathique (contrairement au <del>vendeurs de viande</del> SSII habituelle). Je dirais même qu'ils ont réussi à me convaincre, ils m'ont présenté leurs projets, les deux se ressemble un peu, c'est très orienté web. Ca me convient tout à fait.

Le principal c'est de descendre. Pour un climat plus clement, pour rencontré d'autres personnes, pour voir paris de l'extérieur (ou pas). Je crois surtout que c'est pour passé à l'étape suivante (comment ça c'est la trentaine qui approche ? :p ).

J'attend la suite avec impatience.

A coté de ça, je me bas avec ma dediboiboite pour choisir un environnement pour rails: apache+fastcgi, apache+mongrel, lighttpd+fastcgi, lighttpd+mongrel, webrick ? Pour le moment j'ai un peu tout essayer sauf lighttpd+mongrel, ÃƒÂ§a seras peut-être le couple qui vas me convaincre. Il faut aussi que je migre mon blog pour passer à la nouvelle version de typosphere. Mais ça c'est encore une autre affaire. Surtout que je compte bien me reécrire quelque truc fait maison. Et tout les projets "inside my brain" qui traine lamentablement.....

Je crois que mon passage aux Journée Du Logiciel Libre de samedi vont me faire le plus grand bien. Un voyage en train, au petit matin (j'adore être dehors pour ces heures de la journée :5h -&gt; 8h), une journée avec des tas d'infos à engranger, de gens à rencontrer (si ma timidité ne fait pas trop des siennes) bref, un grand bol d'air pour l'esprit: ça me feras du bien !

