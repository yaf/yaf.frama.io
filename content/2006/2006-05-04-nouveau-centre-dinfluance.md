---
layout: post
title: Nouveau centre d'influance ?
---

On dit souvent que nous sommes dans l'aire de l'information. La communication est l'arme absolue (enfin je pense qu'il y a encore des bombe et des fusil qui tue des gens mais bon...).

Disons que l'information influence énormement les masses. On le sais Hitler savais très bien manipuler les foules, il s'etait plus ou moins inspirer des divers reflexion de grand philosophe par rapport aux foules, aux masses.

Ces dernières années, c'est la télévision qui influence énormement de façon de penser. Attention je parle de masse, pas de chaque individu qui peut interpréter une information différement. Mais dans une globalité, on entends l'immigration c'est pas bien, alors beaucoup pense l'immigration c'est pas bien. Après on dit ce film est null, personne vas le voir.

La télévision est influencé par les journaux. C'est la presse écrite qui sert énormement de moteur. C'est souvent elle qui décide hier de qu'elle serons les titre d'aujourd'hui. La télévision creuse plus profond, y ajoute des images.

Du coup on ne parles en ce moment plus que de l'affaire des affaires de l'état, de corbeau, et eventuellement de crash d'avion...

Et le moustique tueur ? et la grippe aviaire ? et les famines ? et les secheresse ?

Quand je lit [les problèmes de canard qui se passe à deux pas de chez moi](http://www.glazman.org/weblog/dotclear/index.php?2006/05/01/1750-en-vrac) (oui oui j'habite pas tres loin de sieur Glaz ;-)), je me dit qu'il manque de l'info, du suivi sur les histoires.

D'un autres coté, je suis en train de me dire que la guere de l'informations, via ce magnifique cadeau qu'est la grande toile, est peut-être en train d'être rendu au "peuple". Quand on voit que des blogueurs [ comme Loic Le Meur](http://www.loiclemeur.com/france/2006/05/mon_moblog_util.html) sont pris en référence dans certains quotidien des plus lu et donc influents. Bon ce n'est qu'une photo mais ça signifie que les redacteur d'influence qui écrivent pour ce journal lisent le blog du monsieur presque tout le monde...

Un autre exemple, plus international, est l'apparition régulière du monsieur Mozilla Europe: Tristan Nitot [qui  se fait des apparition dans l'International Herald Tribune !](http://standblog.org/blog/2006/04/27/93114760-en-vrac)

Bref, tout ça pour dire que de plus en plus j'ai la confirmation que le web pourrais bien faire avancé les choses dans le bon sens. Du moins je l'espère et je veux y croire, je veux lire de plus en plus d'avis comme ceux de Tritant et sa position face au pétrole.
J'aimerais que de plus en plus soit influancé par ce genre d'idée plutot que par les débilité télévisielle trop souvent servi à l'heure du repas de monsieur tout le monde (donc vous et moi).

Et comme dirais certains:

*just my two cents*

