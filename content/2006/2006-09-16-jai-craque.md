---
layout: post
title: J'ai craqué
---

J'ai une grosse tendance à me disperser. Plein d'idée en tête, plein de nouveauté à explorer. (quel rapport avec le titre ? J'y viens).

Quand j'ai découvert en 2000 qu'il existait autre chose que windows pour les station de travail, j'ai voulu en savoir plus. J'ai donc installé mon premier linux et exploré la bête. Je ne regrette pas. Puis sont venu les découverte de BeOS, reactOs, QNX, \*BSD... Je crois avoir essayer chacun d'eux (un doute sur reactOS). Celui qui ma le plus marqué c'est [OpenBSD](http://www.openbsd.org). Mais voilà, OpenBSD n'est pas orienté (pour le moment) UserFriendly clickodromesque. Pour un utilisateur comme moi c'est un peu délicat. J'apprend beaucoup, mais je n'arrive pas à avoir une station de travail où je suis efficace.

Du coup, j'ai craqué. Pour continuer mon expériences avec OpenBSD, n'ayant pas d'autres machine que mon mac mini sous la main (pour le moment), j'ai souscript à l'offre [dédibox](http://www.dedibox.fr) pour bénéficier d'une machine de "location" qui pourras me servir de serveur (:)) mais surtout de zone d'apprentissage. Je sais qu'OpenBSD n'est pas suporté officiellement, mais j'ai vu que plusieurs personnes ont mis en place des solutions simple pour palier à ce manque: [http://open.bsdedibox.net/](http://open.bsdedibox.net/), et le [wiki officieux des installations bsd sur dedibox](http://www.bsdedibox.net/). Comme ça je vais pouvoir continuer à apprendre à me servir d'OpenBSD, sans les aspect station de travail c'est dommage, mais c'est déjà ça. Du coup je garde une Debian, ma distribution linux préféré :), pour mac mini. Avec Debian, j'arrive à m'installer un environnement efficace.

J'espère qu'un jour j'arreterais de changer de système toute les semaines, ça seras plus facile pour pouvoir avancer dans les projets que j'ai en tête. Je compte sur ces 30 euros par moi pour me permettre d'avancer. En tout cas ça me permettras de me prosterner devant la puissance et la grace d'openBSD tout en utilisant l'efficacité d'une distribution Debian.

*Une ligne éditoriale sur un blog ? Oui ça permet d'être plus cohérent. Mais après tout ici c'est comme indiqué dans le titre, une extension de mon esprit. Mes billets suivent le vent de mes idées.*

