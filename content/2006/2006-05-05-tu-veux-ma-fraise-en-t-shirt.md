---
layout: post
title: Tu veux ma fraise en t-shirt ?
---

Un petit mot rapide pour accompagné l'ajout de cet micro-banière, comme on dit, dans ma barre de menu.

[![LaFraise.com :: éditeur de bien jolis Tee-shirts](http://www.lafraise.com/pub/lafraise.png)](http://www.lafraise.com/index.php?parrain=37959_7269 "LaFraise.com :: éditeur de bien jolis T-shirts")

Je trouve le concept très interessant, très "équilibré". Des graphistes (amateur ou non) propose des visuels pour t-shirt, les personnes inscrites sur le site votent pour ces visuels (voir pose des options d'achat). Ceci permet à [Patrice](http://www.lafraise.com/designer-5-.html) et ses acolytes de <s>fraise</s> faire un choix "démocratique" et surtout de ne pas mettre en vente des visuel qui n'aurais aucun succès.

Outre ce système de vote qui evite de trop nombreux invendu, le graphiste dont un visuel serais imprimer reçoit une prime ! d'abord de 500 euros, ces dernier temps, avec la monté en puissance des ventes, elle est passé à 1000 euros ! Je trouve ce concept équilibré, plutot que de réduire le tarif des t-shirt d'un ou deux euros, on augmente les primes, ce qui augmente le nombre de visuel de qualité, ce qui augmente le choix de t-shirt originaux.

 Le coté petite série ajoute de la valeur au t-shirt et forme une sorte de caste, secte des t-shirt la fraise. Maintenant quand je vois un t-shirt la fraise, je ne peut retenir un sourire :-)

Ca fait un moment que je doit faire une photos d'un t-shirt la fraise en ma possession, je la posterais ici également.

**La fraise c'est bon mangez'en ;-)**

