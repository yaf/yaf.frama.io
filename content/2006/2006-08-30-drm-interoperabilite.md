---
layout: post
title: DRM, intéropérabilitée
---



[Bertrand Lemaire (journaliste
 pour ](http://blog2.lemondeinformatique.fr/management_du_si/)[le monde informatique](http://www.lemondeinformatique.fr/))
 nous donne, sur son blog, un exemple d'utilisation intélligente de
 <b>standard ouvert</b> et d'<b>intéropérabilité</b>.
 Ce sont [
 l'exemple des militaires et de leur systême de communication](http://blog2.lemondeinformatique.fr/management_du_si/2006/08/interoprabilit_.html) dont il s'agit.


> Heureusement pour la sécurité nationale, le Conseil Constitutionnel ne s'occupe pas
 des marchés militaires et n'y censure donc pas la notion d'intéropérabilité.


Les veinards ! Alors on peut emmerder les petites gens, mais les militaires non ?
Pourquoi ce qui est bon pour les militaires n'est pas bon pour nous ?
Après tout, c'est en parti à eux qu'on doit Internet, je pense que l'éconnomie en
profite pas mal. Alors si l'intéropérabilité est bien pour eux, pourquoi pas pour nous et l'éconnomie ?



Désolé pour ce billet un peu *coup d'épée dans l'eau*, surtout
que l'on a pas besoin d'un exemple de plus pour savoir que l'intéropérabilité
est trés importante, voir nécessaire au bon développement de
technologie (on vas pas revenir sur le coup des téléphone portable, tout le monde à compris ?  :) ).



