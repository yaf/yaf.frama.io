---
layout: post
title: Et encore un hack tout vilain
---

Ca devient pénible: voilà encore un problème avec IE. J'avais déjà [décortiqué  un de mes problème avec sa façon de traiter les childNodes](http://typouype.org/articles/2006/05/04/ie-mon-amour), voilà maintenant un problème avec son traitement de CSS.

Le context: Je voulais mettre en place un système de "message" (d'erreur ou d'info) sur mes interfaces web. Ces messages ne doivent pas ouvrir de popup (javascript ou nouvelle fenêtre) parce que je ne veux pas ;-) NA !

Donc j'ouvre [mon éditeur favori: SciTE](http://typouype.org/articles/2006/06/08/scite-le-bonus-indispensable), je crée une petite page simple pour mettre en place mon systême de message. Bien, je par donc sur une fonction javascript show_message(monMessage) qui vas créer un DIV avec un ID en fonction du type de message (erreur, info). Par le biais de cet ID je vais faire le CSS qui vas bien. Pour l'info, un fond bleu, pour l'erreur un fond rouge. Classique.

Ajouter un DIV au body de ma page n'est pas sorcier. Maintenant il faut que je le place, par dessus mon contenu, je prend la décision de le placer en bas, sur toute la largeur. Alors voyons, je vais utiliser la propriété CSS position:fixed. Ca fixera le DIV en bas, tranquille et par dessus les autres. Parfait. (en bas du billet j'ai mis les fichiers qui m'on servi de test)

Essayons quand même sous IE car les personnes ayant fait cette appli à l'époque ne l'on fait que sous IE et elle ne passe pas sur des navigateur recents (pour le moment, j'y travail ;-) ).

Oulala ! Encore heureux que j'ajoute le DIV de message en bas de la page, parce que le position:fixed sous IE se transforme en position:relative :-/ (voir static ?)

Bon c'est pas gagné. Je regarde divers solution de Hack pour faire marcher ça. Moui , voyons, moui. Bon finalement deuxième problème, le fameux SELECT qui passe par dessus le DIV ! grrr. (je ferais un autre billet la dessus).

Finalement je vais me rabattre sur un IFRAME. C'est moche, c'est mal, mais je note quelque part que quand tout les écran fonctionnerons sous des navigateur recents, je viendrais changer tout ça.
En gros me voilÃƒ  avec une solution bancale, pas propre (même le rendu graphique est pas super), pas souple (pour le moment je me suis pas casser la tête pour le style appliquer dans le IFRAME): LE BONHEUR !

Encore une fois MERCI IE d'être un dinosaure tellement gros qu'on est obligé d'essayer de faire avec ! J'ai passé une très bonne après midi !

* [la page qui marche sous un navigateur recent uniquement, mais proprement](http://zone.typouype.org/message_test_div.html)

* [la page qui marche avec tout les navigateur (enfin, disons le dinosaure et les autres plus recent) mais avec un sale IFRAME et des Hack spécifique pour le vieux.](http://zone.typouype.org/message_test_iframe.html)

Un grand merci au divers site que j'ai parcouru pour comparer les hacks disponible sur IE. Et surtout à [annevankesteren.nl](http://annevankesteren.nl/test/examples/ie/position-fixed.html) qui contient un exemple simple sur lequel je me suis basé.

