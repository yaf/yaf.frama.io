---
layout: post
title: Migration - domaine et hebergement
---

Si vous voyez ce message, c'est que vous êtes maintenant sur ma nouvelle machine. En fait je dit ma, mais je partage cette dédibox avec [Jean-mi de la comté](http://www.lacomte.net).

J'ai également basculé ma gestion de nom de domaine (enfin ça c'est fait depuis le 29/11/2006) chez [Gandi](http://www.gandi.net). Une équipe efficace dont j'apprécie la franchise, et que je voulais remercié, encourager. C'est maintenant chose faite puisque je leur confie [typouype.org](http://www.typouype.org).

Alors qui dit hébergement sur dédibox dit aussi plus d'administration à faire. C'est sur que je pourrais plus aller chouiner chez Arnaud pour qu'il répare mon fastcgi :p. Mais je vois ça d'un autre oeil: j'ai une machine "online" en location :). Elle est bien sur sous [openbsd](http://www.openbsd.org) et tourne avec [lighttpd](http://www.lighttpd.net/) et [Mongrel](http://mongrel.rubyforge.org/) (quoi vous pensiez que j'allais revenir sur php ? ça vas pas non :p).

Pour le moment il n'y a que nos blogs respectifs qui tournent dessus. Mais je (et je crois que jean-mi aussi ;-)) compte bien mettre en place d'autres "services" maison. A suivre donc.

*Enfin en ce moment je suis pas mal occupé avec mes affaires de changement de boulot, de déménagement. Je m'y prends n'importe comment, j'applique la méthode alarache. C'est pas bien, je vais sûrement m'en mordre un peu les doigts, mais là tout de suite j'arrive pas à faire autrement*

