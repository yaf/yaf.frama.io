---
layout: post
title: "Politique: Arretons de vouloir toujours plus"
---

Consommation, consommation, c’est bon pour l’économie il paraît. Mais tellement mauvais pour la planête et quelque part pour le social.

Ca fait un moment que je me demande pourquoi les humains on cette éternelle envie d’avoir une plus grosse voiture, une plus grosse maison, un plus gros salaire, un plus gros bureau. C’est durant la période ou je jouais à Ultima Online (un MMORPG), quand je voyais des joueurs se battre pour savoir qui avais la meilleur armes, la plus rare, la plus grosse que j’ai commencé à prendre consience de ce phénomène (qui existe peut-être depuis la nuit des temps). En prenant du recule on se rend compte que c’est pareil dans tout les domaines.

Il faudrais arreter de consommer autant. Ne serais-ce que pour épargné notre planête, mais aussi pour permettre à tous d’accéder à un minimum.

Oui je sais on pourrais me prendre pour un communiste, ça ne serais pas la première fois. Mais sans parler de couleur politique, j’aimerais ne plus voir de gens qui vivent avec moins d’un euro par jour et à coté un sportif, musicien ou autres avoir dix fois plus.

Et c’est durant mes webrandos que j’ai découvert que certaines personnes avais commencé à se regrouper, sur le web au moins (c’est déjà ça) pour essayer de trouver des solutions.

Le concept c’est la **décroissance**: [www.decroissance.info](http://www.decroissance.info).

Un wiki viens d’ouvrir ces portes pour que tous puissent participer à la réfléxion général: [colportage.decroissance.info](http://colportage.decroissance.info).

Je ne sais pas encore dans quel mesure j’arriverais à participer. Mais quand je broie du noir en essayant pensant au triste futur que l’on construit, j’aime lire les propositions et autres avis emmanant, ça me permet de rêve à un avenir moins gris.

