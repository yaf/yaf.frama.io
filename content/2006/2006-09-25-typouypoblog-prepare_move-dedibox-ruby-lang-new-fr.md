---
layout: post
title: typouypOblOg.prepare_move(dedibox) - ruby-lang.new(fr)
---

Vous l'aviez compris, j'ai acquis une dédibox. Je suis en train de la paramétrer, d'installer tout ce qu'il faut pour que [Jean-mi (pour le moment c'est ça vieille url qui traine)](http://vsjmbessot.freecontrib.org/) et moi puissions nous y installer. Pour le moment c'est le strict minimum. Ca me permet de me replonger dans le [système que j'aime tant \[openbsd pour ceux qui n'aurais pas suivi\]](http://openbsd.org). Je plonge aussi  (enfin) mon nez dans l'installation de Rails.

Chez [typhon](http://typhon.net) en mutualisé, je n'avais rien à faire. Mais là, c'est ce qui est interessant, je me penche sur ces problèmes d'installation/paramétrage. Je ferais un petit billet la dessus plus tard :)

Je suis content d'avoir enfin mis mon bouquins rails sur le devant de mon clavier !

A propos de ruby, si j'ai mis ruby-lang.new(fr) dans le titre, c'etait pour signaler que JD (un éminant rubyiste francophone :) ) à intégrer l'équipe du site pour travailler sur une version francophone :) ([en préview ici](http://preview.ruby-lang.org/fr/). Si vous etes un(e) habitué(e) de la langue de shakespear, votre aide seras surement la bienvenue (passé(e) faire un tour sur le channel #rubyfr sur irc.freenode.net JD traine souvent dans le coin ;-))

