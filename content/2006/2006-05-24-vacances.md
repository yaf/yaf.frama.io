---
layout: post
title: Vacances
---

Bon je sais ça se fait pas, y'en as qui ne partes pas. Mais ça fait un moment que j'ai pas quitté la région parisienne :-)

Du coup c'est petite balade en montagne ! Au printemps ! ça fait bien 10 ans que je n'y suis pas aller à cette période. Je sent que je vais apprécier. Je vais essayer de faire pas mal de photos, j'emène [Du bon usage de la piraterie](http://www.amazon.fr/exec/obidos/ASIN/291296959X/403-6993584-5208433?%5Fencoding=UTF8). Je pense que ça vas me plaire !

Je me suis promis aussi de faire un petit topo de ce que j'ai pensé, retenu de [ L'économie des logiciels](http://www.amazon.fr/exec/obidos/ASIN/2707138444/403-6993584-5208433?%5Fencoding=UTF8). Je l'ai déjà fini, mais je préfère prendre le temps de faire un billet réfléchi ;-)

Bien, je prend le risque de laisser ouvert à tout vent ce petit coin de web. On verras bien en rentrant, qui sait, je refermerais peut-être les track et autres commentaire finalement.

